'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync').create();

gulp.task('watch', function () {

    browserSync.init({
        //logLevel: "debug",
        port : 3333,
        open: false,
        directory: true,
        server: "./wwwroot/",
        browser: "google chrome"
    });

	gulp.watch("./wwwroot/**/*.html").on('change', browserSync.reload);
    gulp.watch("./wwwroot/**/*.css").on('change', browserSync.reload);
    gulp.watch("./wwwroot/**/*.js").on('change', browserSync.reload);
});

gulp.task('default', gulp.series('watch')); 
