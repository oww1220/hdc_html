/* 개발 반영시 삭제 [ 임시 ] */
var isLogin = "true";
var path = "";
var checkStrs = new Array();
checkStrs[0] = "_pc/";
checkStrs[1] = "_PC/";
for(var i=0; i<checkStrs.length; i++) {
	if(document.location.href.split(checkStrs[i]).length > 1 && document.location.href.split(checkStrs[i])[1].indexOf("/") > -1) {
		path = "../";
	}
}
document.write("<script type=\"text/javascript\" src=\""+path+"js/jquery.mCustomScrollbar.concat.min.js\"></s"+"cript><script type=\"text/javascript\" src=\""+path+"js/jquery-ui-Datepicker-1.11.2.js\"></s"+"cript>");
/* end : 개발 반영시 삭제 [ 임시 ] */

/* 개발 반영시 주석 제거 */
/*
if(typeof(commonResourcePath) == 'undefined'){
	commonResourcePath = '/estore/_ui/desktop/common';
}
var path = commonResourcePath+"/shilladfshome/kr/";
*/
/* end : 개발 반영시 주석 제거 */


$(document).ready(function(){	
	'use strict';
	
	$(".datepicker").each(function(){
		var thisData = $(this).data();
		var _default = {
			minDateValue : "-0d",
			maxDateValue : null,
		}
		var _option = $.extend(_default, thisData);
		_option.yearRange = "c-100:c+10";
		
		if( $(this).hasClass("no-limit") ){
			_option.minDateValue = null;
			_option.maxDateValue = null;
		}
		
		if( $(this).hasClass("before-limit") ){
			_option.minDateValue = null;
			_option.maxDateValue = "-0d";
		}

		if($(this).hasClass("birth-limit") ){
			_option.minDateValue = "-100y";
			_option.maxDateValue = "-14y";
			//_option.defaultDate = '-30y'; //30살기준
			_option.yearRange = "-100:-14";  // 기본노출 추가 14세부터  -100년
		}
		//기념일 기준
		if($(this).hasClass("anniversary-limit") ){
			_option.minDateValue = "-100y";
			_option.maxDateValue = "+100y";
			_option.yearRange = "-100:+100";  // 기본노출 추가 +- 100년
		}
		//출국일 기준
		if($(this).hasClass("departure-limit") ){
			_option.minDateValue = "-0d";
			_option.maxDateValue = "+30y";   
			_option.yearRange = "-0d:+30";  // 기본노출 추가 -100년  부터 +30년
		}
		//고객의 소리 기준
		if($(this).hasClass("voc-limit") ){
			_option.minDateValue = "-100y";
			_option.maxDateValue = "-0d";   
			_option.yearRange = "-100:+0";  // 기본노출 추가 작성일부터 -100년
		}
		
		//여권만료일 추가
		if($(this).hasClass("passportExDate-limit") ){
			_option.minDateValue = "-0d";
			_option.maxDateValue = "+30y";   
			_option.yearRange = "-0d:+30";  
		}

		$(this).datepicker({
			changeMonth:true,
			changeYear:true,
			showOn:'button',
			buttonImage:path+'img/common/ico_calendar.png',
			buttonImageOnly:true,
			showMonthAfterYear:true,
			minDate: _option.minDateValue,  //최소 기간
			maxDate: _option.maxDateValue,  //최대 노출
			yearRange: _option.yearRange,  //노출되는 범위
			dateFormat : "yy-mm-dd",
			dayNamesMin:['일','월','화','수','목','금','토'],
			monthNamesShort: [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" ],
			defaultDate : _option.defaultDate
		}); 
	});
	
	// file upload
	$(".file_select_wrap").each(function(){
		var _this = $(this);
		var inputFile = _this.find("input[type='file']");
		var inputText = _this.find("input[type='text']");
		
		inputFile.on({
			"change" : function(){
				inputText.val($(this).val());
			}
		});
		
		_this.find(".delete").on({
			"click" : function(e){
				inputText.val("");
				inputFile.replaceWith(inputFile.clone(true))
				e.preventDefault();
			}
		});
	});
	
	// bookmark 개발 적용시 주석처리 요망.
	var bookmarkTitle 	= "신라아이파크면세점";
	var bookmarkUrl	 	= "http://www.shillaipark.com/";

	// bookmark 개발 적용시 주석처리 요망. 
	$(".btn_bookmark").click(function(e) {
		// IE
		if(document.all) {
			window.external.AddFavorite(bookmarkUrl,bookmarkTitle); 
		} 
		// safari,chrome,firefox
		else { 
			alert( "Internet Explorer가 아닌 브라우저에서는 즐겨찾기 기능이 제한됩니다.\n\n" + (navigator.userAgent.toLowerCase().indexOf("mac") != - 1 ? "Command/Cmd" : "Control") + " + D 를 눌러서 즐겨찾기에 추가 해 주세요.");
		}
		e.preventDefault();
	});
	
	var windowHeigt = $(window).height();
	$(document).on("click",".datepicker_close",function(){
		$.datepicker._hideDatepicker();
	});	
	$(".foot_area").attr("style","width:931px !important");

	// placeholder
	var browserVer = navigator.userAgent.toLowerCase();
	if( browserVer.indexOf("msie 7") !== -1 || browserVer.indexOf("msie 8") !== -1 || browserVer.indexOf("msie 9") !== -1  ){
		$("input[placeholder] , textarea[placeholder]").each(function(){
			var othis 			= $(this);
			var holder			= othis.attr("placeholder");
			
			if( othis.data("placeholderType") == "text" ){
				var inputFontsize 	= parseInt(othis.css("fontSize"));
				var inputFontColor 	= othis.css("color");
				var inputHeight		= parseInt(othis.outerHeight(true));
				var inputWidth		= parseInt(othis.outerWidth(true));
				var posLeft 		= parseInt(othis.css("paddingLeft"));
				var posTop 			= parseInt((othis.outerHeight()-inputFontsize)/2);
				var textAlign 		= othis.css("textAlign");
				
				if( parseInt(othis.css("marginTop")) > 0 ){
					posTop = posTop + parseInt(othis.css("marginTop"));
				}
				if( parseInt(othis.css("marginLeft")) > 0 && textAlign == "left" ){
					posLeft	= posLeft + parseInt(othis.css("marginLeft"));
				}
				if( parseInt(othis.css("marginRight")) > 0 && textAlign == "right" ){
					posLeft	= posLeft + parseInt(othis.css("marginRight"));
				}
				var inputWrap 		= "<div class='PH_wrap' style='position:relative; display:inline-block; width:"+ inputWidth +"px; height:"+ inputHeight +"px;' ></div>";
				var placeholder 	= "<span class='PH_replace' style='position:absolute; margin:0; padding:0; top:"+ posTop +"px; "+ (textAlign == "right" ? "right" : "left") +":"+ posLeft +"px; color:#bbb;'>"+ holder +"</span>";
				
				othis.wrap(inputWrap);
				othis.parent("div").append(placeholder);
				
				if( othis.val() ){
					othis.next().hide();
				}
				
				othis.on("focusin focusout",function(e){
					e = e || window.event;
					if(e.type == "focusin"){
						if($.trim(othis.val()) == "" ){
							othis.next().hide();
						}
					}else if(e.type == "focusout"){
						if($.trim(othis.val()) == ""){
							othis.next().show();
						}
					}else{	
						othis.addClass("focus");								
					}
				});
				
				othis.next().on({
					"click" : function(){
						othis.trigger("focusin").focus();
					}
				});
			} 
			else {
				othis.val(holder);
				othis.on("focusin focusout",function(e){
					e = e || window.event;
					if(e.type === "focusin"){
						if($.trim(othis.val()) === holder){
							othis.val("").addClass("focus");
						}
					}else if(e.type === "focusout"){
						if($.trim(othis.val()) === ""){
							othis.removeClass("focus").val(holder);
						}
					}else{	
						othis.addClass("focus");								
					}
				});
			}
		});
	}  	
	
	// number check
	inputNumberCheck();
	
	/* initial layer */
	$(".initial_layer").each(function(){
		layerpopup($(this).attr("id"));
	});
	
	/* top search */
	var totalSearchInput = $("header .total_search input[type='text']");
	var totalSearchArea = $("header .total_search .result_area");
	
	$("body").on({
		"click" : function(){
			if( totalSearchInput.data("focus") == "off" && totalSearchArea.data("mouse") == "off" ){
				$(".total_search .result_area").hide();
			}
		}
	});
	
	totalSearchArea.data("mouse" , "off").on({
		"mouseenter" : function(){
			$(this).data("mouse", "on");	
		}, 
		"mouseleave" : function(){
			$(this).data("mouse", "off");	
		}
	});

	// 개발 적용시 주석처리 요망.	
	totalSearchInput.data("focus" , "off").on("focusin focusout keyup",function(e){
		e = e || window.event;
		var val = $.trim($(this).val());
		if(e.type === "focusin"){	
			$(this).data("focus", "on");
			if(val !== ""){
				$("#result_save").hide();
				$("#result_auto").show();
			}else{		
				$("#result_auto").hide();	
				$("#result_save").show();
			}
		}else if(e.type === "keyup"){
			if(val !== ""){
				$("#result_save").hide();
				$("#result_auto").show();
			}else{	
				$("#result_auto").hide();	
				$("#result_save").show();
			}		
		} else if(e.type === "focusout"){
			$(this).data("focus", "off");
		}
	});

	// 개발 적용시 주석처리 요망.		
	$(".result_area .close_area .close").on("click",function(){
		$(this).parents(".result_area").hide();
	});
	
	$("#result_save .search_delete").on("click",function(){
		$(this).parents("li").remove();
	});
	
	$("#result_save .delete_all").on("click",function(){
		$(this).parents(".result_area").find(".search_word .search_save li").remove();
	});
	
	// path indicator
	var pathIndicator = $(".path");
	
	pathIndicator.find(".selectbtn").on({
		"mouseenter , focusin" : function(){
			$(this).next().show();
		}
	});
	
	pathIndicator.find(".selectwrap").each(function(){
		var listW = $(this).find(".selectlist").outerWidth(true);
		$(this).width(listW - 22);		
	});
	
	pathIndicator.find(".selectwrap").on({
		"mouseleave" : function(){
			$(this).find(".selectlist").hide();
		}
	});
	
	pathIndicator.find(".selectlist > li a").on({
		"click" : function(e){
			var _this = $(this);
			var text = _this.text();
			_this.parents(".selectlist").hide().prev().text(text);
			e.preventDefault();
		}
	});
	
	// CheckBox & RadioButton Design	
	checkboxChecked();
	labelChecked();
	$(document).on("click change",".css-checkbox", function(){
		var bp = 0;
		var othis = $(this);
		bp = othis.is(":checked") ? -50 : 0;
		
		if( othis.parents("thead").size() > 0 || othis.parent(".chk_area").size() > 0 || othis.parents(".tit_top").size() > 0 ){
			bp = othis.is(":checked") ? -75 : -20;
		}
		
		if(othis.attr("class").indexOf("css-checkbox-") > -1){			
			var color = othis.attr("class").split("css-checkbox-")[1];
			switch(color) {
				case "red":
					bp = othis.is(":checked") ? -325 : -300;
				break;
				case "orange":
					bp = othis.is(":checked") ? -375 : -350;
				break;		
				case "yellow":
					bp = othis.is(":checked") ? -425 : -400;
				break;		
				case "lemon":
					bp = othis.is(":checked") ? -475 : -450;
				break;		
				case "lime":
					bp = othis.is(":checked") ? -525 : -500;
				break;		
				case "green":
					bp = othis.is(":checked") ? -575 : -550;
				break;		
				case "skyblue":
					bp = othis.is(":checked") ? -625 : -600;
				break;		
				case "blue":
					bp = othis.is(":checked") ? -675 : -650;
				break;		
				case "darkblue":
					bp = othis.is(":checked") ? -725 : -700;
				break;		
				case "white":
					bp = othis.is(":checked") ? -775 : -750;
				break;		
				case "gray":
					bp = othis.is(":checked") ? -825 : -800;
				break;
				case "disable":
					bp = othis.is(":checked") ? -850 : 0;
				break;	
				case "all":
					bp = othis.is(":checked") ? -75 : -20;
				break;
				case "compare":
					bp = othis.is(":checked")  ? -920 : -880;
				break;
			}
		}	
		if(othis.hasClass("css-checkbox2")){
			bp = othis.is(":checked")  ? -250 : -200;
		}

		if(othis.parents("table").length > 0 && othis.parents("tr").hasClass("disable")){return false;}
		othis.next("label").css("background-position","0 "+bp+"px");	
	});	
	radioChecked();
	$(document).on("click",".css-radiobox", function() {
		if($(this).is(":checked")) {
			$("input[name='"+$(this).attr("name")+"']").siblings("label").css("background-position", "0 -100px");
			$(this).next("label").css("background-position", "0 -150px");
		}
	});
	
	// CheckBox Design2

	/* header */	
	if( $("#main_visual").size() > 0 ){
		
	} else {
		$(".navigation .category_view").hide();
	}
	
	$("header .navigation .btn_close a").on("click",function(){
		$(".navigation .category_wrap").hide();
		$("nav .category_view .view_list li a").removeClass("on");
	});	
	$(".navigation .category_view .view_list li").on("mouseover click",function(){
		$("nav .category_view .view_list li a").removeClass("on");
		$(this).find("a").addClass("on");
		$(".navigation .category_wrap").hide().find(".category_list").hide();
		$(this).find(".category_wrap").show().find(".category_list").show();
	});
	$(".navigation .category_view")/* .add(".navigation .category_wrap") */.on("mouseleave",function(e){
		e = e || window.event;		
		var categoryoffset = $(".navigation .category_view").offset();
		//if(e.pageY < 166 || e.pageY > 621 || e.pageX < categoryoffset.left || e.pageX > categoryoffset.left + 931){
			
			if( $("#main_visual").size() > 0 ){
				$(".navigation").find(".category_wrap").hide().end().find(".category_view .view_list li a").removeClass("on");
				$(".navigation .shopin_menu li.shop_duty.main a").removeClass("on"); //2016-10-10 수정 
			} else {
				$(".navigation .category_view").hide().end().find(".category_view .view_list li a").removeClass("on");
				$(".navigation .shopin_menu li a.on").removeClass("on");
				//$(".navigation .shopin_menu li").eq(0).find("a").addClass("on");
			}
		//}
	});

	$(".navigation .shopin_menu li > a").on({
		"mouseenter" : function(e){
			var idx = $(this).parent().index();
			$(".navigation .shopin_menu li a.on").removeClass("on");
			$(this).addClass("on");
			if( idx == 0 ){
			   	$(".category_view").show();
				$(".brand_view").hide();
			}
			else if( idx == 1 ){
				$(".category_view").hide();/*20190716적립금수정*/
				$(".brand_view").show();
			}
			else{
				console.log(3);
			}
			e.preventDefault();
		}
	});
	
	$(".brand_view").on("mouseleave", function(e){
		$(".brand_view").hide();
		$(".navigation .shopin_menu li a").removeClass("on");
		//$(".navigation .shopin_menu li").eq(0).find("a").addClass("on");
		
		if( $("#main_visual").size() > 0 ){
			$(".category_view").show();
			$(".category_view .category_wrap").hide();/*20190716적립금수정*/
		} 
		e.preventDefault();
	});
	
	$(".header_top").on({
		"mouseenter" : function(){
			$(".brand_view").trigger("mouseleave");	
			$(".navigation .category_view").trigger("mouseleave");
		}
	});

	/*2019 적립금프로젝트(마우스오버문제-mouseleave 이벤트 핸들러 삭제)s*/
	$(".shop_duty").on({
		"mouseenter" : function(){
			$(".navigation .category_view .view_list").find(">li:first-child > a").trigger("mouseenter");
		},
		/*
		"mouseleave" : function(e){
			$(".category_view").hide();
			$(".navigation .shopin_menu li a").removeClass("on");
			//$(".navigation .shopin_menu li").eq(0).find("a").addClass("on");
			
			if( $("#main_visual").size() > 0 ){
				$(".category_view").show();
				$(".navigation").find(".category_wrap").hide().end().find(".category_view .view_list li a").removeClass("on");
			} 
			e.preventDefault();
		}
		*/
	});
	/*2019 적립금프로젝트(e)*/
	
	$(".gnb_menu").on({
		"mouseenter" : function(){
			$(".brand_view").trigger("mouseleave");	
			$(".navigation .category_view").trigger("mouseleave");
		}
	});
	/* 2016-10-19 수정 */

	/*$("header .navigation .btn_close a").on("click",function(){
		$(".navigation .category_wrap").hide();
		$("nav .category_view .view_list li a").removeClass("on");
	});
	$(".navigation .category_view .view_list li").on("mouseover click",function(){
		var menu = $(this).attr("class").split("view_item")[1];
		$("nav .category_view .view_list li a").removeClass("on");
		$(this).find("a").addClass("on");
		$(".navigation .category_wrap").show().find(".category_list").hide();
		$("#category"+menu).show();
	});
	$(".navigation .category_view").add(".navigation .category_wrap").on("mouseleave",function(e){
		e = e || window.event;
		var categoryoffset = $(".navigation .category_view").offset();
		if(e.pageY < 166 || e.pageY > 621 || e.pageX < categoryoffset.left || e.pageX > categoryoffset.left + 931){
			$(".navigation").find(".category_wrap").hide().end()
				.find(".category_view .view_list li a").removeClass("on");
		}
	});*/
	
	/* //header  */
	
	
	/* my page */
	
	$(".myshopping_info .benefit_area").find("dl").each(function(){
		var _this = $(this);
		var number = _this.find("dd em").text();
		number = number.replace(/[^0-9]/g, "");
		
		if( number.length >= 7 ){
			_this.find("dd em").addClass("on");
		} else {
			_this.find("dd em").removeClass("on");
		}
	});
	
	/* end : my page */
	
	
	// 상품수량 체크
	productAmountCheck();
	
	$(document).on("click",".plus",function(e){
		var $amountInput 	= $(this).siblings(".pcount");
		var minAmount 		= Number($amountInput.siblings(".minorder").val()) || 1;
		var maxAmount 		= Number($amountInput.siblings(".maxorder").val()) || 999999;
		var packageAmount 	= Number($amountInput.siblings(".ordercount").val()) || 1;
		var checkedAmount 	= Number($amountInput.val());
		
		if ( minAmount > packageAmount ){
			if( (minAmount % packageAmount) > 0 ){
				minAmount = ( minAmount + (packageAmount - (minAmount % packageAmount)));
			} else {
				minAmount = minAmount;
			}
		} else {
			minAmount = packageAmount;
		}
		
		if( maxAmount > packageAmount ){
			if ( (maxAmount % packageAmount) > 0 ){
				maxAmount = ( maxAmount - (maxAmount % packageAmount ));
			} else {
				maxAmount = maxAmount /* - packageAmount;  */
			}
		} else {
			maxAmount = maxAmount;
		}
	
		if( checkedAmount % packageAmount > 0 ){
			checkedAmount = checkedAmount - (checkedAmount % packageAmount);
		}
		
		if( checkedAmount < minAmount ){
			checkedAmount = minAmount;
		} else if( checkedAmount >= maxAmount ){
			checkedAmount = maxAmount;
			alert("해당 상품은 최대 "+maxAmount+"개 까지 구입이 가능합니다.");
		} else {
			for( var i=0; i < packageAmount; i++ ){
				checkedAmount ++;
			}
		} 
		
		$amountInput.val(checkedAmount);
		$amountInput.data("prevCount" , checkedAmount);
		
		$("[id^='addToCartBtn']").data("quantity", checkedAmount); 	//카트 버튼에 개수 추가 / sds 추가
		$("[id^='buynowBtn']").data("quantity", checkedAmount); 	// 바로구매 버튼에 개수 추가 / sds 추가			
		
		var _countConfirmBtn = $(this).parents(".amount_box").find(".pconut_submit");
		var currentCount = Number($(this).siblings(".pcount").data("currentCount"));
		var orderSelect	 		= $(this).parents(".amount_box").find("select");	// 2016.02.24 수정버튼 활성화 오류수정을 위해 추가 

		if( _countConfirmBtn.length){
			if( orderSelect.data("selectIndex") == undefined ){
				if( Number($(this).siblings(".pcount").val()) != currentCount ) {
					_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "block" });
					_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "none" });
				} else {
					_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "none" });
					_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "block" });
				}	
			} else {
				if(  Number($(this).siblings(".pcount").val()) != currentCount  || orderSelect.find("option:selected").index() != orderSelect.data("selectIndex") ) {
					_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "block" });
					_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "none" });
				} else {
					_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "none" });
					_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "block" });
				}	
			}
		}	
		
		e.preventDefault();
	});
	
	$(document).on("click",".minus",function(e){
		var $amountInput 	= $(this).siblings(".pcount");
		var minAmount 		= Number($amountInput.siblings(".minorder").val()) || 1;
		var maxAmount 		= Number($amountInput.siblings(".maxorder").val()) || 999999;
		var packageAmount 	= Number($amountInput.siblings(".ordercount").val()) || 1;
		var checkedAmount 	= Number($amountInput.val());
		var currentAmount	= Number($amountInput.val());
		
		// min,max 유효성 체크
		if ( minAmount > packageAmount ){
			if( (minAmount % packageAmount) > 0 ){
				minAmount = ( minAmount + (packageAmount - (minAmount % packageAmount)));
			} else {
				minAmount = minAmount;
			}
		} else {
			minAmount = packageAmount;
		}
		
		if( maxAmount > packageAmount ){
			if ( (maxAmount % packageAmount) > 0 ){
				maxAmount = ( maxAmount - (maxAmount % packageAmount ));
			} else {
				maxAmount = maxAmount /* - packageAmount;  */
			}
		} else {
			maxAmount = maxAmount;
		}
		
		if( checkedAmount % packageAmount > 0 ){
			checkedAmount = checkedAmount - (checkedAmount % packageAmount);
		}
		
		if( currentAmount <= maxAmount ){
			for( var i=0; i < packageAmount; i++ ){
				checkedAmount --;
			}
		}
		
		if( checkedAmount < minAmount ){
			alert("해당 상품은 최소 "+minAmount+"개 부터 구입이 가능합니다.");
			checkedAmount = minAmount;
		} else if( checkedAmount > maxAmount ){
			checkedAmount = maxAmount;
			//alert("최대 수량은 " + maxAmount + "개입니다.");
		} 
		
		$amountInput.val(checkedAmount);
		$amountInput.data("prevCount" , checkedAmount);
		
		$("[id^='addToCartBtn']").data("quantity", checkedAmount); 	//카트 버튼에 개수 추가 / sds 추가
		$("[id^='buynowBtn']").data("quantity", checkedAmount); 	// 바로구매 버튼에 개수 추가 / sds 추가			
		
		var _countConfirmBtn = $(this).parents(".amount_box").find(".pconut_submit");
		var currentCount = Number($(this).siblings(".pcount").data("currentCount"));
		var orderSelect	 		= $(this).parents(".amount_box").find("select");	// 2016.02.24 수정버튼 활성화 오류수정을 위해 추가 
		
		if( _countConfirmBtn.length){
			if( orderSelect.data("selectIndex") == undefined ){
				if( Number($(this).siblings(".pcount").val()) != currentCount ) {
					_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "block" });
					_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "none" });
				} else {
					_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "none" });
					_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "block" });
				}	
			} else {
				if( Number($(this).siblings(".pcount").val()) != currentCount || orderSelect.find("option:selected").index() != orderSelect.data("selectIndex") ) {
					_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "block" });
					_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "none" });
				} else {
					_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "none" });
					_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "block" });
				}	
			}
		}	
		
		e.preventDefault();
	});
	
	$(document).on("change keyup", ".pcount", function(e){
		var $amountInput 	= $(this);
		var minAmount 		= Number($amountInput.siblings(".minorder").val()) || 1;
		var maxAmount 		= Number($amountInput.siblings(".maxorder").val()) || 999999;
		var packageAmount 	= Number($amountInput.siblings(".ordercount").val()) || 1;
		var checkedAmount 	= Number($amountInput.val());
		var _countConfirmBtn = $amountInput.parents(".amount_box").find(".pconut_submit");
		var currentCount 	= Number($amountInput.data("currentCount"));
		var orderSelect	 		= $(this).parents(".amount_box").find("select");	// 2016.02.24 수정버튼 활성화 오류수정을 위해 추가 

		// min,max 유효성 체크
		if ( minAmount > packageAmount ){
			if( (minAmount % packageAmount) > 0 ){
				minAmount = ( minAmount + (packageAmount - (minAmount % packageAmount)));
			} else {
				minAmount = minAmount;
			}
		} else {
			minAmount = packageAmount;
		}
		
		if( maxAmount > packageAmount ){
			if ( (maxAmount % packageAmount) > 0 ){
				maxAmount = ( maxAmount - (maxAmount % packageAmount ));
			} else {
				maxAmount = maxAmount; 
			}
		} else {
			maxAmount = packageAmount;
		}
		
		if( e.type == "change" ){
			if( checkedAmount < minAmount ){
				alert("해당 상품은 최소 "+minAmount+"개 부터 구입이 가능합니다.");
				$amountInput.val(minAmount);
				$amountInput.data("prevCount", minAmount);
			}
			else  if( checkedAmount > maxAmount ){
				alert("해당 상품은 최대 "+maxAmount+"개 까지 구입이 가능합니다.");
				$amountInput.val(maxAmount);
				$amountInput.data("prevCount", maxAmount);
			}
			else if( (checkedAmount % packageAmount) > 0 ){
				alert("선택하신 상품의 수량은 묶음단위 수량으로만 변경이 가능합니다. ");
				$amountInput.val($amountInput.data("prevCount"));
			} else {
				$amountInput.data("prevCount", checkedAmount);
			}
			$("[id^='addToCartBtn']").data("quantity", Number($amountInput.val())); 	//카트 버튼에 개수 추가 / sds 추가
			$("[id^='buynowBtn']").data("quantity", Number($amountInput.val())); 	// 바로구매 버튼에 개수 추가 / sds 추가			
		}
		
		if( e.type == "keyup" ){
			if( isNumber($(this).val()) == false ){
				$amountInput.val($amountInput.data("prevCount"));
			}	
		}
		
		if( _countConfirmBtn.length){
			if( orderSelect.data("selectIndex") == undefined ){
				if( Number($amountInput.val()) != Number($amountInput.data("currentCount")) ) {
					_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "block" });
					_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "none" });
				} else {
					_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "none" });
					_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "block" });
				}	
			} else {
				if( Number($amountInput.val()) != Number($amountInput.data("currentCount")) || orderSelect.find("option:selected").index() != orderSelect.data("selectIndex") ) {
					_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "block" });
					_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "none" });
				} else {
					_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "none" });
					_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "block" });
				}	
			}
		}		
	});
	
	/* cart */
	/* 직항 경유 */
	$(".direct .chk_list input[id^=radio]").on("change",function(){ 
		var othis = $(this).parents(".chk_list");
		if(othis.find("li").eq(1).find("input[type='radio']").is(":checked")){
			$(".direct_txt").hide();
			$(".via_txt").show();
			othis.siblings("select").show().prop("disabled" , false);
			alert("환승 시 액체류나 젤류 등을 동반할 시 투명비닐을 준비하셔야 합니다.\n자세한 정보는 기내반입제한품목을 확인하시기 바랍니다.");
		}else{
			$(".direct_txt").show();
			$(".via_txt").hide();
			$(".direct .select_via").find("option").eq(0).prop("selected" , true);
			$(".direct .or_txt").hide();
			othis.siblings("select").hide();
		}
	});	 
	
	$(".direct .select_via").on("change",function(){
		var othisval = $(this).val();
		var msg = "환승 중 액체류 개인 소지를 위해 비닐 지퍼백을 준비해 주세요.\n보다 자세한 내용은 ‘기내반입 제한 안내’를 참조하시기 바랍니다.";
		
		if(othisval === '15') {
			$(".direct .or_txt").hide();
			$(".direct .precondition").show();
			msg = "미국을 경유하실 경우, 불투명용기 및 금속용기에 담긴 액체류, 젤류 제품은 반입이 제한됩니다.\n\n(기타 투명용기에 담긴 제품은 구매, 반입이 가능합니다.)\n\n다만, 경유 후 도착지가 미국령, 호주령, 캐나다, 버진아일랜드인 경우에는 구매하실 수 없습니다.\n\n\n계속 주문하시겠습니까? (취소를 선택하시면, 장바구니로 되돌아갑니다.)";
			if(!confirm(msg)) {
				viewCart();
			}
		} else if( othisval ==='1' || othisval === '4' || othisval === '101' || othisval === '16' || othisval === '18' || othisval === '13' || othisval === '5' || othisval === '19' || othisval === '20' ){
			$(".direct .or_txt").hide();
			$(".direct .impossible").show();
			msg = "선택하신 국가를 경유하시는 경우에는 액체류, 젤류 제품을 구매하실 수 없습니다.\n\n다만, 개인물품 휴대 기내반입조건에 충족되는 경우 구입 가능 하오니 기내반입제한 안내를 확인해 보시기 바랍니다.\n\n\n계속 주문하시겠습니까? (취소를 선택하시면, 장바구니로 되돌아갑니다.)";
			if(!confirm(msg)) {
				viewCart();
			}
		}
		/* else if(  ) {
			msg = "선택하신 국가를 경유하실 경우에는 액체류, 젤류 제품의 구매가 가능합니다.\n\n(환승시 해당국가의 보안규정이 달라 액체류에 대한 압수절차를 따라야 할 경우가 있으니, 이용하시는 항공사에 사전문의 해주시기 바랍니다.)\n\n다만, 경유 후 도착지가 미국령, 호주령, 캐나다, 버진아일랜드인 경우에는 구매하실 수 없습니다.\n\n\n계속 주문하시겠습니까? (취소를 선택하시면, 장바구니로 되돌아갑니다.)";
			if(!confirm(msg)) {
				viewCart();
			}
		} */
		else if(othisval === '21') {
			$(".direct .or_txt").hide();
			$(".direct .possible").show();
			msg = "그 외의 국가를 경유하실 경우에는 액체류, 젤류 제품의 구매가 가능합니다.\n\n(환승시 해당국가의 보안규정이 달라 액체류에 대한 압수절차를 따라야 할 경우가 있으니, 이용하시는 항공사에 사전문의 해주시기 바랍니다.)\n\n다만, 경유 후 도착지가 미국령, 호주령, 캐나다, 버진아일랜드인 경우에는 구매하실 수 없습니다.\n\n\n계속 주문하시겠습니까? (취소를 선택하시면, 장바구니로 되돌아갑니다.)";
			if(!confirm(msg)) {
				viewCart();
			}
		} else {
			$(".direct .or_txt").hide();
		}
	});
	
	$(".cartlist_table .total_box .btn_toggle").on({
		"click" : function(e){
			var _this = $(this);
			
			if( _this.find(">a").hasClass("on") ){
				_this.find(">a").removeClass("on").prop("href", "#total_box");
				_this.parent().prop("id", "");
				$(".cartlist_table .dis_wrap").hide();
				e.preventDefault();
			} else {
				_this.find(">a").addClass("on").prop("href", "#cart_sum");
				_this.parent().prop("id", "cart_sum");
				$(".cartlist_table .dis_wrap").show();	
			}
		}
	});
	
	$(".cart_payment .method").each(function(){
		var _this = $(this);
		var radioEach = _this.find("input[type='radio']");
		
		radioEach.on({
			"change" : function(){
				
				setTimeout(function(){
					var paymentTable 		= $(".cart_payment .payment_info_box");
					var paymentTableHeight 	= paymentTable.height();
					var paymentBoxHeight 	= $(".cart_payment .pay_box").height();
					
					if( paymentTableHeight < paymentBoxHeight ){
						paymentTable.css({"padding-bottom" : paymentBoxHeight-paymentTableHeight });
					} else {
						paymentTable.css({"padding-bottom" : "" });
					}
					dch = $(document).height();
				}, 100);
				
			}
		});
	});
	
	
	/* //order */

	
	//O2O checked
	var o2oAllCheckBox 	=  $(".o2o_pickup_wrap .cartlist_table > table .css-checkbox");
	var o2oEachCheckBox =  $(".o2o_pickup_wrap .th_skip .css-checkbox").not(":disabled");
	var o2oEachCheckBoxSize = o2oEachCheckBox.size(); 
	
	o2oAllCheckBox.on({
		"change" : function(){
			if( $(this).prop("checked") == true ){
				o2oEachCheckBox.prop("checked" , true).next().css("background-position", "0 -50px");
				$(".o2o_pickup_wrap .th_skip .table_normal tr").not(".product_tit").each(function(){
					$(this).find(".choice_list > li").each(function(){
						$(this).find(".chk_list > li:eq(0) input[type='radio']").prop("checked", true).next().css("background-position", "0 -150px");
						$(this).find(".chk_list > li:eq(1) input[type='radio']").next().css("background-position", "0 -100px");
					});
				});
			} else {
				o2oEachCheckBox.prop("checked" , false).next().css("background-position", "0 0");
				$(".o2o_pickup_wrap .th_skip .table_normal tr").not(".product_tit").each(function(){
					$(this).find(".choice_list > li").each(function(){
						$(this).find(".chk_list > li:eq(0) input[type='radio']").next().css("background-position", "0 -100px");
						$(this).find(".chk_list > li:eq(1) input[type='radio']").prop("checked", true).next().css("background-position", "0 -150px");
					});
				});
			}
		}
	});
	
	o2oEachCheckBox.on({
		"change" : function(){
			if( o2oEachCheckBox.filter(":checked").size() == o2oEachCheckBoxSize ){
				o2oAllCheckBox.prop("checked" , true).next().css("background-position", "0 -75px");
			} else {
				o2oAllCheckBox.prop("checked" , false).next().css("background-position", "0 -20px");
			}
			
			if( $(this).prop("checked") == true ){
				$(this).parents("tr").next().find(".choice_list > li").each(function(){
					$(this).find(".chk_list > li:eq(0) input[type='radio']").prop("checked", true).next().css("background-position", "0 -150px");
					$(this).find(".chk_list > li:eq(1) input[type='radio']").next().css("background-position", "0 -100px");
				});
			} else {
				$(this).parents("tr").next().find(".choice_list > li").each(function(){
					$(this).find(".chk_list > li:eq(0) input[type='radio']").next().css("background-position", "0 -100px");
					$(this).find(".chk_list > li:eq(1) input[type='radio']").prop("checked", true).next().css("background-position", "0 -150px");
				});
			}
		}
	});
	
	$(".o2o_pickup_wrap .table_normal tr").each(function(){
		var _this = $(this);
		_this.find("input[type='radio']").on({
			"change" : function(){
				
				var listSize 	= _this.find(".choice_list > li").size();
				var checked1 	= 0;
				var checked2 	= 0;
				var allChecked 	= 0;
				
				
				_this.find(".chk_list").each(function(){
					if( $(this).find(">li").eq(1).find("input[type='radio']").prop("checked") == true ){
						checked1++;
					}
					if( $(this).find(">li").eq(0).find("input[type='radio']").prop("checked") == true ){
						checked2++;
					}
				});
				if( checked1 > 0 ){
					_this.prev(".product_tit").find("input[type='checkbox']").prop("checked" , false).next().css("background-position", "0 0");
					o2oAllCheckBox.prop("checked" , false).next().css("background-position", "0 -20px");
				}
				
				if( checked2 == listSize ){
					_this.prev(".product_tit").find("input[type='checkbox']").prop("checked" , true).next().css("background-position", "0 -50px");
					$(".o2o_pickup_wrap .table_normal .product_tit").each(function(){
						
						if( $(this).find("input[type='checkbox']").prop("checked") == false ){
							allChecked ++;
						}
					});
					if( allChecked == 0 ){
						o2oAllCheckBox.prop("checked" , true).next().css("background-position", "0 -75px");
					}
				}
			}
		});
	});
	/* end : cart */
	
	
	/* sky_scrapper */
	/*
	function sky_scrapper(){
		var screenW = $(window).width();
		if(screenW > 1600 && !$(".sky_scrapper").hasClass("sky_scrapper_on")){
			$(".sky_scrapper").css("width","304px").addClass("sky_scrapper_on");
		}else if(screenW < 1600 && !$(".sky_scrapper").hasClass("sky_scrapper_on")){
			$(".sky_scrapper").css("width","44px").removeClass("sky_scrapper_on");	
		}
	}
	*/
	//sky_scrapper();
	$(window).resize(function(){		
		//sky_scrapper();
		windowHeigt = $(window).height();
	});
	
	
	$(".sky_scrapper .today_list_sky .area").height(windowHeigt - 98)
	.mCustomScrollbar({
		//theme:"minimal"
	});
	$(".sky_scrapper .cart_list_sky .area").height(windowHeigt - 169)
	.mCustomScrollbar({
		//theme:"minimal"
	});
	$(".sky_scrapper .my_info .area").height(windowHeigt - 40)
	.mCustomScrollbar({
		//theme:"minimal"
	});
	/* $(".sky_scrapper .mCSB_scrollTools").each(function(){
		$(this).height($(this).parents(".mCustomScrollBox").height() -20).css("top","10px");
	});	 */
	if($(".sky_scrapper .cart_list_sky .area").find("li").length < 1){
		$(".sky_scrapper .cart_list_sky .area").find("ul").hide().end().find(".null").show();
	}
	if($(".sky_scrapper .today_list_sky .area").find("li").length < 1){
		$(".sky_scrapper .today_list_sky .area").find("ul").hide().end().find(".null").show();
	}
	
	var skyScrapperActiveIdx = -1;
	$(".sky_scrapper .menu ul a").on("click",function(){
		skyScrapperActiveIdx = $(this).parent().index();
		var menu = Number($(this).parents("li").attr("class").split("m")[1]);

		if ((menu === 1) && (typeof(isLogin) == 'undefined' || isLogin != "true")){
			ACC.customer.openLoginPopup();
		}else{
			$(".sky_scrapper .pop_newwin").hide();
			$(".sky_scrapper").find(".cont > div").hide().eq(menu-1).show();
			if($(".sky_scrapper").find(".cont > div").eq(menu-1).find(".mCustomScrollBox:visible").length > 0){
				$(".sky_scrapper").find(".cont > div").eq(menu-1).find(".mCSB_scrollTools").height($(".sky_scrapper").find(".cont > div").eq(menu-1).find(".mCustomScrollBox").height() -20).css("top","10px");
			}
			
			if(skyScrapperActiveIdx == 0 && isLogin != "true"){
				skyScrapperActiveIdx = 2; /* 개발 수정 */
			} 
			
			$(".sky_scrapper").animate({"width":304},{duration:300,complete:function(){
				$(".sky_scrapper").addClass("sky_scrapper_on");    
			}});
			$(".sky_scrapper .menu .btn_top").animate({"right":260},{duration:300});
			$(".sky_scrapper .menu ul a").removeClass("active");
			$(this).addClass("active");
		}
	});

	setTimeout(function(){
		$(".sky_scrapper .pop_newwin").fadeOut();
	} , 20000);

	$(".sky_scrapper .pop_newwin .login h4 a").on("click",function(){
		$(".sky_scrapper .pop_newwin").hide();
	});

	$(".sky_scrapper .menu .btn_toggle .close").on("click",function(){
		if( $(".sky_scrapper").hasClass("sky_scrapper_on") ){
			$(this).attr("title","open");
			$(".sky_scrapper").animate({"width":44},{duration:300,complete:function(){
				$(".sky_scrapper").removeClass("sky_scrapper_on").find(".cont > div").hide();
			}});
			$(".sky_scrapper .menu .btn_top").animate({"right":0},{duration:300});
			$(".sky_scrapper .menu ul a").removeClass("active");
		} else {
			$(this).attr("title","close");
			if(skyScrapperActiveIdx < 0 ){
				skyScrapperActiveIdx = 2;
				$(".sky_scrapper").find(".cont > div").eq(skyScrapperActiveIdx).show().find(".mCSB_scrollTools").height($(".sky_scrapper").find(".cont > div").eq(skyScrapperActiveIdx).find(".mCustomScrollBox").height() -20).css("top","10px");
				$(".sky_scrapper .menu li").eq(skyScrapperActiveIdx).find("a").addClass("active");
			}
			
			if(skyScrapperActiveIdx == 0 && isLogin != "true"){
				skyScrapperActiveIdx = 2; /* 개발 수정 */
			} 
			
			$(".sky_scrapper").find(".cont > div").eq(skyScrapperActiveIdx).show();
			$(".sky_scrapper .menu li").eq(skyScrapperActiveIdx).find("a").addClass("active");
			$(".sky_scrapper").animate({"width":304},{duration:300,complete:function(){
				$(".sky_scrapper").addClass("sky_scrapper_on");				
			}});
		}
	});

	$(".sky_scrapper .menu .btn_top a").on("click",function(){
		$("html,body").animate({scrollTop:0},100);
		return false;
	});

	$(".sky_scrapper .my_info .order .tit_bg").on("click",function(){
		if($(this).hasClass("tit_bg_on")){
			$(this).removeClass("tit_bg_on").siblings(".time").slideDown();
		}else{
			$(this).addClass("tit_bg_on").siblings(".time").slideUp();
		}
	});
	
	$(".sky_scrapper .order .time .more").on({
		"click" : function(){
			var _this = $(this);
			
			if( _this.hasClass("on") ){
				_this.removeClass("on");
				_this.parents(".time").removeClass("on");
				_this.find("span").text("더보기");
			} else {
				_this.addClass("on");
				_this.parents(".time").addClass("on");
				_this.find("span").text("닫기");
			} 
		}
	});
	
	myInfoRecommendProduct(); //개발 반영시 주석처리 요망.
	
	skyScrapperProductListCheck(); 
	
	/* $(document).on("click",".sky_scrapper .tit_top .checkbox input[type='checkbox']",function(){
		var othis = $(this);
		othis.parents(".tit_top").siblings(".area").find("input[type='checkbox']").each(function(){
			if(othis.is(":checked")){
				$(this).prop("checked",true).siblings("label").css("background-position"," 0px -50px");
			}else{
				$(this).prop("checked",false).siblings("label").css("background-position"," 0px 0px");
			}
		});
	});
	$(document).on("click",".sky_scrapper .area input[type='checkbox']",function(){
		
		var checkedflag = true;
		var othis = $(this);
		var input = othis.parents(".area").siblings(".tit_top").find("input[type='checkbox']");
		var label = othis.parents(".area").siblings(".tit_top").find("label");
		for(i=0;i < othis.parents(".area").find("input[type='checkbox']").length; i++){
			if(!othis.parents(".area").find("input[type='checkbox']").eq(i).is(":checked")){
				checkedflag = false;			
			}
		}
		if(checkedflag){
			input.prop("checked",true);
			label.css("background-position"," 0px -50px");
		}else{
			input.prop("checked",false);
			label.css("background-position"," 0px 0px");	
		}
	}); */
	/* //sky_scrapper */
	
	$(".tab_a_btn a").on("click",function(){
		$(this).parents("ul").find("a").removeClass("on").end().end().addClass("on");
	});	
	
	/* mainVisual  17.09.12 삭제
	if($("#main_visual:visible").length > 0){
		var main_bg = $("<div/>")
			.attr("id","main_visual_bg")
			.css("top",$("#container").offset().top);
	
		$("#wrap").append(main_bg);
		$("#main_visual_bg").css("background-color","#"+$("#main_visual_color0").val());
		
		var mainVisualNum = 0;
		var mainVisualani = setTimeout(function(){mainVisual();},5000);
		// 개발 적용시 on을 live로 변경 요망.
		$("#main_visual").on("focusout mouseleave",function(){
			$(this).find(".menu ul").hide();
		}).find(".visual_detail").on("mouseover focusin focusout mouseleave",function(e){
			e = e||window.event; 
			if(e.type === "mouseover" || e.type === "focusin"){			
				clearTimeout(mainVisualani);			
			}else{
				if($("#main_visual .btn_area a").attr("class") === "play"){return false;}
				mainVisualani = setTimeout(function(){mainVisual();},5000);
			}	
		}).end().find(".menu ul").on("focusout mouseleave",function(){
			$(this).hide();
		}).end().find(".btn_area a").on("click",function(){
			var oclass = $(this).attr("class");
			if(oclass === "pause"){
				$(this).removeClass("pause").addClass("play").find("img").attr({"src":$(this).find("img").attr("src").replace("ico_pause_s","ico_play_s"),"alt":"재생"});
				clearTimeout(mainVisualani);
			}else{
				$(this).removeClass("play").addClass("pause").find("img").attr({"src":$(this).find("img").attr("src").replace("ico_play_s","ico_pause_s"),"alt":"일시정지"});
				mainVisualani = setTimeout(function(){mainVisual();},5000);
			}
		}).end().find(".menu a").on("mouseover focusin focusout mouseleave",function(e){
			e = e||window.event; 		
			if(e.type === "mouseover" || e.type === "focusin"){			
				clearTimeout(mainVisualani);			
				var num = $("#main_visual .menu a.sub").index(this);
				if($(this).parent("li").find("ul").length > 0){
					$(this).parent("li").siblings("li").find("a").removeClass("on").end().find("ul").hide();
					$(this).addClass("on").siblings("ul").show();				
					
					if(num < 0 ){
						num = $("#main_visual .menu a.sub").index($(this).siblings("ul").find("a.sub").eq(0));
					 }				
				}
				$("#main_visual	.visual_detail").find("li").hide().eq(num).show();
				var bg_color = $("#main_visual_color"+num).val();
				if( bg_color !== null && bg_color !== "" && bg_color !== undefined){
					$("#main_visual_bg").css("background-color","#"+bg_color);
				}
			}else{
				if($("#main_visual .btn_area a").attr("class") === "play"){return false;}
				mainVisualani = setTimeout(function(){mainVisual();},5000);
			}
		});
	}
	
	
	function mainVisual(){		
		clearTimeout(mainVisualani);
		$("#main_visual").find(".menu ul").hide().end()
		.find(".visual_detail").find("li").hide();
		mainVisualNum++; 
		if(mainVisualNum === $("#main_visual .visual_detail").find("li").length){mainVisualNum = 0;}
		var bg_color = $("#main_visual_color"+mainVisualNum).val();
		if( bg_color !== null && bg_color !== "" && bg_color !== undefined){
			$("#main_visual_bg").css("background-color","#"+bg_color);
		}		
		$("#main_visual").find(".visual_detail").find("li").eq(mainVisualNum).show().end().end().end()
		.find(".menu").find("a").removeClass("on").end().children("li").children("a").eq(parseInt(mainVisualNum/3)).addClass("on");
		mainMovie();			
	}
	function mainMovie(){		
		mainVisualani = setTimeout(function(){mainVisual();},5000);	
	}	*/
	

	
	/* mainVisual */
	if( $(".newarrival_wrap").size() > 0 ){
		var slideNum	= 5;
	} else {
		var slideNum	= 3;
	}
	slideList("brand_sale",slideNum,1,true); // (메인)기획전배너,브랜드 롤링_2016-04-29
	slideList("single_slide",slideNum,1,false); // (메인)세일 롤링_2016-06-09 개발 적용시 주석처리 요망.
	//slideList("best_out",4,4);
	//slideList("marketing_banner");
	slideList("event_banner",4);
		//slideList("sale_slide",slideNum,1,false);
	slideList("sale_slide",5,5,true);
	slideList("interest_prod_slide",3,3,false);
	slideList("detail_prod_slide",4,1,false);
	slideList("bn_set4",3,false); // 17.07.21  기획전 배너 수정

	/* mainslide type2 */
	function specials_sale(cnmae){
		var oslide = $("."+cnmae);
		var list = oslide.find(".list");
		var nav = null,navcount = 0,playbtn = null,play = true;
		
		list.prepend(list.find("li:last-child"));
			
		if(oslide.find(".banner_nav:visible").length > 0){
			oslide.css({ "visibility" : "visible" });
			
			nav = oslide.find(".banner_nav").find("ul");
			var navLength = nav.find("li").size();
			
			if(nav.children("li").length > list.children("li").length){
				var count = nav.children("li").length - list.children("li").length;
				for(var i = 0;i < count;i++){
					nav.children("li").last().remove();
				}
			}			
			playbtn = oslide.find(".btn_play");
			playbtn.find("a").on("click",function(){
				$(this)	.hide().siblings("a").show();
				if($(this).hasClass("play")){
					specialsAni = setTimeout(function(){slideNext2();},5000);
					play = true;
				}else{
					play = false;
					clearTimeout(specialsAni);
				}
				return false;
			});
			nav.find("a").on("click",function(e){
				clearTimeout(specialsAni);
				var thisIdx 	= $(this).parent().index();
				var activeIdx 	= nav.find("a.on").parent().index();
				var checkIdx;
				
				if( thisIdx > activeIdx ){
					checkIdx = thisIdx - activeIdx;
				} else {
					checkIdx = (navLength - activeIdx)+thisIdx;
				}
				for(var i = 0;i < checkIdx;i++){
					slideNext2();
				}	
				clearTimeout(specialsAni);
				e.preventDefault();
			});			
		}
		
		function slideNext2(){
			clearTimeout(specialsAni);		
			list.children("li").removeClass("on");		
			list.append(list.children("li").eq(0).clone());
			list.children("li").eq(0).remove();
			list.children("li").eq(1).addClass("on");
			if(nav !== null){				
				navcount++;
				if(list.children("li").length <= navcount){navcount = 0;}
				nav.find("a").removeClass("on").eq(navcount).addClass("on");
			}
			slideReset2();
		}			
		var specialsAni = setTimeout(function(){slideNext2();},5000);
		function slideReset2(){
			if(!play){return false;}
			specialsAni = setTimeout(function(){slideNext2();},5000);
			return false;
		}						
		oslide.find(".prev").on("click",function(e){
			e.preventDefault();
			clearTimeout(specialsAni);
			list.children("li").removeClass("on");
			list.prepend(list.children("li:last").clone());
			list.children("li:last").remove();
			list.children("li").eq(1).addClass("on");
			if(nav !== null){
				navcount--;
				if(navcount < 0 ){navcount = list.children("li").length-1;}
				nav.find("a").removeClass("on").eq(navcount).addClass("on");				
			}			
			slideReset2();
		});
		oslide.find(".next").on("click",function(e){
			e.preventDefault();
			slideNext2();
		});	
		list.add(nav).on("mouseover mouseleave mouseout mouseup mousemove focusin focusout",function(e){
			e = e || window.event;
			if(e.type === "mouseover" || e.type === "mouseout" || e.type === "mouseup" || e.type === "mousemove" || e.type === "focusin"){
				clearTimeout(specialsAni);
			}else if(play){
				specialsAni = setTimeout(function(){slideNext2();},1000);
			}
		});	
	}
	
	
	/* //mainslide type2 */
	specials_sale("specials_sale");
	/* //mainslide type2 */
	specials_sale("event_sale .event");
	specials_sale("onlyshilla_wrap .img_banner_slide");
	
	/* maintab */
	
	/* 2015.12.30 */
	function tab(iname,num){
		var count =	0;
		if(num !== null && num !== "" && num !== undefined){count = num;}
		
		var $tab = $("#"+iname);
		$tab.find(".tab_menu").eq(0).find("a").each(function(){
			if(iname !== "branch_area" && $tab.find(".btn_move:visible").length > 0){
				$(this).parents("li").css({"width":($(this).parents("ul").find("a").index(this) === 0 ? $(this).parents("li").width()-1 :$(this).parents("li").width()),"text-align":"center"});		
				var areaWidth = 0;
				for(var i = 0; i < $tab.find(".tab_menu").eq(0).find("a").length; i++){
					areaWidth = areaWidth+$tab.find(".tab_menu").eq(0).find("li").eq(i).width();				
				}
				$tab.find(".area").eq(0).add($tab.find(".area").eq(0).siblings(".area")).width(areaWidth-2);
				$(this).css({"white-space":"nowrap","padding-left":"0","padding-right":"0"});
			}			
			$(this)
			.on("click",function(){
				if($tab.hasClass("brand_head")){
					$tab.parents(".brand_wrap").find(".search_result").hide().empty();
				}
				$tab.find(".tab_menu").eq(0).find("a").removeClass("on");											
				$(this).addClass("on");
				var ohref = $(this).attr("href");				
				if($(ohref).length > 0)	{
					$tab.find(".area").eq(0).add($tab.find(".area").eq(0).siblings(".area")).hide();
					$(ohref).show();	
					if($tab.siblings(".area_con").length > 0){ //마케팅 브랜드몰					
						$tab.siblings(".area_con").hide();
						if($tab.siblings(".null").length > 0){$tab.siblings(".null").hide();}
						if(! $tab.hasClass("brand_head")){
							$(ohref+" > a").removeClass("on");
						}
						
						$(ohref+"_con").show();
						if(! $tab.hasClass("brand_head")){
							$(ohref+"_con .box").show();
						}
					}	
					if($(ohref).hasClass("sort_cont")){
						if(! $tab.hasClass("brand_head")){
							$(ohref).find("a").eq(0).click();
						}
					}
				}
				/* 내부 슬라이드 있을경우
				if(ohref.has(".next")){
					setTimeout(function(){ohref.find(".next").click()},3000);
				}
				*/
				return false;
			});
			/* 내부 슬라이드(hot_sale_tab) 있을경우
			if(iname == "hot_sale_tab" && $($(this).attr("href")).has(".next")){
				slideList($(this).attr("href").split("#")[1],4);
			}
			*/
		});
		/* end : 2015.12.30 */
		
		
		if($tab.find(".btn_move:visible").length > 0){
			$tab.find(".btn_move a").on("click",function(){
				if($(this).hasClass("prev")){					
					$tab.find(".area:visible").prepend($tab.find(".area:visible").children("li").last().clone()).children("li").last().remove();
				}else{
					$tab.find(".area:visible").append($tab.find(".area:visible").children("li").first().clone()).children("li").first().remove();
				}
			});
		}
		if($tab.find("div[id^='store_phone']").length > 0){
			$tab.find("div[id^='store_phone']").each(function(){
				tab($(this).attr("id"));
			});
		}
		
	}
	
	function tab2(iname,cname){
		$("#"+iname+" .tab_menu").find("a").each(function(){
			$(this).on("click",function(){
				$("#"+iname+" .tab_menu").find("li").removeClass("on");	
				$(this).parents("li").addClass("on");
				var ohref = $($(this).attr("href"));
				ohref.siblings("."+cname).hide();
				ohref.show();
				if($("#"+iname).has(".btn_off")){
					
				}
				return false;
			});
		});
		
	}	
	function tab3(iname){
		var $tab = $("#"+iname);
		$tab.find(".tab_menu").eq(0).find("a").each(function(){			
			$(this).on("click",function(){
				$tab.find(".tab_menu").eq(0).find("a").removeClass("on");						
				$tab.find(".area2").hide();
				$(this).addClass("on");
				var ohref = $($(this).attr("href"));
				ohref.show();	
				return false;
			});
		});
		
	}	
	
	tab("detail_tab");
	tab("detail_tab2");
	tab("brand_tab");
	tab("f_brand_tab");
	tab("brand2_tab");
	tab("default_tab");
	tab("default_tab2");		
	tab("default_tab3");
	tab2("category","chk_list");
	tab3("tab0105");
	tab("category_tab");//2016.03.21 추가
	tab3("brand_sort_tab");//2016-09-28 추가
	
	
	// sub brand auto complete
	var exhibitionSearchInput 	= $(".exhibition_wrap .search_wrap .search_lnp input[type='text']");
	var exhibitionAutoCompArea	= $(".exhibition_wrap .search_wrap .auto_complete");
	
	exhibitionSearchInput.each(function(){
		var _this = $(this);
		
		_this.on("focusin focusout blur keyup",function(e){
			e = e || window.event;
			var val = $.trim($(this).val());
			if(e.type === "focusin"){
				if(val){
					exhibitionAutoCompArea.show();
				}else{
					exhibitionAutoCompArea.hide();
				}
			}else if(e.type === "keyup"){
				if(val){
					exhibitionAutoCompArea.show();
				}else{
					exhibitionAutoCompArea.hide();
				}
			} 
		});
		
		_this.next(".delete").on({
			"click" : function(e){
				_this.val("");
				e.preventDefault();
			}
		});
	});
	
	$(document).on("click" , ".exhibition_wrap .search_wrap .auto_complete > ul > li a" , function(e){
		exhibitionSearchInput.val($(this).text());
		exhibitionAutoCompArea.hide();
		$(".exhibition_wrap .search .btn_a_white").trigger("click");
		e.preventDefault();
	}).on("mouseleave" , ".exhibition_wrap .search_wrap .auto_complete" , function(){
		$(".exhibition_wrap .search_wrap .auto_complete").hide();
	});
	
	$(".exhibition_wrap .btn_exhibition_detail").on({
		"click" : function(e){
			var _this = $(this);
			if( _this.hasClass("toggle_on") ){
				_this.removeClass("toggle_on");
				$(".exhibition_wrap .condition").hide();
			} else {
				_this.addClass("toggle_on");
				$(".exhibition_wrap .condition").show();
			}
			e.preventDefault();
		}
	});
	
	$(".exhibition_wrap .condition .chk_list").each(function(){
		var _this = $(this);
		var allCheckBox 	= _this.find(">li").eq(0).find("input[type='checkbox']");
		var eachCheckBox 	= _this.find(">li").eq(0).siblings().find("input[type='checkbox']");
		var checkboxLength 	= _this.find(">li").eq(0).siblings().size();
			
		allCheckBox.on({
			"change" : function(){
				if( $(this).prop("checked") == true ){
					eachCheckBox.prop("checked" , true).next().css({ "background-position" : "0 -50px" }); 
				} else {
					eachCheckBox.prop("checked" , false).next().css({ "background-position" : "0 0" });; 
				}
			}
		});
		
		eachCheckBox.on({
			"change" : function(){
				if( eachCheckBox.filter(":checked").size() == checkboxLength ){
					allCheckBox.prop("checked" , true).next().css("background-position", "0 -50px");
				} 
				else {
					allCheckBox.prop("checked" , false).next().css("background-position", "0 0");
				}
			}
		});
	});
	
	
	
	// best shop tab
	$(".best_selling").each(function(){
		var _this = $(this);
		
		_this.find(".hotsale_list .tab_menu").find("a").on({
			"click" : function(e){
				if(! $(this).hasClass("on") ){
					var idx = $(this).parent().index();
					_this.find(".hotsale_list .tab_menu").find("a").removeClass("on");
					$(this).addClass("on");
					//_this.find(".product_list").removeClass("on").eq(idx+1).addClass("on");
				}
				e.preventDefault();
			}
		});
	});
	
	
	
	
	
	/*
	$(".category_brand_toggle").on("click",function(){
		if($(this).hasClass("toggle_on")){
			$(this).addClass("toggle_off").removeClass("toggle_on");
			$("#category_brand").css("height","100%");
		}else{
			$(this).addClass("toggle_on").removeClass("toggle_off");
			$("#category_brand").animate({"height":70},{duration:500});
		}
		return false;
	});
	*/



	/******* brand mall ********/

	//lnb
	if( $(".brand_mall_lnb").size() > 0 ){
		

		brandMallLnb.init();
	}

	/******* end : brand mall ********/


	/* detail top layer tab */
	$("#detail_con_tab a").on("click",function(){
		if($(this).hasClass("on") || $(this).parent().hasClass("tab_cart") ){return false;}
		$("#detail_con_tab a").removeClass("on");
		$(this).addClass("on");		
		$($(this).attr("href")).show().siblings(".tab_cont").removeAttr("style").hide();		
		var top = $($(this).attr("href")).offset().top;
		if($("#detail_con_tab").hasClass("fixed")){
			if($(this).attr("href") === "#detail_con_tab1"){top = top + 52;}
			$("html,body").animate({scrollTop:top},100);
		}
		return false;
	});
	
	/* //detail top layer tab */
	$(".detail_view .gift .popup_brand_detail").on("click",function(){
		layerpopup("popup_brand");
		return false;
	});
	
	$(".detail_view .popup_card").on("click",function(){

		layerpopup("popup_card");

		//$("#popup_card").show();
		return false;
	});
	
	/* $(".popup_like_product_add").on("click",function(){
		layerpopup("popup_like_product_add");
		return false;
	}); */
	
	$(".detail_view .popup_jeju").on("click",function(){
		layerpopup("popup_jeju");
		return false;
	});
	
	/* detail page cart dim */
	if($(".pr_wrap .btn_off:visible").length > 0){
		$("#detail_con_tab a.cart").addClass("off");
	}
	
	/* detail contents product img */
	$(document).on("click", ".detail_view .detail_img a", function(){
		if($(this).hasClass("on")){return false;}
		$(this).parents("ul").find("a").removeClass("on");
		$(this).addClass("on").parents(".detail_img").find("P").find("img").attr("src",$(this).attr("data-url"));		
	});

	// detail contents product (up/down hide slide)
	todayProduct($(".detail_img .detail_img_small"), $(".detail_img .detail_img_small .next"), $(".detail_img .detail_img_small .prev"),4);

	
	/* detail map */
	$(".detail_view .tab4 .txt_img .txt a").on("click mouseover",function(){
		if($(this).hasClass("on")){return false;}
		$(this).parents("ul").find("a").removeClass("on");
		$(this).addClass("on");
		var mapnum = $(".detail_view .tab4 .txt_img .txt a").index(this);
		$(".detail_view .tab4 .txt_img .img").find('li').eq(mapnum).addClass('on').siblings().removeClass('on');
		
	});
	
	/* detail table */
	$(document).on("click",".detail_view .table_normal .left a",function(){
		var othis = $(this);
		var otable = $(this).parents("table");
		if(othis.hasClass("on")){
			othis.removeClass("on").parents("tr").next(".table_view_tr").hide();		
			if(othis.parents("tr").next(".table_view_tr").hasClass("last")){
				othis.parents("tr").addClass("last");		
			}
			return false;
		}
		otable.find(".table_tit").removeClass("on");
		otable.find(".table_view_tr").hide();
		otable.find(".table_view_tr:last-child").prev("tr").addClass("last");
		othis.addClass("on").parents("tr").next(".table_view_tr").show();		
		if(othis.parents("tr").hasClass("last")){
			othis.parents("tr").removeClass("last").next(".table_view_tr").addClass("last");
		}
		return false;
	});
	
	/* detail review popup */
	$(".detail_view .add_review").on("click",function(){
		window.open("product_detail_popup_qna2.html",'','width=672,height=700');
		return false;
	});
	
	/* category */
	if($(".lnb_wrap:visible").length > 0){
		var promoh = $(".category_wrap").find("div[class^=promotion]").height();
		$(".category_wrap").find(".lnb_wrap").height(promoh);		//sds요청 크기 고정으로 불필요 > 브라우져별 호환성이슈
		if($(".lnb_wrap .brand").length > 0){
			var lnbsub = $(".lnb_wrap .lnb > li").length;
			switch(lnbsub) {
				case 10:
					lnbsub = 1;
				break;
				case 9:
					lnbsub = 2;
				break;
				case 8:
					lnbsub = 2;
				break;
				case 7:
					lnbsub = 2;
				break;
				case 6:
					lnbsub = 3;
				break;
				case 5:
					lnbsub = 3;
				break;
				case 3:
					lnbsub = 5;
				break;
				case 2:
					lnbsub = 6;
				break;
				case 1:
					lnbsub = 6;
				break;			
			}
			$(".lnb_wrap .lnb").height(promoh-216-(lnbsub*49));
		}
	
	}
	$(".category_wrap .lnb_wrap .brand dt a").on("click",function(){
		if($(this).hasClass("on")){
			$(this).removeClass("on").parents(".lnb_wrap").find(".lnb").show().siblings(".brand").removeClass("brand_on");
			
		}else{
			$(this).addClass("on").parents(".lnb_wrap").find(".lnb").hide().siblings(".brand").addClass("brand_on");
		}
		return false;		
	});
	$(".category_wrap .lnb_wrap .brand dd a").on("focusin",function(){
		if($(this).parents(".brand").find("a").index(this) > 3){
			$(".category_wrap .lnb_wrap .brand dt a").addClass("on").parents(".lnb_wrap").find(".lnb").hide().siblings(".brand").addClass("brand_on");
		}
	});
	
	/* power search */ 
	commonFacetUI();
	
	$(".power_search .btn_payment").find("a").on({
		"click" : function(e){
			var productListArea = $(".product_list_wrap");
			productListArea.find(".product_list .list").removeClass("compare");
			productListArea.find(".tab_area .btn_compare").removeClass("on btn_red_s").addClass("btn_brown_s").text("상품비교");
			productListArea.find(".compare_btn").find(".btn_com").hide().end().find("input[type='checkbox']").prop("checked" , false).next().css({ "background-position" : "0px -880px" });
			e.preventDefault();
		}
	});

	/* marketing */
	$(".global_box .country_select a").on("click",function(){
		if($(this).hasClass("on")){return false;}
		var othis = $(this);
		othis.parents("ul").find("a").removeClass("on").end().end().addClass("on");
		var country_select = $(".brand_box .brand_select dl");
		var countrycode = othis.attr("data-countrycode");
		var countryhtml = othis.html();
		var countrytext = othis.find("span").text();
		country_select.find("dt img").attr({"src":country_select.find("dt img").attr("src").split("ico_country")[0]+"ico_country"+countrycode+".png","alt":countrytext});
		country_select.find("dd").html(countryhtml);
		//return false();
	});
	$(".brand_box .brand_select a").on("click",function(){
		if($(this).hasClass("on")){return false;}
		$(this).parents("ul").find("a").removeClass("on").end().end().addClass("on");
		//return false();
	});
	
	// 쿠폰존
	$(".marketing_coupon_wrap .coupon_list").find(".btn_area a").on("click", function(e){
		var _this = $(this);
		var checkbox = $(".marketing_coupon_wrap .coupon_list").find(".checkbox");

		if ( _this.hasClass("on") ) {
			checkbox.prop("checked", true);
		}else if ( _this.hasClass("off") ) {
			checkbox.prop("checked", false);
		}

		e.preventDefault();
	});
	
	// 청첩장 등록
	$(".wedding_popup .wedding_form").find("input[type='radio']").on({
		"change" : function(){
			if( $(this).filter(":checked").index() == 0 ){
				$(".spouse_id").show();
				$(".spouse_sns").hide();
			} else {
				$(".spouse_id").hide();
				$(".spouse_sns").show();
			}
		}
	});
	
	// detail con tab
	var detail_con_tabOffsetTop = "",
		detail_con_tab = null,
		detail_con = null;
	if($("#detail_con_tab:visible").length > 0){
		detail_con_tab = $("#detail_con_tab");
		detail_con_tabOffsetTop =$("#detail_con_tab").offset().top;
		detail_con = {
			"1":$("#detail_con_tab1"),
			"2":$("#detail_con_tab2"),
			"3":$("#detail_con_tab3"),
			"4":$("#detail_con_tab4"),
			"5":$("#detail_con_tab5")
		};
	}
	
	//main bestproduct wing		
	var best_productOffsetTop =  "",
		best_product = null,
		bp =  "";
	if($("#best_product:visible").length > 0){
		best_productOffsetTop = $("#best_product").offset().top;
		best_product = $("#best_product .quick");
		bp = $("#best_product1").offset().top-50;
	}	
	
	
	
	
	/* new Arrival Brand slide */
	var $event = $(".newarrival_wrap .top_banner");
	var $eventContent = $event.find(".img > li");
	var $eventTab = $event.find(".btn_visual > ul > li");
	var $eventArrow = $event.find(".arrow");

	var eventTotal = $eventContent.length;
	var eventNum = 0;
	var pageNum = 0;

	$eventTab.each(function(_i) {
		$(this).css({ left:$(this).width() * (_i) });
	}).bind("mouseenter focusin", function(_e) {
		eventNum = $eventTab.index(this);
		event_content_change();
	});

	if (eventTotal > 1) {
		$eventArrow.show().bind("click", function(_e) {
			_e.preventDefault();
			if ($(this).hasClass("prev")) {
				if (eventNum == pageNum) pageNum = (pageNum == 0) ? eventTotal - 1 : pageNum - 1;
				eventNum = (eventNum == 0) ? eventTotal - 1 : eventNum - 1;
			} else {
				var temp = pageNum + 4;
				if (temp > eventTotal - 1) temp -= eventTotal;
				if (eventNum == temp) pageNum = (pageNum == eventTotal - 1) ? 0 : pageNum + 1;
				eventNum = (eventNum == eventTotal - 1) ? 0 : eventNum + 1;
			}

			for (var i = 0; i < eventTotal; i++) {
				var index = i + pageNum;
				if (index > eventTotal - 1) index -= eventTotal;
				$eventTab.eq(index).css({ left:$eventTab.eq(i).width() * i });
			}
			event_content_change();
		});
	}

	function event_content_change() {
		$eventContent.hide().eq(eventNum).show();
		$eventTab.removeClass("on").eq(eventNum).addClass("on");
	}
	
	
	
	
	
	
	// 스카이스크래퍼
	
	var mainSkyScrapperBanner,
		mainSkyScrapperBannerOffsetTop; 
	
	if( $(".sky_scrapper_banner:visible").length > 0 ){
		mainSkyScrapperBanner = $(".sky_scrapper_banner");
		mainSkyScrapperBannerOffsetTop = $(".sky_scrapper_banner").offset().top;
	}
	
	var leftScrollmenuOffsetTop = "",
		leftScrollmenuPositionTop = "",
		leftScrollmenu = null;
	//var leftScrollmenuMarginTop = 10;	
	if($("#lnb:visible").length > 0){
		leftScrollmenu = $("#lnb");
		leftScrollmenuOffsetTop = leftScrollmenu.offset().top;
		leftScrollmenuPositionTop = leftScrollmenu.position().top;
	}	
	var rightScrollmenuOffsetTop = "",
		rightScrollmenuPositionTop = "",
		rightScrollmenuMarginTop = 10,
		rightScrollmenu = null;
	if($("#rnb:visible").length > 0){
		rightScrollmenu =  $("#rnb");
		rightScrollmenuOffsetTop = rightScrollmenu.offset().top;
		rightScrollmenuPositionTop = rightScrollmenu.position().top;
	}		
	var paybox = null,
		payboxOffsetTop = "",
		payboxHeight = "",
		footerH = $("#footer").height(),
		payboxParent,
		dch = $(document).height();

	if($(".pay_box:visible").length > 0){
		paybox = $(".pay_box");
		payboxParent = $(".cart_list .dis_wrap"),
		payboxOffsetTop = paybox.offset().top;
		payboxH();		
	}
	function payboxH(){
		payboxHeight = paybox.height();
	}
	$(window).scroll(function(){		
		var dsT = $(document).scrollTop(),
		//var tmp = ($(document).scrollTop()+leftScrollmenuPositionTop+leftScrollmenuMarginTop)-leftScrollmenuOffsetTop;
			tmp = (dsT+rightScrollmenuPositionTop+rightScrollmenuMarginTop)-rightScrollmenuOffsetTop,
			bpo =  dsT-best_productOffsetTop-85,
			bpv = dsT+windowHeigt;
		if(rightScrollmenu !== null){
			if(tmp>155) {
				/*if(best_productOffsetTop > bpv){
					rightScrollmenu.animate({'top':tmp}, {duration:'slow', easing:'swing', queue:false});
				}*/
				rightScrollmenu.animate({'top':tmp}, {duration:'slow', easing:'swing', queue:false});
			}
			else {
				leftScrollmenu.animate({'top':leftScrollmenuPositionTop}, {duration:'slow', easing:'swing', queue:false});
				rightScrollmenu.animate({'top':rightScrollmenuPositionTop}, {duration:'slow', easing:'swing', queue:false});
			}
		}
		if(best_product !== null){
			if(bpo > 0){
				best_product.addClass("fixed");
				best_product.find("a").removeClass("on");			
				best_product.find("a").eq(parseInt((dsT-bp)/624)).addClass("on");		
			}else{
				best_product.removeClass("fixed");
			}
		}				
		
		if(detail_con_tab !== null){
			if(detail_con_tabOffsetTop < dsT ){
				detail_con_tab.addClass("fixed");
				detail_con_tab.find("a").removeClass("on");			
				for(var i in detail_con){
					var n = i-1;
					if(detail_con[i].is(":visible")){
					//if(detail_con[i].offset().top < dsT + 120 && detail_con[i].height()+detail_con[i].offset().top > dsT){ 스크롤시 
						detail_con[i].css("padding-top","82px");
						detail_con_tab.find("a").eq(n).addClass("on");	
						break;
					}					
				}				
			}else{
				detail_con_tab.removeClass("fixed");
				for(var j in detail_con){
					detail_con[j].css("padding-top","30px");					
				}				
			}
		}
		if(paybox !== null ){
			payboxH();
			if(paybox.parents(".info_box").size() > 0){
				if(payboxOffsetTop < dsT && payboxHeight < windowHeigt){
					paybox.addClass("fixed");
					if(dch - footerH -10 < bpv ){
						paybox.css("top",(dch-footerH-bpv - 10));
					}else{
						paybox.css("top",0);
					}
				}else{
					paybox.removeClass("fixed");
				}
			} else {
				if(payboxOffsetTop < dsT && payboxHeight < windowHeigt){
					paybox.addClass("fixed");
					if( dsT > payboxOffsetTop+(payboxParent.height()-payboxHeight) ){
						paybox.css("top", (payboxOffsetTop+(payboxParent.height()-payboxHeight))-dsT );
					}else{
						paybox.css("top",0);
					}
				}
				else{
					paybox.removeClass("fixed");
				}
			}
		}
		
		// main sky_scrapper left banner
		if($(".sky_scrapper_banner:visible").size() > 0 ){
			
			if( $(window).scrollTop() > mainSkyScrapperBannerOffsetTop ){
				mainSkyScrapperBanner.addClass("fixed");
			} else {
				mainSkyScrapperBanner.removeClass("fixed");
			}
		}
	});

	// detail visual img
	var detail_pr_img = 0;
	if($(".detail_view .pr_wrap .img ul.productimg_thum li").length > 5 ){
		for(var i = 5;i < $(".detail_view .pr_wrap .img ul.productimg_thum li").length; i++ ){
			$(".detail_view .pr_wrap .img ul.productimg_thum").find("li").eq(i).hide();
		}
		detail_pr_img = 5;
	} else {
		$(".detail_view .pr_wrap .img").find(".btn_prev").hide().end().find(".btn_next").hide();
	}
	
	
	if($(".detail_view .detail_img li").length > 4 ){
		for(var j = 4;j < $(".detail_view .detail_img li").length; j++ ){
			$(".detail_view .detail_img").find("li").eq(j).hide();
		}
	}
	$(".detail_view .pr_wrap .img .btn_prev").on("click",function(){
		if(5 < detail_pr_img){
			detail_pr_img--;
			$(".detail_view .pr_wrap .img ul.productimg_thum li").eq(detail_pr_img).hide();						
			$(".detail_view .pr_wrap .img ul.productimg_thum li").eq(detail_pr_img - 5).show();
		}else{
			//alert("无上一张图片");
		}
	});
	$(".detail_view .pr_wrap .img .btn_next").on("click",function(){
		var imgl = $(".detail_view .pr_wrap .img ul.productimg_thum li").length;
		if(imgl > detail_pr_img){
			$(".detail_view .pr_wrap .img ul.productimg_thum li").eq(detail_pr_img - 5).hide();						
			$(".detail_view .pr_wrap .img ul.productimg_thum li").eq(detail_pr_img).show();
			detail_pr_img++;	
		}else{		
			//alert("无下一张图片");
		}
	});
	
	
	if($(".detail_view .pr_wrap:visible").length > 0){
		productDetailZoom();
	}
		
	
	// detail 연관상품 계산
	if($(".detail_view .recommend:visible").length > 0){
		var recommend = $(".detail_view .recommend");
		var sale = Number(recommend.find("li:first .sale").text().split("$")[1]);
		var regular = Number(recommend.find("li:first .regular").text().split("$")[1]);
		recommend.find(".total").find(".sale_price").text("$"+regular).end().find(".dc_price").text("$"+sale).end().find("p:first span").text(1);
		recommend.find(".scroll input[type='checkbox']").on("click",function(){
			var saleP = sale;
			var regularP = regular;
			var num = 1;
			for(i=0;i < recommend.find(".scroll input[type='checkbox']").length; i++){
				if(recommend.find(".scroll input[type='checkbox']").eq(i).is(":checked"))	{
					saleP = saleP+Number(recommend.find(".scroll > div > ul > li").eq(i).find(".sale").text().split("$")[1]);
					regularP = regularP+Number(recommend.find(".scroll > div > ul > li").eq(i).find(".regular").text().split("$")[1]);
					num++;
				}
			}			
			recommend.find(".total").find(".sale_price").text("$"+regularP).end().find(".dc_price").text("$"+saleP).end().find("p:first span").text(num);
		});
		
	}
	function exhibition(iname){
		var area  = $("#"+iname),list = area.find(".img"),count = area.find(".count"),num = 1,listlegnth = list.find("li").length;
		count.html("<em>1</em> / "+listlegnth);
		list.find("li").eq(0).addClass("on");

		area.find(".prev").on("click",function(){
			if(num < 2){return false;}
			num--;
			list.find("li").removeClass("on").eq(num-1).addClass("on");
			count.html("<em>"+num+"</em> / "+listlegnth);
		}).end().find(".next").on("click",function(){
			if(num >= listlegnth){return false;}
			list.find("li").removeClass("on").eq(num).addClass("on");
			num++;
			count.html("<em>"+num+"</em> / "+listlegnth);
		});

	}
	exhibition("exhibition");
	
	// category banner slide	
	function bannerslide(iname){	
		if($("#"+iname).length < 1){return false;}
		var oslide = $("#"+iname);
		var inameAni = null;
		var list = oslide.find(".img").eq(0);
		var listLength = list.find(".imgcon").length;
		var nav =  oslide.find(".slide_paging");
		var count = 0;
		var playflag = false;
		var review_info = null;
		if(oslide.find(".review_info").length > 0){
			review_info = oslide.find(".review_info");
		}		
		list.find(".imgcon").removeClass("on").eq(count).addClass("on");
		nav.find("a").removeClass("on").eq(count).addClass("on");
		function slideNext2(){
			clearTimeout(inameAni);		
			if(playflag){return false;}
			count++;
			if(count === listLength){count = 0;}
			list.find(".imgcon").removeClass("on").eq(count).addClass("on");
			nav.find("a").removeClass("on").eq(count).addClass("on");
			if(review_info !== null){
				review_info.hide().eq(count).show();
			}
			inameAni = setTimeout(function(){slideNext2();},2000);		
		}	
		function slidePrev2(){
			clearTimeout(inameAni);		
			if(playflag){return false;}
			count--;
			if(count < 0){count = listLength-1;}
			list.find(".imgcon").removeClass("on").eq(count).addClass("on");
			nav.find("a").removeClass("on").eq(count).addClass("on");
			if(review_info !== null){
				review_info.hide().eq(count).show();
			}
			inameAni = setTimeout(function(){slideNext2();},2000);		
		}			
		inameAni = setTimeout(function(){slideNext2();},2000);					
		list.on("mouseover mouseleave mouseout mouseup mousemove focusin focusout",function(e){
			e = e || window.event;
			if(e.type === "mouseover" || e.type === "mouseout" || e.type === "mouseup" || e.type === "mousemove" || e.type === "focusin"){
				clearTimeout(inameAni);
			}else{
				inameAni = setTimeout(function(){slideNext2();},1000);
			}
		});
		nav.find("a").on("mouseover mouseleave mouseout mouseup mousemove focusin focusout",function(e){
			e = e || window.event;
			count = nav.find("a").index(this);
			if(e.type === "mouseover" || e.type === "mouseout" || e.type === "mouseup" || e.type === "mousemove" || e.type === "focusin"){
				clearTimeout(inameAni);
				list.find(".imgcon").removeClass("on").eq(count).addClass("on");
				nav.find("a").removeClass("on").eq(count).addClass("on");			
			}else{
				inameAni = setTimeout(function(){slideNext2();},1000);
			}
		});
		oslide.find(".promotion_button a").on("click",function(e){
			$(this)	.hide().siblings("a").show();
			if($(this).hasClass("play")){
				playflag = false;
				inameAni = setTimeout(function(){slideNext2();},1000);
			}else{
				playflag = true;
				clearTimeout(inameAni);				
			}
			e.preventDefault();
		});
		if(	oslide.find(".prev").length > 0){
			oslide.find(".prev").on("click",function(){
				slidePrev2();
			}).end().find(".next").on("click",function(){
				slideNext2();
			});
		}
	}

	$.fn.singleNavSlide = function(options){
		var settings = {
			auto			: true,
			pagingClass 	: "slide_paging",
			imgAreaClass 	: "img",
			activeClass 	: "on",
			thumbEvent		: "mouseenter",
			speed			: 3000
		}
		
		$.extend(settings, options);
		var _this 			= $(this);
		var imgList 		= _this.find("."+settings.imgAreaClass).find(">li");
		var listLengh 		= imgList.size();
		var activeIdx		= 0;
		var status			= true;
		
		var pagingListHtml 	= "";
		var interval;
		
		if( imgList.size() == 1 ){
			_this.find(".btn_area").hide();
			return;
		} else {
			_this.find(".btn_area").show();
		}
		
		for( var i = 0; i < listLengh; i++){
			pagingListHtml += '<li><a href="javascript:void(0)" '+  (i == 0 ? "class='on'" : ""  )  +' >'+(i+1)+'</a></li>';
		}
		
		var pagingHtml 	= '<ul class="'+settings.pagingClass+' clear_both">'+pagingListHtml+'</ul>';
		
		_this.append(pagingHtml);
		
		var pagingDot = _this.find("."+settings.pagingClass).find("a");
		
		function pagingEvent(){
			pagingDot.on("mouseenter , click" , function(e){
				
				if( e.type == settings.thumbEvent ){
					autoStop();
					activeIdx = $(this).parent().index();
					pagingDot.removeClass(settings.activeClass);
					$(this).addClass(settings.activeClass);
					imgList.filter(":visible").hide();
					imgList.eq(activeIdx).show();
				}	
				e.preventDefault();
			});
			
			_this.on({
				"mouseenter , focusin" : function(){
					if( status ){
						autoStop();
					}
				},
				"mouseleave , focusout" : function(){
					if( status ){
						autoStrat();
					}
				}
			});
			
			_this.find(".pause").on({
				"click" : function(e){
					$(this).hide();
					_this.find(".play").show();
					status = false;
					autoStop();
					e.preventDefault();
				}
			});
			
			_this.find(".play").on({
				"click" : function(e){
					$(this).hide();
					_this.find(".pause").show();
					status = true;
					autoStrat();
					e.preventDefault();
				}
			});
			
		}
		
		function autoStrat(){
			if( settings.auto == true ){
				autoStop();
				interval = setInterval(function(){
					if( activeIdx < listLengh-1 ){
						activeIdx ++;
					} else if( activeIdx == listLengh-1){
						activeIdx = 0;
					}
					pagingDot.removeClass(settings.activeClass);
					pagingDot.eq(activeIdx).addClass(settings.activeClass);
					imgList.filter(":visible").hide();
					imgList.eq(activeIdx).show();
				}, settings.speed);
			}
		}
		
		function autoStop(){
			clearInterval(interval)
		}
		
		function init(){
			pagingEvent()
			autoStrat()			
		}
		
		init();
	}
	
	
	$.fn.brandSlide = function(tab){
		$(this).each(function(){
			var _this = $(this);
			var btnNext 	= _this.find(".next");
			var btnPrev 	= _this.find(".prev"); 			
			var rollWrap 	= _this.find(".inner");
			var contWrap	= _this.next(".outer");
			var rollUl	 	= rollWrap.find(">ul:first-child");
			var rollList 	= rollUl.find(">li");
			var totalSize 	= rollList.size();
			var moving 		= rollList.outerWidth() + parseInt(rollList.css("marginLeft")) + parseInt(rollList.css("marginRight"));
			var activeIdx = 0;
			
			if( tab ){
				rollList.each(function(i){
					$(this).attr("data-index" , i)
				});
				
				rollList.find("a").on({
					"click" : function(e){
						activeIdx = $(this).parent().attr("data-index");
						rollList.find("a").removeClass("on");
						$(this).addClass("on");
						contWrap.find(">div").hide().eq(activeIdx).show();
						e.preventDefault();
					} 
				});
			}
			
			btnPrev.on({
				"click" : function(e){
					moveLeft();
					e.preventDefault();
				}
			});
			
			btnNext.on({
				"click" : function(e){
					moveRight();
					e.preventDefault();
				}
			});
			
			function moveRight(){
				rollUl.stop().animate({
					left:-moving
				}, 500, function(){
					rollUl.css({
						"left" : 0
					});
					rollList.filter(":first-child").appendTo(rollUl);
				});
				
			}
			
			function moveLeft(){
				rollList.filter(":last-child").prependTo(rollUl);
				rollUl.css({
					"left" : -moving
				});
				rollUl.stop().animate({
					left: 0
				}, 500);
			}
		});
	}
	
	if( $("#marketing").size() > 0 ){	
		var marketSlideCreateTmp		= [];
		var marketSlideEachInterval		= [];
		var marketSlideState			= [];		
		var marketNavSlideCreateTmp		= [];
		var marketNavSlideEachInterval	= [];
		var marketNavSlideState			= [];		
		
		$.fn.marketSlide = function(options){
			
			var settings = {
				auto 		 	: true,
				navigate		: true,
				link			: false,
				button			: true,
				mimiumSize 		: 1,
				move			: 1,
				navEvent		: "mouseenter"
			}
			
			$.extend(settings, options);
			
			$(this).each(function(i){
				var _this 		= $(this);
				var btnPrev 	= _this.find(".prev"); 
				var btnNext 	= _this.find(".next"); 
				var rollWrap 	= _this.find(".inner");
				var contWrap	= _this.find(".outer");
				var rollUl	 	= rollWrap.find(">ul:first-child");
				var rollList 	= rollUl.find(">li");
				var totalSize 	= rollList.size();
				var moving 		= rollList.outerWidth() + parseInt(rollList.css("marginLeft")) + parseInt(rollList.css("marginRight"));
				var activeIdx 	= 0;
				var tabIdx 		= _this.parents(".tab_cont").index() - 1;
				var moveListSize;
				var check;
				
				if( totalSize < settings.mimiumSize ){
					btnPrev.hide();
					btnNext.hide();
					return;
				}
				
				if( settings.button == false ){
					btnPrev.hide();
					btnNext.hide();
				}
				
				if ( (totalSize - settings.mimiumSize) > settings.move ){
					moveListSize = settings.move;
				} else {
					moveListSize = totalSize - settings.mimiumSize;
				}
				
				if( settings.navigate == true ){
					var pagingListHtml = "";
					
					for( var i = 0; i < totalSize; i++){
						pagingListHtml += '<li><a href="#" '+  (i == 0 ? "class='on'" : ""  )  +' >'+(i+1)+'</a></li>';
					}
					
					var pagingHtml 	= '<ul class="slide_paging clear_both">'+pagingListHtml+'</ul>';
					_this.append(pagingHtml);
					var pagingDot = _this.find(".slide_paging > li");
					
					pagingDot.find("a").on("mouseenter ,  click" , function(e){
						if( e.type == settings.navEvent ){
							var currentIdx = $(this).parent().index();
							pagingDot.find("a").removeClass("on");
							$(this).addClass("on");
							
							if( currentIdx - activeIdx > 0 ){
								for( var i=0; i < (currentIdx - activeIdx); i++ ){
									moveRight();
								}
								activeIdx = currentIdx;
							} else {
								for( var i=0; i < parseInt(activeIdx - currentIdx); i++ ){
									moveLeft();
								}
								activeIdx = currentIdx;
							}
						}
						e.preventDefault();
					});
				}
				
				if( settings.link == true ){
					rollList.each(function(i){
						$(this).attr("data-idx", i);
						/* 개발 수정 */
						if(i == 0){
							$(this).find("a").addClass("on");
						}
					});
					rollList.find("a").on({
						"click" : function(e){
							activeIdx = $(this).parent("li").attr("data-idx");
							rollList.find("a").removeClass("on");
							$(this).addClass("on");
							contWrap.find(">div").hide();
							contWrap.find(">div").eq(activeIdx).show();
							e.preventDefault();
						}
					});
				}
				
				if( settings.auto == true ){
					_this.on({
						"mouseenter" : function(){
							slideStop();
						},
						"mouseleave" : function(){
							slideStart();
						}
					});
				}
				
				btnPrev.on({
					"click" : function(){
						slideStop();
						moveLeft();
						if( settings.navigate == true ){
							pageLeft();
						}
						if( settings.link == true ){
							rollList.filter(":first-child").find("a").trigger("click");
						}
					}
				});
				
				btnNext.on({
					"click" : function(){
						slideStop();
						moveRight();
						if( settings.navigate == true ){
							pageRight();
						}
						
					}
				});
				
				if( marketSlideCreateTmp[tabIdx] ){
					tabIdx = tabIdx*10;
				} 
				
				marketSlideCreateTmp[tabIdx] = function(){
					slideStop();
					marketSlideEachInterval[tabIdx] = setInterval(function(){
						moveRight();
						if( settings.link == true ){
							var target = rollList.find("a.on");
							var idx 	= target.parent().next().attr("data-idx");
							target.removeClass("on").parent().next().find("a").addClass("on");
							contWrap.find(">div").hide().eq(idx).show();
						}
						if( settings.navigate == true ){
							pageRight();
						}
					}, 3000);
				}
				
				function slideStart(){
					marketSlideCreateTmp[tabIdx]();
				}
				
				function slideStop(){
					clearInterval(marketSlideEachInterval[tabIdx]);
				}
				
				function moveRight(){
					/* 개발 수정 */
					var roll = $(".inner > ul li");
					var tmpListSize = moveListSize;
					moveListSize = roll.index(roll.find("a.on").closest('li')) + 1;
					/* 개발 수정 */
					
					rollUl.stop(false,true).animate({
						left:-(moving*moveListSize)
					}, 500, function(){
						rollUl.css({
							"left" : 0
						});
						check = 0;
						listAppend(moveListSize);
						moveListSize = tmpListSize;/* 개발 수정 */
						if( settings.link == true ){
							rollList.filter(":first-child").find("a").trigger("click");
						}
					});
				}
				
				function pageRight(){
					if( activeIdx < totalSize-1 ){
						activeIdx ++;
					} else if( activeIdx == totalSize-1){
						activeIdx = 0;
					}
					pagingDot.find("a").removeClass("on");
					pagingDot.eq(activeIdx).find("a").addClass("on");
				}
				
				function pageLeft(){
					if( activeIdx > 0 ){
						activeIdx --;
					} else if( activeIdx == 0){
						activeIdx = totalSize-1;
					}
					pagingDot.find("a").removeClass("on");
					pagingDot.eq(activeIdx).find("a").addClass("on");
				}
				
				function moveLeft(){
					rollUl.stop(false,true);
					check = 0;
					listPrepend(moveListSize)
					rollUl.css({
						"left" : -(moving*moveListSize)
					});
					rollUl.animate({
						left: 0
					}, 500);
					if( settings.link == true ){
						rollList.filter(":first-child").find("a").trigger("click");
					}
				}
				
				function listAppend(num){
					rollList.filter(":first-child").appendTo(rollUl);
					check ++;
					if( check < num ){
						listAppend(num);
					}
				}
				
				function listPrepend(num){
					rollList.filter(":last-child").prependTo(rollUl);
					check ++;
					if( check < num ){
						listPrepend(num);
					}
				}
				
				slideStart();
			});	
		}
		
		$.fn.marketNavSlide = function(options){
			var settings = {
				auto			: true,
				pagingClass 	: "slide_paging",
				imgAreaClass 	: "img",
				activeClass 	: "on",
				speed			: 3000
			}
			
			$.extend(settings, options);
			var _this 			= $(this);
			var imgList 		= _this.find("."+settings.imgAreaClass).find(">li");
			var listLengh 		= imgList.size();
			var activeIdx		= 0;
			
			var pagingListHtml 	= "";
			var tabIdx 			= _this.parents(".tab_cont").index() - 1;
			
			marketNavSlideState[tabIdx] = true;
			
			if( imgList.size() == 1 ){
				_this.find(".btn_area").hide();
				return;
			} else {
				_this.find(".btn_area").show();
			}
			
			for( var i = 0; i < listLengh; i++){
				pagingListHtml += '<li><a href="#" '+  (i == 0 ? "class='on'" : ""  )  +' >'+(i+1)+'</a></li>';
			}
			
			var pagingHtml 	= '<ul class="'+settings.pagingClass+' clear_both">'+pagingListHtml+'</ul>';
			
			_this.append(pagingHtml);
			
			var pagingDot = _this.find("."+settings.pagingClass).find("a");
			
			function pagingEvent(){
				pagingDot.on({
					"mouseenter" : function(){
						slideStart();
						activeIdx = $(this).parent().index();
						pagingDot.removeClass(settings.activeClass);
						$(this).addClass(settings.activeClass);
						imgList.filter(":visible").hide();
						imgList.eq(activeIdx).show();
					},
					"click" : function(e){
						e.preventDefault();
					}
				});
				
				if( settings.auto == true ){
					_this.on({
						"mouseenter , focusin" : function(){
							if( status ){
								autoStop();
							}
						},
						"mouseleave , focusout" : function(){
							if( status ){
								autoStrat();
							}
						}
					});
				}
				
				_this.find(".pause").on({
					"click" : function(e){
						$(this).hide();
						_this.find(".play").show();
						slideStop();
						marketNavSlideState[tabIdx] = false;
						e.preventDefault();
					}
				});
				
				_this.find(".play").on({
					"click" : function(e){
						$(this).hide();
						_this.find(".pause").show();
						marketNavSlideState[tabIdx] = true;
						slideStart();
						e.preventDefault();
					}
				});
			}
			
			
			if( marketNavSlideCreateTmp[tabIdx] ){
				tabIdx = tabIdx*10;
			} 
			
			marketNavSlideCreateTmp[tabIdx] = function() {
				clearInterval(marketNavSlideEachInterval[tabIdx]);
				marketNavSlideEachInterval[tabIdx] = setInterval(function(){
					if( activeIdx < listLengh-1 ){
						activeIdx ++;
					} else if( activeIdx == listLengh-1){
						activeIdx = 0;
					}
					pagingDot.removeClass(settings.activeClass);
					pagingDot.eq(activeIdx).addClass(settings.activeClass);
					imgList.filter(":visible").hide();
					imgList.eq(activeIdx).show();
				}, settings.speed);
			}
			
			function slideStart(){
				marketNavSlideCreateTmp[tabIdx]();
			}
			
			function slideStop(){
				clearInterval(marketNavSlideEachInterval[tabIdx]);
			}
			
			function init(){
				pagingEvent();
				slideStart();			
			}
			init();
		}
		
		var mainMarketing = function(){
			
			var target 		= $("#marketing");
			var tab 		= target.find(".tab_menu");
			var tabList 	= target.find(".tab_menu > li");
			var tab2		= target.find(".img_menu");
			var tabLength	= target.find(".tab_menu > li").size();
			var content1 	= target.find(">div");
			var tabInterval;
			var tabIdx 		= 0;
			
			// 서브 메뉴 tab
			tab2.each(function(i){
				var _this = $(this);
				var tab2Idx		=  0;
				var tab2Length 	= _this.find("ul > li").size();			
				var tabIdx 			= _this.parents(".tab_cont").index() - 1;
				
				_this.find("li > a").on({
					"click" : function(e){
						tab2Idx = $(this).parent().index();
						_this.find("li > a").removeClass("on");
						_this.next(".img_menu_cont").find(">div").hide().eq(tab2Idx).show();
						$(this).addClass("on");
						e.preventDefault();
					}
				});
				
				function tab2Init(){
					_this.find("li > a").removeClass("on");
					_this.find("li").eq(tab2Idx).find(">a").addClass("on");
					_this.next(".img_menu_cont").find(">div").hide().eq(tab2Idx).show();
				}
				tab2Init();
			});
			
			$("*[data-slide-type='slide_typeA']").marketSlide({
				mimiumSize 	: 3,
				button		: false
			});
			$("*[data-slide-type='slide_typeB']").marketSlide({
				auto		: false, 
				mimiumSize	: 1
			});
			$("*[data-slide-type='slide_typeC']").marketSlide({  //  브랜드롤링
				auto 		 	: true,
				navigate		: false,
				link			: true,
				mimiumSize 		: 1,
				move 			: 1
			});
			$("*[data-slide-type='slide_typeD']").marketSlide({
				auto		: false, 
				navigate	: false,
				mimiumSize	: 4,
				move		: 4
			});
			
			$("#mk_plan_banner").marketNavSlide();
			$("#mk_brand_banner").marketNavSlide();
			$("#mk_gift_banner").marketNavSlide();
			
			// 대메뉴 tab
			tabList.find("> a").on({
				"click" : function(e){
					tabIdx = $(this).parent().index();
					tab.find(">li > a").removeClass("on");
					$(this).addClass("on");
					content1.hide();
					$("#marketCont"+(tabIdx+1)).show();
					
					activeSlide();
					
					e.preventDefault();
				}
			});
			
			target.on({
				"mouseenter" : function(){
					allClearInterval();
				},
				"mouseleave" : function(){
					activeSlide();
				}
			});
			
			/* function randomRange(n1, n2) {
				return Math.floor( (Math.random() * (n2 - n1 + 1)) + n1 );
			} */
			
			function tabInit(){
				tabList.find("> a").removeClass("on");
				tabList.eq(tabIdx).find(">a").addClass("on");
				content1.hide();
				$("#marketCont"+(tabIdx+1)).show();
				activeSlide();
			}
			
			
			function activeSlide(){
				allClearInterval();
				if( tabIdx == 1 || tabIdx == 3 ){
					allSetInterval(tabIdx);
				}
				
				if( tabIdx == 5 ){
					if(marketNavSlideCreateTmp[tabIdx] && marketNavSlideState[tabIdx] == true){
						marketNavSlideCreateTmp[tabIdx]();
					}
				}
			}
			
			function allClearInterval(){
				$.each( marketSlideEachInterval, function(i){
					clearInterval(marketSlideEachInterval[i]);
				});
				$.each( marketNavSlideEachInterval, function(i){
					clearInterval(marketNavSlideEachInterval[i]);
				});
			}
			
			function allSetInterval(targetIdx){
				allClearInterval();
				
				if(marketSlideCreateTmp[targetIdx] ){
					marketSlideCreateTmp[targetIdx]();
				}
				
				if(marketNavSlideCreateTmp[targetIdx] && marketNavSlideState[targetIdx] == true){
					marketNavSlideCreateTmp[targetIdx]();
				}
				if( targetIdx == 3 ) {
					marketNavSlideCreateTmp[targetIdx*10]();
				}
			}
			
			tabInit();
		}
		
		// 메인 마케팅 배너
		mainMarketing();
	}
	
	if($("#event_banner_small ul li").length > 0){
		bannerslide("event_banner_small");
		$("#event_banner_small").singleNavSlide({
			auto : true,
			speed : 5000
		});
	}
	
	bannerslide("defualt_banner");
	bannerslide("category_banner");
	bannerslide("promotion2");
	//bannerslide("event_banner_small"); // 개발 적용시 주석 처리 요망.
	bannerslide("event_banner_num1");
	bannerslide("event_banner_num2");
	bannerslide("event_banner_num3");
	
	$("#event_banner_big").singleNavSlide({
		auto : true,
		speed : 4000
	});
	/* $("#event_banner_small").singleNavSlide({ // 개발 적용시 주석 처리 요망.
		auto : true,
		speed : 5000
	}); */
	$("#event_banner_num1").singleNavSlide({
		auto : true,
		speed : 5000
	});
	$("#event_banner_num2").singleNavSlide({
		auto : true,
		speed : 5000
	});
	$("#event_banner_num3").singleNavSlide({
		auto : true,
		speed : 5000
	});
	
	$("#brand_event_banner").singleNavSlide({
		auto : true,
		speed : 5000
	});
		
	$("#sky_scrapper_banner1").singleNavSlide();
	$("#sky_scrapper_banner2").singleNavSlide();
	$("#brandSlide").brandSlide();
	
	/* popup close */
	$(".pop_close").on("click",function(){
		self.close();
	});
	// 항공편 검색
	$(".airport_info a").on("click",function(){
		$("#ly_aircode").hide();
		$(".airport_info a").removeClass("on");
		$(this).addClass("on");
		$("#aircode").val($(this).text().split("(")[1].split(")")[0]).addClass("focus");
	});
	$("#aircode").on("keyup focus",function(e){
		e = e || window.event;
		var val = $.trim($(this).val().toUpperCase());
		if(e.keyCode === 40){
			if($("#ly_aircode:visible").length > 0){
				$("#ly_aircode ul a").eq(0).addClass("on").focus();
			}
		}else if(val === null || val === "" || val === undefined ){
			$("#ly_aircode").hide().find("ul").html("");					
		}else{
			$("#ly_aircode ul").html("");		
			for(var i =0;i < $(".airport_info a").length; i++){	
			 	var text = $(".airport_info a").eq(i).text();			
				if(text.indexOf(val) > -1 ){
					$("<li>").html("<a href='#url'>"+text.split(val)[0]+"<em>"+val+"</em>"+text.split(val)[1]+"</a>").appendTo("#ly_aircode ul");
					$("#ly_aircode").show();
				}
			}	
		}
	});	
	$(document).on("click keyup","#ly_aircode ul a",function(e){ 	
		e = e || window.event;
		if(e.type === "click"){
			var text = $(this).text();
			for(var i =0;i < $(".airport_info a").length; i++){	
				if($(".airport_info a").eq(i).text() === text ){
				 	$(".airport_info a").eq(i).focus().click();	
					break;
				}				
			}			
		}else if(e.keyCode === 40){
			if($("#ly_aircode ul a").index(this) < $("#ly_aircode ul a").length-1){
				$(this).removeClass("on").parents("li").next().find("a").focus().addClass("on");
			}
		}else if(e.keyCode === 38){
			if($("#ly_aircode ul a").index(this) > 0){
				$(this).removeClass("on").parents("li").prev().find("a").focus().addClass("on");
			}else{
				$(this).removeClass("on");
				$("#aircode").focus();
			}
		}		
	});	
	
	//qna table
	if($(".table_normal tr.ans:visible").length > 0){
		$(".table_normal tr.ans").hide();
	}
	$(document).on("click",".table_normal tr a",function(e){
		if($(this).parents("tr").next("tr").hasClass("ans")){
			$(this)
				.parents(".table_normal").find(".ans").hide().end().end()
				.parents("tr").next(".ans").slideDown();
		}
		eventBubblingChecked( e, $(this).attr("href") );
	});
	
	//cart gift
	gift_listradiobox();

	$(document).on("click",".cart_wrap .gift_list .css-radiobox",function(){
		$(this).parents("ul")
			.find("img").not(".ico").removeClass("on").end().end().end()
			.siblings("dl").find("img").not(".ico").addClass("on");
	});
	
	$(document).on("click",".cart_wrap .gift_list dl",function(){
		
		var inputType = $(this).prev().prev();
		
		if( inputType.hasClass("css-radiobox") ){
			$(this).parents("ul")
				.find("label").css("background-position","0px -100px").end()
				.find("img").not(".ico").removeClass("on").end().end().end()
				.siblings(".css-radiobox").attr("checked",true)
				.siblings(".css-radiolabel").css("background-position","0px -150px")
				.siblings("dl").find("img").not(".ico").addClass("on");
		} 
		else if ( inputType.hasClass("css-checkbox") ){
			if( inputType.prop("checked") == true ){
				inputType.prop("checked" , false).next().css("background-position","0px 0px").next().find(".img img").removeClass("on");
			} else {
				inputType.prop("checked" , true).next().css("background-position","0px -50px").next().find(".img img").addClass("on");
			}
		}
	});
	
	
	// table checkbox
	$(document).on("change","table .css-checkbox", function(){
		var _this = $(this);
		var allCheckBox = _this.parents("table").find("thead tr .css-checkbox");
		var eachCheckBox = _this.parents("table").find("tbody tr .css-checkbox").filter(function(){ return !$(this).parents("tr").hasClass("disable") });
		
		if( _this.parents("thead").size() > 0 ){
			if( _this.prop("checked") == true ){
				eachCheckBox.prop("checked", true).next().css("background-position","0 -50px");
				_this.next().css("background-position","0 -75px");
			} else {
				eachCheckBox.prop("checked", false).next().css("background-position","0 0");
				_this.next().css("background-position","0 0");
			}
		} else {
			if( eachCheckBox.filter(":checked").size() == eachCheckBox.size() ){
				allCheckBox.prop("checked", true).next().css("background-position","0 -75px");
			} else {
				allCheckBox.prop("checked", false).next().css("background-position","0 -20px");
			}
		}
	});
	
	if($(".cart_popup:visible").length > 0){
		$(".cart_popup .coupon_list a").on("click",function(){
			var othis = $(this);
			var couponp = Number($.trim(othis.parents("li").find("span").text().split("$")[1]));
			var couponT = Number($.trim(othis.parents("td").next("td").text().split("$")[1]));
			othis.parents("td").next("td").text("$"+(couponT - couponp)).end().end()
			.parents("li").remove();
			
		});
		$(document).on("click",".cart_popup .uescheck .css-checkbox",function(){
			var othis = $(this);
			var pointp = othis.parents(".uescheck").find("span").eq(0).text();
			if(othis.is(":checked")){
				othis.parents("td").next("td").text(pointp);
			}else{
				othis.parents("td").next("td").text("$0");
			}
		});
		$(".cart_popup .usepoint input[type='text']")
			.val(0)	
			.on("keyup",function(e){
				e = e || window.event;				
				if(e.keyCode < 48 || e.keyCode > 57 ){$(this).val("");}
				if(Number($(this).val()) > Number($(this).parents(".usepoint").find("li span").eq(0).text().split("$")[1]) || Number($(this).val()) < 0){
					alert("超过可使用的积分金额。\n请输入订购金额的30%以内的积分金额。");
					$(this).val("");
				}
				$(this).parents("td").next("td").text("$"+$(this).val());
				
		});
		$(".cart_popup .useselectpoint select").on("change",function(){
			var othis = $(this);
			var pointp = othis.val();
			if(pointp > 0){
				othis.parents("td").next("td").text("$"+pointp);
			}else{
				othis.parents("td").next("td").text("$0");
			}
		});
	}
	
	
	/* customer */
	// textArea maxLength 
	$(document).on("keyup","textarea.textcount",function(e){
		e = e || window.event;
		
		var othis = $(this);
		var count = othis.parents("tr").find("span.textcount em");
		var ls_str = othis.val();
		var li_str_len = ls_str.length; //전체길이
		var i = 0;
		var li_byte = 0;   //한글일경우 2, 그외글자는 1을 더함
		var ls_one_char = "";  //한글자씩 검사				
		var defaultByte = 1000;
		
		if( $(this).data("byte") ){
			defaultByte = Number($(this).data("byte"));
		}
		
		for(i=0; i< li_str_len; i++){
			ls_one_char = ls_str.charAt(i);   //한글자 추출
			if(escape(ls_one_char).length > 4){ 
			  li_byte ++;   //한글이면 2를 더한다
			}else{
			  li_byte++;     //한글아니면 1을 다한다
			}					
		}	
		count.text(li_byte);
		textarea_maxlength( othis, count, defaultByte);
	});

	function textarea_maxlength(obj, limitGuideTxt, maxLength){
		if( obj.val().length > maxLength ){
			var thisTxt = obj.val();
			thisTxt = thisTxt.substring(0, maxLength)
			obj.val(thisTxt);
			limitGuideTxt.text(maxLength);
		}
	}
	
	if($(".brand_wrap:visible").length > 0){		
		$(".brand_wrap .box").each(function(){
			$(this).find(".tit").css({ "margin-top" : -14 });
		});
	}
	
	/* 2015.12.30 */
	
	$(".brand_wrap .sort_cont").each(function(){
		$(this).find("a").each(function(k,v){
			var indiAlpha = $(this).text();
			var hasBrand = false;
			var sortCont = $(this).parents(".sort_cont");
			
			$("#"+sortCont.attr("id")+"_con > div").each(function(k,v){
				if(indiAlpha == $(this).find(".tit").text()){
					hasBrand = true;
				}
			})
			if(hasBrand || k == 0){
				$(this).removeClass("disabled");
			}else{
				$(this).addClass("disabled");
			}
		});
	});
	
	$(".brand_wrap .sort_cont a:not('.disabled')").on("click",function(e){
		var sortcategory = $(this).parents(".sort_cont").attr("id");
		var text = $(this).text();
		var result = false;
		
		$(this).addClass("on").siblings("a").removeClass("on").parents(".brand_wrap").find(".null").hide();
		if(text === "전체"){
			if($("#sort_list .customer .on").text() == '가나다순') {
				if($("#store_phone_hangul_con").html() == '' && $(".searchresults_list").html() == '') {
					$(".searchresults_none").remove();
					$("#detail_tab1").append('<p class="searchresults_none">검색 결과가 없습니다.</p>');
					return false;
				}
			}

			if($("#sort_list .customer .on").text() == '알파벳순') {
				if($("#store_phone_alphabet_con").html() == '' && $(".searchresults_list").html() == '') {
					$(".searchresults_none").remove();
					$("#detail_tab1").append('<p class="searchresults_none">검색 결과가 없습니다.</p>');
					return false;
				}
			}
			
			$("#"+sortcategory+"_con").find(".box").show();			
			return false;	
		}
		
		$("#"+sortcategory+"_con .box:not('.notice')").hide();
		for(var i = 0;i < $("#"+sortcategory+"_con").find(".tit").length;i++){	
			if($("#"+sortcategory+"_con").find(".tit").eq(i).text() === text){
				$("#"+sortcategory+"_con").find(".box").eq(i).show();
				result = true;
			}
		}
		if(!result){$(this).parents(".brand_wrap").find(".null").show();}
		
		$(this).parents(".brand_wrap").find(".search_result").hide().empty();
		$(this).parents(".brand_wrap").find(".null").hide();
		e.preventDefault();
	});
	/* end : 2015.12.30 */
	
	// exhibition banner
	if($(".visual_lnb:visible").length > 0){
		promo_rolling();
	}
	
	//marketing gift top banner
	var list_rolling_start = null;
	var list_rolling = false;		
	if($("#visual_gift:visible").length > 0){	
		var visual_gift = $("#visual_gift");
		visual_gift.on("mouseleave",function(){
			list_rolling_start = setTimeout(function(){list_rolling_fx();},2000);
		}).find(".product_list ").on("mouseover mouseleave",function(e){
			e = e||window.event; 
			if(e.type === "mouseover"){			
				clearTimeout(list_rolling_start);			
			}else{
				list_rolling_start = setTimeout(function(){list_rolling_fx();},2000);
			}	
		}).end().find(".banner_nav > a").on("click",function(){
			if($(this).hasClass("play")){
				$(this).removeClass("play").addClass("pause");
				clearTimeout(visual_slide);	
				clearTimeout(list_rolling_start);	
			}else{
				$(this).removeClass("pause").addClass("play");	
				visual_slide = setTimeout(function(){visual_lnbslide();},2000);
			}
		}).end().find(".list_rolling a").on("click",function(){
			if(list_rolling){return false;}
			list_rolling = true;
			var list = visual_gift.find(".product_list").children("ul:visible");
			if($(this).hasClass("prev")){
				list.css("left","-108px").children("li").eq(3).removeClass("on").end().end()
				.prepend(list.children("li").last().clone().removeClass("on"))
				.animate({"left":124},{duration:300,complete:function(){
					list.children("li").eq(1).addClass("on").end().eq(3).nextAll().removeClass("on").end().end().last().remove();
					list_rolling = false;
				}});
			}else{
				list.children("li").eq(1).removeClass("on").end().end()
				.append(list.children("li").first().clone().removeClass("on"))
				.animate({"left":"-108px"},{duration:300,complete:function(){
					list.css("left","124px").children("li").eq(1).removeClass("on").end().eq(4).addClass("on").nextAll().removeClass("on").end().end().first().remove();
					list_rolling = false;
				}});
			}
		}).end().find(".slide_menu").find("a").on("click",function(){
			var slideId = $(this).attr("href");
			var index = visual_gift.find(".slide_menu a").index(this);
			$(this).parents("li").addClass("on").siblings("li").removeClass("on");
			$("#visual_gift_indicator").find("a").removeClass("on").eq(index).addClass("on");
			visual_gift.find(".tit_rolling").find("li").removeClass("on").eq(index).addClass("on");
			visual_gift.find(".product_list ul.list").hide();
			$(slideId)
			.css("width",($(slideId).children("li").length*232))			
			.show().children("li").eq(1).addClass("on")
			.next("li").addClass("on").next("li").addClass("on");			
			list_rolling_start = setTimeout(function(){list_rolling_fx();},3000);			
			return false;
		}).eq(0).click();
		$("#visual_gift_indicator a").on("click",function(){
			visual_gift.find(".slide_menu").find("a").eq($("#visual_gift_indicator a").index(this)).click();
			return false;
		});		
	}
	var list_rolling_category = 1;
	var list_rolling_count = 0;
	function list_rolling_fx(){
		clearTimeout(list_rolling_start);	
		list_rolling_count++;			
		if($("#visual_gift"+list_rolling_category).children("li").length-2 <= list_rolling_count ){
			list_rolling_count = 0;			
			if(list_rolling_category === 4){list_rolling_category = 0;}
			$("#visual_gift .slide_menu").find("a").eq(list_rolling_category).click();
			list_rolling_category++;
		}else{
			$("#visual_gift .list_rolling a.next").click();	
		}
		list_rolling_start = setTimeout(function(){list_rolling_fx();},2000);
	}
	// marketing kore banner
	var marketing_korea = null;
	var mkc = 0;
	var mkc2 = 0;
	if($(".marketing_korea:visible").length > 0){
		var firstobj = $(".marketing_korea").find(".visual_contents").eq(mkc2);
		marketing_korea = setTimeout(function(){marketing_korea_fx(firstobj);},3000);	
		$(".marketing_korea").on("mouseover mouseleave",function(e){
			e = e||window.event; 
			if(e.type === "mouseover"){			
				clearTimeout(marketing_korea);		
			}else{
				marketing_korea = setTimeout(function(){marketing_korea_fx(firstobj);},2000);
			}	
		}).find(".menu a").on("click",function(){
			mkc2 = $(".marketing_korea .menu a").index(this);
			clearTimeout(marketing_korea);
			marketing_korea_fx2(mkc2);
			var obj = $(".marketing_korea").find(".visual_contents").eq(mkc2);
			marketing_korea = setTimeout(function(){marketing_korea_fx(obj);},2000);
		}).end().find(".visual_contents .btn_area a").on("click",function(){	
			var obj = $(".marketing_korea").find(".visual_contents").eq(mkc2);				
			if($(this).hasClass("prev")){
				mkc--;
				marketing_korea_fx3(obj,mkc);
			}else if($(this).hasClass("next")){
				mkc++;
				marketing_korea_fx3(obj,mkc);
			}else{
				var num = obj.find(".banner_nav").find("a").index(this);
				marketing_korea_fx3(obj,num);
			}
			$(".marketing_korea .visual_detail ul.img").children("li").eq(mkc2);						
		});
	}
	function marketing_korea_fx(obj){
		clearTimeout(marketing_korea);
		mkc++;
		if(mkc > obj.find(".contents_slider").children("li").length-1){
			mkc = 0;
			mkc2++;
			if(mkc2 >  $(".marketing_korea").find(".visual_contents").length - 1){mkc2 = 0;}
			obj = $(".marketing_korea").find(".visual_contents").eq(mkc2);
			marketing_korea_fx2(mkc2);	
		}
		marketing_korea_fx3(obj,mkc);	
		marketing_korea = setTimeout(function(){marketing_korea_fx(obj);},2000);	
	}
	function marketing_korea_fx2(num){
		$(".marketing_korea").find(".menu").find("li").eq(num).addClass("on").siblings("li").removeClass("on").end().end().end().end()
		.find(".visual_detail ul.img").children("li").eq(num).addClass("on").siblings("li").removeClass("on");	
	}
	function marketing_korea_fx3(obj,num){
		mkc = num;
		if(mkc > obj.find(".contents_slider").children("li").length-1){
			mkc = 0;
		}else if(mkc < 0){
			mkc = obj.find(".contents_slider").children("li").length-1;
		}
		obj.find(".contents_slider").children("li").eq(mkc).addClass("on").siblings("li").removeClass("on").end().end().end().end().find(".banner_nav").find("a").removeClass("on").eq(mkc).addClass("on");		
	}
	
	/* singup  */
	//agree_check
	$(".agree_check").on("click",function(){
		$(this).css("background","url('"+path+"../img/common/ico_check_green.png') no-repeat 1px 0").parents(".agree_info").find(".css-checkbox").each(function(){
			if($(this).is(":checked")){return false;}			
			$(this)	.click();			
		});
		return false;
	});
	
	$(".agree_info .css-checkbox").on("click",function(){		
		var checkedall = true;
		$(this).parents("ul").find(".css-checkbox").each(function(){
			if(!$(this).is(":checked")){
				checkedall = false;
			}
		});
		if(checkedall){
			$(".agree_check").css("background","url('"+path+"../img/common/ico_check_green.png') no-repeat 1px 0");
		}else{
			$(".agree_check").css("background","url('"+path+"../img/sub/sp_signup.png') no-repeat -374px -168px");
		}		
	});
	
	if($(".cs_cont .faq dl:visible").length > 0){
		$(document).on("click",".cs_cont .faq dl dt a",function(){
			if($(this).hasClass("on")){
				$(this).removeClass("on").parents("dt").next("dd").hide();
			}else{
				$(this).addClass("on").parents("dt").next("dd").show().siblings("dd").hide().end().end().siblings("dt").find("a").removeClass("on");
			}
			return false;
		});				
	}
	
	$(document).on("click",".btn_popup_layer",function(){
		var targetO = $(this).attr("href").split("#")[1];
		layerpopup(targetO);
		return false;
	});
	
	$(document).on("click",".btn_findair",function(){
		if($(this).parents("table").find(".depart_place").find("option:selected").text().indexOf("공항") > -1 ){
			layerpopup("popup_notice2");
		}else{
			layerpopup("popup_notice3");	
		}		
		return false;
	});	

	if($(".depart_place").length > 0){
		$(".depart_place").on("change" , function(){
			depart_place($(this));
		});
	}
	
	$("#place01").on("change",function(){
		var num = $("#place01").val();
		place(num);
	});
	
	$(".view_type a").on("click",function(){
		var othis = $(this);
		if(othis.parents("li").hasClass("on")){return false;}
		othis.addClass("on").parents("li").siblings("li").find("a").removeClass("on");
		if(othis.parents("li").hasClass("img")){
			othis.parents(".product_list_wrap").find(".product_list").hide().siblings(".view_img").show();
		}else{
			othis.parents(".product_list_wrap").find(".product_list").hide().siblings(".view_list").show();
		}
		return false;
	});	
	
	$(".accordion_menu dt a").on("click",function(){
		if($(this).parents("dt").hasClass("on")){
			$(this).parents("dt").removeClass("on");
		}else{
			$(this).parents("dt").addClass("on");
		}
		return false;
	});
	
	$(".toggle_list dt a").on("click",function(){
		if($(this).hasClass("on")){
			$(this).removeClass("on").parents("dt").removeClass("on");
		}else{
			$(this).addClass("on").parents("dt").addClass("on");
		}
		return false;
	});	
	
	$(".csform_type1").on("change",function(){
		var text = $(this).find("option:selected").text();
		var data = new Array();
		if(text === "회원"){
			data = ["개인 정보","출국 정보","여권 정보","멤버스 정보"];		
			$(".csform_type2").html("<option value='' selected='selected'>문의유형2 선택</option>"+selecthtml(data)).removeAttr("disabled");			
		}else if(text === "상품"){
			data = ["상품 문의","스페셜오더","주문/결제","환불/취소"];		
			$(".csform_type2").html("<option value='' selected='selected'>문의유형2 선택</option>"+selecthtml(data)).removeAttr("disabled");
		}else if(text === "서비스"){
			data = ["사은품/사은권","교환권/쿠폰/별","이벤트/웨딩샵","적립금/OK캐시백","이용 문의","기타"];		
			$(".csform_type2").html("<option value='' selected='selected'>문의유형2 선택</option>"+selecthtml(data)).removeAttr("disabled");
		}else{
			$(".csform_type2").html("<option value='' selected='selected'>문의유형2 선택</option>").attr("disabled","disabled");
			$(".csform_type3").html("<option value='' selected='selected'>문의유형3 선택</option>").attr("disabled","disabled");
		}
	});
	
	$(document).on("change",".csform_type2",function(){
		var text = $(this).find("option:selected").text();
		var data = new Array();
		switch(text){
			case "개인 정보":
				data = ["ID/PW","주민번호/아이핀","재외국인","회원탈퇴"];
			break;
			case "출국 정보":
				data = ["출국 일시","비행기 편명","출국 장소","도착 장소"];
			break;
			case "여권 정보":
				data = ["여권 이름","여권 번호"];
			break;
			case "멤버스 정보":
				data = ["맴버스 카드 발급/재발급","맴버스 등급","맴벗 혜택","가족 맴버스 신청"];
			break;
			case "상품 문의":
				data = ["상품 재고","상품 재입고","상품 상태","상품 브랜드","상품 교환","상품 AS","상품 구매"];
			break;
			case "스페셜오더":
				data = ["스페셜오더 상품","스페셜오더 브랜드","스페셜오더 신청"];
			break;
			case "주문/결제":
				data = ["주문","주문 시간 확인","무통장/계좌이체 결제","신용카드 결제","휴대폰 결제"];
			break;
			case "환불/취소":
				data = ["출국 취소","상품 미수령","재구매로 인한 취소","무통장/계좌이체 환불","주문 취소 해지","휴대폰 결제 취소/환불"];
			break;
			case "사은품/사은권":
				data = ["사은품 문의","사은권 문의"];
			break;
			case "교환권/쿠폰/별":
				data = ["교화권 다시 출력","쿠폰 사용","별 적립","별 사용"];
			break;
			case "이벤트/웨딩샵":
				data = ["이벤트 문의","이벤트 당첨자 발표","카드사 제휴 이벤트","웨딩샵 문의","웨딩샵 청첩장 등록"];
			break;
			case "적립금/OK캐시백":
				data = ["적립금 사용","적립금 소멸","상품권 전환 적립금","OK캐쉬백 적립","OK캐쉬백 사용"];
			break;
			case "이용 문의":
				data = ["면세점 이용/쇼핑 문의","상품 인도장 문의"];
			break;
		}
		$(".csform_type3").html("<option value='' selected='selected'>문의유형3 선택</option>"+selecthtml(data)).removeAttr("disabled");
		if(text === "기타"){
			$(".csform_type3").html("<option value='' selected='selected'>문의유형3 선택</option>").attr("disabled","disabled");	
		}
	});
	
	$(document).on("click",".btn_add_product_form",function(){
		var form = 	$(this).parents(".product_form").clone();
		var num = $(this).parents(".product_form").index();

		form.find("input[type='text']").val("")
			.end().find("textarea").val("");

		form.addClass("product_form_add").find("label").each(function(){
			var oid = $(this).attr("for");
			$(this).attr("for",oid+num);
			$("#"+oid).attr("id",oid+num);
		});
		form.insertAfter($(".product_form").last());
		inputNumberCheck();
		return false;
	});
	
	$(".tooltip_m").on("mouseover mouseleave",function(e){
		e = e || window.event;
		if(e.type === "mouseover"){
			$(this)	.next(".tooltip_layer").fadeIn();
		}else{
			$(this)	.next(".tooltip_layer").fadeOut();
		}
	});
	
	$(".mypage_wrap .sort_cont a").on("click",function(){
		var sortcategory 	= $(this).parents(".sort_cont").attr("id");
		var noResult 		= $(this).parents(".mypage_wrap").find(".searchresults_none");
		var alphabetBox 	= $("#"+sortcategory+"_con").find(".alphabet_box");
		var text = $(this).text();
		var result = false;
		
		$(this).addClass("on").siblings("a").removeClass("on").parents(".mypage_wrap").find(".searchresults_none").hide();
		if(text === "전체"){
			alphabetBox.show();
			if( $("#"+sortcategory+"_con").find(".alphabet_box").length ){
				noResult.hide();
			} else {
				noResult.show();
			}
			return false;	
		}		
		alphabetBox.hide();
		for(var i = 0;i < $("#"+sortcategory+"_con").find("strong").length;i++){	
			if($("#"+sortcategory+"_con").find("strong").eq(i).text() === text){
				alphabetBox.eq(i).show();
				result = true;
			}
		}
		
		if(!result){
			noResult.show();
		}
		return false;
	});
	
	/* 
	// 3hourshop
	$(".marketing_3hour .btn_m_gray").on("click",function(){
		if($(this).parents(".order_choice").find(".choice:visible").length > 0){
			if(	($("div.choice > select.select_spot option:selected").text() === "출국장소") ||
				($("p.choice > select.select_spot option:selected").text() === "브랜드 구분") ||
				(! $(".order_choice .passport_date .datepicker").val()) ||
				($(".marketing_3hour .order_choice .time select").eq(0).find("option:selected").text() === "시") ||
				($(".marketing_3hour .order_choice .time select").eq(1).find("option:selected").text() === "분")){return false;}
			$(this).parents(".choice").hide().siblings(".result").show();
		}else{
			$(".marketing_3hour").find(".datepicker").val("출국일").end().find("select.select_spot option:eq(0)").prop("selected",true).end().find(".order_choice .time select").eq(0).find("option:eq(0)").prop("selected",true);
			$(".choice .select_spot ").find("option:eq(0)").prop("selected",true);
			$(this).parents(".result").hide().siblings(".choice").show();
		}
		return false;
	}); */

	
	/* member */
	$(".member_contents .nationals #certification1, .member_contents .nationals #a1").on({
		"change" : function(){
			if( $(this).prop("checked") == true ){
				$(".nationals .selfCertification").show();
				$(".nationals .member01").css("display" , "block");
				$(".nationals .member02").css("display" , "none");
				$(".nationals .email_area").hide();
				$(".nationals .certification").show();
			}		
		}
	});
	
	$(".member_contents .nationals #certification2 , .member_contents .nationals #a2").on({
		"change" : function(){
			if( $(this).prop("checked") == true ){
				$(".nationals .selfCertification").hide();
				$(".nationals .member01").css("display" , "none");
				$(".nationals .member02").css("display" , "block");
				$(".nationals .email_area").show();
				$(".nationals .certification").hide();
			}		
		}
	});
	/* end : member */
	

	/******** my page ********/

	// 쿠폰 등록 레이어
	$(".btn_coupon_save_regi").on({
		"click" : function(e){
			layerpopup("coupon_save_regi", false)
			e.preventDefault();
		}
	});

	/* //멤버십 등급 자세히 보기 새창 팝업
	$(".btn_member_class").on("click",function(){
		window.open("../cart/cart_popup_membership.html",'','width=590,height=642,toolbar=no,left=300,top=200');
		return false;
	}); */
	
	// 테이블 아코디언
	$("table[data-type='table-accodian']").each(function(){
	   	var options = $(this).data();
		var eventTarget = options.eventTarget;
		var actionTarget = options.actionTarget;
		
		$(eventTarget).find("a").on({
			"click" : function(e){
				
				// 링크가 2개 이상일 경우
				if( $(this).parents("tr").find("a").size() > 1 ){
					if( $(this).hasClass("on") ){
						$(this).parents("tr").find("a").removeClass("on");
						$(this).parents("tr").nextUntil("tr:not('"+actionTarget+"')").hide();
					} else {
						$(eventTarget).find("a").removeClass("on");
						$(this).parents("tr").find("a").addClass("on");
						$(actionTarget).hide();
						$(this).parents("tr").nextUntil("tr:not('"+actionTarget+"')").show();
					}
				}
				// 링크가 1개일 경우
				else {
					if( $(this).hasClass("on") ){
						$(this).removeClass("on");
						$(this).parents("tr").nextUntil("tr:not('"+actionTarget+"')").hide();
					} else {
						$(eventTarget).find("a").removeClass("on");
						$(this).addClass("on");

						$(actionTarget).hide();
						$(this).parents("tr").nextUntil("tr:not('"+actionTarget+"')").show();
					}
				}
				e.preventDefault();
			}
		});
	});

	//여권 정보 입력 안내 레이어
	$(".btn_passport_info_guide").on({
		"click" : function(e){
			layerpopup("passport_info_guide");
			e.preventDefault();
		}
	});
	
	
	//친구의 관심 상품 리스트
	var popupFriendInterest = $("#popup_friend_interst_list");
	popupFriendInterest.find(".chk_list input[type='radio']").on({
		"change" : function(){
			if( popupFriendInterest.find(".chk_list input[type='radio']").filter(":checked").parents("li").index() == 0 ){
				popupFriendInterest.find(".input_id").show();
				popupFriendInterest.find(".input_sns").hide();
			} else {
				popupFriendInterest.find(".input_id").hide();
				popupFriendInterest.find(".input_sns").show();
			}
		}
	});
	
	

	// tooltip
	tooltipEvent();
	

	var interestedBrandId 	= [];
	var interestedBrandOptionSize;
	
	//관심 브랜드 선택
	$("*[data-select]").each(function(i){
		var _this 				= $(this);
		var selectEventTarget 	= _this.attr("data-select");
		var selectAppendTarget  = $("*[data-append-target='"+selectEventTarget+"']");
		interestedBrandOptionSize 	= selectAppendTarget.children().size();
		interestedBrandId[i]	= [];
		
		_this.on({
			"change" : function() {
				var brandTxt = _this.find("option:selected").text();
				var brandVal = _this.find("option:selected").val();
				
				if ( _this.find("option:selected").index() != 0  && $.inArray( brandVal, interestedBrandId[i] ) == -1 && interestedBrandOptionSize < 3  ) {
					interestedBrandId[i].push(brandVal);
					selectAppendTarget.append("<span>" + brandTxt + " <a href='#'>삭제</a><input type='hidden' value='" + brandVal + "'/></span>");
					interestedBrandOptionSize++;

					selectAppendTarget.find("a").off("click").on({
						"click": function (e) {
							remove($(this));
							e.preventDefault();
						}
					});
				}
			}
		});

		selectAppendTarget.find("a").on({
			"click" : function(e){
				remove($(this));
				e.preventDefault();
			}
		});

		function remove(target){
			var selectVal = target.next().val();
			interestedBrandId[i][$.inArray(selectVal,interestedBrandId[i])] = null;
			target.parent("span").remove();
			interestedBrandOptionSize --;
		}
	});
	
	// 관심 카테고리 선택
	$(".interested_category").each(function(){
		var _this = $(this);
		
		_this.find("input[type='checkbox']").on({
			"change" : function(){
				var checkedLength = _this.find("input[type='checkbox']:checked").size();
				
				if( checkedLength > 2 ){
					_this.find("input[type='checkbox']").not(":checked").prop("disabled" , true);
				} else {
					_this.find("input[type='checkbox']").not(":checked").prop("disabled" , false);
				}
				inputFormLableChange();
			}
		})
	});

	$(".btn_title_modify").on({
		"click" : function(e){
			var titModityArea = $(".title_modify").find(">div").eq(1);
			titModityArea.show();
			e.preventDefault();
		}
	});
	
	$(".btn_title_confirm").on({
		 "click" : function(e){
			var titArea 		= $(".title_modify").find(">div").eq(0); 
			var titModityArea 	= $(".title_modify").find(">div").eq(1); 
			var title = titModityArea.find("input[type='text']").val();
			
			if( title ){
				titArea.find(".tit").text(title);
				titModityArea.hide();
			} else {
				
			}
			e.preventDefault();
		 }
	});
	
	// 주문 취소 레이어
	$(".btn_order_cancel").on({
		"click" : function(e){
			layerpopup("order_cancel")
			e.preventDefault();
		}
	});
	
	// 이메일 공유 레이어
	$(".btn_interes_share_email").on({
		"click" : function(e){
			layerpopup("interes_share_email");
			e.preventDefault();
		}
	});
	
	
	// 약관 동의 전체 체크
	var agreeCheckbox 		= $(".terms").find("input[type='checkbox']").filter("[id^=agree]");
	var agreeCheckboxLength = agreeCheckbox.size();
	$("#all_agree").on({
		"change" : function(){
			if( $(this).prop("checked") == true ){
				agreeCheckbox.prop("checked" , true).next().css("background-position", "0 -50px");
			} else {
				agreeCheckbox.prop("checked" , false).next().css("background-position", "0 0");
			}
		}
	});
		
	agreeCheckbox.on({
		"change" : function(){
			if( agreeCheckbox.filter(":checked").size() == agreeCheckboxLength ){
				$("#all_agree").prop("checked" , true).next().css("background-position", "0 -50px");
			} 
			else {
				$("#all_agree").prop("checked" , false).next().css("background-position", "0 0");
			}
		}
	});
	
	$(".my_inte_event").find("[id^='detail_tab'].area").each(function(){
		var _this 			= $(this);
		var allCheckbox 	= _this.find(".chk_area input[type='checkbox']");
		var eachCheckbox 	= _this.find("ul li input[type='checkbox']");
		
		allCheckbox.on({
			"change" : function(){
				if( $(this).prop("checked") == true ){
					eachCheckbox.prop("checked" , true).next().css({ "background-position" : "0 -50px" }); 
				} else {
					eachCheckbox.prop("checked" , false).next().css({ "background-position" : "0 0" });
				}
			}
		});
		
		eachCheckbox.on({
			"change" : function(){
				if( eachCheckbox.filter(":checked").size() == eachCheckbox.size() ){
					allCheckbox.prop("checked" , true).next().css({ "background-position" : "0 -75px" }); 
				} else {
					allCheckbox.prop("checked" , false).next().css({ "background-position" : "0 -20px" });
				}
			}
		});
	});
	
	
	// 수신, 비수신	
	$("[data-radio-receive]").each(function(){
		var _this = $(this);
		var target = _this.data("radioReceive");
		_this.on({
			"change" : function(){
				$("[data-receive-target='"+ target +"']").each(function(){
					$(this).find("input[type='checkbox']").prop({ "disabled" : false });
				});
				inputFormLableChange();
			}
		});
	});
	
	$("[data-radio-noreceive]").each(function(){
		var _this = $(this);
		var target = _this.data("radioNoreceive");
		_this.on({
			"change" : function(){
				$("[data-receive-target='"+ target +"']").each(function(){
					$(this).find("input[type='checkbox']").prop({ "disabled" : true, "checked" : false });
				});
				inputFormLableChange();
			}
		});
	});


	
	var myRegularMember		= $(".my_regular_member");	
	myRegularMember.find(".agree_area input[type='checkbox']").on({
		"change" : function(){
			if( $(this).prop("checked") == true ){
				$(".disable").hide();
			} else {
				addInfoReset();
				$(".disable").show();
			}
		}
	});
	
	function addInfoReset(){
		var target = myRegularMember.find(".add_information");
		target.find("input[type='checkbox']").prop({ "checked" : false, "diabled" : false, "readonly" : false }).next().css("background-position", "0 0");
		target.find("input[type='radio']").prop({ "checked" : false, "diabled" : false, "readonly" : false }).next().css("background-position", "0 -100px");
		target.find("input[type='text']").val("");
		target.find("select").find("option:first-child").prop("selected" , true);
		
		target.find("[data-append-target='interested-brand']").empty();
		
		interestedBrandOptionSize = 0;
		$("*[data-select]").each(function(i){
			interestedBrandId[i] = [];
		});
		
	}
	
	/******** end : my page ********/


	/* member */
	// country phone select
	$(".tel_box").each(function(){
		var _this 	= $(this);
		var  select = _this.find(">select");
		
		select.on({
			"change" : function(){
				var dataId = $(this).find("option:selected").data("id");
				_this.find("*[data-target]").hide();
				_this.find("*[data-target*='"+ dataId +"']").show();
			}
		});
	});
	
	/* end : member */
	
	tableScroll();
	
	$(document).on("click", ".btn_compare", function(e){
		if(isLogin == "true"){
			if($(this).hasClass("on")){
				$(this).removeClass("on btn_red_s").addClass("btn_brown_s").text("상품비교").parents(".product_list_wrap").find(".product_list .list").removeClass("compare");
			}else{
				$(this).removeClass("btn_brown_s").addClass("on btn_red_s").text("상품비교 끄기").parents(".product_list_wrap").find(".product_list .list").addClass("compare");
				//comparecheck();
			}
		} else {
			if(confirm("상품비교기능은 로그인 하셔야 이용하실 수 있습니다. 로그인 하시겠습니까?")){
				loginPopup(contextPath+'/login');
			}
		}
		e.preventDefault();
	});
	
	$(document).on("click", ".css-checkbox-compare", function(){
		if(typeof(product_compare) != 'undefined'){ // 개발 체크 순서 소팅 추가
			product_compare.setData($(this));
		}
		comparecheck($(this));
	});

	$(document).on("click", ".btn_com", function(e){
		//compare_set();
		tableScroll();
		if(typeof(product_compare) != 'undefined'){ 
			product_compare.init(); 
		}
		layerpopup("compare_popup_layer");
		e.preventDefault();
	});

	// satisfaction
	var chkList = $(".search_survey .survey_box .chk_list");
	var chkListli = chkList.find("li");

	chkListli.on("click", function() {
		chkListli.removeClass("on").find("input:radio").prop("checked", true).next().css("background-position", "0px -100px");
		$(this).addClass("on").find("input:radio").prop("checked", true).next().css("background-position", "0px -150px");
	});
	
	// sub brand trigger
	var subBrandGift = $(".sub_brand3");
	subBrandGift.find(".tab_menu a").on({
		"click" : function(e){
			var _this 		= $(this);
			var targetId	= _this.attr("href");
			var searchInupt = $("#gift_search");
			
			if( searchInupt.val() && searchInupt.val() != searchInupt.attr("placeholder") ){
				subBrandGift.find(".search .btn_a_white").trigger("click");
			} else {
				subBrandGift.find(targetId).find(".total").trigger("click");
			}
			e.preventDefault();
		}
	});

	
	/* skyscrapper 2017-06-09 추가 */
	$(".sky_scrapper_new .btn_view1").on("click",function(){
		layerpopup2("pop_orderTime");
		$(".order_possible .choice").show();
		$(".order_possible .result").hide();
		return false;
	});
	$(".order_possible .choice .btn_red_s").on("click",function(){
		$(".order_possible .choice").hide();
		$(".order_possible .result").show();
	});
	$(".order_possible .result .btn_red_s").on("click",function(){
		$(".order_possible .choice").show();
		$(".order_possible .result").hide();
	});
	$(".order_possible .result .btn_gray_s").on("click",function(){
		$("#pop_orderTime").hide();
	});

	$(".sky_scrapper_new .btn_view2").on("click",function(){
		layerpopup2("pop_dutyFreeGuide");
		return false;
	});
	todayProduct($(".recommend_list .list_view"), $(".recommend_list .list_view .next"), $(".recommend_list .list_view .prev"),2); //추천상품
	todayProduct($(".recently_list .list_view"), $(".recently_list .list_view .next"), $(".recently_list .list_view .prev"),2); //최근본상품

	$(".sky_scrapper_new .btn_top a").on("click",function(){
		$("html,body").animate({scrollTop:0},100);
		return false;
	});
	/* //2017-06-09 수정*/

});

function table_scroll_width(){
	var taableInner = $(".table_scroll_inner");
	taableInner.width(taableInner.find(".table_tbody").length * taableInner.find(".table_tbody").outerWidth());	
}

function comparecheck(obj){
	var compareCheckbox = $(".product_list").find(".css-checkbox-compare");
	if(compareCheckbox.filter(":checked").length > 1 && compareCheckbox.filter(":checked").length < 11  ){
		compareCheckbox.parent().siblings(".btn_com").css({ "display" : "none" }).end().end().end().find(".css-checkbox-compare:checked").parent().siblings(".btn_com").css({ "display" : "block" });		
	} else if( compareCheckbox.filter(":checked").length == 11 ){
		obj.prop("checked", false).next().css({"background-position" : "0 -880px"});	
		alert("최대 10개의 상품까지 비교 가능합니다.");
	}
	else{
		compareCheckbox.parent().siblings(".btn_com").css({ "display" : "none" });
	}
}

function selecthtml(data){
	var select_html = "";
	for(var i in data){
		select_html = select_html+"<option value=''>"+data[i]+"</option>";
	}
	return select_html;
}

function checkboxChecked(){
	$(".css-checkbox:checked + label").css("background-position", "0 -50px");
}

function inputFormLableChange(){
	$("input[type='radio']").each(function(){ //개발 적용시 주석 처리 요망.
		var _this = $(this);
		if( _this.is(":checked") ){
			_this.next().css("background-position","0 -150px").removeClass("lable_disabled");
		} else if( _this.is(":disabled") ){
			_this.next().addClass("lable_disabled");
		} else {
			_this.next().css("background-position","0 -100px").removeClass("lable_disabled");
		}
	});
	
	$("input[type='checkbox']").each(function(){
		var _this = $(this);
		if( _this.is(":checked") ){
			_this.next("background-position","0 -50px").removeClass("lable_disabled");
		} else if( _this.is(":disabled") ){
			_this.next().addClass("lable_disabled");
		} else {
			_this.next().css("background-position","0 0px").removeClass("lable_disabled");
		}
	});
}

function labelChecked(){
	$(".css-label").each(function(){
		var bp = 0;
		var othis = $(this);
		bp = othis.prev(".css-checkbox").is(":checked") ? -50 : 0;
		
		if( othis.parents("thead").size() > 0 || othis.parent(".chk_area").size() > 0 || othis.parents(".tit_top").size() > 0 ){
			bp = othis.prev(".css-checkbox").is(":checked") ? -75 : -20;
		}
		
		if(othis.attr("class").indexOf("css-label-") > -1){			
			var color = othis.attr("class").split("css-label-")[1];
			switch(color) {
				case "red":
					bp = othis.prev(".css-checkbox").is(":checked") ? -325 : -300;
				break;
				case "orange":
					bp = othis.prev(".css-checkbox").is(":checked") ? -375 : -350;
				break;		
				case "yellow":
					bp = othis.prev(".css-checkbox").is(":checked") ? -425 : -400;
				break;		
				case "lemon":
					bp = othis.prev(".css-checkbox").is(":checked") ? -475 : -450;
				break;		
				case "lime":
					bp = othis.prev(".css-checkbox").is(":checked") ? -525 : -500;
				break;		
				case "green":
					bp = othis.prev(".css-checkbox").is(":checked") ? -575 : -550;
				break;		
				case "skyblue":
					bp = othis.prev(".css-checkbox").is(":checked") ? -625 : -600;
				break;		
				case "blue":
					bp = othis.prev(".css-checkbox").is(":checked") ? -675 : -650;
				break;		
				case "darkblue":
					bp = othis.prev(".css-checkbox").is(":checked") ? -725 : -700;
				break;		
				case "white":
					bp = othis.prev(".css-checkbox").is(":checked") ? -775 : -750;
				break;		
				case "gray":
					bp = othis.prev(".css-checkbox").is(":checked") ? -825 : -800;
				break;	
				case "disable":
					bp = othis.prev(".css-checkbox").is(":checked")  ? -850 : 0;
				break;	
				case "all":
					bp = othis.prev(".css-checkbox").is(":checked")  ? -75 : -20;
				break;
				case "compare":
					bp = othis.prev(".css-checkbox").is(":checked")  ? -920 : -880;
				break;
			}
		}
		if(othis.prev(".css-checkbox").hasClass("css-checkbox2")){
			bp = othis.prev(".css-checkbox").is(":checked")  ? -250 : -200;
		}
		othis.css("background-position", "0 "+bp+"px");
	});	
}

function radioChecked(){
	$(".css-radiobox:checked + label").css("background-position", "0 -150px");
}

function viewCart(){
	document.location = "/estore/kr/ko/cart";
}

function gift_listradiobox(){
	$(".cart_wrap .gift_list .css-radiobox").each(function(){
		if($(this).is(":checked")){
			$(this)	.siblings(".css-radiolabel")
				.css("background-position","0px -150px")
				.siblings("dl").find("img").not(".ico").addClass("on");
		}
	});
	
	$(".cart_wrap .gift_list .css-checkbox").each(function(){
		$(this).on({
			"change" : function(){
				if( $(this).prop("checked") == true ){
					$(this).nextAll("dl").find("img").addClass("on");
				} else {
					$(this).nextAll("dl").find("img").removeClass("on");
				}
			}
			
		})
	});
}

function layerpopup(layer, pos){
	var popup = $("#"+layer),
		margintop = ($(window).height() - popup.height())/2,
		left = (931-popup.width())/2;
	if($(window).height() <= popup.height()){margintop = 0;}		
	var top = $(document).scrollTop() - 170 + margintop;	
	if(popup.parents("#container").length < 1){
		left = ($(window).width()-popup.width())/2;
		top = $(document).scrollTop() + margintop;

		if(layer == 'popup_email'){ //개발수정
			left = ($(window).width())/2; 
			top = $(document).scrollTop() + 120 + margintop;
		}
	}	
	if(left < 0){left = 0;}

	popup.show().find(".btn_close").on("click",function(e){
		if( $(this).data("prevent") != "on" ){
			popup.hide();
		}
		e.preventDefault();
	});

	if( pos == false ){
	} else {
		popup.css({"position":"absolute","left":left,"top":top});
	}
}

/* 2016-09-06 레이어 팝업 추가 */
function layerpopup2(layer, pos){
	var popup = $("#"+layer);

	$(".float_layer").hide();
	popup.show().find(".layer_close").on("click",function(e){
		if( $(this).data("prevent") != "on" ){
			popup.hide();
		}
		e.preventDefault();
	});
}

function cartPaymentInfoViaInit(){
	var val =  $(".cart_payment").find(".depart_place").val();
	var viaArea = $(".cart_payment").find(".direct");
	
	if( val == "P" || val == "U" || val == "I" ){
		viaArea.find(".chk_list > li:eq(0) input[type='radio']").prop({ "checked" : true , "disabled" : true }).next().css("background-position" , "0px -150px");
		viaArea.find(".chk_list > li:eq(1) input[type='radio']").prop({ "checked" : false , "disabled" : true }).next().css("background-position" , "0px -100px");
	} else if ( val == "" ){
		viaArea.find("input[type='radio']").prop({ "checked" : false , "disabled" : true }).next().css("background-position" , "0px -100px");
	}
	else {
		viaArea.find("input[type='radio']").prop({ "disabled" : false });
	}
	
	var idx = viaArea.find("input[type='radio']:checked").parents("li").index();
	if( idx == 0 ){
		viaArea.find("select").hide().find("option:eq(0)").prop("selected" , true);
	} 
	else if( idx == 1 ) {
		viaArea.find("select").show();
		$(".via_txt").show();
		$(".direct_txt").hide();
		
		var othisval = viaArea.find("select").val();
		if(othisval === '15') {
			$(".direct .or_txt").hide();
			$(".direct .precondition").show();
		} else if( othisval ==='1' || othisval === '4' || othisval === '101' || othisval === '16' || othisval === '18' || othisval === '13' || othisval === '5' || othisval === '19' || othisval === '20' ){
			$(".direct .or_txt").hide();
			$(".direct .impossible").show();
		}
		else if(othisval === '21') {
			$(".direct .or_txt").hide();
			$(".direct .possible").show();
		} else {
			$(".direct .or_txt").hide();
		}
	}
}

function depart_place(othis){
	var val 	= othis.val();
	var viaArea = othis.parents("table").find(".direct");
	
	if( val == "P" || val == "U" || val == "I" ){
		viaArea.find(".chk_list > li:eq(0) input[type='radio']").prop({ "checked" : true , "disabled" : true }).next().css("background-position" , "0px -150px");
		viaArea.find(".chk_list > li:eq(1) input[type='radio']").prop({ "checked" : false , "disabled" : true }).next().css("background-position" , "0px -100px");
	} else if ( val == "" ){
		viaArea.find("input[type='radio']").prop({ "checked" : false , "disabled" : true }).next().css("background-position" , "0px -100px");
	}
	else {
		viaArea.find(".chk_list > li:eq(0) input[type='radio']").prop({ "checked" : true , "disabled" : false }).next().css("background-position" , "0px -150px");
		viaArea.find(".chk_list > li:eq(1) input[type='radio']").prop({ "checked" : false , "disabled" : false }).next().css("background-position" , "0px -100px");
	}
	
	$(".via_txt").hide();
	$(".direct_txt").show();
	$(".direct .or_txt").hide();
	
	viaArea.find(".select_via").hide().find("option:first-child").prop("selected" , true);
	place(othis.find("option:selected").index() < 1 ? 1 : othis.find("option:selected").index());
}

function place(num){
	$("#place01").val(num).attr("selected","selected").parents(".place_airport").siblings(".schedule").hide();
	$("#schedule"+num).show();				
}

function selecttab(tid,num){
	var count = num - 1;
	$("#"+tid).find(".tab_menu").eq(0).find("a").removeClass("on").eq(count).addClass("on")
	.end().end().end().end().find(".area").hide().eq(count).show();	
}

function eventBubblingChecked( target, _href ){
	if( _href.indexOf("#") > -1 || _href == "" || _href == "undefined" ){
		target.preventDefault();
	}
}

function isNumber(s) {
	s += ''; 
	s = s.replace(/^\s*|\s*$/g, '');
	if (isNaN(s)) return false;
	return true;
}
	
var detailimg = null;
	
function productDetailZoom(){
	detailimg = $(".detail_view .pr_wrap .img");
	detailimg.find("p.zoom img").on("load",function(){
		if($(this).width() >= 900){
			detailimg.find("p.productimg").css("cursor","pointer").on("mouseenter",function(e){
				e = e || window.event;
				imgzoom(e,$(this));
			}).on("mouseleave",function(){
				imgzoomoff($(this));
			});
		}else{
			detailimg.find("p.productimg").css("cursor","default").off("mouseenter");
		}
	}).each(function(){
		if(this.complete){
			$(this).load();
		}
	});
	detailimg.find("ul.productimg_thum li a").on("click",function(){
		var othis = $(this);
		othis.parents("ul").find("a").removeClass("on");
		othis.addClass("on");
		detailimg.find("p.productimg img").eq(0).attr("src",othis.find("img").attr("src"));	
		detailimg.find("p.zoom img").attr("src",othis.find("img").attr("data-image")).on("load",function(){
			if($(this).width() >= 900){
				detailimg.find("p.productimg").css("cursor","pointer").on("mouseenter",function(e){
					e = e || window.event;
					imgzoom(e,$(this));
				}).on("mouseleave",function(){
					imgzoomoff($(this));
				});
			}else{
				detailimg.find("p.productimg").css("cursor","default").off("mouseenter");
			}
		});
		return false;
	});
}
		
function imgzoom(e,obj){
	var zoom = detailimg.find("p.zoom");
	var zoomimg = zoom.find("img");
	zoom.css("visibility","visible").find("img").css("width" , 2000);
	
	var othisoffset = obj.offset();
	var opoint = obj.find(".area");				
	var mat = (zoomimg.width()/320)*(-1);
	var lpoint = e.pageX - othisoffset.left -  30;
	var tpoint = e.pageY - othisoffset.top - 30;			
	opoint.css({"left":lpoint,"top":tpoint}).show();
	obj.on("mousemove",function(e){
		lpoint = e.pageX - othisoffset.left -  30;
		tpoint = e.pageY - othisoffset.top - 30;
		if(lpoint > -30 && lpoint < 280 && tpoint > -30 && tpoint < 280){
			opoint.css({"left":lpoint,"top":tpoint});					
			zoomimg.css("margin",(mat*tpoint)+"px 0 0 "+(mat*lpoint)+"px");					
		}
	});
}
	
function imgzoomoff(obj){
	var zoom = detailimg.find("p.zoom");
	var zoomimg = zoom.find("img");
	obj.off("mousemove").find(".area").hide();
	detailimg.find("p.zoom").css("visibility","hidden");
	zoomimg.css({ "width" : "auto" });					
}	

/* detail product (up/down hide slide) */
function todayProduct(_$targetObj, _$btnNext, _$btnPrev, _$targetNo) {
	var listCon = _$targetObj
	var list = listCon.find('li');
	var listView = _$targetNo;
	var listTotal = list.length;
	var num = 0;
	var k;
	
	if( listTotal <= listView ){
		_$btnNext.hide();
		_$btnPrev.hide();
	}
	
	for (i=listView; i<listTotal; i++) {
		list[i].style.display = 'none';
	}

	_$btnNext.bind('click', function(e) {
		e.preventDefault();

		if (num < (listTotal- _$targetNo)) {
			num++
			for (i=0; i<listTotal; i++) {
				list[i].style.display = 'none';
			}
			for (k=0; k<listView; k++) {
				if (list[k+num]) {
					list[k+num].style.display = 'block';
				}
			}
		}
	});

	_$btnPrev.bind('click', function(e) {
		e.preventDefault();
		if (num > 0) {
			num--;
			for (i=0; i<listTotal; i++) {
				list[i].style.display = 'none';
			}
			for ( k=0; k<listView; k++) {
				list[k+num].style.display = 'block';
			}
		}
	});
}

// product compare
function tableScroll(){
	if($(".table_scroll").length > 0){
		table_scroll_width();
		$(".table_scroll").mCustomScrollbar({axis:"x", scrollByX : 0, scrollToX : 0})
			.find(".css-checkbox").on("click",function(){
				if($(this).is(":checked")){
					var truenum = 0;
					$(this)	.parents(".table_scroll").find(".css-checkbox").not($(this).parents(".table_scroll").find("tr.disable .css-checkbox")).each(function(){
						if($(this).is(":checked")){truenum++;}

						if(truenum === $(this).parents(".table_scroll").find(".css-checkbox").not($(this).parents(".table_scroll").find("tr.disable .css-checkbox")).length){
							$(this)	.parents(".table_scroll").siblings(".th_area").find(".css-checkbox").prop("checked",true)
								.siblings("label").css("background-position","0px -50px");
						}
					});
				}else{
					$(this)	.parents(".table_scroll").siblings(".th_area").find(".css-checkbox").prop("checked",false)
						.siblings("label").css("background-position","0px 0px");
				}
				
				var checkedLength = $(".table_scroll").find(".css-checkbox").filter(":checked").size();
				$(".compare .notice .btn_area p").text("선택상품" +" " + checkedLength + "개");
			}).end().siblings(".th_area").find(".css-checkbox").on("click",function(){
				if($(this).is(":checked")){
					$(this)	.parents(".th_area").siblings(".table_scroll").find(".css-checkbox").each(function(){
						if(!$(this).is(":checked")){
							$(this).click();
						}
					});
				}else{
					$(this)	.parents(".th_area").siblings(".table_scroll").find(".css-checkbox").prop("checked",false)
						.siblings("label").css("background-position","0px 0px");
				}
				
				var checkedLength = $(".table_scroll").find(".css-checkbox").filter(":checked").size();
				$(".compare .notice .btn_area p").text("선택상품" +" " + checkedLength + "개");
			});
	}
}	

//tooltip
function tooltipEvent(){
	$(".tooltip_wrap").each(function(){
		var _this 		= $(this);
		var btnLayerH 	= _this.find(".btn_tooltip_layer").height();
		var layerH 		= _this.find(".tooltip_layer").height();
		
		_this.find(".tooltip_layer").css({ "top" : -((layerH/2)-(btnLayerH/2)) });
		
		_this.find("a").on({
			"mouseenter" : function(){
				_this.find(".tooltip_layer").show();	
			},
			
			"mouseleave" : function(){
				_this.find(".tooltip_layer").hide();	
			}
			
		});
	});
}
	
function promo_rolling() {
	var _autoplay={'bool':false, 'id':null, 'sec':3000};
	var _ishover=false;

	init_category_rolling();
	category_autoplay(true);

	function init_category_rolling(){
		var scope=$('.event_wrap');
		var container=scope.find('.visual_lnb');
		var content=container.find('.list');
		var prev=container.find('.btn_move .prev');
		var next=container.find('.btn_move .next');
		var list={'focus':0, 'total':-1, 'width':-1, 'height':-1};
		var total=content.find('>li').length;
		// 1. init
		if(total<=8){
			$(prev).addClass('end');
			$(next).addClass('end');
		}else{
			$(prev).removeClass('end');
			$(next).removeClass('end');
		};
		
		// 2. li-event
		$(content).find('>li').each(function(a){
			$(this).on({
				'mouseenter':function(){
					$(content).find('>li').removeClass('on');
					$(this).addClass('on');
					
					if(total===8){
						$(prev).addClass('end');
						$(next).addClass('end');
					}else{
						$(prev).removeClass('end');
						$(next).removeClass('end');
					}

					_ishover=true;              
				},
				'mouseleave':function(){
					_ishover=false;
					if(_autoplay.bool){
						category_autoplay(true);
					}
				}
			});
		});
		
		// 3. prev-event
		$(prev).on({
			'click':function(e){
				var n=$(content).find('>li.on').index();
				$(content).find('>li').removeClass('on');

				if(n<1 && total>=8){
					var first=$(content).find('>li:first');
					var last=$(content).find('>li:last');

					$(last).insertBefore($(first));
					$(content).find('>li:eq(0)').addClass('on');
				}else{
					n=(n==0)?total:n;
					$(content).find('>li:eq('+(n-1)+')').addClass('on');
				}
				e.preventDefault();
			},
			'mouseenter':function(){
				_ishover=true;
			},
			'mouseleave':function(){
				_ishover=false;
				if(_autoplay.bool){
					category_autoplay(true);
				}
			}
		});
		
		// 4. next-event
		$(next).on({
			'click':function(e){
				var n=$(content).find('>li.on').index();
				$(content).find('>li').removeClass('on');

				if(n>6){
					var first=$(content).find('>li:first');
					var last=$(content).find('>li:last');

					$(first).insertAfter($(last));
					$(content).find('>li:eq(7)').addClass('on');
				}else{
					n=(n<0)?n-1:n;
					n=(n+1>=total)?-1:n;
					$(content).find('>li:eq('+(n+1)+')').addClass('on');
				}
				e.preventDefault();
			},
			'mouseenter':function(){
				_ishover=true;
			},
			'mouseleave':function(){
				_ishover=false;
				if(_autoplay.bool){
					category_autoplay(true);
				}
			}
		});
		
		// 5. play/pause-event
		$(scope).find('.btn_control a').on({
			'click':function(e){
				if($(this).hasClass('pause')){
					$(this).hide();
					$(scope).find('.btn_control a.play').css("display" , "block");
					_autoplay.bool=false;
					category_autoplay(false);
				}else{
					$(this).hide();
					$(scope).find('.btn_control a.pause').css("display" , "block");
					_autoplay.bool=true;
					category_autoplay(true);
				}
				e.preventDefault();
			}
		});
	};

	/** Category-Autoplay */
	function category_autoplay(bool){
		
		var total=$('.event_wrap .visual_lnb .list').find('>li').length;
		
		clearInterval(_autoplay.id);
		if(total<=1) return false;
		
		if(bool){
			_autoplay.id=setInterval(function(){
				if(!_ishover){
					$('.event_wrap .visual_lnb .btn_move .next').trigger('click');
				}else{
					category_autoplay(true);
				};
			}, _autoplay.sec);
		};
	}
}	
	
// detail slide	
function slideList(cname,num,count,auto){
	$("."+cname).each(function(){
		if($(this).length < 1){return}
		
		var oslide = $(this);
		var cnameAni = null;
		var scount = 1;
		var mt = 500;
		if(count !== null && count > 1 ){
			scount = count;
			mt = 1000;
		}
		var list = oslide.find(".img_list");
		var ll = Number(list.children().length);

		if(ll < Number(num)+1){
			//oslide.find(".prev").add($("."+cname+"").find(".next")).hide();
			oslide.find(".prev").hide();
			oslide.find(".next").hide();
			return;
		} else {
			oslide.find(".prev").show();
			oslide.find(".next").show();
		}
		if(ll < count+num){
			scount = ll - Number(count);
		}
		var mlw = Number(list.children().width());
		var clickable = true;
		list.css("width",ll*(mlw+2));
		function slideNext(){
			if(!clickable){return false;}
			clickable = false;
			clearTimeout(cnameAni);
			list.stop().animate({"margin-left":-mlw*scount},{duration:mt,complete:function(){
				for(var i=0;i<scount;i++){
					list.append(list.children().eq(0).clone());
					list.children().eq(0).remove();
				}
				slideReset($(this));
			}});
		}

		cnameAni = setTimeout(function(){slideNext();},3000);
		if( auto == false ){
			clearTimeout(cnameAni);
		}

		function slideReset(othis){
			othis.css("margin-left",0);
			cnameAni = setTimeout(function(){slideNext();},3000);
			if( auto == false ){
				clearTimeout(cnameAni);
			}
			clickable = true;
			return false;
		}
		oslide.find(".prev").on("click",function(e){
			if(!clickable){return false;}
			clickable = false;
			e.preventDefault();
			clearTimeout(cnameAni);
			list.stop();
			for(var i=0;i<scount;i++){
				list.prepend(list.children(":last").clone());
				list.children(":last").remove();
			}
			list.css("margin-left",-mlw*scount)
				.stop().animate({"margin-left":0},{duration:mt,complete:function(){
					slideReset($(this));
				}});
		});
		oslide.find(".next").on("click",function(e){
			e.preventDefault();
			slideNext(cname);
		});
		oslide.on("mouseover mouseleave mouseout mouseup mousemove focusin focusout",function(e){
			e = e || window.event;
			if(e.type === "mouseover" || e.type === "mouseout" || e.type === "mouseup" || e.type === "mousemove" || e.type === "focusin"){
				clearTimeout(cnameAni);
			}else{
				cnameAni = setTimeout(function(){slideNext();},3000);
				if( auto == false ){
					clearTimeout(cnameAni);
				}
			}
		});
	});
}

// number check
function inputNumberCheck(){
	$("*[data-value-type='number']").each(function(){
		var _this = $(this);
		_this.off("keyup").on({
			"keyup" : function(){
				var thisValue = _this.val();
				if( isNumber(thisValue) == false ){
					var numberVal = thisValue.replace( /[^0-9]/g, '');
					_this.val( numberVal );
				};
			}
		});
	});
}

function facetOptionCheck(){
	if( $(".facet_more").size() > 0 ){
		$(".more_conditional").show();
	} else {
		$(".more_conditional").hide();
	}
}

function commonFacetUI(priceMinVal,priceMaxVal){
	var minprice = 0,maxprice = 100;
	var inputmin = 0,inputmax = 100;
	var checkMaxVal, checkMinVal;
	
	if($(".power_search:visible").length > 0){
		if( priceMinVal ){
			$(".search_slidebar #minrange").val(priceMinVal);
		}
		if( priceMaxVal ){
			$(".search_slidebar #maxrange").val(priceMaxVal);
		}
		
		if(! $(".power_search").hasClass("category_page") || $(".facet_more").size() == 0 ){
			$(".more_conditional").hide();
		} else {
			$(".more_conditional").show();
		}
		
		$(".power_search").find("label").each(function(){
			$(this).attr("title",$(this).text());
		});
		$(".power_search").find(".css-checkbox").each(function(){
			var othis = $(this);
			if(othis.is(":checked")){
				ischeck(othis);			
			}
		});
		if($(".select_item").find("dd.select_label").length < 1){
			$(".select_item").find("dd.none_selected .select_info").show();
		}
		minprice = Number($("#minrange").val());
		maxprice = Number($("#maxrange").val());
		inputmin = minprice;
		inputmax = maxprice;
		checkMinVal = minprice;
		checkMaxVal = maxprice;
		$("#slidebar_bubble_right .price").text("$"+maxprice);
		$("#slidebar_bubble_left")
			.css("left",0 - String(minprice).length*8 -10)
			.find(".price").text("$"+minprice);
		$("#minprice").val(minprice);
		$("#maxprice").val(maxprice);		
		if($(".choice_more:visible").length > 0){
			$(".choice_more").mCustomScrollbar();
		}
		
		$(".power_search .btn_language li a").off("click").on({
			"click" : function(e){
				var idx = $(this).parent().index();
				if(! $(this).hasClass("on") ){
					$(".power_search .btn_language li a").removeClass("on");
					$(this).addClass("on");
					$(".power_search .choice_more .chk_list").hide().eq(idx).show();
				}
				e.preventDefault();
			}
		});
	}	
	var slidebarmax = 335;
	var slidebarmin = 10;
	var sliderobj = null;
	
	$("#minprice").off("keydown").off("focusout").on("focusout keydown" , function(e){
		e =e || window.event;
		if(e.type === "focusout" || (e.type === "keydown" && e.keyCode === 13)){
			var thisn = $(this).val();	
			if(Number(thisn) >= Number(checkMaxVal) || Number(thisn) < Number(inputmin)){
				$(this).val(checkMinVal);
			}else{			
				var w = Number((thisn-minprice)/(maxprice - minprice)*335);
				w = (w >= Number($("#slidebar_bubble_right").attr("left")) - 10)? w - 10 : w;			
				$("#slidebar_bubble_left .price").text("$"+thisn);
				$("#slidebar_left").css("left",w);
				$("#slidebar_right").siblings(".dim_mask").css("width",w);
				$("#slidebar_bubble_left").css("left",w - String(thisn).length*8 -10).attr("left",w);
				slidebarmin = w + 10;
				checkMinVal = thisn;
			}
		}	
	});
	$("#maxprice").off("keydown").off("focusout").on("focusout keydown", function(e) {
		e = e || window.event;
		if ( e.type === "focusout" || (e.type === "keydown" && e.keyCode === 13) ) {
			var thisn = $(this).val();
			if ( Number(thisn) > Number(inputmax) || Number(thisn) <= Number(checkMinVal) ) {
				$(this).val(checkMaxVal);
			} else {
				var w = Number((thisn-minprice)/(maxprice - minprice) * 335);
				w = ( w <= Number($("#slidebar_bubble_left").attr("left")) + 10
					|| maxprice === parseInt(thisn) ) ? w + 10 : w;
				$("#slidebar_bubble_right .price").text("$" + thisn);
				$("#slidebar_right").css("left", w);
				$("#slidebar_right").siblings(".mask").css("width", w);
				$("#slidebar_bubble_right").css("left", w + 3).attr("left", w + 3);
				slidebarmax = w - 10;
				checkMaxVal = thisn;
			}
		}
	});
	$(document).off("mouseup").on("mouseup",function(){	
		$("#search_slidebar").removeClass("search_slidebar_wrap");
		$("#container").off("mousemove");
		sliderobj = null;
		$(document).off("mousemove");
	});
	
	$("#slidebar_left").off("mousedown").on("mousedown",function(e){
		e = e || window.event;
		sliderobj = {
			left:$("#slidebar_left"),
			mask:$("#slidebar_right").siblings(".dim_mask"),
			lbubble:$("#slidebar_bubble_left"),
			input:$("#minprice")
		};	
		var left = Number((e.pageX - $(".search_slidebar").offset().left - 20).toFixed());
		var num = ((left/335)*(maxprice - minprice)+minprice).toFixed();		
		$("#search_slidebar").addClass("search_slidebar_wrap");
		$(document).on("mousemove",function(e){			
			e = e || window.event;
			left = Number((e.pageX - $(".search_slidebar").offset().left - 20).toFixed());
			num = ((left/335)*(maxprice - minprice)+minprice).toFixed();
			if(num < minprice){num = minprice;}		
			if(num > maxprice){num = maxprice;}		
			if(left >= -3){	
				left = (left < 0 ? 0 :left);
				slidemin(left,num);
				slidebarmin = left + 10;
			}		
		});
	});
	$("#slidebar_right").off("mousedown").on("mousedown",function(e){
		e = e || window.event;
		sliderobj = {
			right:$("#slidebar_right"),
			mask:$("#slidebar_right").siblings(".mask"),
			rbubble:$("#slidebar_bubble_right"),
			input:$("#maxprice")
		};		
		var left = Number((e.pageX - $(".search_slidebar").offset().left - 20).toFixed());
		var num = (((left-10)/335)*(maxprice - minprice)+minprice).toFixed();			
		$("#search_slidebar").addClass("search_slidebar_wrap");
		$(document).on("mousemove",function(e){	
			e = e || window.event;		
			left = Number((e.pageX - $(".search_slidebar").offset().left - 20).toFixed());
			num = (((left-10)/335)*(maxprice - minprice)+minprice).toFixed();
			if(num < minprice){num = minprice;}		
			if(num > maxprice){num = maxprice;}				
			if(left < 346){
				slidemax(left,num);
				slidebarmax = left - 10;	
			}
			
		});
	});
	
	function slidemin(w,num){	
		if(sliderobj === null){return false;}
		if(w >= -1 && w < slidebarmax ){				
			if(w >= slidebarmax){w = slidebarmax;}
			if(w <= 0 ){w = 0;}
			sliderobj.input.val(num);
			sliderobj.left.css("left",w);
			sliderobj.mask.css("width",w);
			sliderobj.lbubble.css("left", w - String(num).length*8 -10).attr("left",w)
			.find(".price").text("$"+num);	
			checkMinVal = num;							
		}
	}
	
	function slidemax(w,num){
		if(sliderobj === null){return false;}
		if(w > slidebarmin && w < 346 ){			
			if(w <= slidebarmin){w = slidebarmin;}
			sliderobj.right.css("left",w);
			sliderobj.mask.css("width",w);
			sliderobj.rbubble.css("left",w+4).attr("left",w)
			.find(".price").text("$"+num);
			sliderobj.input.val(num);	
			checkMaxVal = num;		
		}
	}
	
	function ischeck(obj){
		var select_item = $(".select_item");			
		select_item.show();
		var dd = $("<dd>")
			.addClass("select_label")
			.html(obj.siblings("label").text()+"<a href='#url'><img src='"+path+"img/common/ico_delete.png' alt='delete'></a>")
			.attr("data-id",obj.attr("id"));		
		$(".select_item").append(dd);		
		select_item.find("dd.none_selected .select_info").hide();	
		dd.find("a").on("click",function(){
			var athis = $(this);					
			var power_search = $(".power_search").find("input[type='checkbox']:checked");
			for(var i=0;i < power_search.length;i++){
				if(power_search.eq(i).attr("id") === athis.parents("dd").attr("data-id")){
					power_search.eq(i)
						.prop("checked",false)
						.siblings("label").css("background-position","0 0");
					break;
				}			
			}	
			athis.parents("dd").remove();
			if(select_item.find("dd.select_label").length < 1){
				select_item.find("dd.none_selected .select_info").show();
			}
		});		
	}
	
	function ischeckCategory(obj){
		var select_item = $(".select_item");			
		select_item.show();
		
		select_item.find("[data-id^='facetCategoryS']").remove();
		
		if( obj.attr("id").indexOf("facetCategoryL") > -1 ){
			select_item.find("[data-id^='facetCategoryL']").remove();
		}
		
		if( obj.index() == 0 && obj.attr("id").indexOf("facetCategoryL") > -1 ){
			// all cetegory
		} else {
			var dd = $("<dd>")
				.addClass("select_label")
				.html(obj.find("em").text()+"<a href='#url'><img src='"+path+"img/common/ico_delete.png' alt='delete'></a>")
				.attr("data-id",obj.attr("id"));
				
			$(".select_item").append(dd);		
			select_item.find("dd.none_selected .select_info").hide();	
			dd.find("a").on("click",function(){
				var _this = $(this);					
				var dataId = _this.parent("dd").data("id");
				
				$("#"+dataId).removeClass("on");
				_this.parents("dd").remove();
				
				if( dataId.indexOf("facetCategoryL") > -1 ){
					select_item.find("[data-id^='facetCategoryS']").remove();
					$(".power_search .btn_result").eq(0).addClass("on");
					$(".power_search .brand_choice a").removeClass("on");
				}
				
				facetConditionAreaEmptyGuide();
			});		
		}
		facetConditionAreaEmptyGuide();
	}
	
	function facetConditionAreaEmptyGuide(){
		var select_item = $(".select_item");
		if(select_item.find("dd.select_label").length < 1){
			select_item.find("dd.none_selected .select_info").show();
		}
	}
	
	var facetCategoryH = $(".power_search .p_brand").height();
	
	$(".power_search .brand_choice").each(function(i){
		$(this).find("a").each(function(j){
			$(this).attr({ "id" : "facetCategoryS_"+i+"_"+j });
		});
	});
	
	$(".power_search .btn_result").each(function(i){
		$(this).attr({ "id" : "facetCategoryL_"+i });
	});
	
	$(".power_search .toggle_btn").off("click").on("click",function(){
		if($(this).hasClass("toggle_on")){
			$(this).removeClass("toggle_on").addClass("toggle_off").parents("th").next("td").removeClass("on");		
			$(".power_search .p_brand").css({ "height" : facetCategoryH });
			$(".choice_more").mCustomScrollbar("destroy");
		}else{
			$(this).removeClass("toggle_off").addClass("toggle_on").parents("th").next("td").addClass("on");			
			$(".power_search .p_brand").css({ "height" : "auto" });
			$(".choice_more").mCustomScrollbar();
		}
		return false;
	});
	
	/* $(".power_search .btn_language a").off("click").on("click",function(){
		$(this).addClass("on").parents("li").siblings("li").find("a").removeClass("on");
		return false;
	}); */
	
	$(".power_search .btn_result").off("click").on("click",function(e){
		$(".brand_choice a").removeClass("on");
		if(! $(this).hasClass("on") ){
			$(this).addClass("on").siblings(".btn_result").removeClass("on");
			$(".brand_choice").show();
			
			if( $(".facet_more").size() > 0 ){
				$(".more_conditional").show();
				if(! $(".power_search").hasClass("category_page") ){
					if(! $(".more_conditional").hasClass("on") ){
						$(".more_conditional").addClass("on");
						$(".facet_more").hide();
					}
					$(".select_item").find("dd[data-id^='facet_more']").remove();
				}
			}	
			priceSlideReset();
			$(".brand_choice").hide();
			var id = $(this).attr("href");
			$(id).show();
			ischeckCategory($(this));
			
			if($(this).hasClass("cateFirst")){
				facet.callAttr($(this).data('value'));
			};
			
		}	
		e.preventDefault();
	});	
	
	$(".brand_choice a").off("click").on("click",function(e){
		if(! $(this).hasClass("on") ){
			$(".brand_choice a").removeClass("on");
			$(this).addClass("on").siblings("a").removeClass("on");
			ischeckCategory($(this));
		}
		e.preventDefault();
	});
	
	$(".power_search input[type='checkbox']").off("click").on("click",function(){
		var othis = $(this);
		var select_item = $(".select_item");
		if(othis.is(":checked")){
			ischeck(othis);		
		}else{
			var selectitem = select_item.find("dd.select_label");
			for(var i=0;i < selectitem.length;i++){
				if(selectitem.eq(i).attr("data-id") === othis.attr("id")){
					selectitem.eq(i).remove();
					break;
				}			
			}
			facetConditionAreaEmptyGuide();			
		}
	});
	
	$(".power_search .btn_reset").add(".power_search .none_selected .all").off("click").on("click",function(){
		$(".power_search .select_item dd.select_label").remove();
		$(".power_search input[type='checkbox']").prop("checked",false)
			.siblings("label").css("background-position","0 0");	
		$(".select_item").find("dd.none_selected .select_info").show();
		$(".power_search .p_category > a").eq(0).trigger("click");
		$(".power_search .p_category .brand_choice > a").removeClass("on");
		priceSlideReset();
		return false;
	});
	
	$(".more_conditional a").off("click").on("click",function(){
		if($(".more_conditional").hasClass("on")){
			$(".more_conditional").removeClass("on");	
			$(".facet_more").show();
			//$(".select_item").show()
		}else{
			$(".more_conditional").addClass("on");
			$(".facet_more").hide();
			/* if($(".select_item").find("dd.select_label").length < 1){
				$(".select_item").hide();
			} */
		}		
		return false;
	});
	
	function priceSlideReset(){
		var defaultWidth = 345;
		
		checkMinVal = minprice;
		checkMaxVal = maxprice;
		
		$("#minprice").val(minprice);
		$("#maxprice").val(maxprice);
		
		$(".search_slidebar .dim_mask").width(0);
		$(".search_slidebar .mask").width(defaultWidth);
		
		$("#slidebar_left").css({ "left" : 0 });
		$("#slidebar_right").css({ "left" : defaultWidth+"px" });
		$("#slidebar_bubble_left").removeAttr("left").css({ "left" : "" }).find(".price").text("$"+ minprice );
		$("#slidebar_bubble_right").attr("left", defaultWidth).css({ "left" : defaultWidth+3+"px" }).find(".price").text("$"+ maxprice );
	}
}

function skyScrapperProductListCheck(){
	$(".sky_scrapper .cont > div").each(function(){
		var _this = $(this);
		var allCheckbox 	= _this.find(".tit_top input[type='checkbox']");
		var eachCheckbox 	= _this.find(".area input[type='checkbox']");
		
		allCheckbox.on({
			"change" : function(){
				
				if( $(this).prop("checked") == true ){
					eachCheckbox.filter(":enabled").prop("checked" , true).next("label").css("background-position","0 -50px");
					$(this).next("label").css("background-position","0 -75px");
				} else {
					eachCheckbox.filter(":enabled").prop("checked" , false).next("label").css("background-position","0 0");
					$(this).next("label").css("background-position","0 -20px");
				}
				
				selectProdCheck();
			}
		});
		
		eachCheckbox.on({
			"change" : function(){
				var eachCheckboxLength 	= eachCheckbox.size();
				var eachCheckedLengh 	= eachCheckbox.filter(":checked").size();
				
				if( eachCheckedLengh == eachCheckboxLength ){
					allCheckbox.prop("checked" , true).next("label").css("background-position","0 -75px");
				} else {
					allCheckbox.prop("checked" , false).next("label").css("background-position","0 -20px");
				}
				selectProdCheck();
			}
		});
		
		function selectProdCheck(){
			var eachCheckedLengh 	= eachCheckbox.filter(":checked").size();
			_this.find(".total_box > p.txt_red > em").text(eachCheckedLengh+"개");
		}
	});
}

function skyScrapperCartListInit(){
	var cartList = $(".sky_scrapper .cart_list_sky");
	cartList.find(".area").height($(window).height() - 169).mCustomScrollbar({}).find(".mCSB_scrollTools").height(cartList.find(".mCustomScrollBox").height() -20).css("top","10px");
	cartList.find(" .tit_top input[type='checkbox']").prop("checked" , false).next().css({ "background-position" : "0 -20px" });
	cartList.find(".total_box > p.txt_red > em").text(0+"개");
	skyScrapperProductListCheck();
}

function skyScrapperTodayListInit(){
	var TodayList = $(".sky_scrapper .today_list_sky");
	TodayList.find(".area").height($(window).height() - 98).mCustomScrollbar("destroy").mCustomScrollbar({}).find(".mCSB_scrollTools").height(TodayList.find(".mCustomScrollBox").height() -20).css("top","10px");
	TodayList.find(" .tit_top input[type='checkbox']").prop("checked" , false).next().css({ "background-position" : "0 -20px" });
	TodayList.find(".total_box > p.txt_red > em").text(0+"개");
	skyScrapperProductListCheck();
}

// brand mall lnb
var brandMallLnb = {

	setting : {
		depth1 : $(".brand_mall_lnb "),
		depth2 : $(".brand_mall_lnb .depth01"),
		depth3 : $(".brand_mall_lnb .depth02"),
		speed  : "fast"
	},

	init : function(){
		this.event();
		this.reset();
	},

	event : function(){
		brandMallLnb.setting.depth1.find(">li>a").on({
			"click" : function(e){
				var _this = $(this);
				var _thisParent = _this.parent();
				if( _this.next(".depth01").size() > 0 ){
					if( _thisParent.hasClass("on") ){
						_thisParent.removeClass("on").find(".depth01").slideUp(brandMallLnb.setting.speed , function(){
							$(this).find(".depth02").hide();
						});
					} else {
						brandMallLnb.setting.depth1.find(">li.on").removeClass("on").find(".depth01").slideUp(brandMallLnb.setting.speed , function(){
							$(this).find(".depth02").hide();
						});
						_thisParent.addClass("on").find(".depth02").show().end().find(".depth01").slideDown(brandMallLnb.setting.speed).find(">li").data({"active" : "on"});
					}
					e.preventDefault();
				}
			}
		});

		brandMallLnb.setting.depth2.find(">li>a").on({
			"click" : function(e){
				var _this = $(this);
				var _thisParent = _this.parent();
				if( _this.next(".depth02").size() > 0 ){
					if( _thisParent.data("active") == "on" ){
						_thisParent.data({"active" : "off"}).find(".depth02").slideUp(brandMallLnb.setting.speed);
					} else {
						_thisParent.data({"active" : "on"}).find(".depth02").slideDown(brandMallLnb.setting.speed);
					}
					e.preventDefault();
				}
			}
		});
	},

	reset : function(){
		brandMallLnb.setting.depth1.find(">li").each(function(){
			var _this = $(this);
			if( _this.find(".depth01").size() < 1 ){
				_this.find("a").addClass("no_depth");
			}
		});
		//brandMallLnb.setting.depth1.find(">li.on .depth01").show();
		//brandMallLnb.setting.depth2.find("> li").data({"active" : "on"}).filter(".on").find(".depth02").show();
	}
}

function myInfoRecommendProduct(){
	$(".sky_scrapper .my_info .product").each(function(){
		var _this = $(this);
		_this.find(".tit_paging div p:first-child").html("<span>1</span>/"+_this.find(".product_info li").length);
		_this.find(".tit_paging a").on("click",function(){
			var list = _this.find(".product_info li");
			var count = _this.find(".tit_paging div p:first-child span");
			var num = Number(count.text());
			var totalnum = Number(list.length);
			if($(this).hasClass("prev") && num > 1){
				list.hide();
				num--;
				list.eq(num-1).show();
				count.text(num);
			}else if($(this).hasClass("next") && num < totalnum){
				list.hide();
				list.eq(num).show();
				num++;
				count.text(num);
			}
		});
	});
}

function productAmountCheck(){
	if($(".minorder").length > 0){
		$("input.pcount").each(function(){
			var _this 				= $(this);
			var minAmount 			= Number(_this.siblings(".minorder").val());
			var maxAmount 			= Number(_this.siblings(".maxorder").val());
			var packageAmount 		= Number(_this.siblings(".ordercount").val());
			var orderSelect	 		= _this.parents(".amount_box").find("select");
			var _countConfirmBtn 	= _this.parents(".amount_box").find(".pconut_submit");
			
			_this.data("currentCount", Number(_this.val()));
			_this.data("prevCount" , _this.val());
			orderSelect.data("selectIndex", orderSelect.find("option:selected").index() );
			
			orderSelect.on({
				"change" : function(){
					if( _countConfirmBtn.length){
						if( Number(_this.val()) != Number(_this.data("currentCount")) || orderSelect.find("option:selected").index() != orderSelect.data("selectIndex") ) {
							_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "block" });
							_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "none" });
						} else {
							_countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "none" });
							_countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "block" });
						}	
					}	
				}
			});
		});		
	}
	
	$(".edit_option").each(function(){
		var _this = $(this);
		var orderSelect	 		= _this.find("select");
		var countConfirmBtn 	= _this.find(".pconut_submit");
		
		orderSelect.data("selectIndex", orderSelect.find("option:selected").index());
		
		orderSelect.on({
			"change" : function(){
				if( countConfirmBtn.length){
					if( orderSelect.find("option:selected").index() != orderSelect.data("selectIndex") ) {
						countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "block" });
						countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "none" });
					} else {
						countConfirmBtn.filter(".btn_a_red_r").css({ "display" : "none" });
						countConfirmBtn.filter(".btn_a_gray_r").css({ "display" : "block" });
					}	
				}	
			}
		});
	});
}

// number type max length limit
function inputNumberMaxLengthCheck(object){
	if (object.value.length > object.maxLength){
		object.value = object.value.slice(0, object.maxLength);
	}    
}

//임시
$(document).ready(function(){
	/* $(".detail_view .best_review .list dl dt a").on("click", function(){
		if ($(this).parents("dl").hasClass("on"))
		{
			$(this).parents("dl").removeClass("on")
		} else {
			$(this).parents("dl").addClass("on");
		}
	}); */

	// gnb 브랜드 내 액션
	$(".brand_view").find(".list_lang_wrap").eq(0).show();
	$(".brand_view .select_lang a").on("click",function(){
		var tabIdx = $(this).parent("li").index();
		$(".brand_view .select_lang li").removeClass("on");
		$(this).parent("li").addClass("on");
		$(".brand_view").find(".list_lang_wrap").hide();
		$(".brand_view").find(".list_lang_wrap").eq(tabIdx).show();
		return false;
	});

	var brandView = $("brand_view");
	var langListWrap = $(".list_lang_wrap");
	var btnlang = langListWrap.find(".list_lang li");
	var brandList = $(".brand_list_wrap");
	var brandListIdx = 0;

	btnlang.find("a").each(function(k,v){
		var indiAlpha = $(this).text();
		var hasBrand = false;
		$(this).parents(".list_lang").next().find("> div").each(function(k,v){
			if(indiAlpha == $(this).find("strong").text()){
				hasBrand = true;
			}
		});
		
		if(hasBrand){
			$(this).parent("li").removeClass("disabled");
		}else{
			$(this).parent("li").addClass("disabled");
		}
	});
	
	$(".list_lang").each(function(){
		$(this).find("> li:not('.disabled')").each(function(i){
			$(this).attr("data-index" , i);
		});
	});
	
	btnlang.find("a").on("click", function(e){
		if(! $(this).parent("li").hasClass("disabled") ){
			var brandsListHeight = 0;
			var brandListScroll =0;
		
			$(this).parent("li").addClass("on").siblings().removeClass("on");
			brandListIdx = $(this).parent("li").data("index");
			
			for (var i =0; i<brandListIdx ; i++)
			{
				brandListHeight = $(this).parents(".list_lang_wrap").find(brandList).find(".brand_list").eq(i).outerHeight();
				brandListScroll = brandListScroll + brandListHeight;
			}
			brandList.animate({ scrollTop : brandListScroll }, 300);
		}
		e.preventDefault();
	});
	
});

/* 메인 탭 최근게시물 2016.02.01 추가 */
 $(document).ready(function() {
	$(".tab_box").each(function(i){
	    var $bbsobj=$(this);
	    $(this).find("li dl dt").each(function(j){
	        var $curobj=$(this);
	        if(j == 0) {
	            $bbsobj.find(".content1").hide();
	            $($curobj.children("a:eq(0)").attr("href")).show();
	        }

	        $curobj.bind("keyup mouseover", function() {
	                $bbsobj.find(".content1").hide();
	                $bbsobj.find(".tabclass img").each(function(){
	                    $(this).attr("src", $(this).attr("src").replace(/_on.png/, '_off.png'));
	                });
	                imagename =  $(this).find("img").attr("src");
	                $(this).find("img").attr("src", imagename.replace(/_off.png/, '_on.png'));
	                var $targetul=$($(this).children("a:eq(0)").attr("href"));
	                $targetul.show();
	        });
	        //end hover
	        $curobj.click(function(){
	            var $targetul=$($(this).children("a:eq(0)").attr("href"));
	            $targetul.show();
	        });
	    });
		});	
	});




// 유동적인 검색바 추가

	$(document).ready(function(){	
	'use strict';

	var windowHeigt = $(window).height();
	$(document).on("click",".datepicker_close",function(){
		$.datepicker._hideDatepicker();
	});	
	
	//main bestproduct wing		
	var best_productOffsetTop =  "",
		best_product = null,
		bp =  "";
	
	
	// 스카이스크래퍼	
	var mainSkyScrapperBanner,
		mainSkyScrapperBannerOffsetTop; 
	/* 17.06.14 main sky_scrapper right banner 추가 s */
	var mainSkyScrapperBanners,
		mainSkyScrapperBannersOffsetTop; 
	/* 17.06.14 main sky_scrapper right banner 추가 e */
	
	if( $(".search_bar:visible").length > 0 ){
		mainSkyScrapperBanner = $(".search_bar");
		mainSkyScrapperBannerOffsetTop = $(".search_bar").offset().top;
	}
	/* 2016-08-29 추가 */
	if( $(".fixed_bar:visible").length > 0 ){
		mainSkyScrapperBanner = $(".fixed_bar");
		mainSkyScrapperBannerOffsetTop = $(".fixed_bar").offset().top;
	}
	/* //2016-08-29 추가 */
	/* main sky_scrapper right banner 2017-06-07 추가 17.06.14 수정 s */
	if( $(".sky_scrapper_new:visible").length > 0 ){
		mainSkyScrapperBanners = $(".sky_scrapper_new");
		mainSkyScrapperBannersOffsetTop = $(".sky_scrapper_new").offset().top;
	}
	/* // main sky_scrapper right banner 2017-06-07 추가 17.06.14 수정 e */

	/* 둥둥이 banner 17.06.26  닫힘버튼 추가  s */
	$(document).on("click" , ".doongdoonge_banner .btn_close", function(e){
		$(this).parents(".doongdoonge_banner").hide();
		e.preventDefault();
	});	
	/* 둥둥이 banner 17.06.26  닫힘버튼 추가  e */


	var rightScrollmenuOffsetTop = "",
		rightScrollmenuPositionTop = "",
		rightScrollmenuMarginTop = 10,
		rightScrollmenu = null;
	if($("#rnb:visible").length > 0){
		rightScrollmenu =  $("#rnb");
		rightScrollmenuOffsetTop = rightScrollmenu.offset().top;
		rightScrollmenuPositionTop = rightScrollmenu.position().top;
	}		

	$(window).scroll(function(){		
		var dsT = $(document).scrollTop(),
		//var tmp = ($(document).scrollTop()+leftScrollmenuPositionTop+leftScrollmenuMarginTop)-leftScrollmenuOffsetTop;
			tmp = (dsT+rightScrollmenuPositionTop+rightScrollmenuMarginTop)-rightScrollmenuOffsetTop,
			bpo =  dsT-best_productOffsetTop-85,
			bpv = dsT+windowHeigt; 
		if(rightScrollmenu !== null){
			if(tmp>155) {
				/*if(best_productOffsetTop > bpv){
					rightScrollmenu.animate({'top':tmp}, {duration:'slow', easing:'swing', queue:false});
				}*/
				rightScrollmenu.animate({'top':tmp}, {duration:'slow', easing:'swing', queue:false});
			}
			else {
				leftScrollmenu.animate({'top':leftScrollmenuPositionTop}, {duration:'slow', easing:'swing', queue:false});
				rightScrollmenu.animate({'top':rightScrollmenuPositionTop}, {duration:'slow', easing:'swing', queue:false});
			}
		}
		// main sky_scrapper left banner
		if($(".search_bar:visible").size() > 0 ){
			
			if( $(window).scrollTop() > mainSkyScrapperBannerOffsetTop ){
				mainSkyScrapperBanner.addClass("fixed");
			} else {
				mainSkyScrapperBanner.removeClass("fixed");
			}
		}
		/* 2016-08-29 추가 */
		if($(".fixed_bar:visible").size() > 0 ){
			
			if( $(window).scrollTop() > mainSkyScrapperBannerOffsetTop ){
				mainSkyScrapperBanner.addClass("fixed");
			} else {
				mainSkyScrapperBanner.removeClass("fixed");
				$('.category_menu').hide();
			}
		}
		/* //2016-08-29 추가 */
		/* main sky_scrapper right banner 2017-06-07 추가 17.06.14 수정 s */
		if($(".sky_scrapper_new:visible").size() > 0 ){
			
			if( $(window).scrollTop() > mainSkyScrapperBannersOffsetTop ){
				mainSkyScrapperBanners.addClass("fixed");
			} else {
				mainSkyScrapperBanners.removeClass("fixed");
			}
		}
		/* // main sky_scrapper right banner 2017-06-07 추가 17.06.14 수정 e */
	});

	/* 2016-08-29 추가 */
	$(".fixed_bar .bar_menu").on("mouseover click",function(e){
		e = e || window.event;
		$(this).children().addClass('on');
		$('.category_menu').show();
	});
	$(".fixed_bar .bar_menu").on("mouseleave",function(e){
		e = e || window.event;
		$(this).children().removeClass('on');
		$('.category_menu').hide();
	});
	/* //2016-08-29 추가 */
	
});
	

// 상단띠배너
//function layer_toggle(obj) {
//	if (obj.style.display == 'block') obj.style.display = 'none';
//	else if (obj.style.display == 'none') obj.style.display = 'block';
//}
$(document).ready(function() {
    var ax = $(".top_bar");
   //var ac = $("#header").outerHeight();
    var ac = 171;
    var r = 0;
    if (ax.length < 1) {
        r = 0
    }
   /* ax.css({
        "background-color": "#" + (ax.find("#topBarBgColor").val())
    });*/
    ax.find(".top_bar_btn").on({
        click: function(j) {
            if ($(".top_bar").hasClass("close")) {
                $(".top_bar").removeClass("close");
                ac = $("#header").outerHeight();
                r = ax.outerHeight();
                $("#main_visual_bg").css({
                    top: ac + r
                });
                if ($(".sky_scrapper_banner:visible").length > 0) {
                    k = $(".sky_scrapper_banner").offset().top
                }
                if ($(".boutique .sub_category").length > 0) {
                    $(".sub_category").css({
                        top: ac - 29
                    })
                }
               cookieStorage.set("top_banner", "open");
            } else {
                $(".top_bar").addClass("close");
                $("#main_visual_bg").css({
                    top: ac
                });
                if ($(".sky_scrapper_banner:visible").length > 0) {
                    k = $(".sky_scrapper_banner").offset().top
                }
                if ($(".boutique .sub_category").length > 0) {
                    $(".sub_category").css({
                        top: ac - r - 29
                    })
                }
                cookieStorage.set("top_banner", "close");
            }
            j.preventDefault()
        }
    });

});

// 메인_탭(테마)
function jquery_tab_action(obj){
	jQuery(obj).parent().find('li.active').removeClass('active');
	jQuery(obj).addClass('active'); 
	return false; 
}

/* mainVisual 17.09.12 신규작업 s */
$( document ).ready(function () {
    if ( !$('#main_visual:visible').length ) return;

    var _$target = $( '#main_visual' ),
        _$menuArea = _$target.find( '.menu' ),
        _$menuGroup = _$menuArea.find( '> li' ),
        _$menus = _$menuGroup.find( 'li > a' ),
        _$bannerGroup = _$target.find( '.visual_detail > .img' ),
        _$banners = _$bannerGroup.find( '> li' ),
        _$btnArea = _$target.find( '.newBtnArea' ),
        _$prev = _$btnArea.find( '.prev' ),
        _$next = _$btnArea.find( '.next' ),
        _$play = _$btnArea.find( '.play' ),
        _$pause = _$btnArea.find( '.pause' );

    var _delay = 4000,
        _currentIdx = 0,
        _bannerLength = _$banners.length,
        _interval, _isPlaying = true;


    initialize();

    /** ========== Methods ========== */
    function initialize () {
        _$menuGroup.each( function ( idx ) {
            $( this ).attr( 'data-idx', idx );
            $( this ).find( 'a' ).attr( 'data-idx', idx );
        });

        _$menus.each( function ( sidx ) {
            $( this ).attr( 'data-sidx', sidx );
        });

        //default set
        setBackground();
        setBgColor(0);
        setEvents();
        play();
    }

    //auto play start
    function play () {
        if ( !_isPlaying ) return;
        if ( _interval ) clearInterval( _interval );

        _interval = setInterval( function () {
            next()
        }, _delay);
    }

    //auto play stop
    function pause () {
        if ( _interval ) clearInterval( _interval );
    }

    //다음 배너 활성화
    function next () {
        var sidx = _currentIdx + 1;

        if ( sidx >= _bannerLength ) {
            sidx = 0;
        }

        activeBanner( sidx );
    }

    //이전 배너 활성화
    function prev () {
        var sidx = _currentIdx - 1;

        if ( sidx < 0 ) {
            sidx = _bannerLength - 1;
        }

        activeBanner( sidx );
    }

    //베너 활성화
    function activeBanner ( sidx ) {
        var idx = _$menus.eq( sidx ).data( 'idx' );

        _$menuGroup.find( '> a' ).removeClass( 'on' );
        _$menuGroup.eq( idx ).find( '> a' ).addClass( 'on' );
        _$banners.hide().eq( sidx ).show();
        setBgColor( sidx );

        _currentIdx = sidx;
    }

    function setEvents () {
        //menu event
        /*_$menuGroup.find( 'a' ).on( 'focusin focusout', function (e) {
            var idx = $( e.currentTarget ).data( 'idx' ),
                sidx = $( e.currentTarget ).data( 'sidx' );

            if ( e.type === 'focusin' ) {
                _$menuGroup.eq( idx ).find( 'ul' ).show();
                activeBanner( sidx );
            } else {
                _$menuGroup.eq( idx ).find( 'ul' ).hide();
            }
        });*/
        _$menuGroup.on( 'mouseenter mouseleave', function (e) {
            var sidx = $( e.currentTarget ).find( '[data-sidx]:eq(0)' ).data( 'sidx' );

            if ( e.type === 'mouseenter' ) {
                $( e.currentTarget ).find( 'ul' ).show();
                activeBanner( sidx );
            } else {
                $( e.currentTarget ).find( 'ul' ).hide();
            }
        });
		 _$menus.on( 'mouseenter', function (e) {
            var sidx = $( e.currentTarget ).data( 'sidx' );

            if ( e.type === 'mouseenter' ) {
                activeBanner( sidx );
            }
        });

        _$next.on( 'click', function (e) {
            next();
        });

        _$prev.on( 'click', function (e) {
            prev();
        });

        _$play.on( 'click', function (e) {
            _isPlaying = true;
            _$pause.show();
            _$play.hide();			
			play();
        });

        _$pause.on( 'click', function (e) {
            _isPlaying = false;
            _$pause.hide();
            _$play.show();
            pause();
        });

        _$target.on( 'focusout mouseleave', function (e) {
            play();
        });

        _$target.on( 'focusin mouseenter', function (e) {
            pause();
        });
    }

    //bg 변경
    function setBgColor ( sidx ) {
        var bgColor = $( '#main_visual_color' + sidx ).val();
        if ( bgColor ) {
            $( '#main_visual_bg' ).css( 'background-color', '#' + bgColor );
        }
    }

    //기본 bg 세팅
    function setBackground () {
        var main_bg = $( '<div/>' ).attr( 'id', 'main_visual_bg' ).css( 'top', $('#container').offset().top );
        $( '#wrap' ).append( main_bg );
    }
});
/* //mainVisual 17.09.12  e */

/*20180620스카이스크래퍼 수정*/
$(function(){
	var targets=$(".sky_scrapper_banner.main, .sky_scrapper_new.main");
	if(targets.length>0){
		function scrappers(){
			var winT=$(window).scrollTop();
			var tag=$(".conWrap").offset().top-70;	
			if(winT > tag ){
				targets.addClass("mainFix");
			}
			else{
				targets.removeClass("mainFix");
			}
		}
		scrappers();
		$(window).on("scroll",scrappers);
	}
});
