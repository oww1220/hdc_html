
$(document).ready(function(){	
	'use strict';
	var browserVer = navigator.userAgent.toLowerCase();
	
	if( browserVer.indexOf("msie 7") !== -1 || browserVer.indexOf("msie 8") !== -1 || browserVer.indexOf("msie 9") !== -1  ){
		$("input[placeholder]").each(function(){
			var othis = $(this);
			var holder = othis.attr("placeholder");
			
			othis.val(holder);
			othis.on("focusin focusout change keypress",function(e){
				e = e || window.event;
				if(e.type === "focusin"){
					if($.trim(othis.val()) === holder){
						othis.val("").addClass("focus");
					}
				}else if(e.type === "focusout"){
					if($.trim(othis.val()) === ""){
						othis.removeClass("focus").val(holder);
					}
				}else{	
					othis.addClass("focus");								
				}
			});
			   
		});
	} 	
	
	/* top search */
	$("header .total_search input[type='text']").on("focusin blur keyup",function(e){
		e = e || window.event;
		var val = $.trim($(this).val());
		if(e.type === "focusin"){			
			if(val !== ""){
				$("#result_save").hide();
				$("#result_auto").show();
			}else{		
				$("#result_auto").hide();	
				$("#result_save").show();
			}
		}else if(e.type === "keyup"){
			if(val !== ""){
				$("#result_save").hide();
				$("#result_auto").show();
			}else{	
				$("#result_auto").hide();	
				$("#result_save").show();
			}		
		}else{
			
		}
	});
	$(".result_area .close_area .close").on("click",function(){
		$(this).parents(".result_area").hide();
	});
	$("#result_save .search_delete").on("click",function(){
		$(this).parents("li").remove();
	});
	$("#result_save .delete_all").on("click",function(){
		$(this).parents(".result_area").find(".search_word .search_save li").remove();
	});

	/* header */
	
	$(".navigation .category_view").hide();
	$(".navigation .shopin_menu li > a").removeClass("on");
	
	$(".navigation .category_view .view_list li").on("mouseover click",function(){
		$(".navigation .category_view .view_list li a").removeClass("on");
		$(this).find("a").addClass("on");
		$(".navigation .category_wrap").hide().find(".category_list").hide();
		$(this).find(".category_wrap").show().find(".category_list").show();
	});
	$(".navigation .category_view").add(".navigation .category_wrap").on("mouseleave focusout",function(e){
		e = e || window.event;	
		$(".navigation").find(".category_view").hide().end().find(".category_view .view_list li a").removeClass("on");
		$(".navigation .shopin_menu li > a").removeClass("on");
	});
	
	$(".navigation .shopin_menu li > a").on({
		"mouseenter" : function(e){
			var idx = $(this).parent().index();
			$(".navigation .shopin_menu li a.on").removeClass("on");
			$(this).addClass("on");
			if( idx == 0 ){
			   	$(".category_view").show();
				$(".brand_view").hide();
			}
			else if( idx == 1 ){
				$(".brand_view").show();
				$(".category_view").hide();
			}
			e.preventDefault();
		}
	});
	
	$(".brand_view").find(".btn_close").on("click", function(e){
		$(".brand_view").hide();
		$(".navigation .shopin_menu li a").removeClass("on");
		e.preventDefault();
	});


	// gnb 브랜드 내 액션
	$(".brand_view").find(".list_lang_wrap").eq(0).show();
	$(".brand_view .select_lang a").on("click",function(){
		var tabIdx = $(this).parent("li").index();
		$(".brand_view .select_lang li").removeClass("on");
		$(this).parent("li").addClass("on");
		$(".brand_view").find(".list_lang_wrap").hide();
		$(".brand_view").find(".list_lang_wrap").eq(tabIdx).show();
		return false;
	});

	var brandView = $("brand_view");
	var langListWrap = $(".list_lang_wrap");
	var btnlang = langListWrap.find(".list_lang li a");
	var brandList = $(".brand_list_wrap");
	var brandListIdx = 0;

	btnlang.on("click", function(){
		var brandListHeight = 0;
		var brandListScroll =0;
	
		$(this).parent("li").addClass("on").siblings().removeClass("on");
		brandListIdx = $(this).parent("li").index();
		for (var i =0; i<brandListIdx ; i++)
		{
			brandListHeight = $(this).parents(".list_lang_wrap").find(brandList).find(".brand_list").eq(i).outerHeight();
			brandListScroll = brandListScroll + brandListHeight;
		}
		brandList.scrollTop(brandListScroll);
		return false;
	});

	
	
	// footer 지점 바로 가기
	$(".foot_wrap .fgnb .link a").on("click", function(){
		if ($(this).parents(".link").hasClass("on"))
		{
			$(this).parents(".link").removeClass("on");
		} else {
			$(this).parents(".link").addClass("on");
		}
	});
});