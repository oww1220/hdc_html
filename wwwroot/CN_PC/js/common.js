/* 개발 반영시 삭제 [ 임시 ] */
var isLogin = "true";
var path = "";
var checkStrs = new Array();
checkStrs[0] = "_pc/";
checkStrs[1] = "_PC/";
for(var i=0; i<checkStrs.length; i++) {
	if(document.location.href.split(checkStrs[i]).length > 1 && document.location.href.split(checkStrs[i])[1].indexOf("/") > -1) {
		path = "../";
	}
}
document.write("<script type=\"text/javascript\" src=\""+path+"js/jquery.mCustomScrollbar.concat.min.js\"></s"+"cript><script type=\"text/javascript\" src=\""+path+"js/jquery-ui-Datepicker-1.11.2.js\"></s"+"cript>");
/* end : 개발 반영시 삭제 [ 임시 ] */


/* 개발 반영시 주석 제거 */
/*
if(typeof(commonResourcePath) == 'undefined'){
	commonResourcePath = '/estore/_ui/desktop/common';
}

var path = commonResourcePath+"/shilladfshome/cn/";
*/
/* end : 개발 반영시 주석 제거 */


$(document).ready(function(){	
	'use strict';	
	
	$(".datepicker").each(function(){
		var thisData = $(this).data();
		var _default = {
			minDateValue : "-0d",
			maxDateValue : null,
		}
		var _option = $.extend(_default, thisData);
		_option.yearRange = "c-100:c+10";
		
		if( $(this).hasClass("no-limit") ){
			_option.minDateValue = null;
			_option.maxDateValue = null;
		}
		
		if($(this).hasClass("birth-limit") ){
			_option.minDateValue = "-100y";
			_option.maxDateValue = "-14y";
			//_option.defaultDate = '-30y'; //30살기준
			_option.yearRange = "-100:-14";  // 기본노출 추가 14세부터  -100년
		}
		//기념일 기준
		if($(this).hasClass("anniversary-limit") ){
			_option.minDateValue = "-100y";
			_option.maxDateValue = "+100y";
			_option.yearRange = "-100:+100";  // 기본노출 추가 +- 100년
		}
		//출국일 기준
		if($(this).hasClass("departure-limit") ){
			_option.minDateValue = "-100y";
			_option.maxDateValue = "+30y";  
			_option.yearRange = "-100:+30";  // 기본노출 추가 -100년  부터 +30년
		}
		//고객의 소리 기준
		if($(this).hasClass("voc-limit") ){
			_option.minDateValue = "-100y";
			_option.maxDateValue = "-0d"; 
			_option.yearRange = "-100:+0";  // 기본노출 추가 작성일부터 -100년
		}

		$(this).datepicker({
			changeMonth:true,
			changeYear:true,
			showOn:'button',
			buttonImage:path+'img/common/ico_calendar.png',
			buttonImageOnly:true,
			langtit:"请选择日期",
			closeBtnShow : true,
			showMonthAfterYear:true,
			//minDate:'-0d',
			minDate: _option.minDateValue,  //최소 기간
			maxDate: _option.maxDateValue,  //최대 노출
			yearRange: _option.yearRange,   //노출되는 범위
			dayNamesMin:['日','一','二','三','四','五','六'],
			monthNamesShort: [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" ],
			dateFormat:'yy-mm-dd',
			defaultDate : _option.defaultDate
		}); 
		
	});
	
	$(document).on("click",".datepicker_close",function(){
		$.datepicker._hideDatepicker();
	});	
	
	// file upload
	$(".file_select_wrap").each(function(){
		var _this = $(this);
		var inputFile = _this.find("input[type='file']");
		var inputText = _this.find("input[type='text']");
		
		inputFile.on({
			"change" : function(){
				inputText.val($(this).val());
			}
		});
		
		_this.find(".delete").on({
			"click" : function(e){
				inputText.val("");
				inputFile.replaceWith(inputFile.clone(true))
				e.preventDefault();
			}
		});
	});
	
	// bookmark 개발적용시 주석처리 요망
	var bookmarkTitle 	= "新罗爱宝客免税店(购物之旅)";
	var bookmarkUrl	 	= "http://cn.shilladfs.com/";
	
	// bookmark 개발적용시 주석처리 요망 
	$(".popup_favoritepage .btn_m_red").click(function(e) {
		// IE
		if(document.all) {
			window.external.AddFavorite(bookmarkUrl,bookmarkTitle); 
		} 
		// safari,chrome,firefox
		else { 
			alert( "IE以外的浏览器不支援自动收藏网页功能.\n\n" + "请按 " + (navigator.userAgent.toLowerCase().indexOf("mac") != - 1 ? "Command/Cmd" : "Ctrl") + "+D 直接收藏本网页.");
		}
		e.preventDefault();
	});
	
	// placeholder
	var browserVer = navigator.userAgent.toLowerCase();
	if( browserVer.indexOf("msie 7") !== -1 || browserVer.indexOf("msie 8") !== -1 || browserVer.indexOf("msie 9") !== -1  ){
		$("input[placeholder] , textarea[placeholder]").each(function(){
			var othis 			= $(this);
			var holder			= othis.attr("placeholder");
			
			if( othis.data("placeholderType") == "text" ){
				var inputFontsize 	= parseInt(othis.css("fontSize"));
				var inputFontColor 	= othis.css("color");
				var inputHeight		= parseInt(othis.outerHeight(true));
				var inputWidth		= parseInt(othis.outerWidth(true));
				var posLeft 		= parseInt(othis.css("paddingLeft"));
				var posTop 			= parseInt((othis.outerHeight()-inputFontsize)/2);
				var textAlign 		= othis.css("textAlign");
				
				if( parseInt(othis.css("marginTop")) > 0 ){
					posTop = posTop + parseInt(othis.css("marginTop"));
				}
				if( parseInt(othis.css("marginLeft")) > 0 && textAlign == "left" ){
					posLeft	= posLeft + parseInt(othis.css("marginLeft"));
				}
				if( parseInt(othis.css("marginRight")) > 0 && textAlign == "right" ){
					posLeft	= posLeft + parseInt(othis.css("marginRight"));
				}
				var inputWrap 		= "<div class='PH_wrap' style='position:relative; display:inline-block; width:"+ inputWidth +"px; height:"+ inputHeight +"px;' ></div>";
				var placeholder 	= "<span class='PH_replace' style='position:absolute; margin:0; padding:0; top:"+ posTop +"px; "+ (textAlign == "right" ? "right" : "left") +":"+ posLeft +"px; color:#bbb;'>"+ holder +"</span>";
				
				othis.wrap(inputWrap);
				othis.parent("div").append(placeholder);
				
				if( othis.val() ){
					othis.next().hide();
				}
				
				othis.on("focusin focusout",function(e){
					e = e || window.event;
					if(e.type == "focusin"){
						if($.trim(othis.val()) == "" ){
							othis.next().hide();
						}
					}else if(e.type == "focusout"){
						if($.trim(othis.val()) == ""){
							othis.next().show();
						}
					}else{	
						othis.addClass("focus");								
					}
				});
				
				othis.next().on({
					"click" : function(){
						othis.trigger("focusin").focus();
					}
				});
			} 
			else {
				othis.val(holder);
				othis.on("focusin focusout",function(e){
					e = e || window.event;
					if(e.type === "focusin"){
						if($.trim(othis.val()) === holder){
							othis.val("").addClass("focus");
						}
					}else if(e.type === "focusout"){
						if($.trim(othis.val()) === ""){
							othis.removeClass("focus").val(holder);
						}
					}else{	
						othis.addClass("focus");								
					}
				});
			}
		});
	}
	
	/* initial layer */
	$(".initial_layer").each(function(){
		layerpopup($(this).attr("id"));
	});
	
	/* top search */
	var totalSearchInput = $("header .total_search input[type='text']");
	var totalSearchArea = $("header .total_search .result_area");
	
	$("body").on({
		"click" : function(){
			if( totalSearchInput.data("focus") == "off" && totalSearchArea.data("mouse") == "off" ){
				$(".total_search .result_area").hide();
			}
		}
	});
	
	totalSearchArea.data("mouse" , "off").on({
		"mouseenter" : function(){
			$(this).data("mouse", "on");	
		}, 
		"mouseleave" : function(){
			$(this).data("mouse", "off");	
		}
	});
	
	// 개발 적용시 주석처리 요망.
	totalSearchInput.data("focus" , "off").on("focusin focusout keyup",function(e){
		e = e || window.event;
		var val = $.trim($(this).val());
		if(e.type === "focusin"){
			$(this).data("focus", "on");
			if(val !== ""){
				$("#result_save").hide();
				$("#result_auto").show();
			}else{		
				$("#result_auto").hide();	
				$("#result_save").show();
			}
		}else if(e.type === "keyup"){
			if(val !== ""){
				$("#result_save").hide();
				$("#result_auto").show();
			}else{	
				$("#result_auto").hide();	
				$("#result_save").show();
			}		
		}else if(e.type === "focusout"){
			$(this).data("focus", "off");
		}
	});
	
	// 개발 적용시 주석처리 요망.
	$(".result_area .close_area .close").on("click",function(){
		$(this).parents(".result_area").hide();
	});
	
	$("#result_save .search_delete").on("click",function(){
		$(this).parents("li").remove();
	});
	
	$("#result_save .delete_all").on("click",function(){
		$(this).parents(".result_area").find(".search_word .search_save li").remove();
	});
	
	// path indicator
	var pathIndicator = $(".path");
	
	pathIndicator.find(".selectbtn").on({
		"mouseenter , focusin" : function(){
			$(this).next().show();
		}
	});
	
	//pathIndicator.find(".selectwrap").each(function(){
		//var listW = $(this).find(".selectlist").outerWidth(true);
		//$(this).width(listW - 22);		
	//});
	
	pathIndicator.find(".selectwrap").on({
		"mouseleave" : function(){
			$(this).find(".selectlist").hide();
		}
	});
	
	pathIndicator.find(".selectlist > li a").on({
		"click" : function(e){
			var _this = $(this);
			var text = _this.text();
			_this.parents(".selectlist").hide().prev().text(text);
			e.preventDefault();
		}
	});
	
	// sns
	$(".sns_wrap").find(".btn_togle").on({
		"click" : function(e){
			var _this = $(this);
			if( _this.hasClass("on") ){
				_this.removeClass("on");
				$(".sns_wrap").find(".sns").height(26);
			} else {
				_this.addClass("on");
				$(".sns_wrap").find(".sns").height("auto");
			}
			e.preventDefault();
		}
	});
	
	// CheckBox & RadioButton Design	
	checkboxChecked();
	labelChecked();
	$(document).on("click change",".css-checkbox", function(){
		var bp = 0;
		var othis = $(this);

		bp = othis.is(":checked") ? -50 : 0;
		
		if( othis.parents(".tit_top").size() > 0 ){
			bp = othis.is(":checked") ? -75 : -20;
		}
		
		if(othis.attr("class").indexOf("css-checkbox-") > -1){	
			var color = othis.attr("class").split("css-checkbox-")[1];
			switch(color) {
				case "red":
					bp = othis.is(":checked") ? -325 : -300;
				break;
				case "orange":
					bp = othis.is(":checked") ? -375 : -350;
				break;		
				case "yellow":
					bp = othis.is(":checked") ? -425 : -400;
				break;		
				case "lemon":
					bp = othis.is(":checked") ? -475 : -450;
				break;		
				case "lime":
					bp = othis.is(":checked") ? -525 : -500;
				break;		
				case "green":
					bp = othis.is(":checked") ? -575 : -550;
				break;		
				case "skyblue":
					bp = othis.is(":checked") ? -625 : -600;
				break;		
				case "blue":
					bp = othis.is(":checked") ? -675 : -650;
				break;		
				case "darkblue":
					bp = othis.is(":checked") ? -725 : -700;
				break;		
				case "white":
					bp = othis.is(":checked") ? -775 : -750;
				break;		
				case "gray":
					bp = othis.is(":checked") ? -825 : -800;
				break;
				case "disable":
					bp = othis.is(":checked") ? -850 : 0;
				break;	
				case "all":
					bp = othis.is(":checked") ? -75 : -20;					
				break;	
				case "compare":
					bp = othis.is(":checked")  ? -920 : -880;
				break;	
			}
		}	
		if(othis.hasClass("css-checkbox2")){
			bp = othis.is(":checked")  ? -250 : -200;
		}
		if(othis.parents("table").length > 0 && othis.parents("tr").hasClass("disable")){return false;}
		othis.next("label").css("background-position","0 "+bp+"px");
	});	
	radioChecked();
	$(document).on("click",".css-radiobox", function() {
		if($(this).is(":checked")) {
			$("input[name='"+$(this).attr("name")+"']").siblings("label").css("background-position", "0 -100px");
			$(this).next("label").css("background-position", "0 -150px");
		}
	});
	
	// CheckBox Design2

	/* header */
	$("header .unb li").on("mouseover mouseleave focusin focusout",function(e){
		e = e || window.event;
		if(e.type === "mouseover" || e.type === "focusin"){
			$(this)	.find(".qrimg").show();
		}else{
			$(this).find(".qrimg").hide();
		}		
	});	
	$(".cn_view_list > li").each(function(){
		$(this).find(".cn_dept_arrow").css("top",$(this).offset().top-150);
	}).on("mouseover",function(){
		$(this).find(".cn_dept_arrow").css("top",$(this).offset().top-150);	
	});
	
	if($("#main_visual").size() > 0){
		$(".navigation .cn_category_view").show();//2016-12-27 수정
	} else {
		$(".cn_shop_duty").removeClass("main");//2016-12-27 수정
		$(".navigation .cn_category_view").hide();//2016-10-17 수정
	}

	/* 2016-10-10 추가 */
	$(".navigation .cn_category_view .cn_view_list > li").on("mouseover click",function(){
		$(".navigation .cn_shopin_menu .cn_shop_duty > a").addClass("on");//2016-12-27 수정
		$("nav .cn_category_view .cn_view_list > li > a").removeClass("on");
		$(this).find("> a").addClass("on");
		$(".navigation .cn_dept_wrap").hide();
		$(this).find(".cn_dept_wrap").show();
	});
	$(".navigation .cn_category_view").on("mouseleave",function(e){
		e = e || window.event;
			
		if( $("#main_visual").size() > 0 ){
			$(".navigation .cn_category_view").show();//2016-12-27 수정
			$(".navigation").find(".cn_dept_wrap").hide().end().find(".cn_category_view .cn_view_list > li > a").removeClass("on");
			$(".navigation .cn_shopin_menu li.cn_shop_duty.main > a").removeClass("on");
		} else {
			$(".navigation .cn_category_view").hide();//2016-12-27 수정
			$(".navigation .cn_dept_wrap").hide().end().find(".cn_category_view .cn_view_list > li > a").removeClass("on");//2016-10-17 수정
			$(".navigation .cn_shopin_menu > li > a").removeClass("on");
		}
	});

	$(".navigation .cn_shopin_menu > li > a").on({
		"mouseenter" : function(e){
			var idx = $(this).parent().index();	
			$(".navigation .cn_shopin_menu > li > a.on").removeClass("on");//2016-12-27 수정
			$(this).addClass("on");		
			if( idx == 0 ){
			   	$(".cn_category_view").show().find(".cn_view_list").show();//2016-12-27 수정
				$(".cn_brand_view").hide();
			}
			else if( idx == 1 ){
				$(".cn_brand_view").show();
				//2016-12-27 수정
				if( $("#main_visual").size() < 1 ){
					$(".cn_category_view").hide();
				}
				//2016-12-27 수정
				
			}
			e.preventDefault();
		}
	});
	
	$(".cn_brand_view").on("mouseleave", function(e){
		$(".cn_brand_view").hide();
		$(".navigation .cn_shopin_menu > li > a").removeClass("on");
		
		if( $("#main_visual").size() > 0 ){
			$(".cn_brand_view").hide();
			$(".cn_category_view").show();
		} 
		e.preventDefault();
	});
	
	$(".header_top").on({
		"mouseenter" : function(){
			$(".cn_brand_view").trigger("mouseleave");	
			$(".navigation .cn_category_view").trigger("mouseleave");
		}
	});

	/* 2016-10-19 수정 */
	$(".cn_shop_duty").on({
		"mouseenter" : function(){
			$(".navigation .cn_category_view .cn_view_list").find(">li:first-child > a").trigger("mouseenter");
		},
		"mouseleave" : function(){
			if( $("#main_visual").size() > 0 ){
				$(".cn_category_view").show();
				$(".navigation").find(".cn_dept_wrap").hide().end().find(".cn_category_view .cn_view_list > li > a.on").removeClass("on");//2016-12-27 수정
			} 
			//$(".cn_category_view").hide();//2016-12-27 수정
			//$(".navigation .cn_shopin_menu li a").removeClass("on");//2016-12-27 수정
						
			e.preventDefault();
		}
	});
	
	$(".cn_gnb_menu").on({
		"mouseenter" : function(){
			$(".cn_brand_view").trigger("mouseleave");	
			$(".navigation .cn_category_view").trigger("mouseleave");
		}
	});
	/* //2016-10-19 수정 */
	/* 2016-10-10 추가 */
	
	/* gnb in out */
	/*
	$(".cn_shop_duty a").on("mouseover mouseleave keydown",function(e){
		e = e || window.event;		
		if(e.type === "mouseover"){
			$(".cn_shop_duty").addClass("main");
			$(".cn_category_view").show();
			$(".cn_brand_view").hide();
		}else if(e.keyCode === 13){
			if($(".cn_shop_duty").hasClass("main")){
				if($(document).find(".main_wrap").length > 0){return false;}
				$(".cn_category_view").hide();
				$(".cn_shop_duty").removeClass("main");
			}else{
				$(".cn_shop_duty").addClass("main");
				$(".cn_category_view").show();		
			}
		}else if(e.keyCode === 9 && !(e.shiftKey)){
			if($(".cn_shop_duty").hasClass("main")){
				$(".cn_view_item01").find("a").eq(0).focus().on("keydown",function(e){
					e = e || window.event;		
					if(e.keyCode === 9 && e.shiftKey){
						$(".cn_shop_duty a").focus();
						setTimeout(function(){$(".cn_shop_duty a").focus();},50);
					}
				});
				setTimeout(function(){$(".cn_view_item01").find("a").eq(0).focus();},50);
			}
		}else if(e.pageY < 163){
			if($(document).find(".main_wrap").length > 0){return false;}
			$(".cn_category_view").hide();
			$(".cn_shop_duty").removeClass("main");
		}		
	});
	$(document).on("keydown",".cn_view_item07 .cn_sub_menu a:last",function(){
		$(".cn_shop_product a:first").focus().on("keydown",function(e){
			e = e || window.event;		
			if(e.keyCode === 9 && !(e.shiftKey)){
				$(".cn_shop_duty a").focus();
			}
		});
		setTimeout(function(){$(".cn_shop_product a:first").focus();},50);
		if($(document).find(".main_wrap").length > 0){return false;}
		$(".cn_category_view").hide();
		$(".cn_shop_duty").removeClass("main");
	});
	
	//$(".cn_sort_tab_contents").mCustomScrollbar(); 
	$(".cn_category_view").on("mouseleave",function(){
		if($(document).find(".main_wrap").length > 0){return false;}
		$(".cn_category_view").hide();
		$(".cn_shop_duty").removeClass("main");
	});
	
	$(".cn_category_view").on("mouseenter",function(){
		if($(document).find(".main_wrap").length > 0){return false;}
		$(".cn_category_view").show();
		$(".cn_shop_duty").addClass("main");
	});
	
	$(".cn_shop_product a").on("mouseenter mouseleave keydown",function(e){
		e = e || window.event;		
		if(e.type === "mouseenter"){
			$(".cn_brand_view").addClass("on");
			$(".cn_shop_product").addClass("on");
			$(".cn_shop_duty").addClass("off");
		}else if(e.keyCode === 13){
			if($(".cn_brand_view").hasClass("on")){
				$(".cn_brand_view").removeClass("on");
				$(".cn_shop_product").removeClass("on");
			}else{
				$(".cn_brand_view").addClass("on");
				$(".cn_shop_product").addClass("on");	
			}
		}else if(e.keyCode === 9 && !(e.shiftKey)){
			if($(".cn_brand_view").hasClass("on")){
				$(".cn_brand_view").find("a").eq(0).focus().on("keydown",function(e){
					e = e || window.event;		
					if(e.keyCode === 9 && e.shiftKey){
						$(".cn_shop_product a").focus();
						setTimeout(function(){$(".cn_shop_product a").focus();},50);
					}
				});
				setTimeout(function(){$(".cn_brand_view").find("a").eq(0).focus();},50);
			}
		}else if(e.pageY < 163){
			$(".cn_brand_view").removeClass("on");
			$(".cn_shop_product").removeClass("on");
			$(".cn_shop_duty").removeClass("off");
		}		
	});
	$(".cn_brand_view").on("mouseleave",function(){
		$(".cn_brand_view").removeClass("on");
		$(".cn_shop_product").removeClass("on");
		$(".cn_shop_duty").removeClass("off");
	});
	
	$(".cn_brand_view").on("mouseenter",function(){
		$(".cn_brand_view").addClass("on");
		$(".cn_shop_product").addClass("on");
		$(".cn_shop_duty").addClass("off");
	});*/
	
	function cnGnbAlphabetValidate(){
        $(".cn_brand_view .cn_sort_cont a").each(function(k,v){
            var indiAlpha = $(this).text();
            var hasBrand = false;
            $(".cn_tab_cont.on em").each(function(k,v){
                if(indiAlpha == $(this).text()){
                    hasBrand = true;
                }
            })
            if(hasBrand){
                $(this).removeClass("disabled");
            }else{
                $(this).addClass("disabled");
            }
        });
        $(".cn_brand_view .cn_sort_cont a").removeAttr("data-index");
		
		$(".cn_brand_view .cn_sort_cont a").filter(":not('.disabled')").each(function(i){
            $(this).attr("data-index" , i);
        });
    }
	
	function cnGnbAlphabetValidateCategory(){
		$(".cn_category_view .cn_brand_area").each(function(){
			var _this = $(this);
			
			_this.find(".cn_sort_cont a").each(function(k,v){
				var indiAlpha = $(this).text();
				var hasBrand = false;
				_this.find(".cn_sort_tab_contents.on .cn_brand_part").each(function(k,v){
					if(indiAlpha == $(this).find(".part_tit").text()){
						hasBrand = true;
					}
				})
				if(hasBrand){
					$(this).removeClass("disabled");
				}else{
					$(this).addClass("disabled");
				}
			});
			_this.find(".cn_sort_cont a").removeAttr("data-index");
			
			_this.find(".cn_sort_cont a").filter(":not('.disabled')").each(function(i){
				$(this).attr("data-index" , i);
			});
		});
    }
	
    cnGnbAlphabetValidate();
	cnGnbAlphabetValidateCategory();
	
	$(".cn_brand_view .cn_brand_sort .cn_sort_list a").on("click",function(e){
		var target = $(this).parents(".cn_brand_view");
		
        if($(this).hasClass("on")){return false;}
        $(this).parents(".cn_sort_list").find("li").removeClass("on");
        $(this).parents("li").addClass("on");
        $(".cn_brand_part_gp").removeClass("on");
        $(".cn_brand_part_gp").eq($(".cn_brand_view .cn_brand_sort .cn_sort_list a").index(this)).addClass("on");
		
		if( $(this).parent().index() == 0 ){
			$(this).parents(".cn_sort_list").next().find(".etc").text("etc");
		} else {
			$(this).parents(".cn_sort_list").next().find(".etc").text("其他");
		}
		
        cnGnbAlphabetValidate();
		
		target.find(".cn_sort_cont a").removeClass("select");
		target.find(".cn_brand_part_gp").animate({scrollTop : 0}, 300);
		
		e.preventDefault();
    });
	
	$(".cn_brand_view .cn_sort_cont a").on("click",function(e){
		if(! $(this).hasClass("disabled") ){
			var _this = $(this);
			var brandListIdx = $(this).attr("data-index");
			var brandListMarginTop = parseInt($(".cn_brand_view .cn_brand_part").eq(brandListIdx-1).css("marginTop"));
			var targetCont, offsetY;
			
			_this.siblings("a").removeClass("select");
			_this.addClass("select");
			if( $(".cn_sort_list").find("li.on").index() == 0 ){
				targetCont = $(".cn_tab_cont.en_sort");
			} else {
				targetCont = $(".cn_tab_cont.cn_sort");
			}
			
			var brandListScrollArry = [0];
			targetCont.find(".cn_scroll > div:not('.all')").each(function(i){
				brandListScrollArry.push( brandListScrollArry[i] + $(this).outerHeight(true) );
			});
			
			offsetY = brandListScrollArry[brandListIdx] + brandListMarginTop;
			targetCont.animate({ scrollTop : offsetY }, 300);
		}
		e.preventDefault();
	});
	
	$(".cn_category_view .cn_dept_list a.title").on("click",function(){
		var othis = $(this);
		if(othis.hasClass("on")){return false;}
		othis.parents(".cn_dept_list")
			.find("a.title").removeClass("on").end()
			.find(".cn_cata_detail").removeClass("on").end().end()
			.addClass("on");
		var ohref = othis.attr("href");
		$(ohref).addClass("on");
		return false;
	});	
	
	$(".cn_category_view .cn_brand_sort .cn_sort_list a").on("click",function(e){
		var target = $(this).parents(".cn_brand_area");
		
		if($(this).hasClass("on")){return false;}
		$(this).parents(".cn_sort_list").find("a").removeClass("on");
		$(this).addClass("on");
		$(this).parents(".cn_brand_area").find(".cn_sort_tab_contents").removeClass("on");
		$(this).parents(".cn_brand_area").find(".cn_sort_tab_contents").eq($(this).parent().index()).addClass("on");
		
		if( $(this).parent().index() == 0 ){
			$(this).parents(".cn_sort_list").next().find(".etc").text("etc");
		} else {
			$(this).parents(".cn_sort_list").next().find(".etc").text("其他");
		}
		
		cnGnbAlphabetValidateCategory();
		
		target.find(".cn_sort_cont a").removeClass("select");
		target.find(".cn_sort_tab_contents").animate({scrollTop : 0}, 300);
		
		e.preventDefault();
	});
	
	$(".cn_category_view .cn_sort_cont a").on("click",function(e){
		
		if(! $(this).hasClass("disabled") ){
			var _this = $(this).parents(".cn_brand_area");
			var brandListIdx = $(this).attr("data-index");
			var brandListMarginTop = parseInt($(".cn_category_view .cn_brand_part").eq(brandListIdx-1).css("marginBottom"));
			var targetCont, offsetY;
			
			$(this).siblings("a").removeClass("select");
			$(this).addClass("select");
			targetCont = _this.find(".cn_sort_tab_contents.on");
			
			var brandListScrollArry = [0];
			
			targetCont.find("> div").each(function(i){
				brandListScrollArry.push( brandListScrollArry[i] + $(this).outerHeight(true) );
			});
			
			offsetY = brandListScrollArry[brandListIdx]// + brandListMarginTop;
			targetCont.animate({ scrollTop : offsetY }, 300);
		}
		e.preventDefault();
	});
	/* //header  */
	
	/* footer */
	$(".family_site").on({
		"change" : function(){
			window.open($(this).val(), "_blank");
		}
	});
	/* end : footer */
	
	/* order */
	// product amount check
	productAmountCheck();
	
	$(document).on("click",".plus",function(e){
		var $amountInput 	= $(this).siblings(".pcount");
		var minAmount 		= Number($amountInput.siblings(".minorder").val()) || 1;
		var maxAmount 		= Number($amountInput.siblings(".maxorder").val()) || 999999;
		var packageAmount 	= Number($amountInput.siblings(".ordercount").val()) || 1;
		var checkedAmount 	= Number($amountInput.val());
	
		if ( minAmount > packageAmount ){
			if( (minAmount % packageAmount) > 0 ){
				minAmount = ( minAmount + (packageAmount - (minAmount % packageAmount)));
			} else {
				minAmount = minAmount;
			}
		} else {
			minAmount = packageAmount;
		}
		
		if( maxAmount > packageAmount ){
			if ( (maxAmount % packageAmount) > 0 ){
				maxAmount = ( maxAmount - (maxAmount % packageAmount ));
			} else {
				maxAmount = maxAmount /* - packageAmount;  */
			}
		} else {
			maxAmount = maxAmount;
		}
	
		if( checkedAmount % packageAmount > 0 ){
			checkedAmount = checkedAmount - (checkedAmount % packageAmount);
		}
		
		if( checkedAmount < minAmount ){
			checkedAmount = minAmount;
		} else if( checkedAmount >= maxAmount ){
			checkedAmount = maxAmount;
			alert("该商品最多"+maxAmount+"个为止可订购。");
		} else {
			for( var i=0; i < packageAmount; i++ ){
				checkedAmount ++;
			}
		} 
		
		$amountInput.val(checkedAmount);
		$amountInput.data("prevCount" , checkedAmount);
		
		$("[id^='addToCartBtn']").data("quantity", checkedAmount); //카트 버튼에 개수 추가 / sds 추가
		$("[id^='buynowBtn']").data("quantity", checkedAmount); 	// 바로구매 버튼에 개수 추가 / sds 추가
		
		var _countConfirmBtn = $(this).parents(".amount_box").find(".pconut_submit");
		var currentCount = Number($(this).siblings(".pcount").data("currentCount"));
		if( _countConfirmBtn.length){
			
			if( Number($(this).siblings(".pcount").val()) != currentCount  ){
				_countConfirmBtn.filter(".btn_on").css({ "display" : "block" });
				_countConfirmBtn.filter(".btn_off").css({ "display" : "none" });
			} else {
				_countConfirmBtn.filter(".btn_on").css({ "display" : "none" });
				_countConfirmBtn.filter(".btn_off").css({ "display" : "block" });
			}	
		}	
		e.preventDefault();
	});

	$(document).on("click",".minus",function(e){
		var $amountInput 	= $(this).siblings(".pcount");
		var minAmount 		= Number($amountInput.siblings(".minorder").val()) || 1;
		var maxAmount 		= Number($amountInput.siblings(".maxorder").val()) || 999999;
		var packageAmount 	= Number($amountInput.siblings(".ordercount").val()) || 1;
		var checkedAmount 	= Number($amountInput.val());
		var currentAmount	= Number($amountInput.val());
		
		
		// min,max 유효성 체크
		if ( minAmount > packageAmount ){
			if( (minAmount % packageAmount) > 0 ){
				minAmount = ( minAmount + (packageAmount - (minAmount % packageAmount)));
			} else {
				minAmount = minAmount;
			}
		} else {
			minAmount = packageAmount;
		}
		
		if( maxAmount > packageAmount ){
			if ( (maxAmount % packageAmount) > 0 ){
				maxAmount = ( maxAmount - (maxAmount % packageAmount ));
			} else {
				maxAmount = maxAmount /* - packageAmount;  */
			}
		} else {
			maxAmount = maxAmount;
		}
		
		if( checkedAmount % packageAmount > 0 ){
			checkedAmount = checkedAmount - (checkedAmount % packageAmount);
		}
		
		if( currentAmount <= maxAmount ){
			for( var i=0; i < packageAmount; i++ ){
				checkedAmount --;
			}
		}
		
		if( checkedAmount < minAmount ){
			alert("该商品最少"+minAmount+"个为止可订购。");
			checkedAmount = minAmount;
		} else if( checkedAmount > maxAmount ){
			checkedAmount = maxAmount;
		} 
		
		$amountInput.val(checkedAmount);
		$amountInput.data("prevCount" , checkedAmount);
		
		$("[id^='addToCartBtn']").data("quantity", checkedAmount); //카트 버튼에 개수 추가 / sds 추가
		$("[id^='buynowBtn']").data("quantity", checkedAmount); 	// 바로구매 버튼에 개수 추가 / sds 추가
		
		var _countConfirmBtn = $(this).parents(".amount_box").find(".pconut_submit");
		var currentCount = Number($(this).siblings(".pcount").data("currentCount"));
		
		if( _countConfirmBtn.length){
			if( Number($(this).siblings(".pcount").val()) != currentCount ){
				_countConfirmBtn.filter(".btn_on").css({ "display" : "block" });
				_countConfirmBtn.filter(".btn_off").css({ "display" : "none" });
			} else {
				_countConfirmBtn.filter(".btn_on").css({ "display" : "none" });
				_countConfirmBtn.filter(".btn_off").css({ "display" : "block" });
			}	
		}	
		
		e.preventDefault();
	});
	
	$(document).on("change keyup", ".pcount", function(e){
		var $amountInput 	= $(this);
		var minAmount 		= Number($amountInput.siblings(".minorder").val()) || 1;
		var maxAmount 		= Number($amountInput.siblings(".maxorder").val()) || 999999;
		var packageAmount 	= Number($amountInput.siblings(".ordercount").val()) || 1;
		var checkedAmount 	= Number($amountInput.val());
		var _countConfirmBtn = $amountInput.parents(".amount_box").find(".pconut_submit");
		var currentCount 	= Number($amountInput.data("currentCount"));

		// min,max 유효성 체크
		if ( minAmount > packageAmount ){
			if( (minAmount % packageAmount) > 0 ){
				minAmount = ( minAmount + (packageAmount - (minAmount % packageAmount)));
			} else {
				minAmount = minAmount;
			}
		} else {
			minAmount = packageAmount;
		}
		
		if( maxAmount > packageAmount ){
			if ( (maxAmount % packageAmount) > 0 ){
				maxAmount = ( maxAmount - (maxAmount % packageAmount ));
			} else {
				maxAmount = maxAmount; 
			}
		} else {
			maxAmount = packageAmount;
		}
		
		if( e.type == "change" ){
			if( checkedAmount < minAmount ){
				alert("该商品最少"+minAmount+"个为止可订购。");
				$amountInput.val(minAmount);
				$amountInput.data("prevCount", minAmount);
			}
			else  if( checkedAmount > maxAmount ){
				alert("该商品最多"+maxAmount+"个为止可订购。");
				$amountInput.val(maxAmount);
				$amountInput.data("prevCount", maxAmount);
			}
			else if( (checkedAmount % packageAmount) > 0 ){
				alert("您选择的此项商品只能以绑定数量为单位做数量变更。");
				$amountInput.val($amountInput.data("prevCount"));
			} else {
				$amountInput.data("prevCount", checkedAmount);
			}
			$("[id^='addToCartBtn']").data("quantity", Number($amountInput.val())); //카트 버튼에 개수 추가 / sds 추가
			$("[id^='buynowBtn']").data("quantity", Number($amountInput.val())); 	// 바로구매 버튼에 개수 추가 / sds 추가
		}
		
		if( e.type == "keyup" ){
			if( isNumber($(this).val()) == false ){
				$amountInput.val($amountInput.data("prevCount"));
			}	
		}
		
		if( _countConfirmBtn.length){
			if( Number($amountInput.val()) != currentCount  ){
				_countConfirmBtn.filter(".btn_on").css({ "display" : "block" });
				_countConfirmBtn.filter(".btn_off").css({ "display" : "none" });
			} else {
				_countConfirmBtn.filter(".btn_on").css({ "display" : "none" });
				_countConfirmBtn.filter(".btn_off").css({ "display" : "block" });
			}	
		}
	});
	
	/* 직항 경유 */
	$(".direct .chk_list input[id^=radio]").on("change" , function(){ // sds 추가 // 2017-01-20 클래스명 변경
		var othis = $(this).parents(".chk_list");
		if(othis.find("input[type='radio']").eq(1).is(":checked")){
			othis.siblings("select").show().prop("disabled" , false);
			alert("携带液体类或凝胶类等个人物品请准备透明塑料袋，若想确认更详细的信息，请查阅机内限制携带物品清单。");
			$(".viaGuide").show();
			$(".directGuide").hide();
		}else{
			$(".direct .select_via").find("option").eq(0).prop("selected" , true)
			$(".direct .or_txt").hide();
			$(".viaGuide").hide();
			$(".directGuide").show();
			othis.siblings("select").hide();
		}
	});	
	 
	function paymentSelectViaGuideTextShow(othis){
		var othisval = othis.val();
		var msg = "不可购买液体类、凝胶类商品。\n 请再次确认所订购的商品";
		
		if(othisval === "2" || othisval === "8"){
			msg = "最终目的地为美国、澳大利亚的情况下，不可购买液体类、凝胶类商品。\n 请再次确认所订购的商品。";
		} else if(othisval === "10"){
			msg = "以托运方式处可购买液体类、凝胶类产品";
		}
	}
	
	$(".payment_wrap .select_via").on("change",function(){
		var msg = "不可购买液体类、凝胶类商品。\n 请再次确认所订购的商品";
		paymentSelectViaGuideTextShow($(this));
		alert(msg);	
	});

	/* //order */
	
	/* sky_scrapper */
	/*
	function sky_scrapper(){
		var screenW = $(window).width();
		if(screenW > 1600 && !$(".sky_scrapper").hasClass("sky_scrapper_on")){
			$(".sky_scrapper").css("width","304px").addClass("sky_scrapper_on");
		}else if(screenW < 1600 && !$(".sky_scrapper").hasClass("sky_scrapper_on")){
			$(".sky_scrapper").css("width","44px").removeClass("sky_scrapper_on");	
		}
	}
	*/
	
	//sky_scrapper();
	$(window).resize(function(){	
		//sky_scrapper();
	});
	
	$(".sky_scrapper .today_list_sky .area").height($(window).height() - 98)
	.mCustomScrollbar({
		//theme:"minimal"
	});
	
	$(".sky_scrapper .cart_list_sky .area").height($(window).height() - 169)
	.mCustomScrollbar({
		//theme:"minimal"
	});
	
	$(".sky_scrapper .my_info .area").height($(window).height() - 40)
	.mCustomScrollbar({
		//theme:"minimal"
	});
	
	/* $(".sky_scrapper .mCSB_scrollTools").each(function(){
		$(this).height($(this).parents(".mCustomScrollBox").height() -20).css("top","10px");
	});	 */
	
	if($(".sky_scrapper .cart_list_sky .area").find("li").length < 1){
		$(".sky_scrapper .cart_list_sky .area").find("ul").hide().end().find(".null").show();
	}
	
	if($(".sky_scrapper .today_list_sky .area").find("li").length < 1){
		$(".sky_scrapper .today_list_sky .area").find("ul").hide().end().find(".null").show();
	}
	
	var skyScrapperActiveIdx = -1;
	$(".sky_scrapper .menu ul a").on("click",function(){
		skyScrapperActiveIdx = $(this).parent().index();
		var menu = Number($(this).parents("li").attr("class").split("m")[1]);
		if (menu === 1 && (typeof(isLogin) == 'undefined' || isLogin != "true")){ /*개발 수정 */
			$(".sky_scrapper .pop_newwin").show();
			ACC.customer.openLoginPopup();
			return false;	
		}
		$(".sky_scrapper .pop_newwin").hide();
		$(".sky_scrapper")
			.find(".cont > div").hide().eq(menu-1).show();
			if($(".sky_scrapper").find(".cont > div").eq(menu-1).find(".mCustomScrollBox:visible").length > 0){
				$(".sky_scrapper").find(".cont > div").eq(menu-1).find(".mCSB_scrollTools").height($(".sky_scrapper").find(".cont > div").eq(menu-1).find(".mCustomScrollBox").height() -20).css("top","10px");
			}
		$(".sky_scrapper").animate({"width":304},{duration:300,complete:function(){
			$(".sky_scrapper").addClass("sky_scrapper_on");				
		}});
		$(".sky_scrapper .menu .btn_top").animate({"right":260},{duration:300});
		$(".sky_scrapper .menu ul a").removeClass("active");
		$(this).addClass("active");
	});
	
	$(".sky_scrapper .my_info .notice_more").on({
		"click" : function(e){
			var _this = $(this);
			if( _this.hasClass("on") ){
				_this.removeClass("on").find("span").text("更多");
				$(".sky_scrapper .my_info .notice_txt").removeClass("on");
			} else {
				_this.addClass("on").find("span").text("关闭");
				$(".sky_scrapper .my_info .notice_txt").addClass("on");
			}
			e.preventDefault();
		}
	});

	setTimeout(function(){
		$(".sky_scrapper .pop_newwin").fadeOut();
	} , 20000);

	$(".sky_scrapper .pop_newwin .login h4 a").on("click",function(){
		$(".sky_scrapper .pop_newwin").hide();
	});	
	$(".sky_scrapper .menu .btn_toggle .close").on("click",function(){
		if( $(".sky_scrapper").hasClass("sky_scrapper_on") ){
			$(this).attr("title","open");
			$(".sky_scrapper").animate({"width":44},{duration:300,complete:function(){
				$(".sky_scrapper").removeClass("sky_scrapper_on").find(".cont > div").hide();
			}});
			$(".sky_scrapper .menu .btn_top").animate({"right":0},{duration:300});
			$(".sky_scrapper .menu ul a").removeClass("active");
		} else {
			$(this).attr("title","close");
			if(skyScrapperActiveIdx < 0 ){
				skyScrapperActiveIdx = 2;
				$(".sky_scrapper").find(".cont > div").eq(skyScrapperActiveIdx).show().find(".mCSB_scrollTools").height($(".sky_scrapper").find(".cont > div").eq(skyScrapperActiveIdx).find(".mCustomScrollBox").height() -20).css("top","10px");;
				$(".sky_scrapper .menu li").eq(skyScrapperActiveIdx).find("a").addClass("active");
			}
			
			if(skyScrapperActiveIdx == 0 && isLogin != "true"){
				skyScrapperActiveIdx = 2; /* 개발 수정 */
			} 
			
			$(".sky_scrapper").find(".cont > div").eq(skyScrapperActiveIdx).show();
			$(".sky_scrapper .menu li").eq(skyScrapperActiveIdx).find("a").addClass("active");
			$(".sky_scrapper").animate({"width":304},{duration:300,complete:function(){
				$(".sky_scrapper").addClass("sky_scrapper_on");	
				setTimeout(function(){
					$(".sky_scrapper .today_list_sky .area").height($(window).height() - 98).mCustomScrollbar({});
				} , 3000)
			}});
		}
	});
	
	$(".sky_scrapper .menu .btn_top a").on("click",function(){
		$("html,body").animate({scrollTop:0},100);
		return false;
	});

	$(".sky_scrapper .my_info .order .tit_bg").on("click",function(){
		if($(this).hasClass("tit_bg_on")){
			$(this).removeClass("tit_bg_on").next().slideUp();
		}else{
			$(this).addClass("tit_bg_on").next().slideDown();
		}
	});

	
	myInfoRecommendProduct(); //개발 반영 후 삭제
	
	skyScrapperProductListCheck();

	/* $(document).on("click",".sky_scrapper .tit_top .checkbox input[type='checkbox']",function(){
		var othis = $(this);
		othis.parents(".tit_top").siblings(".area").find("input[type='checkbox']").each(function(){
			if(othis.is(":checked")){
				$(this).prop("checked",true).siblings("label").css("background-position"," 0px -50px");
			}else{
				$(this).prop("checked",false).siblings("label").css("background-position"," 0px 0px");
			}
		});
		
		var checkedLength = $(".today_list_sky .area ul li").find("input[type='checkbox']").filter(":checked").size();
		$(".sky_scrapper .cont .btn_area_sky span em").text(checkedLength);
	}); */
	
	/* $(document).on("click",".sky_scrapper .area input[type='checkbox']",function(){
		var checkedflag = true;
		var othis = $(this);
		var input = othis.parents(".area").siblings(".tit_top").find("input[type='checkbox']");
		var label = othis.parents(".area").siblings(".tit_top").find("label");
		for(i=0;i < othis.parents(".area").find("input[type='checkbox']").length; i++){
			if(!othis.parents(".area").find("input[type='checkbox']").eq(i).is(":checked")){
				checkedflag = false;			
			}
		}
		if(checkedflag){
			input.prop("checked",true);
			label.css("background-position"," 0px -75px");
		}else{
			input.prop("checked",false);
			label.css("background-position"," 0px -20px");	
		}
		
		var checkedLength = $(".today_list_sky .area ul li").find("input[type='checkbox']").filter(":checked").size();
		$(".sky_scrapper .today_list_sky .btn_area_sky span em").text(checkedLength);
	}); */
	/* //sky_scrapper */
	
	
	$(".tab_a_btn a").on("click",function(){
		$(this).parents("ul").find("a").removeClass("on").end().end().addClass("on");
	});	
	
	
	/* mainVisual  17.09.15 삭제
	var staff = false;
	if($("#wrap").hasClass("staff")){staff = true;}
	if($("#main_visual:visible").length > 0){
	
		var main_bg = $("<div/>")
			.attr("id","main_visual_bg")
			.css("top",$("#container").offset().top);
		
		$("#wrap").append(main_bg).find(".cn_shop_duty").addClass("main").end().find(".cn_category_view").show();
		$("#main_visual_bg").css("background-color","#"+$("#main_visual_color0").val());
		
		var mainVisualNum = 0;
		var mainVisualani = setTimeout(function(){mainVisual();},4000);
		$("#main_visual").on("focusout mouseleave",function(){
			$(this).find(".menu ul").hide();
		}).find(".visual_detail").on("mouseover focusin focusout mouseleave",function(e){
			e = e||window.event; 
			if(e.type === "mouseover" || e.type === "focusin"){			
				clearTimeout(mainVisualani);			
			}else{
				if($("#main_visual .btn a").attr("class") === "play"){return false;}
				mainVisualani = setTimeout(function(){mainVisual();},4000);
			}	
		}).end().find(".menu ul").on("focusout mouseleave",function(){
			$(this).hide();
		}).end().find(".btn a").on("click",function(){
			var oclass = $(this).attr("class");
			if(oclass === "pause"){
				$(this).removeClass("pause").addClass("play").text("play");
				clearTimeout(mainVisualani);
			}else{
				$(this).removeClass("play").addClass("pause").text("pause");
				mainVisualani = setTimeout(function(){mainVisual();},4000);
			}
		}).end().find(".menu a").on("mouseover focusin focusout mouseleave",function(e){
			e = e||window.event; 		
			if(e.type === "mouseover" || e.type === "focusin"){			
				clearTimeout(mainVisualani);			
				var num = $("#main_visual .menu a.sub").index(this);
				if($(this).parent("li").find("ul").length > 0){
					$(this).parent("li").siblings("li").find("a").removeClass("on").end().find("ul").hide();
					$(this).addClass("on").siblings("ul").show();				
					
					if(num < 0 ){
						num = $("#main_visual .menu a.sub").index($(this).siblings("ul").find("a.sub").eq(0));
					 }				
				}
				$("#main_visual	.visual_detail").find("li").hide().eq(num).show();	
				var bg_color = $("#main_visual_color"+num).val();
				if( bg_color !== null && bg_color !== "" && bg_color !== undefined){
					$("#main_visual_bg").css("background-color","#"+bg_color);
				}					
				
			}else{
				if($("#main_visual .btn a").attr("class") === "play"){return false;}
				mainVisualani = setTimeout(function(){mainVisual();},4000);
			}
		});
	}
	function mainVisual(){		
		clearTimeout(mainVisualani);
		$("#main_visual").find(".menu ul").hide().end()
		.find(".visual_detail").find("li").hide();
		mainVisualNum++; 
		if(mainVisualNum === $("#main_visual .visual_detail").find("li").length){mainVisualNum = 0;}
		var bg_color = $("#main_visual_color"+mainVisualNum).val();
		if( bg_color !== null && bg_color !== "" && bg_color !== undefined){
			$("#main_visual_bg").css("background-color","#"+bg_color);
		}		
		$("#main_visual").find(".visual_detail").find("li").eq(mainVisualNum).show().end().end().end()
		.find(".menu").find("a").removeClass("on").end().children("li").children("a").eq(parseInt(mainVisualNum/3)).addClass("on");
		mainMovie();			
	}
	function mainMovie(){		
		mainVisualani = setTimeout(function(){mainVisual();},4000);	
	}	*/

	/* //mainslide type1 */
	slideList("brand_sale",3);
	slideList("best_out",4,1,false);
	slideList("marketing_banner");
	slideList("newarrival_banner",5,1,false);
	slideList("single_slide",1,1,false);
	slideList("detail_prod_slide",4,1,false);
	slideList("best_slide",5,1,true); //2017-04-05 추가
	slideList("brand_banner",1,false); //17.08.07 추가 17.08.22수정
	
	// new arrival
	function randomRange(n1, n2) {
		return Math.floor( (Math.random() * (n2 - n1 + 1)) + n1 );
	}
	var newArrivalBanner = function(){
		var $prodListObj 	= $('.new_arrival_teb');
		var $prodContObj 	= $('.new_arrival_contents');
		var $pordCheck 		= $prodListObj.find('>li');
		var idx = randomRange(0, 4);
		
		init();
		
		function init(){
			$pordCheck.removeClass("on");
			$pordCheck.eq(idx).addClass("on");
			$prodContObj.find(">div").removeClass("active");
			$prodContObj.find(">div").eq(idx).addClass("active");
		}
		
		$pordCheck.on({
			"mouseenter" : function(){
				idx = $(this).index();
				init();				
			}
		});
	}
	newArrivalBanner();

	/* mainslide type2 */
	function specials_sale(cnmae){
		var oslide = $(".specials_sale");
		var list = oslide.find(".list");
		var nav = null,navcount = 0,playbtn = null,play = true;
		list.find("li").removeClass("on").eq(0).addClass("on");
		list.prepend(list.find("li:last-child"));
			
		if(oslide.find(".banner_nav:visible").length > 0){
			oslide.css({ "visibility" : "visible" });
			
			nav = oslide.find(".banner_nav").find("ul");
			var navLength = nav.find("li").size();
			
			if(nav.children("li").length > list.children("li").length){
				var count = nav.children("li").length - list.children("li").length;
				for(var i = 0;i < count;i++){
					nav.children("li").last().remove();
				}
			}			
			playbtn = oslide.find(".btn_play");
			playbtn.find("a").on("click",function(){
				$(this)	.hide().siblings("a").show();
				if($(this).hasClass("play")){
					play = false;
					clearTimeout(specialsAni);
				}else{
					specialsAni = setTimeout(function(){slideNext2();},3000);
					play = true;
				}
				return false;
			});
			nav.find("a").on("click",function(e){
				clearTimeout(specialsAni);
				var thisIdx 	= $(this).parent().index();
				var activeIdx 	= nav.find("a.on").parent().index();
				var checkIdx;
				
				if( thisIdx > activeIdx ){
					checkIdx = thisIdx - activeIdx;
				} else {
					checkIdx = (navLength - activeIdx) + thisIdx - 1;
				}
				for(var i = 0;i < checkIdx;i++){
					slideNext2();
				}	
				clearTimeout(specialsAni);
				e.preventDefault();				
			});			
		}
		
		function slideNext2(){
			clearTimeout(specialsAni);		
			list.children("li").removeClass("on");		
			list.append(list.children("li").eq(0).clone());
			list.children("li").eq(0).remove();
			list.children("li").eq(1).addClass("on");
			if(nav !== null){				
				navcount++;
				if(list.children("li").length <= navcount){navcount = 0;}
				nav.find("a").removeClass("on").eq(navcount).addClass("on");
			}
			slideReset2();
		}			
		var specialsAni = setTimeout(function(){slideNext2();},3000);
		function slideReset2(){
			if(!play){return false;}
			specialsAni = setTimeout(function(){slideNext2();},3000);
			return false;
		}						
		oslide.find(".prev").on("click",function(e){
			e.preventDefault();
			clearTimeout(specialsAni);
			list.children("li").removeClass("on");
			list.prepend(list.children("li:last").clone());
			list.children("li:last").remove();
			list.children("li").eq(1).addClass("on");
			if(nav !== null){
				navcount--;
				if(navcount < 0 ){navcount = list.children("li").length-1;}
				nav.find("a").removeClass("on").eq(navcount).addClass("on");				
			}			
			slideReset2();
		});
		oslide.find(".next").on("click",function(e){
			e.preventDefault();
			slideNext2();
		});	
		list.add(nav).on("mouseover mouseleave mouseout mouseup mousemove focusin focusout",function(e){
			e = e || window.event;
			if(e.type === "mouseover" || e.type === "mouseout" || e.type === "mouseup" || e.type === "mousemove" || e.type === "focusin"){
				clearTimeout(specialsAni);
			}else if(play){
				specialsAni = setTimeout(function(){slideNext2();},1000);
			}
		});	
	}
	/* //mainslide type2 */
	specials_sale();
	
	/* maintab */
	function tab(iname){
		var count =	0;
		
		var $tab = $("#"+iname);
		$tab.find(".tab_menu").eq(0).find("a").each(function(){
			if(iname !== "branch_area" && $tab.find(".btn_move:visible").length > 0){
				$(this).parents("li").css({"width":($(this).parents("ul").find("a").index(this) === 0 ? $(this).parents("li").width()-1 :$(this).parents("li").width()),"text-align":"center"});		
				var areaWidth = 0;
				for(var i = 0; i < $tab.find(".tab_menu").eq(0).find("a").length; i++){
					areaWidth = areaWidth+$tab.find(".tab_menu").eq(0).find("li").eq(i).width();				
				}
				$tab.find(".area").eq(0).add($tab.find(".area").eq(0).siblings(".area")).width(areaWidth-2);
				$(this).css({"white-space":"nowrap","padding-left":"0","padding-right":"0"});
			}
			$(this)
			.on("click",function(){
				$tab.find(".tab_menu").eq(0).find("a").removeClass("on");
				$(this).addClass("on");
				var ohref = $(this).attr("href");
				if($(ohref).length > 0)	{
					$tab.find(".area").eq(0).add($tab.find(".area").eq(0).siblings(".area")).hide();
					$(ohref).show();
					if($tab.siblings(".area_con").length > 0){ //마케팅 브랜드몰
						$tab.siblings(".area_con").hide();
						if($tab.siblings(".null").length > 0){$tab.siblings(".null").hide();}
						if(! $tab.hasClass("brand_sort")){
							$(ohref+" > a").removeClass("on");
						}
						
						$(ohref+"_con").show();
						if(! $tab.hasClass("brand_sort")){
							$(ohref+"_con .box").show();
						}
						titsort();
					}
					if($(ohref).hasClass("sort_cont")){
						if(! $tab.hasClass("brand_sort")){
							$(ohref).find("a").eq(0).click();
						}
					}
				}
				/* 내부 슬라이드 있을경우
				if(ohref.has(".next")){
					setTimeout(function(){ohref.find(".next").click()},3000);
				}
				*/
				return false;
			});
			/* 내부 슬라이드(hot_sale_tab) 있을경우
			if(iname == "hot_sale_tab" && $($(this).attr("href")).has(".next")){
				slideList($(this).attr("href").split("#")[1],4);
			}
			*/
		});
		if($tab.find(".btn_move:visible").length > 0){
			$tab.find(".btn_move a").on("click",function(){
				if($(this).hasClass("prev")){
					$tab.find(".area:visible").prepend($tab.find(".area:visible").children("li").last().clone()).children("li").last().remove();
				}else{
					$tab.find(".area:visible").append($tab.find(".area:visible").children("li").first().clone()).children("li").first().remove();
				}
			});
		}

	}
	function tab2(iname,cname){
		$("#"+iname+" .tab_menu").find("a").each(function(){
			$(this).on("click",function(){
				$("#"+iname+" .tab_menu").find("li").removeClass("on");	
				$(this).parents("li").addClass("on");
				var ohref = $($(this).attr("href"));
				ohref.siblings("."+cname).hide();
				ohref.show();
				if($("#"+iname).has(".btn_off")){
					
				}
				return false;
			});
		});
		
	}	
	function tab3(iname){
		var $tab = $("#"+iname);
		$tab.find(".tab_menu").eq(0).find("a").each(function(){			
			$(this).on("click",function(){
				$tab.find(".tab_menu").eq(0).find("a").removeClass("on");						
				$tab.find(".area2").hide();
				$(this).addClass("on");
				var ohref = $($(this).attr("href"));
				ohref.show();	
				return false;
			});
		});
		
	}	
	
	tab("best_in_tab");
	tab("detail_tab");
	tab("detail02_tab");
	tab("branch_area");
	tab("brand_tab");
	tab("default_tab");
	tab("tab02");		
	tab2("category","chk_list");
	tab3("tab0105");
	tab3("tab01");
	tab3("brand_sort_tab");//2016-10-20 추가

	/*
	$(".category_brand_toggle").on("click",function(){
		if($(this).hasClass("toggle_on")){
			$(this).addClass("toggle_off").removeClass("toggle_on");
			$("#category_brand").css("height","100%");
		}else{
			$(this).addClass("toggle_on").removeClass("toggle_off");
			$("#category_brand").animate({"height":70},{duration:500});
		}
		return false;
	});
	*/
	
	/* detail top layer tab */
	$("#detail_con_tab a").on("click",function(e){
		if($(this).hasClass("on") || $(this).parent().hasClass("tab6") ){return false;}
		$("#detail_con_tab a").removeClass("on");
		$(this).addClass("on");		
		$($(this).attr("href")).show().siblings(".tab_cont").removeAttr("style").hide();		
		var top = $($(this).attr("href")).offset().top;
		if($("#detail_con_tab").hasClass("fixed")){
			if($(this).attr("href") === "#detail_con_tab1"){top = top + 52;}
			$("html,body").animate({scrollTop:top},100);
		}
				
		eventBubblingChecked( e, $(this).attr("href") );
	});
	/* //detail top layer tab */
	
	/* detail page cart dim */
	if($(".pr_wrap .btn_off:visible").length > 0){
		$("#detail_con_tab a.cart").addClass("off");
	}
	/* detail contents product img */
	$(document).on("click", ".detail_view .detail_img ul a", function(){
		if($(this).hasClass("on")){return false;}
		$(this).parents("ul").find("a").removeClass("on");
		$(this).addClass("on").parents(".detail_img").find("P").find("img").attr("src",$(this).attr("data-url"));		
	});

	// detail contents product (up/down hide slide)
	todayProduct($('.detail_img_slide_wrap'), $('.detailNext'), $('.detailPrev'),4);
	
	var detailCompareUl	= $(".detail_view .recommend .scroll > div > ul");
	var detailCompareLi = detailCompareUl.find(">li");
	detailCompareUl.css({ "width" : detailCompareLi.outerWidth() * detailCompareLi.size() })
	
	/* detail map */
	$(".detail_view .tab4 .txt_img .txt a").on("click mouseover",function(){
		if($(this).hasClass("on")){return false;}
		$(this).parents("ul").find("a").removeClass("on");
		$(this).addClass("on");
		var mapnum = $(".detail_view .tab4 .txt_img .txt a").index(this);
		$(".detail_view .tab4 .txt_img .img").find('li').eq(mapnum).addClass('on').siblings().removeClass('on');
		
	});
	
	/* detail table */
	$(document).on("click",".detail_view .table_normal .left a",function(e){
		var othis = $(this);
		var otable = $(this).parents("table");
		if(othis.hasClass("on")){
			othis.removeClass("on").parents("tr").next(".table_view_tr").hide();		
			if(othis.parents("tr").next(".table_view_tr").hasClass("last")){
				othis.parents("tr").addClass("last");		
			}
			return false;
		}
		otable.find(".table_tit").removeClass("on");
		otable.find(".table_view_tr").hide();
		otable.find(".table_view_tr:last-child").prev("tr").addClass("last");
		othis.addClass("on").parents("tr").next(".table_view_tr").show();	
		if(othis.parents("tr").hasClass("last")){
			othis.parents("tr").removeClass("last").next(".table_view_tr").addClass("last");
		}
		return false;
	});
	
	/* detail review popup */
	$(".detail_view .add_review").on("click",function(){
		window.open("product_detail_popup_qna2.html",'','width=672,height=700');
		return false;
	});
	
	
	// best shop tab
	$(".best_selling").each(function(){
		var _this = $(this);
		
		_this.find(".best_list .tit_tab").find("a").on({
			"click" : function(e){
				if(! $(this).hasClass("on") ){
					var idx = $(this).parent().index();
					_this.find(".best_list .tit_tab").find("a").removeClass("on");
					$(this).addClass("on");
					//_this.find(".product_list").removeClass("on").eq(idx+1).addClass("on");
				}
				e.preventDefault();
			}
		});
	});
	
	
	/* category */
	if($(".lnb_wrap:visible").length > 0){
		var promoh = $(".category_wrap").find("div[class^=promotion]").height() - 2;
		$(".category_wrap").find(".lnb_wrap").height(promoh);		//sds요청 크기 고정으로 불필요 > 브라우져별 호환성이슈
		
		if($(".lnb_wrap .brand").length > 0){
			$(".lnb_wrap").each(function(){				
				var lnbsub = $(this).find(".lnb > li").length;
				switch(lnbsub) {
					case 20:
					case 19:
					case 18:
					case 17:
					case 16:
					case 15:
					case 14:
					case 13:
					case 12:
					case 11:
						lnbsub = 1;
					break;
					case 10:
					case 9:
						lnbsub = 2;
					break;
					case 8:
					case 7:
						lnbsub = 3;
					break;
					case 6:
					case 5:
						lnbsub = 4;
					break;
					case 4:
					case 3:
						lnbsub = 5;
					break;
					case 2:
					case 1:
						lnbsub = 6;
					break;			
				}
				
				$(this).find(".lnb").height(promoh-228-(lnbsub*46)+20);
			});

		}
	
	}
	$(".category_wrap .lnb_wrap .brand dt a").on("click",function(){
		if($(this).hasClass("on")){
			$(this).removeClass("on").parents(".lnb_wrap").find(".lnb").show().siblings(".brand").removeClass("brand_on");
			
		}else{
			$(this).addClass("on").parents(".lnb_wrap").find(".lnb").hide().siblings(".brand").addClass("brand_on");
		}
		return false;		
	});
	$(".category_wrap .lnb_wrap .brand dd a").on("focusin",function(){
		if($(this).parents(".brand").find("a").index(this) > 3){
			$(".category_wrap .lnb_wrap .brand dt a").addClass("on").parents(".lnb_wrap").find(".lnb").hide().siblings(".brand").addClass("brand_on");
		}
	});
	
	/* power search */ 
	commonFacetUI();
	
	var productCompareArea = $(".search_option_box");
	productCompareArea.find(".btn_box a").on({
		"click" : function(e){
			productCompareArea.find(".product_list .list").removeClass("compare_on");
			productCompareArea.find(".btn_compare").removeClass("on");
			productCompareArea.find(".compare").find(".btn_com").hide().end().find("input[type='checkbox']").prop("checked" , false).next().css({ "background-position" : "0px -880px" });
			e.preventDefault();
		}
	});

	/* marketing */
	//new Arrival Brand slide
	var $event = $(".new_arrival_visual");
	var $eventContent = $event.find(".img > li");
	var $eventTab = $event.find(".btn_visual > ul > li");
	var $eventArrow = $event.find(".arrow");

	var eventTotal = $eventContent.length;
	var eventNum = 0;
	var pageNum = 0;

	$eventTab.each(function(_i) {
		$(this).css({ left:$(this).width() * (_i) });
	}).bind("mouseenter focusin", function(_e) {
		eventNum = $eventTab.index(this);
		event_content_change();
	});

	if (eventTotal > 1) {
		$eventArrow.show().bind("click", function(_e) {
			_e.preventDefault();
			if ($(this).hasClass("prev")) {
				if (eventNum == pageNum) pageNum = (pageNum == 0) ? eventTotal - 1 : pageNum - 1;
				eventNum = (eventNum == 0) ? eventTotal - 1 : eventNum - 1;
			} else {
				var temp = pageNum + 4;
				if (temp > eventTotal - 1) temp -= eventTotal;
				if (eventNum == temp) pageNum = (pageNum == eventTotal - 1) ? 0 : pageNum + 1;
				eventNum = (eventNum == eventTotal - 1) ? 0 : eventNum + 1;
			}

			for (var i = 0; i < eventTotal; i++) {
				var index = i + pageNum;
				if (index > eventTotal - 1) index -= eventTotal;
				$eventTab.eq(index).css({ left:$eventTab.eq(i).width() * i });
			}
			event_content_change();
		});
	}

	function event_content_change() {
		$eventContent.hide().eq(eventNum).show();
		$eventTab.removeClass("on").eq(eventNum).addClass("on");
	}


	$(".global_box .country_select a").on("click",function(){
		if($(this).hasClass("on") || $(this).data("countrycode") == "KOREA" ){return false;}
		var othis = $(this);
		othis.parents("ul").find("a").removeClass("on").end().end().addClass("on");
		var country_select = $(".brand_box .brand_select dl");
		var countrycode = othis.attr("data-countrycode");
		var countryhtml = othis.html();
		var countrytext = othis.find("span").text();
		//country_select.find("dt img").attr({"src":country_select.find("dt img").attr("src").split("ico_country")[0]+"ico_country"+countrycode+".png","alt":countrytext});
		country_select.find("dd").html(countryhtml);
		//return false();
	});
	$(".brand_box .brand_select a").on("click",function(){
		if($(this).hasClass("on")){return false;}
		$(this).parents("ul").find("a").removeClass("on").end().end().addClass("on");
		//return false();
	});	
	
	// detail con tab
	var detail_con_tabOffsetTop = "";
	var detail_con_tab = null;
	var detail_con = null;
	if($("#detail_con_tab:visible").length > 0){
		detail_con_tab = $("#detail_con_tab");
		detail_con_tabOffsetTop =$("#detail_con_tab").offset().top;
		detail_con = {
			"1":$("#detail_con_tab1"),
			"2":$("#detail_con_tab2"),
			"3":$("#detail_con_tab3"),
			"4":$("#detail_con_tab4"),
			"5":$("#detail_con_tab5")
		};
	}
	
	//main bestproduct wing		
	var best_productOffsetTop =  "";
	var best_product = null;
	var bp =  "";
	if($("#best_product:visible").length > 0){
		best_productOffsetTop = $("#best_product").offset().top;
		best_product = $("#best_product .quick");
		bp = $("#best_product1").offset().top-50;
	}	
	// 스카이스크래퍼	
	var leftScrollmenuOffsetTop = "";
	var leftScrollmenuPositionTop = "";
	//var leftScrollmenuMarginTop = 10;	
	var leftScrollmenu = null;
	if($("#lnb:visible").length > 0){
		leftScrollmenu = $("#lnb");
		leftScrollmenuOffsetTop = leftScrollmenu.offset().top;
		leftScrollmenuPositionTop = leftScrollmenu.position().top;
	}	
	var rightScrollmenuOffsetTop = "";	
	var rightScrollmenuPositionTop = "";
	var rightScrollmenuMarginTop = 10;
	var rightScrollmenu = null;
	if($("#rnb:visible").length > 0){
		rightScrollmenu =  $("#rnb");
		rightScrollmenuOffsetTop = rightScrollmenu.offset().top;
		rightScrollmenuPositionTop = rightScrollmenu.position().top;
	}		
	
	$(window).scroll(function(){
		var dsT = $(document).scrollTop();
		//var tmp = ($(document).scrollTop()+leftScrollmenuPositionTop+leftScrollmenuMarginTop)-leftScrollmenuOffsetTop;
		var tmp = (dsT+rightScrollmenuPositionTop+rightScrollmenuMarginTop)-rightScrollmenuOffsetTop;
		var bpo =  dsT-best_productOffsetTop-85;
		var bpv = dsT+$(window).height();
		if(rightScrollmenu !== null){
			if(tmp>155) {
				if(best_productOffsetTop > bpv){
					rightScrollmenu.animate({'top':tmp}, {duration:'slow', easing:'swing', queue:false});
				}
				rightScrollmenu.animate({'top':tmp}, {duration:'slow', easing:'swing', queue:false});
			}
			else {
				leftScrollmenu.animate({'top':leftScrollmenuPositionTop}, {duration:'slow', easing:'swing', queue:false});
				rightScrollmenu.animate({'top':rightScrollmenuPositionTop}, {duration:'slow', easing:'swing', queue:false});
			}
		}
		if(best_product !== null){
			if(bpo > 0){
				best_product.addClass("fixed");
				best_product.find("a").removeClass("on");			
				best_product.find("li").eq(parseInt((dsT-bp)/624)).find("a").addClass("on");
			}else{
				best_product.removeClass("fixed");
			}
		}				
		
		if(detail_con_tab !== null){
			if(detail_con_tabOffsetTop < dsT ){
				detail_con_tab.addClass("fixed");
				detail_con_tab.find("a").removeClass("on");			
				for(var i in detail_con){
					var n = i-1;
					if(detail_con[i].is(":visible")){
					//if(detail_con[i].offset().top < dsT + 120 && detail_con[i].height()+detail_con[i].offset().top > dsT){ 스크롤시 
						detail_con[i].css("padding-top","112px");
						detail_con_tab.find("a").eq(n).addClass("on");
						break;
					}					
				}				
			}else{
				detail_con_tab.removeClass("fixed");
				for(var j in detail_con){
					detail_con[j].css("padding-top","60px");					
				}				
			}
		}
		
		var skyBanner 	= $(".sky_scrapper_banner");
		var headerH		=  $("#header").outerHeight();
		if( ! $("#main_visual").length && skyBanner.length ){
			if( dsT > headerH ){
				skyBanner.addClass("fixed");
			} else {
				skyBanner.removeClass("fixed");
			}
		}
	}); 

	// detail visual img
	var detail_pr_img = 0;
	if($(".detail_view .pr_wrap .img ul li").length > 5 ){
		for(var i = 5;i < $(".detail_view .pr_wrap .img ul li").length; i++ ){
			$(".detail_view .pr_wrap .img ul").find("li").eq(i).hide();
		}
		detail_pr_img = 5;
	} else {
		$(".detail_view .pr_wrap .img").find(".btn_prev").hide().end().find(".btn_next").hide();
	}

	$(".detail_view .pr_wrap .img .btn_prev").on("click",function(){
		if(5 < detail_pr_img){
			detail_pr_img--;
			$(".detail_view .pr_wrap .img ul li").eq(detail_pr_img).hide();						
			$(".detail_view .pr_wrap .img ul li").eq(detail_pr_img - 5).show();
		}else{
			//alert("无上一张图片");
		}
	});
	$(".detail_view .pr_wrap .img .btn_next").on("click",function(){
		var imgl = $(".detail_view .pr_wrap .img ul li").length;
		if(imgl > detail_pr_img){		
			$(".detail_view .pr_wrap .img ul li").eq(detail_pr_img - 5).hide();						
			$(".detail_view .pr_wrap .img ul li").eq(detail_pr_img).show();
			detail_pr_img++;	
		}else{		
			//alert("无下一张图片");
		}
	});
	
	if($(".detail_view .pr_wrap:visible").length > 0){
		productDetailZoom();
	}

	/* 17.10.19 마우스오버 돋보기팝업 */
	$(".product_list .btnMoreNew").on("click",function(){
		layerpopup("productDetailNew");
		productDetailZoomS();
		return false;
	});	

	// detail 연관상품 계산
	if($(".detail_view .recommend:visible").length > 0){
		var recommend = $(".detail_view .recommend");
		var sale = Number(recommend.find("li:first .sale").text().split("$")[1]);
		var regular = Number(recommend.find("li:first .regular").text().split("$")[1]);
		recommend.find(".total").find(".sale_price").text("$"+regular).end().find(".dc_price").text("$"+sale).end().find("p:first span").text(1);
		recommend.find(".scroll input[type='checkbox']").on("click",function(){
			var saleP = sale;
			var regularP = regular;
			var num = 1;
			for(i=0;i < recommend.find(".scroll input[type='checkbox']").length; i++){
				if(recommend.find(".scroll input[type='checkbox']").eq(i).is(":checked"))	{
					saleP = saleP+Number(recommend.find(".scroll > div > ul > li").eq(i).find(".sale").text().split("$")[1]);
					regularP = regularP+Number(recommend.find(".scroll > div > ul > li").eq(i).find(".regular").text().split("$")[1]);
					num++;
				}
			}			
			recommend.find(".total").find(".sale_price").text("$"+regularP).end().find(".dc_price").text("$"+saleP).end().find("p:first span").text(num);
		});
		
	}
	// 2017-01-18 상품 상세 탭 : 브랜드 롤링배너 추가 
	function exhibition(iname){
		var area  = $("#"+iname),list = area.find(".img"),count = area.find(".count"),num = 1,listlegnth = list.find("li").length;
		count.html("<em>1</em> / "+listlegnth);
		list.find("li").eq(0).addClass("on");

		area.find(".prev").on("click",function(){
			if(num < 2){return false;}
			num--;
			list.find("li").removeClass("on").eq(num-1).addClass("on");
			count.html("<em>"+num+"</em> / "+listlegnth);
		}).end().find(".next").on("click",function(){
			if(num >= listlegnth){return false;}
			list.find("li").removeClass("on").eq(num).addClass("on");
			num++;
			count.html("<em>"+num+"</em> / "+listlegnth);
		});

	}
	exhibition("exhibition");

	// category banner slide	
	function bannerslide(iname){	
		if($("#"+iname).length < 1){return false;}
		var oslide = $("#"+iname);
		var inameAni = null;
		var list = oslide.find(".img").eq(0);
		var listLength = list.find(".imgcon").length;
		var nav =  oslide.find(".nav");
		var count = 0;
		list.find(".imgcon").removeClass("on").eq(count).addClass("on");
		nav.find("a").removeClass("on").eq(count).addClass("on");		
		function slideNext2(){
			clearTimeout(inameAni);		
			count++;
			if(count === listLength){count = 0;}
			list.find(".imgcon").removeClass("on").eq(count).addClass("on");
			nav.find("a").removeClass("on").eq(count).addClass("on");
			inameAni = setTimeout(function(){slideNext2();},2000);		
		}	
		inameAni = setTimeout(function(){slideNext2();},2000);					
		list.on("mouseover mouseleave mouseout mouseup mousemove focusin focusout",function(e){
			e = e || window.event;
			if(e.type === "mouseover" || e.type === "mouseout" || e.type === "mouseup" || e.type === "mousemove" || e.type === "focusin"){
				clearTimeout(inameAni);
			}else{
				inameAni = setTimeout(function(){slideNext2();},1000);
			}
		});
		nav.find("a").on("mouseover mouseleave mouseout mouseup mousemove focusin focusout",function(e){
			e = e || window.event;
			count = nav.find("a").index(this);
			if(e.type === "mouseover" || e.type === "mouseout" || e.type === "mouseup" || e.type === "mousemove" || e.type === "focusin"){
				clearTimeout(inameAni);
				list.find(".imgcon").removeClass("on").eq(count).addClass("on");
				nav.find("a").removeClass("on").eq(count).addClass("on");			
			}else{
				inameAni = setTimeout(function(){slideNext2();},1000);
			}
		});
	}
	
	bannerslide("defualt_banner");
	bannerslide("category_banner");
	bannerslide("promotion2");
	bannerslide("best_product1_banner");
	bannerslide("best_product2_banner");
	bannerslide("best_product3_banner");
	bannerslide("best_product4_banner");
	bannerslide("best_product5_banner");
	bannerslide("best_product6_banner");
	bannerslide("best_product7_banner");
	bannerslide("best_product8_banner");
	bannerslide("best_product9_banner");
	bannerslide("best_product10_banner");
	bannerslide("best_product11_banner");
	$("#sky_scrapper_banner1").singleNavSlide();
	$("#sky_scrapper_banner2").singleNavSlide();
	$("#sky_scrapper_banner3").singleNavSlide();
	$(".premium_promotion .promotion").singleNavSlide();
	$(".premium_product").singleNavSlide();
	
	// 2017-05-18 상단 우측 롤링배너
	$("#event_banner_small").singleNavSlide({
		auto : true,
		speed : 5000
	});
	
	/* popup close */
	$(".pop_close").on("click",function(){
		self.close();
	});
	
	
	// 항공편 검색
	/* $(".airport_info a").on("click",function(){
		$("#ly_aircode").hide();
		$(".airport_info a").removeClass("on");
		$(this).addClass("on");
		$("#aircode").val($(this).text().split("(")[1].split(")")[0]).addClass("focus");
	});
	$("#aircode").on("keyup focus",function(e){
		e = e || window.event;
		var val = $.trim($(this).val().toUpperCase());
		if(e.keyCode === 40){
			if($("#ly_aircode:visible").length > 0){
				$("#ly_aircode ul a").eq(0).addClass("on").focus();
			}
		}else if(val === null || val === "" || val === undefined ){
			$("#ly_aircode").hide().find("ul").html("");					
		}else{
			$("#ly_aircode ul").html("");		
			for(var i =0;i < $(".airport_info a").length; i++){	
			 	var text = $(".airport_info a").eq(i).text();			
				if(text.indexOf(val) > -1 ){
					$("<li>").html("<a href='#url'>"+text.split(val)[0]+"<em>"+val+"</em>"+text.split(val)[1]+"</a>").appendTo("#ly_aircode ul");
					$("#ly_aircode").show();
				}
			}	
		}
	});	
	$(document).on("click keyup","#ly_aircode ul a",function(e){ 	
		e = e || window.event;
		if(e.type === "click"){
			var text = $(this).text();
			for(var i =0;i < $(".airport_info a").length; i++){	
				if($(".airport_info a").eq(i).text() === text ){
				 	$(".airport_info a").eq(i).focus().click();	
					break;
				}				
			}			
		}else if(e.keyCode === 40){
			if($("#ly_aircode ul a").index(this) < $("#ly_aircode ul a").length-1){
				$(this).removeClass("on").parents("li").next().find("a").focus().addClass("on");
			}
		}else if(e.keyCode === 38){
			if($("#ly_aircode ul a").index(this) > 0){
				$(this).removeClass("on").parents("li").prev().find("a").focus().addClass("on");
			}else{
				$(this).removeClass("on");
				$("#aircode").focus();
			}
		}		
	});	 */
	
	//qna table
	if($(".table_normal tr.ans:visible").length > 0){
		$(".table_normal tr.ans").hide();
	}
	$(document).on("click",".table_normal tr a", function(e){
		if($(this).parents("tr").next("tr").hasClass("ans")){
			if( $(this).hasClass("on") ){
				$(this).removeClass("on").parents("tr").next(".ans").hide();
			} else {
				$(".table_normal tr a").removeClass("on");
				$(this).addClass("on")
					.parents(".table_normal").find(".ans").hide().end().end()
					.parents("tr").next(".ans").show();
			}
		}
		eventBubblingChecked( e, $(this).attr("href") );
	});
	
	//cart gift
	gift_listradiobox();

	$(document).on("click",".cart_wrap .gift_list .css-radiobox",function(){
		$(this).parents("ul")
			.find("img").not(".ico").removeClass("on").end().end().end()
			.siblings("dl").find("img").not(".ico").addClass("on");
	});
	
	
	$(".cart_wrap .gift_list").find(".css-checkbox").each(function(){
		function init( target ){
			if( target.prop("checked") == true ){
				target.parent().find(".img img").addClass("on");
			} else {
				target.parent().find(".img img").removeClass("on");
			}
		}
		init($(this));
		$(this).on({
			"change" : function(){
				init($(this))
			}
		});
	});
	
	
	$(document).on("click",".cart_wrap .gift_list dl",function(){
		
		var inputType = $(this).prev().prev();
		
		if( inputType.hasClass("css-radiobox") ){
			$(this).parents("ul")
				.find("label").css("background-position","0px -100px").end()
				.find("img").not(".ico").removeClass("on").end().end().end()
				.siblings(".css-radiobox").attr("checked",true)
				.siblings(".css-radiolabel").css("background-position","0px -150px")
				.siblings("dl").find("img").not(".ico").addClass("on");
		} 
		else if ( inputType.hasClass("css-checkbox") ){
			if( inputType.prop("checked") == true ){
				inputType.prop("checked" , false).next().css("background-position","0px 0px").next().find(".img img").removeClass("on");
			} else {
				inputType.prop("checked" , true).next().css("background-position","0px -50px").next().find(".img img").addClass("on");
			}
		}
		
	});
	
	// table checkbox
	$(document).on("change","table .css-checkbox", function(){
		var _this 	= $(this);
		var _table 	= _this.parents("table");
		
		var allCheckBox 	= _table.find(".css-checkbox-all");
		var eachCheckBox 	= _table.find(".css-checkbox").not(".css-checkbox-all").filter(function(){ return !$(this).parents("tr").hasClass("disable") });
		
		if( _this.hasClass("css-checkbox-all") ){
			if( _this.prop("checked") == true ){
				eachCheckBox.prop("checked", true).next().css("background-position","0 -50px");
			} else {
				eachCheckBox.prop("checked", false).next().css("background-position","0 0");
			}
		} else {
			if( eachCheckBox.filter(":checked").size() == eachCheckBox.size() ){
				allCheckBox.prop("checked", true).next().css("background-position","0 -75px");
			} else {
				allCheckBox.prop("checked", false).next().css("background-position","0 -20px");
			}
		}
	});
	
	if($(".cart_popup:visible").length > 0){
		$(document).on("click" , ".cart_popup .coupon_list a" , function(e){
			var othis = $(this);
			othis.parents("li").remove();
			e.preventDefault();
		});
		
		/*
		$(document).on("click",".cart_popup .uescheck .css-checkbox",function(){
			var othis = $(this);
			var pointp = othis.parents(".uescheck").find("span").eq(0).text();
			if(othis.is(":checked")){
				othis.parents("td").next("td").text(pointp);
			}else{
				othis.parents("td").next("td").text("$0");
			}
		});
		$(".cart_popup .usepoint input[type='text']")
			.val(0)	
			.on("keyup",function(e){
				e = e || window.event;				
				if(e.keyCode < 48 || e.keyCode > 57 ){$(this).val("");}
				if(Number($(this).val()) > Number($(this).parents(".usepoint").find("li span").eq(0).text().split("$")[1]) || Number($(this).val()) < 0){
					alert("超过可使用的积分金额。\n请输入订购金额的30%以内的积分金额。");
					$(this).val("");
				}
				$(this).parents("td").next("td").text("$"+$(this).val());
				
		}); 
		*/
		
		$(".cart_popup .useselectpoint").each(function(){
			var othis = $(this);
			othis.find("select").on({
				"change" : function(){
					var thisTxt = $(this).find("option:selected").text();
					othis.find(" > div > strong").text(thisTxt);
				}
			});
		});
	}
	
	/* customer */
	// textArea maxLength 
	$(document).on("keyup","textarea.textcount",function(e){
		e = e || window.event;
		var othis = $(this);
		var count = othis.parents("tr").find("span.textcount em");
		var ls_str = othis.val();
		var li_str_len = ls_str.length; //전체길이
		var i = 0;
		var li_byte = 0;   //한글일경우 2, 그외글자는 1을 더함
		var ls_one_char = "";  //한글자씩 검사	

		var defaultByte = 1000;
		
		if( $(this).data("byte") ){
			defaultByte = Number($(this).data("byte"));
		}
		
		for(i=0; i< li_str_len; i++){
			ls_one_char = ls_str.charAt(i);   //한글자 추출
			if(escape(ls_one_char).length > 4){ 
			  li_byte +=2;   //한글이면 2를 더한다
			}else{
			  li_byte++;     //한글아니면 1을 다한다
			}					
		}	
		count.text(li_byte);
		textarea_maxlength( othis, defaultByte );
	});

	function textarea_maxlength(obj, maxLength){
		if( obj.val().length > maxLength ){
			var thisTxt = obj.val();
			thisTxt = thisTxt.substring(0, maxLength)
			obj.val(thisTxt);
		}
	}

	if($(".brand_sort_wrap:visible").length > 0){
		titsort();
	}

	$(".brand_sort .sort_choice .sort_list a").on({
		"click" : function(e){
			var targetCont = $(this).attr("href");
			$(".brand_sort .sort_choice .sort_list a").removeClass("on");
			$(this).addClass("on");
			$(".brand_sort_wrap .sort_cont").hide();
			$(".brand_sort_wrap .tab_cont").hide();
			$(targetCont).show();
			$(targetCont + "_con").show();
			e.preventDefault();
		}
	});
	
	$(".brand_sort_wrap .sort_cont a").each(function(k,v){
		var indiAlpha = $(this).text();
		var hasBrand = false;
		var sortCont = $(this).parents(".sort_cont");
		
		$("#"+sortCont.attr("id")+"_con > div").each(function(k,v){
			if(indiAlpha == $(this).find(".part_tit").text()){
				hasBrand = true;
			}
		})
		if(hasBrand){
			$(this).removeClass("disabled");
		}else{
			$(this).addClass("disabled");
		}
	});
	
	$(".brand_sort_wrap .sort_cont a").on("click",function(e){
		var _this = $(this);
		if(! _this.hasClass("disabled") ){
			var sortcategory = _this.parents(".sort_cont").attr("id");
			var text = _this.text();
			var result = false;
			_this.addClass("select").siblings("a").removeClass("select").parents(".brand_sort_wrap").find(".null").hide();
			$("#"+sortcategory+"_con .brand_part").hide();
			for(var i = 0;i < $("#"+sortcategory+"_con").find(".part_tit").length;i++){
				if($("#"+sortcategory+"_con").find(".part_tit").eq(i).text() === text){
					$("#"+sortcategory+"_con").find(".brand_part").eq(i).show();
					result = true;
				}
			}
			if(!result){_this.parents(".brand_sort_wrap").find(".null").show();}
		}
		e.preventDefault();
	});
	
	if($(".visual_lnb:visible").length > 0){
		$(".visual_lnb .list li dt a").on("click",function(){
			$(this).parents("li").addClass("on").siblings("li").removeClass("on");
		});			
		//$(".visual_lnb .list li").eq(3).find("dt a").click();
		var list = $(".visual_lnb .list");
		var listli = null;		
		var listing = false;
		$(".visual_lnb .btn_move a").on("click",function(){
			var classname = $(this).attr("class");
			if(classname ==="prev" && !listing){
				listing = true;
				listli = list.find("li").last().clone();
				listli.find("dt a").on("click",function(){
					$(this).parents("li").addClass("on").siblings("li").removeClass("on");
				});
				list.css("margin-top","-28px").prepend(listli);
				list.animate({"margin-top":15},{duration:300,complete:function(){
					list.find("li").last().remove();
					listing = false;
				}});
			}else if(classname === "next" && !listing){
				listing = true;
				
				listli = list.find("li").first().clone();
				listli.find("dt a").on("click",function(){
					$(this).parents("li").addClass("on").siblings("li").removeClass("on");
				});
				list.append(listli);
				list.animate({"margin-top":"-28px"},{duration:300,complete:function(){
					list.css("margin-top","15px").find("li").first().remove();
					listing = false;
				}});
				
				if( list.find("li.on").index() == 0 ){
					list.find("li.on").next().find("a").click();
				}
			}
		});
		var visual_slide = setTimeout(function(){visual_lnbslide();},3000);
		$(".visual_lnb").on("mouseover mouseleave",function(e){
			e = e||window.event; 
			if(e.type === "mouseover"){			
				clearTimeout(visual_slide);			
			}else{
				visual_slide = setTimeout(function(){visual_lnbslide();},2000);
			}	
		});
	}
	function visual_lnbslide(){
		clearTimeout(visual_slide);		
		$(".visual_lnb .btn_move a.next").click();
		setTimeout(function(){
			//$(".visual_lnb .list li").eq(3).find("dt a").click();
		},300);	
		visual_slide = setTimeout(function(){visual_lnbslide();},2000);
	}
	
	
	//marketing gift top banner
	function visualGift(){
		var _this 			= $("#visual_gift");
		var ctrl			= _this.find(".btn_control");
		var tabMenu 		= _this.find(".slide_menu");
		var rollBox 		= _this.find(".list");
		var fadeImg			= _this.find(".tit_rolling ul");
		var prev			= _this.find(".prev");
		var next			= _this.find(".next");
		var rollList 		= rollBox.find(">li");
		var rollListSize 	= rollList.size();
		var rollListW 		= rollList.outerWidth();
		var move			= 3;
		var speed			= 500;
		var duraton 		= 3000;
		var defaultLeft 	= 123;
		var tid;
		var state			= true;
		var activeIdx		= 1;
		var check   =true;
		
		next.on({
			"click" : function(e){
				moveRight();
				e.preventDefault();
			}
		});
		
		prev.on({
			"click" : function(e){
				moveLeft();
				e.preventDefault();
			}
		});
		
		_this.on({
			"mouseenter" : function(){
				stop();
			},
			"mouseleave" : function(){
				if( state == true ){
					auto();
				}	
			}
		});
		
		ctrl.on({
			"click" : function(){
				var _this = $(this);
				if( _this.hasClass("pause") ){
					_this.removeClass("pause").addClass("play");
					state = false;
					stop();
				} else {
					_this.removeClass("play").addClass("pause");
					state = true;
					auto();
				}
			}
		});
		
		tabMenu.find("a").on({
			"click" : function(e){
				
				if(! $(this).parent().hasClass("on") ){
					var currentIdx 				= tabMenu.find("li.on").index();
					var idx 					= $(this).parent("li").index();
					var animateRightListSize 	= rollList.filter(":first-child").next().nextUntil("[data-category-index='"+idx+"']").size() + 1;
					var animateLeftListSize		= rollListSize - animateRightListSize;
					
					
					check = false;
					slideMenu(idx);
					
					if( idx > currentIdx ){
						for( var i=0; i < animateRightListSize/move; i++ ){
							moveRight();
						} 
						imgFade(idx);
					} else {
						for( var i=0; i < animateLeftListSize/move; i++ ){
							moveLeft();
						}
						imgFade(idx);
					}
					check = true;
				}
				e.preventDefault();
			}
		});
		
		function auto(){
			clearInterval(tid);
			tid = setInterval(function(){
				moveRight();
			}, duraton);
		}
		
		function stop(){
			clearInterval(tid);
		}
		
		function moveRight(){
			rollBox.stop(true,true).animate({ "left" : -(move*rollListW) + defaultLeft }, speed , function(){
				for( var i = 0; i < move; i++ ){
					rollBox.append(rollList.filter(":first-child"));
					rollBox.css({ "left" : defaultLeft })
				}
				activeIdx = rollList.filter(":first-child").next().data("categoryIndex");
				if( check == true ){
					slideMenu(activeIdx);
					imgFade(activeIdx);
				}	
			});
		}
		
		function moveLeft(){
			for( var i = 0; i < move; i++ ){
				rollList.filter(":last-child").prependTo(rollBox);
			}	
			activeIdx = rollList.filter(":first-child").next().data("categoryIndex");
			rollBox.css({ "left" : -(move*rollListW) + defaultLeft });
			rollBox.stop(false,false).animate({ "left" : defaultLeft }, speed, function(){
				if( check == true ){
					slideMenu(activeIdx);
					imgFade(activeIdx);
				}
			});
		}
		
		function slideMenu(idx){
			tabMenu.find("li").removeClass("on").eq(idx).addClass("on");
		}
		
		function imgFade(idx){
			fadeImg.find("li").removeClass("on").eq(idx).addClass("on");
		}
		
		
		function init(){
			rollBox.width(rollListSize*rollListW);
			rollBox.prepend(rollList.filter(":last-child"));
			auto();
		}
		
		init();
	}
	
	// visualGift Initialize
	if( $("#visual_gift").size() > 0 ){
		visualGift();
	}
	
	
	
	
	// marketing kore banner
	var marketing_korea = null;
	var	marketing_korea_rolling = false;
	var mkc = 0;
	var mkc2 = 0;
	if($(".marketing_korea:visible").length > 0){
		var firstobj = $(".marketing_korea").find(".visual_contents").eq(mkc2);
		marketing_korea = setTimeout(function(){marketing_korea_fx(firstobj);},3000);	
		$(".visual_korea").on("mouseover mouseleave",function(e){
			e = e||window.event; 
			if(e.type === "mouseover"){			
				clearTimeout(marketing_korea);		
			}else{
				if(marketing_korea_rolling){return false;}
				marketing_korea = setTimeout(function(){marketing_korea_fx(firstobj);},2000);
			}	
		}).find(".menu a").on("click",function(){
			mkc2 = $(".marketing_korea .menu a").index(this);
			clearTimeout(marketing_korea);
			marketing_korea_fx2(mkc2);
			var obj = $(".marketing_korea").find(".visual_contents").eq(mkc2);
			marketing_korea = setTimeout(function(){marketing_korea_fx(obj);},2000);
		}).end().find(".visual_contents .btn_area a").on("click",function(){	
			var obj = $(".marketing_korea").find(".visual_contents").eq(mkc2);				
			if($(this).hasClass("prev")){
				mkc--;
				marketing_korea_fx3(obj,mkc);
			}else if($(this).hasClass("next")){
				mkc++;
				marketing_korea_fx3(obj,mkc);
			}else{
				var num = obj.find(".banner_nav").find("a").index(this);
				marketing_korea_fx3(obj,num);
			}
			$(".marketing_korea .visual_detail ul.img").children("li").eq(mkc2);						
		}).end().find(".btn a").on("click",function(){	
			if($(this).hasClass("play")){
				marketing_korea_rolling = false;
				$(this).removeClass("play").addClass("pause");
				var obj = $(".marketing_korea").find(".visual_contents").eq(mkc2);
				marketing_korea = setTimeout(function(){marketing_korea_fx(obj);},2000);				
			}else{
				marketing_korea_rolling = true;
				clearTimeout(marketing_korea);					
				$(this).removeClass("pause").addClass("play");					
			}
		});
	}
	function marketing_korea_fx(obj){
		clearTimeout(marketing_korea);
		if(marketing_korea_rolling){return false;}
		mkc++;
		
		if(mkc > obj.find(".contents_slider").children("li").length-1){
			mkc = 0;
			mkc2++;
			if(mkc2 >  $(".marketing_korea").find(".visual_contents").length - 1){mkc2 = 0;}
			obj = $(".marketing_korea").find(".visual_contents").eq(mkc2);
			marketing_korea_fx2(mkc2);	
		}
		marketing_korea_fx3(obj,mkc);	
		marketing_korea = setTimeout(function(){marketing_korea_fx(obj);},2000);	
	}
	function marketing_korea_fx2(num){
		$(".marketing_korea").find(".menu").find("li").eq(num).addClass("on").siblings("li").removeClass("on").end().end().end().end()
		.find(".visual_detail ul.img").children("li").eq(num).addClass("on").siblings("li").removeClass("on");	
	}
	function marketing_korea_fx3(obj,num){
		mkc = num;
		if(mkc > obj.find(".contents_slider").children("li").length-1){
			mkc = 0;
		}else if(mkc < 0){
			mkc = obj.find(".contents_slider").children("li").length-1;
		}
		obj.find(".contents_slider").children("li").eq(mkc).addClass("on").siblings("li").removeClass("on").end().end().end().end().find(".banner_nav").find("a").removeClass("on").eq(mkc).addClass("on");		
	}
	
	// thumbnail hide
	$(".marketing_korea .contents_slider").each(function(){
		if($(this).find(">li").size() < 2){
			$(this).next().hide();	
		}
	});
	
	
	
	/* cart */
	//O2O checked
	var o2oAllCheckBox 	=  $(".payment_o2o .cart_item > table .css-checkbox-all");
	var o2oEachCheckBox =  $(".payment_o2o .th_skip .css-checkbox").not(":disabled");
	var o2oEachCheckBoxSize = o2oEachCheckBox.size(); 
	
	o2oAllCheckBox.on({
		"change" : function(){
			if( $(this).prop("checked") == true ){
				o2oEachCheckBox.prop("checked" , true).next().css("background-position", "0 -50px");
				$(".payment_o2o .th_skip .table_normal tr").not(".product_tit").each(function(){
					$(this).find(".choice_list > li").each(function(){
						$(this).find(".chk_list > li:eq(0) input[type='radio']").prop("checked", true).next().css("background-position", "0 -150px");
						$(this).find(".chk_list > li:eq(1) input[type='radio']").next().css("background-position", "0 -100px");
					});
				});
			} else {
				o2oEachCheckBox.prop("checked" , false).next().css("background-position", "0 0");
				$(".payment_o2o .th_skip .table_normal tr").not(".product_tit").each(function(){
					$(this).find(".choice_list > li").each(function(){
						$(this).find(".chk_list > li:eq(0) input[type='radio']").next().css("background-position", "0 -100px");
						$(this).find(".chk_list > li:eq(1) input[type='radio']").prop("checked", true).next().css("background-position", "0 -150px");
					});
				});
			}
		}
	});
	
	o2oEachCheckBox.on({
		"change" : function(){
			if( o2oEachCheckBox.filter(":checked").size() == o2oEachCheckBoxSize ){
				o2oAllCheckBox.prop("checked" , true).next().css("background-position", "0 -75px");
			} else {
				o2oAllCheckBox.prop("checked" , false).next().css("background-position", "0 -20px");
			}
			
			if( $(this).prop("checked") == true ){
				$(this).parents("tr").next().find(".choice_list > li").each(function(){
					$(this).find(".chk_list > li:eq(0) input[type='radio']").prop("checked", true).next().css("background-position", "0 -150px");
					$(this).find(".chk_list > li:eq(1) input[type='radio']").next().css("background-position", "0 -100px");
				});
			} else {
				$(this).parents("tr").next().find(".choice_list > li").each(function(){
					$(this).find(".chk_list > li:eq(0) input[type='radio']").next().css("background-position", "0 -100px");
					$(this).find(".chk_list > li:eq(1) input[type='radio']").prop("checked", true).next().css("background-position", "0 -150px");
				});
			}
		}
	});
	
	$(".payment_o2o .table_normal tr").each(function(){
		var _this = $(this);
		_this.find("input[type='radio']").on({
			"change" : function(){
				var listSize 	= _this.find(".choice_list > li").size();
				var checked1 	= 0;
				var checked2 	= 0;
				var allChecked 	= 0;
				
				_this.find(".chk_list").each(function(){
					if( $(this).find(">li").eq(1).find("input[type='radio']").prop("checked") == true ){
						checked1++;
					}
					if( $(this).find(">li").eq(0).find("input[type='radio']").prop("checked") == true ){
						checked2++;
					}
				});
				if( checked1 > 0 ){
					_this.prev(".product_tit").find("input[type='checkbox']").prop("checked" , false).next().css("background-position", "0 0");
					o2oAllCheckBox.prop("checked" , false).next().css("background-position", "0 -20px");
				}
				
				if( checked2 == listSize ){
					_this.prev(".product_tit").find("input[type='checkbox']").prop("checked" , true).next().css("background-position", "0 -50px");
					$(".payment_o2o .table_normal .product_tit").each(function(){
						
						if( $(this).find("input[type='checkbox']").prop("checked") == false ){
							allChecked ++;
						}
					});
					if( allChecked == 0 ){
						o2oAllCheckBox.prop("checked" , true).next().css("background-position", "0 -75px");
					}
				}
			}
		});
	});
	/* end : cart */
	
	
	/* singup  */
	//agree_check
	$(".agree_info").each(function(){
		var _this 				= $(this);
		var allCheckBox 		= _this.find(".agree_check");
		var eachCheckBox		= _this.find(".css-checkbox");
		var eachCheckBoxSize 	= eachCheckBox.size();
		
		allCheckBox.on({
			"click" : function(){
				var __this = $(this);
				if( __this.hasClass("on") ){
					__this.removeClass("on");
					eachCheckBox.prop("checked" , false).next().css({ "background-position" : "0 0" });
					//addInfoReset();
					$(".disable").show();			
				} else {
					__this.addClass("on");
					eachCheckBox.prop("checked" , true).next().css({ "background-position" : "0 -50px" });
					$(".disable").hide();
				}
			}
		});
		
		eachCheckBox.on({
			"change" : function(){
				if( eachCheckBox.filter(":checked").size() == eachCheckBoxSize ){
					allCheckBox.addClass("on");
					$(".disable").hide();
				} else {
					allCheckBox.removeClass("on");
					addInfoReset();
					$(".disable").show();
				}
			}
		});
	});
	
	/* 2016.03.21 추가 (동의시 작성가능)*/
	
	var interestedBrandId 	= [];
	var interestedBrandOptionSize;
	
	var _top_agree	= $(".top_agree");	
	_top_agree.find("input[type='checkbox']").on({
		"change" : function(){
			if( $(this).prop("checked") == true ){
				$(".disable").hide();
			} else {
				addInfoReset();
				$(".disable").show();
			}
		}
	});

	function addInfoReset(){
		var target = $(".add_information");
		target.find("input[type='checkbox']").prop({ "checked" : false, "diabled" : false, "readonly" : false }).next().css("background-position", "0 0");
		target.find("input[type='radio']").prop({ "checked" : false, "diabled" : false, "readonly" : false }).next().css("background-position", "0 -100px");
		target.find("input[type='text']").val("");
		target.find("select").find("option:first-child").prop("selected" , true);
		
		target.find("[data-append-target='interested-brand']").empty();
		
		interestedBrandOptionSize = 0;
		$("*[data-select]").each(function(i){
			interestedBrandId[i] = [];
		});
		
	}
	/* //2016.03.21 추가 (동의시 작성가능)*/

	if($(".cs_cont .faq dl:visible").length > 0){
		$(document).on("click",".cs_cont .faq dl dt a",function(){
			if($(this).hasClass("on")){
				$(this).removeClass("on").parents("dt").next("dd").hide();
			}else{
				$(this).addClass("on").parents("dt").next("dd").show().siblings("dd").hide().end().end().siblings("dt").find("a").removeClass("on");
			}
			return false;
		});				
	}
	/* 
	// 3hourshop
	$(".marketing_3hour .btn_m_gray").on("click",function(){
		if($(this).parents(".order_choice").find(".choice:visible").length > 0){
			if(	($(".marketing_3hour .datepicker").val() === "出境日") || 
				($("select.select_spot option:selected").text() === "出境地点") ||
				($(".marketing_3hour .order_choice .time select").eq(0).find("option:selected").text() === "点") ||
				($(".marketing_3hour .order_choice .time select").eq(1).find("option:selected").text() === "分") ||
				($(".marketing_3hour .order_choice .select_spot2 option:selected").text() === "品牌分类" )){return false;}
			$(this).parents(".choice").hide().siblings(".result").show();
		}else{
			$(".marketing_3hour").find(".datepicker").val("出境日").end()
			.find("select.select_spot option:eq(0)").prop("selected",true).end()
			.find(".order_choice .time select").eq(0).find("option:eq(0)").prop("selected",true).end().end()
			.next("select").find("option:eq(0)").prop("selected",true);
			$(this).parents(".result").hide().siblings(".choice").show();
		}
		return false;
	}); */
	
	$(".btn_compare").on("click",function(e){
		if(isLogin == "true"){  
			if($(this).hasClass("on")){
				$(this).text("商品比较 开启").removeClass("on").parents(".product_list_wrap").find(".product_list .list").removeClass("compare_on");
			}else{
				$(this).text("商品比较 关闭").addClass("on").parents(".product_list_wrap").find(".product_list .list").addClass("compare_on");
				comparecheck();
			}
		} else {
			if(confirm("您需要先登录网页才能使用商品比较功能。要现在登录吗?")){
				loginPopup(contextPath+'/login');
			}
		}
		e.preventDefault();
	});
	
	$(document).on("click", ".css-checkbox-compare", function(){
		if(typeof(product_compare) != 'undefined'){ // 개발 체크 순서 소팅 추가
			product_compare.setData($(this));
		}
		comparecheck($(this));
	});
	
	
	/* $("select.select_spot").on("change",function(){
		var text = $(this).find("option:selected").text();
		if(text === "出境地点"){return false;}
		$(this).parents(".choice").siblings(".result").find(".select_spot").html(text);
		if(text === "仁川机场" || text === "金浦机场"){
			$(this).siblings(".select_spot2").show().parents(".choice").addClass("incheon").siblings(".result").find(".incheon_spot2").show();			
		}else{
			$(this).siblings(".select_spot2").hide().parents(".choice").removeClass("incheon").siblings(".result").find(".incheon_spot2").hide();			
		}
	}); */
	
	
	
	
	tableScroll();
	
	$(document).on("click", ".btn_com", function(e){
		//compare_set();
		tableScroll();
		if(typeof(product_compare) != 'undefined'){
			product_compare.init(); 
		}
		layerpopup("compare_popup_layer");
		e.preventDefault();
	});
	
	$(document).on("click" , ".popup_layer .btn_close", function(e){
		$(this).parents(".popup_layer").hide();
		e.preventDefault();
	});

	/* right banner 2017.06.23  닫힘버튼 추가  s */
	$(document).on("click" , ".doongdoonge_banner .btn_close", function(e){
		$(this).parents(".doongdoonge_banner").hide();
		e.preventDefault();
	});	
	/* right banner 2017.06.23  닫힘버튼 추가  e */

	if($(".global_box").length > 0){
		$(".global_box").find(".country_select > ul").mCustomScrollbar();
		$(".global_box").find(".brand_select > ul").mCustomScrollbar();
	}
	
	if($(".select_place").length > 0){
		$(".select_place").on("change" , function(){
			depart_place($(this));
		});
	} 
	
	// 경유지 선택
	function depart_place(othis){
		var val 	= othis.val();
		var viaArea = othis.parents("table").find(".direct");
		
		if( val == "P" || val == "U" || val == "I" ){
			viaArea.find(".chk_list > li:eq(0) input[type='radio']").prop({ "checked" : true , "disabled" : true }).next().css("background-position" , "0px -150px");
			viaArea.find(".chk_list > li:eq(1) input[type='radio']").prop({ "checked" : false , "disabled" : true }).next().css("background-position" , "0px -100px");
		} else if ( val == "" ){
			viaArea.find("input[type='radio']").prop({ "checked" : false , "disabled" : true }).next().css("background-position" , "0px -100px");
		}
		else {
			viaArea.find(".chk_list > li:eq(0) input[type='radio']").prop({ "checked" : true , "disabled" : false }).next().css("background-position" , "0px -150px");
			viaArea.find(".chk_list > li:eq(1) input[type='radio']").prop({ "checked" : false , "disabled" : false }).next().css("background-position" , "0px -100px");
		}
		
		viaArea.find(".select_via").hide().find("option:first-child").prop("selected" , true);
	}

	// sub brand auto complete
	$(".brand_sort_wrap .sort_choice input[type='text']").on("focusin focusout blur keyup",function(e){
		e = e || window.event;
		var resultArea = $(".brand_sort_wrap .sort_choice .result");
		var val = $.trim($(this).val());
		if(e.type === "focusin"){
			if(val){
				resultArea.show();
			}else{
				resultArea.hide();
			}
		}else if(e.type === "keyup"){
			if(val){
				resultArea.show();
			}else{
				resultArea.hide();
			}
		} else {
			resultArea.hide();
		}
	});
	
	// 내가1등 auto complete 2016-10-20 수정 
	$(".brand_search_wrap .menu_select input[type='text']").on("focusin focusout blur keyup",function(e){//2016-10-20 수정
		e = e || window.event;
		var resultArea = $(".brand_search_wrap .menu_select .auto_complete");//2016-10-20 수정
		var val = $.trim($(this).val());
		if(e.type === "focusin"){
			if(val){
				resultArea.show();
			}else{
				resultArea.hide();
			}
		}else if(e.type === "keyup"){
			if(val){
				resultArea.show();
			}else{
				resultArea.hide();
			}
		} else {
			resultArea.hide();
		}
	});
	
	// select country
	$("select.select_country").on({
		"change" : function(){
			if( $(this).find("option:selected").text() == "中国" ){
				$("select.select_city").show();
			} else {
				$("select.select_city").hide();
			}
		}
	});
	
	// interested_brand
	$("*[data-select]").each(function(){
		var _this 				= $(this);
		var selectEventTarget 	= _this.attr("data-select");
		var selectAppendTarget  = $("*[data-append-target='"+selectEventTarget+"']");
		var size 				= selectAppendTarget.children().size();
		var btnSelect			= _this.next("a");
		var brandId				= [];
		
		btnSelect.on({
			"click" : function() {
				var brandTxt = _this.find("option:selected").text();
				var brandVal = _this.find("option:selected").val();
					
				if( $.inArray( brandVal, brandId ) > -1 ){
					alert("这是已经选过的品牌");
					return;
				} 
				
				if( size >= 3 ) {
					alert("可选择最多3个喜爱的品牌");
					return;
				}
				
				if( _this.find("option:selected").index() == 0 ){
					alert("请输入您有兴趣的的品牌");
					return;
				}
				
				brandId.push(brandVal);
				selectAppendTarget.append("<span>" + brandTxt + "<a href='#'><img src='"+ path +"img/common/ico_delete.png' alt='delete'></a><input type='hidden' value='" + brandVal + "'/></span>");
				size++;

				selectAppendTarget.find("a").off("click").on({
					"click": function (e) {
						remove($(this));
						e.preventDefault();
					}
				});
			}
		});

		selectAppendTarget.find("a").on({
			"click" : function(e){
				remove($(this));
				e.preventDefault();
			}
		});

		function remove(target){
			var selectVal = target.next().val();
			brandId[$.inArray(selectVal,brandId)] = null;
			target.parent("span").remove();
			size --;
		}
	});
	
	// interested_category
	$(".category_select_wrap ").each(function(){
		var _this = $(this);
		
		_this.find("input[type='checkbox']").on({
			"change" : function(){
				var checkedLength = _this.find("input[type='checkbox']:checked").size();
				
				if( checkedLength > 2 ){
					_this.find("input[type='checkbox']").not(":checked").prop("disabled" , true).addClass("css-checkbox-disable").next().addClass("css-label-disable").addClass("lable_disabled");
				} else {
					_this.find("input[type='checkbox']").not(":checked").prop("disabled" , false).removeClass("css-checkbox-disable").next().removeClass("css-label-disable").removeClass("lable_disabled");
				}
				//inputFormLableChange();
			}
		});
	});
	
	/* member */
	// country phone select
	$(".tel_box").each(function(){
		var _this 	= $(this);
		var  select = _this.find(">select");//20190211휴대폰 번호 UI변경
		
		select.on({
			"change" : function(){
				var dataId = $(this).find("option:selected").data("id");
				_this.find("*[data-target]").hide();
				_this.find("*[data-target*='"+ dataId +"']").show(); // 2016-08-04_수정
			}
		});
	});
	/* end : member */
	
	// satisfaction
	var chkList = $(".serch_survey .survey_box .chk_list");
	var chkListli = chkList.find("li");

	chkListli.on("click", function() {
		chkListli.removeClass("on").find("input:radio").prop("checked", true).next().css("background-position", "0px -100px");
		$(this).addClass("on").find("input:radio").prop("checked", true).next().css("background-position", "0px -150px");
	});

	// 쿠폰 등록 레이어
	$(".btn_coupon_save_regi").on({
		"click" : function(e){
			layerpopup("coupon_save_regi", false)
			e.preventDefault();
		}
	});

});

function table_scroll_width(){
	var taableInner = $(".table_scroll_inner");
	taableInner.width(taableInner.find(".table_tbody").length * taableInner.find(".table_tbody").outerWidth());	
}

function inputFormLableChange(){
	//개발 적용시 주석처리 요망.
	$("input[type='radio']").each(function(){
		var _this = $(this);
		if( _this.is(":checked") ){
			_this.next().css("background-position","0 -150px").removeClass("lable_disabled");
		} else if( _this.is(":disabled") ){
			_this.next().addClass("lable_disabled");
		} else {
			_this.next().css("background-position","0 -100px").removeClass("lable_disabled");
		}
	});
	
	$("input[type='checkbox']").each(function(){
		var _this = $(this);
		if( _this.is(":checked") ){
			if( _this.hasClass("css-checkbox-all") ){
				_this.next("background-position","0 -75px").removeClass("lable_disabled");
			} else {
				_this.next("background-position","0 -50px").removeClass("lable_disabled");
			}
		} else if( _this.is(":disabled") ){
			_this.next().addClass("lable_disabled");
		} else {
			if( _this.hasClass("css-checkbox-all") ){
				_this.next("background-position","0 -20px").removeClass("lable_disabled");
			} else {
				_this.next("background-position","0 0px").removeClass("lable_disabled");
			}
		}
	});
}

function checkboxChecked(){
	$(".css-checkbox:checked").next("label").css("background-position", "0 -50px");
}

function labelChecked(){
	$(".css-label").each(function(){
		var bp = 0;
		var othis = $(this);
		bp = othis.prev(".css-checkbox").is(":checked") ? -50 : 0;
		
		if( othis.parents(".tit_top").size() > 0 || othis.prev(".css-checkbox").hasClass("css-checkbox-all") ){
			bp = othis.prev(".css-checkbox").is(":checked") ? -75 : -20;
		}
		
		if(othis.attr("class").indexOf("css-label-") > -1){			
			var color = othis.attr("class").split("css-label-")[1];
			switch(color) {
				case "red":
					bp = othis.prev(".css-checkbox").is(":checked") ? -325 : -300;
				break;
				case "orange":
					bp = othis.prev(".css-checkbox").is(":checked") ? -375 : -350;
				break;		
				case "yellow":
					bp = othis.prev(".css-checkbox").is(":checked") ? -425 : -400;
				break;		
				case "lemon":
					bp = othis.prev(".css-checkbox").is(":checked") ? -475 : -450;
				break;		
				case "lime":
					bp = othis.prev(".css-checkbox").is(":checked") ? -525 : -500;
				break;		
				case "green":
					bp = othis.prev(".css-checkbox").is(":checked") ? -575 : -550;
				break;		
				case "skyblue":
					bp = othis.prev(".css-checkbox").is(":checked") ? -625 : -600;
				break;		
				case "blue":
					bp = othis.prev(".css-checkbox").is(":checked") ? -675 : -650;
				break;		
				case "darkblue":
					bp = othis.prev(".css-checkbox").is(":checked") ? -725 : -700;
				break;		
				case "white":
					bp = othis.prev(".css-checkbox").is(":checked") ? -775 : -750;
				break;		
				case "gray":
					bp = othis.prev(".css-checkbox").is(":checked") ? -825 : -800;
				break;	
				case "disable":
					bp = othis.prev(".css-checkbox").is(":checked")  ? -850 : 0;
				break;	
				case "all":
					bp = othis.prev(".css-checkbox").is(":checked")  ? -75 : -20;
				break;	
				case "compare":
					bp = othis.prev(".css-checkbox").is(":checked")  ? -920 : -880;
				break;	
			}
		}
		if(othis.prev(".css-checkbox").hasClass("css-checkbox2")){
			bp = othis.prev(".css-checkbox").is(":checked")  ? -250 : -200;
		}
		othis.css("background-position", "0 "+bp+"px");
	});	
}

function radioChecked(){
	$(".css-radiobox:checked + label").css("background-position", "0 -150px");
}

function gift_listradiobox(){
	$(".cart_wrap .gift_list .css-radiobox").each(function(){
		if($(this).is(":checked")){
			$(this).siblings(".css-radiolabel")
				.css("background-position","0px -150px")
				.siblings("dl").find("img").not(".ico").addClass("on");
		}
	});
}

function titsort(){
	$(".brand_sort_wrap .brand_part").each(function(){
		if( $(this).find(".part_tit").height() > 25 ){
			$(this).find(".part_tit").css("margin-top","-"+($(this).find(".part_tit").height()/2)+"px");
		}
	});
}
	
function comparecheck(obj){
	var compareCheckbox = $(".product_list").find(".css-checkbox-compare");
	if(compareCheckbox.filter(":checked").length > 1 && compareCheckbox.filter(":checked").length < 11  ){
		compareCheckbox.siblings(".btn_com").hide().end().end()
			.find(".css-checkbox-compare:checked").siblings(".btn_com").show();			
	} else if( compareCheckbox.filter(":checked").length == 11 ){
		obj.prop("checked", false).next().css({"background-position" : "0 -880px"});	
		alert("最多可选择10项商品进行商品比较");
	}
	else{
		compareCheckbox.siblings(".btn_com").hide();
	}
}

function layerpopup(layer, pos){
	var popup = $("#"+layer),
		margintop = ($(window).height() - popup.height())/2,
		left = (931-popup.width())/2;
	if($(window).height() <= popup.height()){margintop = 0;}		
	var top = $(document).scrollTop() - 170 + margintop;	
	if(popup.parents("#container").length < 1){
		left = ($(window).width()-popup.width())/2;
		top = $(document).scrollTop() + margintop;

		if(layer == 'popup_email'){ //개발수정
			left = ($(window).width())/2; 
			top = $(document).scrollTop() + 120 + margintop;
		}
	}	
	if(left < 0){left = 0;}

	popup.show().find(".btn_close").on("click",function(e){
		if( $(this).data("prevent") != "on" ){
			popup.hide();
		}
		e.preventDefault();
	});

	if( pos == false ){
	} else {
		popup.css({"position":"absolute","left":left,"top":top});
	}
}

function selecttab(tid,num){
	var count = num - 1;
	$("#"+tid).find(".tab_menu").eq(0).find("a").removeClass("on").eq(count).addClass("on")
	.end().end().end().end().find(".area").hide().eq(count).show();	
}

function eventBubblingChecked( target, _href ){
	if( _href.indexOf("#") > -1 || _href == "" || _href == "undefined" ){
		target.preventDefault();
	}
}

var detailimg = null;
function productDetailZoom(){
	detailimg = $(".detail_view .pr_wrap .img");
	detailimg.find("p.zoom img").on("load",function(){
			if($(this).width() >= 900){
				detailimg.find("p.productimg").css("cursor","pointer").on("mouseenter",function(e){
					e = e || window.event;
					imgzoom(e,$(this));
				}).on("mouseleave",function(){
					imgzoomoff($(this));
				});
			} else{
				detailimg.find("p.productimg").css("cursor","default").off("mouseenter");
			}
	}).each(function(){
		if(this.complete){
			$(this).load();
		}
	});
	detailimg.find("ul li a").on("click",function(){
		var othis = $(this);
		othis.parents("ul").find("a").removeClass("on");
		othis.addClass("on");
		detailimg.find("p.productimg img").eq(0).attr("src",othis.find("img").attr("src"));	
		detailimg.find("p.zoom img").attr("src",othis.find("img").attr("data-image")).on("load",function(){
			if($(this).width() >= 900){
				detailimg.find("p.productimg").css("cursor","pointer").on("mouseenter",function(e){
					e = e || window.event;
					imgzoom(e,$(this));
				}).on("mouseleave",function(){
					imgzoomoff($(this));
				});
			}else{
				detailimg.find("p.productimg").css("cursor","default").off("mouseenter");
			}
		});
		return false;
	});
}

function imgzoom(e,obj){
	var zoom = detailimg.find("p.zoom");
	var zoomimg = zoom.find("img");
	zoom.css("visibility","visible").find("img").css("width" , 2000 );
	
	var othisoffset = obj.offset();
	var opoint = obj.find(".area");				
	var mat = (zoomimg.width()/320)*(-1);
	var lpoint = e.pageX - othisoffset.left -  30;
	var tpoint = e.pageY - othisoffset.top - 30;			
	opoint.css({"left":lpoint,"top":tpoint}).show();
	obj.on("mousemove",function(e){
		lpoint = e.pageX - othisoffset.left -  30;
		tpoint = e.pageY - othisoffset.top - 30;
		if(lpoint > -30 && lpoint < 280 && tpoint > -30 && tpoint < 280){
			opoint.css({"left":lpoint,"top":tpoint});					
			zoomimg.css({"margin" :(mat*tpoint)+"px 0 0 "+(mat*lpoint)+"px"});					
		}
	});
}

function imgzoomoff(obj){
	var zoom = detailimg.find("p.zoom");
	var zoomimg = zoom.find("img");
	obj.off("mousemove").find(".area").hide();
	detailimg.find("p.zoom").css("visibility","hidden");
	zoomimg.css({ "width" : "auto" });		
}

/* 17.10.19  이미지 줌 추가 */
function productDetailZoomS(){
	detailimg = $(".detail_view .pr_wrap .img");
	detailimg.find("p.zoom img").on("load",function(){
		if($(this).width() >= 900){
			detailimg.find("p.productimg").css("cursor","pointer").on("mouseenter",function(e){
				e = e || window.event;
				popimgzoom(e,$(this));
			}).on("mouseleave",function(){
				popimgzoomoff($(this));
			});
		}else{
			detailimg.find("p.productimg").css("cursor","default").off("mouseenter");
		}
	}).each(function(){
		if(this.complete){
			$(this).load();
		}
	});
	detailimg.find("ul.productimg_thum li a").on("click",function(){
		var othis = $(this);
		othis.parents("ul").find("a").removeClass("on");
		othis.addClass("on");
		detailimg.find("p.productimg img").eq(0).attr("src",othis.find("img").attr("src"));	
		detailimg.find("p.zoom img").attr("src",othis.find("img").attr("data-image")).on("load",function(){
			if($(this).width() >= 900){
				detailimg.find("p.productimg").css("cursor","pointer").on("mouseenter",function(e){
					e = e || window.event;
					popimgzoom(e,$(this));
				}).on("mouseleave",function(){
					popimgzoomoff($(this));
				});
			}else{
				detailimg.find("p.productimg").css("cursor","default").off("mouseenter");
			}
		});
		return false;
	});
}
function popimgzoom(e,obj){
	var zoom = detailimg.find("p.zoom");
	var zoomimg = zoom.find("img");
	zoom.css("visibility","visible").find("img").css("width" , 1090);
	
	var othisoffset = obj.offset();
	var opoint = obj.find(".area");				
	var mat = (zoomimg.width()/237)*(-1);
	var lpoint = e.pageX - othisoffset.left -  15;
	var tpoint = e.pageY - othisoffset.top - 15;			
	opoint.css({"left":lpoint,"top":tpoint}).show();
	obj.on("mousemove",function(e){
		lpoint = e.pageX - othisoffset.left -  15;
		tpoint = e.pageY - othisoffset.top - 15;
		if(lpoint > -15 && lpoint < 190 && tpoint > -15 && tpoint < 190){
			opoint.css({"left":lpoint,"top":tpoint});					
			zoomimg.css("margin",(mat*tpoint)+"px 0 0 "+(mat*lpoint)+"px");					
		}
	});
}
function popimgzoomoff(obj){
	var zoom = detailimg.find("p.zoom");
	var zoomimg = zoom.find("img");
	obj.off("mousemove").find(".area").hide();
	detailimg.find("p.zoom").css("visibility","hidden");
	zoomimg.css({ "width" : "auto" });					
}
/* //17.10.19  이미지 줌 추가 */

/* detail product (up/down hide slide) */
function todayProduct(_$targetObj, _$btnNext, _$btnPrev, _$targetNo) {
	var listCon = _$targetObj
	var list = listCon.find('li');

	var listView = _$targetNo;
	var listTotal = list.length;
	var num = 0;
	var k;

	if( listTotal <= listView ){
		_$btnNext.hide();
		_$btnPrev.hide();
	}
	
	for (i=listView; i<listTotal; i++) {
		list[i].style.display = 'none';
	}

	_$btnNext.bind('click', function(e) {
		e.preventDefault();

		if (num < (listTotal- _$targetNo)) {
			num++
			for (i=0; i<listTotal; i++) {
				list[i].style.display = 'none';
			}
			for (k=0; k<listView; k++) {
				if (list[k+num]) {
					list[k+num].style.display = 'block';
				}
			}
		}
	});

	_$btnPrev.bind('click', function(e) {
		e.preventDefault();
		if (num > 0) {
			num--;
			for (i=0; i<listTotal; i++) {
				list[i].style.display = 'none';
			}
			for ( k=0; k<listView; k++) {
				list[k+num].style.display = 'block';
			}
		}
	});
}

function slideList(cname,num,count,auto){
	$("."+cname).each(function(){

		if($(this).length < 1){return;}
		var oslide = $(this);
		var cnameAni = null;
		var scount = 1;
		var mt = 500;
		if(count !== null && count > 1 ){
			scount = count;
			mt = 1000;
		}
		var list = oslide.find(".list");
		var ll = Number(list.children("li").length);
		var autoSpeed;


		if(ll < Number(num)+1){
			//oslide.find(".prev").add($("."+cname+"").find(".next")).hide();
			oslide.find(".prev").hide();
			oslide.find(".next").hide();
			return;
		} else {
			oslide.find(".prev").show();
			oslide.find(".next").show();
		}
		
		if(ll < count+num){
			scount = ll - Number(count);
		}
		var mlw = Number(list.children("li").width());
		var clickable = true;
		list.css("width",ll*(mlw+2));
		function slideNext(){
			if(!clickable){return false;}
			clickable = false;
			clearTimeout(cnameAni);
			list.stop().animate({"margin-left":-mlw*scount},{duration:mt,complete:function(){
				for(var i=0;i<scount;i++){
					list.append(list.children("li").eq(0).clone());
					list.children("li").eq(0).remove();
				}
				slideReset($(this));
			}});
		}

		cnameAni = setTimeout(function(){slideNext();},3000);
		if( auto == false ){
			clearTimeout(cnameAni);
		}

		function slideReset(othis){
			othis.css("margin-left",0);
			cnameAni = setTimeout(function(){slideNext();},3000);
			if( auto == false ){
				clearTimeout(cnameAni);
			}
			clickable = true;
			return false;
		}
		oslide.find(".prev").on("click",function(e){
			if(!clickable){return false;}
			clickable = false;
			e.preventDefault();
			clearTimeout(cnameAni);
			list.stop();
			for(var i=0;i<scount;i++){
				list.prepend(list.children("li:last").clone());
				list.children("li:last").remove();
			}
			list.css("margin-left",-mlw*scount)
				.stop().animate({"margin-left":0},{duration:mt,complete:function(){
				slideReset($(this));
			}});
		});
		oslide.find(".next").on("click",function(e){
			e.preventDefault();
			slideNext(cname);
		});
		oslide.on("mouseover mouseleave mouseout mouseup mousemove focusin focusout",function(e){
			e = e || window.event;
			if(e.type === "mouseover" || e.type === "mouseout" || e.type === "mouseup" || e.type === "mousemove" || e.type === "focusin"){
				clearTimeout(cnameAni);
			}else{
				cnameAni = setTimeout(function(){slideNext();},3000);
				if( auto == false ){
					clearTimeout(cnameAni);
				}
			}
		});
	});
}

function tableScroll(){
	if($(".table_scroll").length > 0){
		table_scroll_width();
		$(".table_scroll").mCustomScrollbar({axis:"x", hScroll : false})
		.find(".css-checkbox").on("click",function(){
			if($(this).is(":checked")){
				var truenum = 0;
				$(this)	.parents(".table_scroll").find(".css-checkbox").not($(this).parents(".table_scroll").find("tr.disable .css-checkbox")).each(function(){
					if($(this).is(":checked")){truenum++;}
					
					if(truenum === $(this).parents(".table_scroll").find(".css-checkbox").not($(this).parents(".table_scroll").find("tr.disable .css-checkbox")).length){
						$(this)	.parents(".table_scroll").siblings(".th_area").find(".css-checkbox").prop("checked",true)
							.siblings("label").css("background-position","0px -50px");
					}
				});
			}else{
				$(this)	.parents(".table_scroll").siblings(".th_area").find(".css-checkbox").prop("checked",false)
					.siblings("label").css("background-position","0px 0px");
			}
			
			var checkedLength = $(".table_scroll").find(".css-checkbox").filter(":checked").size();
			$(".compare .notice .btn_area strong").text(checkedLength);
			
		}).end().siblings(".th_area").find(".css-checkbox").on("click",function(){
			if($(this).is(":checked")){		
				$(this)	.parents(".th_area").siblings(".table_scroll").find(".css-checkbox").each(function(){
					if(!$(this).is(":checked")){
						$(this).click();
					}
				});				
			}else{
				$(this)	.parents(".th_area").siblings(".table_scroll").find(".css-checkbox").prop("checked",false)
					.siblings("label").css("background-position","0px 0px");
			}
			
			var checkedLength = $(".table_scroll").find(".css-checkbox").filter(":checked").size();
			$(".compare .notice .btn_area strong").text(checkedLength);
		});	
	}
}

function facetOptionCheck(){
	if( $(".facet_more").size() > 0 ){
		$(".more_select .tit").show();
	} else {
		$(".more_select .tit").hide();
	}
}

function commonFacetUI(priceMinVal,priceMaxVal){
	var minprice = 0,maxprice = 100;
	var inputmin = 0,inputmax = 100;
	var checkMaxVal, checkMinVal;
	
	if($(".power_search:visible").length > 0){
		if( priceMinVal ){
			$(".search_slidebar #minrange").val(priceMinVal);
		}
		if( priceMaxVal ){
			$(".search_slidebar #maxrange").val(priceMaxVal);
		}
		
		if(! $(".power_search").hasClass("category_page") || $(".facet_more").size() == 0 ){
			$(".more_select .tit").hide();
		} else {
			$(".more_select .tit").show();
		}
		
		$(".power_search").find("label").each(function(){
			$(this).attr("title",$(this).text());
		});
		$(".power_search").find(".css-checkbox").each(function(){
			var othis = $(this);
			if(othis.is(":checked")){
				ischeck(othis);			
			}
		});
		if($(".select_item").find("dd.select_label").length < 1){
			$(".select_item").find("dd.none_selected .select_info").show();
		}
		minprice = Number($("#minrange").val());
		maxprice = Number($("#maxrange").val());
		inputmin = minprice;
		inputmax = maxprice;
		checkMinVal = minprice;
		checkMaxVal = maxprice;
		$("#slidebar_bubble_right .price").text("$"+maxprice);
		$("#slidebar_bubble_left")
			.css("left",0 - String(minprice).length*8 -10)
			.find(".price").text("$"+minprice);
		$("#minprice").val(minprice);
		$("#maxprice").val(maxprice);		
		if($(".choice_more:visible").length > 0){
			$(".choice_more").mCustomScrollbar();
		}
		
		$(".category .btn_language li a").off("click").on({
			"click" : function(e){
				var idx = $(this).parent().index();
				if(! $(this).hasClass("on") ){
					$(".category .btn_language li a").removeClass("on");
					$(this).addClass("on");
					$(".category .choice_more .chk_list").hide().eq(idx).show();
				}
				e.preventDefault();
			}
		});
	}	
	var slidebarmax = 335;
	var slidebarmin = 10;
	var sliderobj = null;
	
	$("#minprice").off("focusout").off("keydown").on( "focusout keydown" , function(e){
		e =e || window.event;
		if(e.type === "focusout" || (e.type === "keydown" && e.keyCode === 13)){
			var thisn = $(this).val();	
			
			if(Number(thisn) >= Number(checkMaxVal) || Number(thisn) < Number(inputmin)){
				$(this).val(checkMinVal);
			}else{					
				var w = Number((thisn-minprice)/(maxprice - minprice)*335);
				w = (w >= Number($("#slidebar_bubble_right").attr("left")) - 10)? w - 10 : w;			
				$("#slidebar_bubble_left .price").text("$"+thisn);
				$("#slidebar_left").css("left",w);
				$("#slidebar_right").siblings(".dim_mask").css("width",w);
				$("#slidebar_bubble_left").css("left",w - String(thisn).length*8 -10).attr("left",w);
				slidebarmin = w + 10;
				checkMinVal = thisn;
			}
		}	
	});
	$("#maxprice").off("keydown").off("focusout").on("focusout keydown", function(e) {
		e = e || window.event;
		if ( e.type === "focusout" || (e.type === "keydown" && e.keyCode === 13) ) {
			var thisn = $(this).val();
			if ( Number(thisn) > Number(inputmax) || Number(thisn) <= Number(checkMinVal) ) {
				$(this).val(checkMaxVal);
			} else {
				var w = Number((thisn-minprice)/(maxprice - minprice) * 335);
				w = ( w <= Number($("#slidebar_bubble_left").attr("left")) + 10
					|| maxprice === parseInt(thisn) ) ? w + 10 : w;
				$("#slidebar_bubble_right .price").text("$" + thisn);
				$("#slidebar_right").css("left", w);
				$("#slidebar_right").siblings(".mask").css("width", w);
				$("#slidebar_bubble_right").css("left", w + 3).attr("left", w + 3);
				slidebarmax = w - 10;
				checkMaxVal = thisn;
			}
		}
	});
	$(document).off("mouseup").on("mouseup",function(){		
		$("#search_slidebar").removeClass("search_slidebar_wrap");
		$("#container").off("mousemove");
		sliderobj = null;
		$(document).off("mousemove");
	});
	
	$("#slidebar_left").off("mousedown").on("mousedown",function(e){
		e = e || window.event;
		sliderobj = {
			left:$("#slidebar_left"),
			mask:$("#slidebar_right").siblings(".dim_mask"),
			lbubble:$("#slidebar_bubble_left"),
			input:$("#minprice")
		};	
		var left = Number((e.pageX - $(".search_slidebar").offset().left - 20).toFixed());
		var num = ((left/335)*(maxprice - minprice)+minprice).toFixed();		
		$("#search_slidebar").addClass("search_slidebar_wrap");
		$(document).on("mousemove",function(e){			
			e = e || window.event;
			left = Number((e.pageX - $(".search_slidebar").offset().left - 20).toFixed());
			num = ((left/335)*(maxprice - minprice)+minprice).toFixed();
			if(num < minprice){num = minprice;}		
			if(num > maxprice){num = maxprice;}		
			if(left >= -3){	
				left = (left < 0 ? 0 :left);
				slidemin(left,num);
				slidebarmin = left + 10;
			}		
		});
	});
	
	$("#slidebar_right").off("mousedown").on("mousedown",function(e){
		e = e || window.event;
		sliderobj = {
			right:$("#slidebar_right"),
			mask:$("#slidebar_right").siblings(".mask"),
			rbubble:$("#slidebar_bubble_right"),
			input:$("#maxprice")
		};		
		var left = Number((e.pageX - $(".search_slidebar").offset().left - 20).toFixed());
		var num = (((left-10)/335)*(maxprice - minprice)+minprice).toFixed();			
		$("#search_slidebar").addClass("search_slidebar_wrap");
		$(document).on("mousemove",function(e){		
			e = e || window.event;		
			left = Number((e.pageX - $(".search_slidebar").offset().left - 20).toFixed());
			num = (((left-10)/335)*(maxprice - minprice)+minprice).toFixed();
			if(num < minprice){num = minprice;}		
			if(num > maxprice){num = maxprice;}				
			if(left < 346){
				slidemax(left,num);
				slidebarmax = left - 10;	
			}
			
		});
	});
	
	function slidemin(w,num){	
		if(sliderobj === null){return false;}
		if(w >= -1 && w < slidebarmax ){				
			if(w >= slidebarmax){w = slidebarmax;}
			if(w <= 0 ){w = 0;}
			sliderobj.input.val(num);
			sliderobj.left.css("left",w);
			sliderobj.mask.css("width",w);
			sliderobj.lbubble.css("left", w - String(num).length*8 -10).attr("left",w)
			.find(".price").text("$"+num);	
			checkMinVal = num;							
		}
	}
	
	function slidemax(w,num){
		if(sliderobj === null){return false;}
		if(w > slidebarmin && w < 346 ){			
			if(w <= slidebarmin){w = slidebarmin;}
			sliderobj.right.css("left",w);
			sliderobj.mask.css("width",w);
			sliderobj.rbubble.css("left",w+4).attr("left",w)
			.find(".price").text("$"+num);
			sliderobj.input.val(num);	
			checkMaxVal = num;		
		}
	}
	
	function ischeck(obj){
		var select_item = $(".select_item");			
		select_item.show();
		var dd = $("<dd>")
			.addClass("select_label")
			.html(obj.siblings("label").text()+"<a href='#url'><img src='"+path+"img/common/ico_delete.png' alt='delete'></a>")
			.attr("data-id",obj.attr("id"));		
		$(".select_item").append(dd);		
		select_item.find("dd.none_selected .select_info").hide();	
		dd.find("a").on("click",function(){
			var athis = $(this);					
			var power_search = $(".power_search").find("input[type='checkbox']:checked");
			for(var i=0;i < power_search.length;i++){
				if(power_search.eq(i).attr("id") === athis.parents("dd").attr("data-id")){
					power_search.eq(i)
						.prop("checked",false)
						.siblings("label").css("background-position","0 0");
					break;
				}			
			}	
			athis.parents("dd").remove();
			facetConditionAreaEmptyGuide();
		});		
	}
	
	function ischeckCategory(obj){
		var select_item = $(".select_item");			
		select_item.show();
		
		select_item.find("[data-id^='facetCategoryS']").remove();
		
		if( obj.attr("id").indexOf("facetCategoryL") > -1 ){
			select_item.find("[data-id^='facetCategoryL']").remove();
		}
		
		if( obj.index() == 0 && obj.attr("id").indexOf("facetCategoryL") > -1 ){
			// all cetegory
		} else {
			var dd = $("<dd>")
				.addClass("select_label")
				.html(obj.find("span").text()+"<a href='#url'><img src='"+path+"img/common/ico_delete.png' alt='delete'></a>")
				.attr("data-id",obj.attr("id"));
				
			$(".select_item").append(dd);		
			select_item.find("dd.none_selected .select_info").hide();	
			dd.find("a").on("click",function(){
				var _this = $(this);					
				var dataId = _this.parent("dd").data("id");
				
				$("#"+dataId).removeClass("on");
				_this.parents("dd").remove();
				
				if( dataId.indexOf("facetCategoryL") > -1 ){
					select_item.find("[data-id^='facetCategoryS']").remove();
					$(".power_search .btn_result").eq(0).addClass("on");
					$(".power_search .brand_choice a").removeClass("on");
				}
				
				facetConditionAreaEmptyGuide();
			});		
		}
		facetConditionAreaEmptyGuide();
	}
	
	function facetConditionAreaEmptyGuide(){
		var select_item = $(".select_item");
		if(select_item.find("dd.select_label").length < 1){
			select_item.find("dd.none_selected .select_info").show();
		}
	}
	
	var facetCategoryH = $(".power_search .category").height();
	
	$(".power_search .brand_choice").each(function(i){
		$(this).find("a").each(function(j){
			$(this).attr({ "id" : "facetCategoryS_"+i+"_"+j });
		});
	});
	
	$(".power_search .btn_result").each(function(i){
		$(this).attr({ "id" : "facetCategoryL_"+i });
	});
	
	$(".power_search .toggle_btn").off("click").on("click",function(){
		if($(this).hasClass("toggle_on")){
			$(this).removeClass("toggle_on").addClass("toggle_off").parents("th").next("td").removeClass("on");		
			$(".power_search .category").css({ "height" : facetCategoryH });
			$(".choice_more").mCustomScrollbar("destroy");
		}else{
			$(this).removeClass("toggle_off").addClass("toggle_on").parents("th").next("td").addClass("on");			
			$(".power_search .category").css({ "height" : "auto" });
			$(".choice_more").mCustomScrollbar();
		}
		return false;
	});
	
	/* $(".power_search .btn_language a").off("click").on("click",function(){
		$(this).addClass("on").parents("li").siblings("li").find("a").removeClass("on");
		return false;
	}); */
	
	$(".power_search .btn_result").off("click").on("click",function(e){
		$(".brand_choice a").removeClass("on"); 
		if(! $(this).hasClass("on") ){
			$(this).addClass("on").siblings(".btn_result").removeClass("on");
			$(".brand_choice").show();
			if( $(".facet_more").size() > 0 ){
				$(".more_select .tit").show();
				if(! $(".power_search").hasClass("category_page") ){
					if(! $(".more_select").hasClass("on") ){
						$(".more_select").addClass("on");
						$(".facet_more").hide();
					}
					$(".select_item").find("dd[data-id^='facet_more']").remove();
				}
			}
			priceSlideReset();
			$(".brand_choice").hide();
			var id = $(this).attr("href");
			$(id).show();
			ischeckCategory($(this));
			
			if($(this).hasClass("cateFirst")){
				facet.callAttr($(this).data('value'));
			};
			
		}
		e.preventDefault();
	});	
	
	$(".brand_choice a").off("click").on("click",function(e){
		if(! $(this).hasClass("on") ){
			$(".brand_choice a").removeClass("on");
			$(this).addClass("on").siblings("a").removeClass("on");
			ischeckCategory($(this));
		}
		e.preventDefault();
	});
	
	$(".power_search input[type='checkbox']").off("click").on("click",function(){
		var othis = $(this);
		var select_item = $(".select_item");
		if(othis.is(":checked")){
			ischeck(othis);		
		}else{
			var selectitem = select_item.find("dd.select_label");
			for(var i=0;i < selectitem.length;i++){
				if(selectitem.eq(i).attr("data-id") === othis.attr("id")){
					selectitem.eq(i).remove();
					break;
				}			
			}
			facetConditionAreaEmptyGuide();		
		}
	});
	
	$(".power_search .btn_reset").add(".power_search .none_selected .all").off("click").on("click",function(){
		$(".power_search .select_item dd.select_label").remove();
		$(".power_search input[type='checkbox']").prop("checked",false).siblings("label").css("background-position","0 0");	
		$(".select_item").find("dd.none_selected .select_info").show();
		$(".power_search .space > td > a").eq(0).trigger("click");
		$(".power_search .space .brand_choice > a").removeClass("on");
		priceSlideReset();
		return false;
	});
	
	$(".more_select .tit a").off("click").on("click",function(){
		if($(".more_select").hasClass("on")){
			$(".more_select").removeClass("on");	
			$(".facet_more").show();
			$(".more_select .select_item").show();
		}else{
			$(".more_select").addClass("on");
			$(".facet_more").hide();
			if($(".select_item").find("dd.select_label").length < 1){
				$(".more_select .select_item").hide();			
			}	
		}		
		return false;
	});
	
	function priceSlideReset(){
		var defaultWidth = 345;
		
		checkMinVal = minprice;
		checkMaxVal = maxprice;
		
		$("#minprice").val(minprice);
		$("#maxprice").val(maxprice);
		
		$(".search_slidebar .dim_mask").width(0);
		$(".search_slidebar .mask").width(defaultWidth);
		
		$("#slidebar_left").css({ "left" : 0 });
		$("#slidebar_right").css({ "left" : defaultWidth+"px" });
		$("#slidebar_bubble_left").removeAttr("left").css({ "left" : "" }).find(".price").text("$"+ minprice );
		$("#slidebar_bubble_right").attr("left", defaultWidth).css({ "left" : defaultWidth+3+"px" }).find(".price").text("$"+ maxprice );
	}
}

function skyScrapperProductListCheck(){
	$(".sky_scrapper .cont > div").each(function(){
		var _this = $(this);
		var allCheckbox 	= _this.find(".tit_top input[type='checkbox']");
		var eachCheckbox 	= _this.find(".area input[type='checkbox']");
		
		allCheckbox.on({
			"change" : function(){
				
				if( $(this).prop("checked") == true ){
					eachCheckbox.filter(":enabled").prop("checked" , true).next("label").css("background-position","0 -50px");
					$(this).next("label").css("background-position","0 -75px");
				} else {
					eachCheckbox.filter(":enabled").prop("checked" , false).next("label").css("background-position","0 0");
					$(this).next("label").css("background-position","0 -20px");
				}
				
				selectProdCheck();
			}
		});
		
		eachCheckbox.on({
			"change" : function(){
				var eachCheckboxLength 	= eachCheckbox.size();
				var eachCheckedLengh 	= eachCheckbox.filter(":checked").size();
				
				if( eachCheckedLengh == eachCheckboxLength ){
					allCheckbox.prop("checked" , true).next("label").css("background-position","0 -75px");
				} else {
					allCheckbox.prop("checked" , false).next("label").css("background-position","0 -20px");
				}
				selectProdCheck();
			}
		});
		
		function selectProdCheck(){
			var eachCheckedLengh 	= eachCheckbox.filter(":checked").size();
			_this.find(".btn_area_sky span em").text(eachCheckedLengh);
		}
	});
}

$.fn.singleNavSlide = function(options){
	var settings = {
		auto			: true,
		pagingClass 	: "slide_paging",
		imgAreaClass 	: "img",
		activeClass 	: "on",
		speed			: 3000
	}
	
	$.extend(settings, options);
	var _this 			= $(this);
	var imgList 		= _this.find("."+settings.imgAreaClass).find(">li");
	var listLengh 		= imgList.size();
	var activeIdx		= 0;
	var status			= true;
	
	var pagingListHtml 	= "";
	var interval;
	
	if( imgList.size() == 1 ){
		_this.find(".btn_area").hide();
		return;
	} else {
		_this.find(".btn_area").show();
	}
	
	for( var i = 0; i < listLengh; i++){
		pagingListHtml += '<li><a href="#" '+  (i == 0 ? "class='on'" : ""  )  +' >'+(i+1)+'</a></li>';
	}
	
	var pagingHtml 	= '<ul class="'+settings.pagingClass+' clear_both">'+pagingListHtml+'</ul>';
	
	_this.append(pagingHtml);
	
	var pagingDot = _this.find("."+settings.pagingClass).find("a");
	
	function pagingEvent(){
		pagingDot.on({
			"mouseenter" : function(){
				autoStop();
				activeIdx = $(this).parent().index();
				pagingDot.removeClass(settings.activeClass);
				$(this).addClass(settings.activeClass);
				imgList.filter(":visible").hide();
				imgList.eq(activeIdx).show();
			},
			"click" : function(e){
				e.preventDefault();
			}
		});
		
		_this.on({
			"mouseenter , focusin" : function(){
				if( status ){
					autoStop();
				}
			},
			"mouseleave , focusout" : function(){
				if( status ){
					autoStrat();
				}
			}
		});
		
		_this.find(".pause").on({
			"click" : function(e){
				$(this).hide();
				_this.find(".play").show();
				status = false;
				autoStop();
				e.preventDefault();
			}
		});
		
		_this.find(".play").on({
			"click" : function(e){
				$(this).hide();
				_this.find(".pause").show();
				status = true;
				autoStrat();
				e.preventDefault();
			}
		});
		
	}
	
	function autoStrat(){
		autoStop();
		interval = setInterval(function(){
			if( activeIdx < listLengh-1 ){
				activeIdx ++;
			} else if( activeIdx == listLengh-1){
				activeIdx = 0;
			}
			pagingDot.removeClass(settings.activeClass);
			pagingDot.eq(activeIdx).addClass(settings.activeClass);
			imgList.filter(":visible").hide();
			imgList.eq(activeIdx).show();
		}, settings.speed);
	}
	
	function autoStop(){
		clearInterval(interval)
	}
	
	function init(){
		pagingEvent()
		autoStrat()			
	}
	
	init();
}

function skyScrapperCartListInit(){
	var cartList = $(".sky_scrapper .cart_list_sky");
	cartList.find(".area").height($(window).height() - 169).mCustomScrollbar({}).find(".mCSB_scrollTools").height(cartList.find(".mCustomScrollBox").height() -20).css("top","10px");;
	cartList.find(" .tit_top input[type='checkbox']").prop("checked" , false).next().css({ "background-position" : "0 -20px" });
	cartList.find(".btn_area_sky span em").text(0);
	skyScrapperProductListCheck();
}

function skyScrapperTodayListInit(){
	var TodayList = $(".sky_scrapper .today_list_sky");
	TodayList.find(".area").height($(window).height() - 98).mCustomScrollbar("destroy").mCustomScrollbar({}).find(".mCSB_scrollTools").height(TodayList.find(".mCustomScrollBox").height() -20).css("top","10px");;;
	TodayList.find(" .tit_top input[type='checkbox']").prop("checked" , false).next().css({ "background-position" : "0 -20px" });
	TodayList.find(".btn_area_sky span em").text(0);
	skyScrapperProductListCheck();
}

function myInfoRecommendProduct(){
	$(".sky_scrapper .my_info .product").each(function(){
		var _this = $(this);
		_this.find(".tit_paging div p:first-child").html("<span>1</span>/"+_this.find(".product_info li").length);
		_this.find(".tit_paging a").on("click",function(){
			var list = _this.find(".product_info li");
			var count = _this.find(".tit_paging div p:first-child span");
			var num = Number(count.text());
			var totalnum = Number(list.length);
			if($(this).hasClass("prev") && num > 1){
				list.hide();
				num--;
				list.eq(num-1).show();
				count.text(num);
			}else if($(this).hasClass("next") && num < totalnum){
				list.hide();
				list.eq(num).show();
				num++;
				count.text(num);
			}
		});
	});
}

function isNumber(s) {
	s += ''; 
	s = s.replace(/^\s*|\s*$/g, '');
	if (isNaN(s)) return false;
	return true;
}

function productAmountCheck(){
	if($(".minorder").length > 0){
		$("input.pcount").each(function(){
			var _this 				= $(this);
			var minAmount 			= Number(_this.siblings(".minorder").val());
			var maxAmount 			= Number(_this.siblings(".maxorder").val());
			var packageAmount 		= Number(_this.siblings(".ordercount").val());
			
			_this.data("prevCount" , _this.val());
			_this.data("currentCount" , _this.val());
		});
	}
}

// number type max length limit
function inputNumberMaxLengthCheck(object){
	if (object.value.length > object.maxLength){
		object.value = object.value.slice(0, object.maxLength);
	}    
}


// via choice
function cartPaymentInfoViaInit(){
	var val =  $(".payment_wrap").find(".select_place").val();
	var viaArea = $(".payment_wrap").find(".direct");
	
	if( val == "P" || val == "U" || val == "I" ){
//		viaArea.find(".chk_list > li:eq(0) input[type='radio']").prop({ "checked" : true , "disabled" : true }).next().css("background-position" , "0px -150px");
//		viaArea.find(".chk_list > li:eq(1) input[type='radio']").prop({ "checked" : false , "disabled" : true }).next().css("background-position" , "0px -100px");
		viaArea.find("input[type='radio']").prop({ "disabled" : true });
	} else if ( val == "" ){
		viaArea.find("input[type='radio']").prop({ "checked" : false , "disabled" : true }).next().css("background-position" , "0px -100px");
	}
	else {
		viaArea.find("input[type='radio']").prop({ "disabled" : false });
	}
	
	var idx = viaArea.find("input[type='radio']:checked").parents("li").index();
	if( idx == 0 ){
		viaArea.find("select").hide().find("option:eq(0)").prop("selected" , true);
	} 
	else if( idx == 1 ) {
		viaArea.find("select").show();
	}
}
	
/* 메인 탭 최근게시물 2016.02.01 추가 */
 $(document).ready(function() {
	$(".tab_box").each(function(i){
	    var $bbsobj=$(this);
	    $(this).find("li dl dt").each(function(j){
	        var $curobj=$(this);
	        if(j == 0) {
	            $bbsobj.find(".content1").hide();
	            $($curobj.children("a:eq(0)").attr("href")).show();
	        }

	        $curobj.bind("keyup mouseover", function() {
	                $bbsobj.find(".content1").hide();
	                $bbsobj.find(".tabclass img").each(function(){
	                    $(this).attr("src", $(this).attr("src").replace(/_on.png/, '_off.png'));
	                });
	                imagename =  $(this).find("img").attr("src");
	                $(this).find("img").attr("src", imagename.replace(/_off.png/, '_on.png'));
	                var $targetul=$($(this).children("a:eq(0)").attr("href"));
	                $targetul.show();
	        });
	        //end hover
	        $curobj.click(function(){
	            var $targetul=$($(this).children("a:eq(0)").attr("href"));
	            $targetul.show();
	        });
	    });
		});	
	});





	// 유동적인 검색바 추가

$(document).ready(function(){	
	'use strict';

	var windowHeigt = $(window).height();
	$(document).on("click",".datepicker_close",function(){
		$.datepicker._hideDatepicker();
	});	
	
	//main bestproduct wing		
	var best_productOffsetTop =  "",
		best_product = null,
		bp =  "";
	
	// 스카이스크래퍼
	
	var mainSkyScrapperBanner,
		mainSkyScrapperBannerOffsetTop; 
	
	/* 17.06.23  right banner 추가 s */
	var mainSkyScrapperBanners,
		mainSkyScrapperBannersOffsetTop; 
	/* 17.06.23  right banner 추가 e */

	if( $(".search_bar:visible").length > 0 ){
		mainSkyScrapperBanner = $(".search_bar");
		if($(".main_wrap").length > 0){   /* 개발 수정 */
			mainSkyScrapperBannerOffsetTop = $(".main_wrap").offset().top;	
		} else {
			mainSkyScrapperBannerOffsetTop = $("#container").offset().top;
		}
	}
	/* 2016-08-29 추가 */
	if( $(".fixed_bar:visible").length > 0 ){
		mainSkyScrapperBanner = $(".fixed_bar");
		if($(".main_wrap").length > 0){   /* 개발 수정 */
			mainSkyScrapperBannerOffsetTop = $(".main_wrap").offset().top;	
		} else {
			mainSkyScrapperBannerOffsetTop = $("#container").offset().top;
		}
		//mainSkyScrapperBannerOffsetTop = $(".fixed_bar").offset().top;
	}
	/* //2016-08-29 추가 */

	/*  17.06.23 right banner 추가 s */
	if( $(".doongdoonge_banner:visible").length > 0 ){
		mainSkyScrapperBanners = $(".doongdoonge_banner");
		mainSkyScrapperBannersOffsetTop = $(".doongdoonge_banner").offset().top;
	}
	/* //17.06.23 right banner 추가 e */


	var rightScrollmenuOffsetTop = "",
		rightScrollmenuPositionTop = "",
		rightScrollmenuMarginTop = 10,
		rightScrollmenu = null;
	if($("#rnb:visible").length > 0){
		rightScrollmenu =  $("#rnb");
		rightScrollmenuOffsetTop = rightScrollmenu.offset().top;
		rightScrollmenuPositionTop = rightScrollmenu.position().top;
	}

	$(window).scroll(function(){		
		var dsT = $(document).scrollTop(),
		//var tmp = ($(document).scrollTop()+leftScrollmenuPositionTop+leftScrollmenuMarginTop)-leftScrollmenuOffsetTop;
			tmp = (dsT+rightScrollmenuPositionTop+rightScrollmenuMarginTop)-rightScrollmenuOffsetTop,
			bpo =  dsT-best_productOffsetTop-85,
			bpv = dsT+windowHeigt;
		if(rightScrollmenu !== null){
			if(tmp>155) {
				/*if(best_productOffsetTop > bpv){
					rightScrollmenu.animate({'top':tmp}, {duration:'slow', easing:'swing', queue:false});
				}*/
				rightScrollmenu.animate({'top':tmp}, {duration:'slow', easing:'swing', queue:false});
			}
			else {
				leftScrollmenu.animate({'top':leftScrollmenuPositionTop}, {duration:'slow', easing:'swing', queue:false});
				rightScrollmenu.animate({'top':rightScrollmenuPositionTop}, {duration:'slow', easing:'swing', queue:false});
			}
		}
		// main sky_scrapper left banner
		if($(".search_bar:visible").size() > 0 ){
			
			if( $(window).scrollTop() > mainSkyScrapperBannerOffsetTop ){
				mainSkyScrapperBanner.addClass("fixed");
			} else {
				mainSkyScrapperBanner.removeClass("fixed");
			}
		}
		/* 2016-08-29 추가 */
		if($(".fixed_bar:visible").size() > 0 ){
			
			if( $(window).scrollTop() > mainSkyScrapperBannerOffsetTop ){
				mainSkyScrapperBanner.addClass("fixed");
			} else {
				mainSkyScrapperBanner.removeClass("fixed");
				$('.cn_category_menu').hide();
			}
		}
		/* //2016-08-29 추가 */
		/* right banner 2017-06-23 추가  s */
		if($(".doongdoonge_banner:visible").size() > 0 ){
			
			if( $(window).scrollTop() > mainSkyScrapperBannersOffsetTop ){
				mainSkyScrapperBanners.addClass("fixed");
			} else {
				mainSkyScrapperBanners.removeClass("fixed");
			}
		}
		/* // right banner 2017-06-23 추가 e */
	});

	/* 2016-08-29 추가 */
	$(".fixed_bar .bar_menu").on("mouseover click",function(e){
		e = e || window.event;
		$(this).children().addClass('on');
		$('.cn_category_menu').show();
	});
	$(".fixed_bar .bar_menu").on("mouseleave",function(e){
		e = e || window.event;
		$(this).children().removeClass('on');
		$('.cn_category_menu').hide();
	});
	/* //2016-08-29 추가 */
	
	/* 2016-09-29 추가 */
	// 수신, 비수신	
	$("[data-radio-receive]").each(function(){
		var _this = $(this);
		var target = _this.data("radioReceive");
		_this.on({
			"change" : function(){
				$("[data-receive-target='"+ target +"']").each(function(){
					$(this).find("input[type='checkbox']").prop({ "disabled" : false });
				});
				inputFormLableChange();
			}
		});
	});
	
	$("[data-radio-noreceive]").each(function(){
		var _this = $(this);
		var target = _this.data("radioNoreceive");
		_this.on({
			"change" : function(){
				$("[data-receive-target='"+ target +"']").each(function(){
					$(this).find("input[type='checkbox']").prop({ "disabled" : true, "checked" : false });
				});
				inputFormLableChange();
			}
		});
	});
	/* //2016-09-29 추가 */
	
});

/* 2016-09-29 추가 */
function inputFormLableChange(){
	$("input[type='radio']").each(function(){ //개발 적용시 주석 처리 요망.
		var _this = $(this);
		if( _this.is(":checked") ){
			_this.next().css("background-position","0 -150px").removeClass("lable_disabled");
		} else if( _this.is(":disabled") ){
			_this.next().addClass("lable_disabled");
		} else {
			_this.next().css("background-position","0 -100px").removeClass("lable_disabled");
		}
	});
	
	$("input[type='checkbox']").each(function(){
		var _this = $(this);
		if( _this.is(":checked") ){
			_this.next("background-position","0 -50px").removeClass("lable_disabled");
		} else if( _this.is(":disabled") ){
			_this.next().addClass("lable_disabled");
		} else {
			_this.next().css("background-position","0 0px").removeClass("lable_disabled");
		}
	});
}
/* //2016-09-29 추가 */

// 상단띠배너
//function layer_toggle(obj) {
//	if (obj.style.display == 'block') obj.style.display = 'none';
//	else if (obj.style.display == 'none') obj.style.display = 'block';
//}
$(document).ready(function() {
    var ax = $(".top_bar");
   //var ac = $("#header").outerHeight();
    var ac = 171;
    var r = 0;
    if (ax.length < 1) {
        r = 0
    }
   /* ax.css({
        "background-color": "#" + (ax.find("#topBarBgColor").val())
    });*/
    ax.find(".top_bar_btn").on({
        click: function(j) {
            if ($(".top_bar").hasClass("close")) {
                $(".top_bar").removeClass("close");
                ac = $("#header").outerHeight();
                r = ax.outerHeight();
                $("#main_visual_bg").css({
                    top: ac + r
                });
                if ($(".sky_scrapper_banner:visible").length > 0) {
                    k = $(".sky_scrapper_banner").offset().top
                }
                if ($(".boutique .sub_category").length > 0) {
                    $(".sub_category").css({
                        top: ac - 29
                    })
                }
               cookieStorage.set("top_banner", "open");
            } else {
                $(".top_bar").addClass("close");
                $("#main_visual_bg").css({
                    top: ac
                });
                if ($(".sky_scrapper_banner:visible").length > 0) {
                    k = $(".sky_scrapper_banner").offset().top
                }
                if ($(".boutique .sub_category").length > 0) {
                    $(".sub_category").css({
                        top: ac - r - 29
                    })
                }
                cookieStorage.set("top_banner", "close");
            }
            j.preventDefault()
        }
    });

});


/**
* Category 리뉴얼 LNB 영역
* @2017-04-05
* @param str
* @returns
*/
$( function ($) {
	$( '.lnb' ).on( 'mouseenter', ' > li a', function(e) {
		e.preventDefault();
		var $li = $( this ).closest( 'li' );

		$li.siblings().find( '.cate1' ).hide();
		$li.find( '.cate1' ).show();
	});
	$( '.lnb' ).mouseleave( function (e){
		$( '.lnb>li>.cate1' ).hide();
	});
});


 /**
* Category 리뉴얼 카테고리 별 베스트 탭 영역
* @2017-04-05
* @param str
* @returns
*/
$( function ($) {
	$( '#best_tab li' ).each(function(index){
		$('#best_tab li' ).eq(index).click(function(){
			$('#best_tab li' ).removeClass('on');
			$(this).addClass('on');
			$( '.best_brand_wrap .ctab' ).hide();
			$( '.best_brand_wrap .ctab' ).eq(index).show();			
			return false;
		});
	});
});

 /**
* Category 리뉴얼 category_visual 롤링배너 영역
* @2017-04-05
* @param str
* @returns
*/
( function ( $ ) {

  function CategoryVisual( $el ){
    this.interval = 3000; //자동 재생 인터벌
    this.effectTime = 100; 

    this.$el = $el;
    this.$imgList = this.$el.find( '.banner_img2 .listitem' );
    this.$menuList = this.$el.find( '.slide_paging > li' );
    this.$showMenuList = null;
    this.$moveBtnContainer = this.$el.find( '.btn_move' ).hide();
    this.$next = this.$el.find( '.next');
    this.$prev = this.$el.find( '.prev');
    this.$pause = this.$el.find( '.pause');
    this.$play = this.$el.find( '.play');
    this.itemNum = this.$imgList.length;
    this.currentIndex = 0;
    this.currentPage = 0;
    this.currentMenuIndex = 0;
    this.timer = null;
    this.$imgList.hide().eq( this.currentIndex ).show().addClass( 'on');
    this.$menuList.eq( this.currentIndex ).addClass( 'on');

    var self = this;
    if( this.itemNum > 4 ){
      this.$next.off().on( 'click', function () {
        if( self.timer ){
          self.timerPause();
          self.timerStart();
        }
        self.next();
      });

      this.$prev.off().on( 'click', function () {
        if( self.timer ){
          self.timerPause();
          self.timerStart();
        }
        self.prev();
      });

      this.$moveBtnContainer.show();
    }
    this.$pause.off().on( 'click', function () {
      self.$pause.hide();
      self.$play.show();
      self.timerPause();
    });

    this.$play.off().on( 'click', function () {
      self.$pause.show();
      self.$play.hide();
      self.timerStart();
    });

    this.$menuList.off().on( 'mouseover', function(){
      if( self.timer ){
        self.timerPause();
        self.timerStart();
      }
      self.currentMenuIndex = self.$showMenuList.index( this );
      self.setIndex.call( self, self.$menuList.index( this ) );
    });

    this.$play.trigger( 'click' );
    this.updateMenuStatus();
  }

  CategoryVisual.prototype = {

    setIndex: function( value ) {
      this.deactivateAt( this.currentIndex );
      this.currentIndex = value;
      this.currentPage = Math.value/4
      this.activateAt( this.currentIndex );
    },

    timerPause: function() {
      clearInterval( this.timer );
      this.timer = null;
    },

    timerStart: function() {
      var self = this;
      this.timer = setInterval( function(){ self.next() }, this.interval );
    },

    next: function() {
      var prevIndex = this.currentIndex;
      this.setIndex( this.currentIndex + 1 < this.itemNum ? this.currentIndex+1 : 0, true );
      this.currentMenuIndex++;
      if( this.currentMenuIndex > 3 ){
          this.updateMenuStatus( this.currentIndex - prevIndex );
      }

    },

    prev: function() {
      var prevIndex = this.currentIndex;
      this.setIndex( this.currentIndex - 1 >= 0 ? this.currentIndex-1 : this.itemNum-1, true );
      this.currentMenuIndex--;
      if( this.currentMenuIndex <= 0 ){
          this.updateMenuStatus( this.currentIndex - prevIndex );
      }
    },

    activateAt:function ( index ) {
      this.$imgList.eq( index ).stop().fadeIn( this.effectTime ).addClass( 'on' );
      this.$menuList.eq( index ).addClass( 'on' );
    },

    deactivateAt:function ( index ) {
      this.$imgList.eq( index ).stop().fadeOut( this.effectTime ).removeClass( 'on' );
      this.$menuList.eq( index ).removeClass( 'on' );
    },

    updateMenuStatus:function( dir ) {
      this.$menuList.hide().removeClass( 'show' );
      if(  dir > 0 ){
          this.currentMenuIndex = 4;
          for( var i=this.currentIndex-3 ; i <= this.currentIndex ; i+=1 ){
            this.$menuList.eq( i ).show().addClass( 'show' );
          }
      }else{
          this.currentMenuIndex = 0;
          for( var i=this.currentIndex; i < this.currentIndex+4 ; i+=1 ){
            this.$menuList.eq( i ).show().addClass( 'show' );
          }
      }
      this.$showMenuList = this.$el.find( '.slide_paging > li.show' )
    }
  };

  $.fn.categoryVisual = function () {
    return this.each( function() { new CategoryVisual( $( this ) ); });
  }
})( $ );

$('.category_visual' ).categoryVisual();

/* mainVisual 17.09.15 신규작업 s */
$( document ).ready(function () {
    if ( !$('#main_visual:visible').length ) return;

    var _$target = $( '#main_visual' ),
        _$menuArea = _$target.find( '.menu' ),
        _$menuGroup = _$menuArea.find( '> li' ),
        _$menus = _$menuGroup.find( 'li > a' ),
        _$bannerGroup = _$target.find( '.visual_detail > .img' ),
        _$banners = _$bannerGroup.find( '> li' ),
        _$btnArea = _$target.find( '.newBtnArea' ),
        _$prev = _$btnArea.find( '.prev' ),
        _$next = _$btnArea.find( '.next' ),
        _$play = _$btnArea.find( '.play' ),
        _$pause = _$btnArea.find( '.pause' );

    var _delay = 4000,
        _currentIdx = 0,
        _bannerLength = _$banners.length,
        _interval, _isPlaying = true;


    initialize();

    /** ========== Methods ========== */
    function initialize () {
        _$menuGroup.each( function ( idx ) {
            $( this ).attr( 'data-idx', idx );
            $( this ).find( 'a' ).attr( 'data-idx', idx );
        });

        _$menus.each( function ( sidx ) {
            $( this ).attr( 'data-sidx', sidx );
        });

        //default set
        setBackground();
        setBgColor(0);
        setEvents();
        play();
    }

    //auto play start
    function play () {
        if ( !_isPlaying ) return;
        if ( _interval ) clearInterval( _interval );

        _interval = setInterval( function () {
            next()
        }, _delay);
    }

    //auto play stop
    function pause () {
        if ( _interval ) clearInterval( _interval );
    }

    //다음 배너 활성화
    function next () {
        var sidx = _currentIdx + 1;

        if ( sidx >= _bannerLength ) {
            sidx = 0;
        }

        activeBanner( sidx );
    }

    //이전 배너 활성화
    function prev () {
        var sidx = _currentIdx - 1;

        if ( sidx < 0 ) {
            sidx = _bannerLength - 1;
        }

        activeBanner( sidx );
    }

    //베너 활성화
    function activeBanner ( sidx ) {
        var idx = _$menus.eq( sidx ).data( 'idx' );

        _$menuGroup.find( '> a' ).removeClass( 'on' );
        _$menuGroup.eq( idx ).find( '> a' ).addClass( 'on' );
        _$banners.hide().eq( sidx ).show();
        setBgColor( sidx );

        _currentIdx = sidx;
    }

    function setEvents () {
        //menu event
        /* 17.10.30 숨김
		_$menuGroup.find( 'a' ).on( 'focusin focusout', function (e) {
            var idx = $( e.currentTarget ).data( 'idx' ),
                sidx = $( e.currentTarget ).data( 'sidx' );

            if ( e.type === 'focusin' ) {
                _$menuGroup.eq( idx ).find( 'ul' ).show();
                activeBanner( sidx );
            } else {
                _$menuGroup.eq( idx ).find( 'ul' ).hide();
            }
        });*/
        _$menuGroup.on( 'mouseenter mouseleave', function (e) {
            var sidx = $( e.currentTarget ).find( '[data-sidx]:eq(0)' ).data( 'sidx' );

            if ( e.type === 'mouseenter' ) {
                $( e.currentTarget ).find( 'ul' ).show();
                activeBanner( sidx );
            } else {
                $( e.currentTarget ).find( 'ul' ).hide();
            }
        });
		 _$menus.on( 'mouseenter', function (e) {
            var sidx = $( e.currentTarget ).data( 'sidx' );

            if ( e.type === 'mouseenter' ) {
                activeBanner( sidx );
            }
        });

        _$next.on( 'click', function (e) {
            next();
        });

        _$prev.on( 'click', function (e) {
            prev();
        });

        _$play.on( 'click', function (e) {
            _isPlaying = true;
            _$pause.show();
            _$play.hide();			
			play();
        });

        _$pause.on( 'click', function (e) {
            _isPlaying = false;
            _$pause.hide();
            _$play.show();
            pause();
        });

        _$target.on( 'focusout mouseleave', function (e) {
            play();
        });

        _$target.on( 'focusin mouseenter', function (e) {
            pause();
        });
    }

    //bg 변경
    function setBgColor ( sidx ) {
        var bgColor = $( '#main_visual_color' + sidx ).val();
        if ( bgColor ) {
            $( '#main_visual_bg' ).css( 'background-color', '#' + bgColor );
        }
    }

    //기본 bg 세팅
    function setBackground () {
        var main_bg = $( '<div/>' ).attr( 'id', 'main_visual_bg' ).css( 'top', $('#container').offset().top );
        $( '#wrap' ).append( main_bg );
    }
});
/* //mainVisual 17.09.15 e */

 /**
* 회원가입 부가정보 입력
* @2017-11-06
*/
$( function ($) {
	$( '.complete .btn.red' ).click(function(){
		$( '.table_info' ).show();
	});
	$( '.sign_wrap .btn.small' ).click(function(){
		$( '.table_info' ).show();
	});
});

/* 20180209브랜드관 추가 */

//브랜드 lnb 인스턴스 팩토리 함수
;(function($,window){
	var BrandMallLnb = window.BrandMallLnb || {};
	
	BrandMallLnb=(function(){
		var instanceN=0;
		var BrandMallLnb=function(elm){
			this.elm=$(elm); 
			this.setting={
				depth1 : this.elm.find(".brand_mall_lnb"),
				depth2 : this.elm.find(".brand_mall_lnb .depth01"),
				depth3 : this.elm.find(".brand_mall_lnb .depth02"),
				speed  : "fast",
				instanceN:instanceN++
			};    
			this.init();
		}
		return BrandMallLnb;
	})();
	
	BrandMallLnb.prototype={
		constructor:BrandMallLnb,
		init:function(){
			this.event();
			this.reset();
		},
		event:function(){
			var self=this;
			this.setting.depth1.find(">li>a").on({
				"click" : function(e){
					var _this = $(this);
					var _thisParent = _this.parent();
					
					if( _this.next(".depth01").size() > 0 ){
						if( _thisParent.hasClass("on") ){
							
							_thisParent.removeClass("on").find(".depth01").slideUp(self.setting.speed , function(){
								$(this).find(".depth02").hide();
							});
						} else {
							self.setting.depth1.find(">li.on").removeClass("on").find(".depth01").slideUp(self.setting.speed , function(){
								$(this).find(".depth02").hide();
							});
							_thisParent.addClass("on").find(".depth02").show().end().find(".depth01").slideDown(self.setting.speed).find(">li").data({"active" : "on"});
						}
						//e.preventDefault();
					}
					else{
						self.setting.depth1.find(">li.on").removeClass("on").find(".depth01").slideUp(self.setting.speed , function(){
							$(this).find(".depth02").hide();
						});
						self.setting.depth2.find(">li.on").removeClass("on");
					}
					e.preventDefault();
				}
			});
		
			this.setting.depth2.find(">li>a").on({
				"click" : function(e){          
					var _this = $(this);
					var _thisParent = _this.parent();
					if( _this.next(".depth02").size() > 0 ){
						if( _thisParent.data("active") == "on" ){
							_thisParent.data({"active" : "off"}).find(".depth02").slideUp(self.setting.speed);
						} else {
							_thisParent.data({"active" : "on"}).find(".depth02").slideDown(self.setting.speed);
						}
						//e.preventDefault();
					}
					else{
						self.setting.depth2.find(">li.on").removeClass("on");
						_thisParent.addClass("on");
						e.preventDefault();
					}
				}
			});
		},
		reset : function(){
			this.setting.depth1.find(">li").each(function(){
				var _this = $(this);
				if( _this.find(".depth01").size() < 1 ){
					_this.find("a").addClass("no_depth");
				}
			});			
		}
	}
	
	$.fn.brandMallLnb = function() {
		this.each(function() {
			return new BrandMallLnb(this);
		});
	};
	
})(jQuery,window);

$( function () {
	$(".lnb_wrap02").brandMallLnb();
});
/* //20180209브랜드관 추가 e*/


/*20180620스카이스크래퍼 수정*/
$(function(){
	var targets=$(".sky_scrapper_banner.main, .doongdoonge_banner.main");
	if(targets.length>0){
		function scrappers(){
			var header=$("#header").height();
			var skyH=$(".sky_scrapper_banner.main").height();
			var tagH=$(".new_arrival").height();
			var topBh=$(".top_banner").height();
			var winT=$(window).scrollTop();
			var end=$(".new_arrival").offset().top;
			var tag=$(".brand_sale").offset().top-70;	
			var tagE=(end+tagH)-(header+topBh+skyH);
			var tagA=(tagE+tagH)-70;	
			if(winT > tag ){
				targets.addClass("mainFix");
				if(winT>tagA){
					$(".sky_scrapper_banner.main").css({
						top:tagE
					});
					targets.addClass("mainAb");
				}
				else{
					$(".sky_scrapper_banner.main").css({
						top:""
					});
					targets.removeClass("mainAb");
				}
			}
			else{
				targets.removeClass("mainFix");
			}
		}
		scrappers();
		$(window).on("scroll",scrappers);
	}
});


/*-----------------------2019 적립금프로젝트-----------------------------*/
$(function(){
	$(".datepicker_area").on("click", function(e){
		$(this).find(".ui-datepicker-trigger").click();
		return false;
	});
	$(".mypage_wrap .my_mileage_list").on("click",".btn_detail_info", function(){
		var id = $(this).attr("data-id");
		$(this).next().find(".popup_layer").show();
		layerpopup(id, false);
	});
});