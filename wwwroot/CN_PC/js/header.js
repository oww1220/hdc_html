$(document).ready(function(){	
	'use strict';	
	
	var browserVer = navigator.userAgent.toLowerCase();
	
	if( browserVer.indexOf("msie 7") !== -1 || browserVer.indexOf("msie 8") !== -1 || browserVer.indexOf("msie 9") !== -1  ){
		$("input[placeholder]").each(function(){
			var othis = $(this);
			var holder = othis.attr("placeholder");
			
			othis.val(holder);
			othis.on("focusin focusout change keypress",function(e){
				e = e || window.event;
				if(e.type === "focusin"){
					if($.trim(othis.val()) === holder){
						othis.val("").addClass("focus");
					}
				}else if(e.type === "focusout"){
					if($.trim(othis.val()) === ""){
						othis.removeClass("focus").val(holder);
					}
				}else{	
					othis.addClass("focus");								
				}
			});
			   
		});
	} 	
	
	
	/* top search */
	$("header .total_search input[type='text']").on("focusin blur keyup",function(e){
		e = e || window.event;
		var val = $.trim($(this).val());
		if(e.type === "focusin"){			
			if(val !== ""){
				$("#result_save").hide();
				$("#result_auto").show();
			}else{		
				$("#result_auto").hide();	
				$("#result_save").show();
			}
		}else if(e.type === "keyup"){
			if(val !== ""){
				$("#result_save").hide();
				$("#result_auto").show();
			}else{	
				$("#result_auto").hide();	
				$("#result_save").show();
			}		
		}else{
			
		}
	});
	$(".result_area .close_area .close").on("click",function(){
		$(this).parents(".result_area").hide();
	});
	$("#result_save .search_delete").on("click",function(){
		$(this).parents("li").remove();
	});
	$("#result_save .delete_all").on("click",function(){
		$(this).parents(".result_area").find(".search_word .search_save li").remove();
	});

	
	/* header */
	
	$(".navigation .cn_shopin_menu li").removeClass("main");
	$("header .unb li").on("mouseover mouseleave focusin focusout",function(e){
		e = e || window.event;
		if(e.type === "mouseover" || e.type === "focusin"){
			$(this)	.find(".qrimg").show();
		}else{
			$(this).find(".qrimg").hide();
		}		
	});	
	$(".cn_view_list > li").each(function(){
		$(this).find(".cn_dept_arrow").css("top",$(this).offset().top-150);
	}).on("mouseover",function(){
		$(this).find(".cn_dept_arrow").css("top",$(this).offset().top-150);	
	});
	
	/* gnb in out */
	$(".cn_shop_duty a").on("mouseover mouseleave keydown",function(e){
		e = e || window.event;		
		if(e.type === "mouseover"){
			$(".cn_shop_duty").addClass("main");
			$(".cn_category_view").show();		
		}else if(e.keyCode === 13){
			if($(".cn_shop_duty").hasClass("main")){
				if($(document).find(".main_wrap").length > 0){return false;}
				$(".cn_category_view").hide();
				$(".cn_shop_duty").removeClass("main");
			}else{
				$(".cn_shop_duty").addClass("main");
				$(".cn_category_view").show();		
			}
		}else if(e.keyCode === 9 && !(e.shiftKey)){
			if($(".cn_shop_duty").hasClass("main")){
				$(".cn_view_item01").find("a").eq(0).focus().on("keydown",function(e){
					e = e || window.event;		
					if(e.keyCode === 9 && e.shiftKey){
						$(".cn_shop_duty a").focus();
						setTimeout(function(){$(".cn_shop_duty a").focus();},50);
					}
				});
				setTimeout(function(){$(".cn_view_item01").find("a").eq(0).focus();},50);
			}
		}else if(e.pageY < 163){
			if($(document).find(".main_wrap").length > 0){return false;}
			$(".cn_category_view").hide();
			$(".cn_shop_duty").removeClass("main");
		}		
	});
	$(document).on("keydown",".cn_view_item07 .cn_sub_menu a:last",function(){
		$(".cn_shop_product a:first").focus().on("keydown",function(e){
			e = e || window.event;		
			if(e.keyCode === 9 && !(e.shiftKey)){
				$(".cn_shop_duty a").focus();
			}
		});
		setTimeout(function(){$(".cn_shop_product a:first").focus();},50);
		if($(document).find(".main_wrap").length > 0){return false;}
		$(".cn_category_view").hide();
		$(".cn_shop_duty").removeClass("main");
	});
	$(".cn_sort_tab_contents").mCustomScrollbar(); 
	$(".cn_category_view").on("mouseleave",function(){
		if($(document).find(".main_wrap").length > 0){return false;}
		$(".cn_category_view").hide();
		$(".cn_shop_duty").removeClass("main");
	});	
	$(".cn_shop_product a").on("mouseover mouseleave keydown",function(e){
		e = e || window.event;		
		if(e.type === "mouseover"){
			$(".cn_sort_cont a.all").click();
			$(".cn_brand_view").addClass("on");
			$(".cn_shop_product").addClass("on");
		}else if(e.keyCode === 13){
			if($(".cn_brand_view").hasClass("on")){
				$(".cn_brand_view").removeClass("on");
				$(".cn_shop_product").removeClass("on");
			}else{
				$(".cn_sort_cont a.all").click();
				$(".cn_brand_view").addClass("on");
				$(".cn_shop_product").addClass("on");	
			}
		}else if(e.keyCode === 9 && !(e.shiftKey)){
			if($(".cn_brand_view").hasClass("on")){
				$(".cn_brand_view").find("a").eq(0).focus().on("keydown",function(e){
					e = e || window.event;		
					if(e.keyCode === 9 && e.shiftKey){
						$(".cn_shop_product a").focus();
						setTimeout(function(){$(".cn_shop_product a").focus();},50);
					}
				});
				setTimeout(function(){$(".cn_brand_view").find("a").eq(0).focus();},50);
			}
		}else if(e.pageY < 163){
			$(".cn_brand_view").removeClass("on");
			$(".cn_shop_product").removeClass("on");
		}		
	});
	$(".cn_brand_view").on("mouseleave",function(){
		$(".cn_brand_view").removeClass("on");
		$(".cn_shop_product").removeClass("on");
	});
	$(".cn_brand_view .cn_brand_sort .cn_sort_list a").on("click",function(){
		if($(this).hasClass("on")){return false;}
		$(this).parents(".cn_sort_list").find("li").removeClass("on");
		$(this).parents("li").addClass("on");
		$(".cn_brand_part_gp").removeClass("on");
		$(".cn_brand_part_gp").eq($(".cn_brand_view .cn_brand_sort .cn_sort_list a").index(this)).addClass("on");
	});
	$(".cn_brand_view .cn_sort_cont a").on("click",function(){
		if($(this).hasClass("select")){return false;}
		$(this)	.siblings("a").removeClass("select");
		$(this)	.addClass("select");
		if($(this).hasClass("all")){
			$(".cn_brand_part_gp .cn_brand_part").show();
			return false;
		}		
		$(".cn_brand_part_gp .cn_brand_part").hide();		
		for(var i=0;i < $(".cn_brand_part_gp").find("em.part_tit").length; i++){
			if($(".cn_brand_part").eq(i).find("em.part_tit").text() === $(this).text()){
				$(".cn_brand_part").eq(i).show();
			}
		}
		return false;
	});	
	$(".cn_category_view .cn_dept_list a.title").on("click",function(){
		var othis = $(this);
		if(othis.hasClass("on")){return false;}
		othis.parents(".cn_dept_list")
			.find("a.title").removeClass("on").end()
			.find(".cn_cata_detail").removeClass("on").end().end()
			.addClass("on");
		var ohref = othis.attr("href");
		$(ohref).addClass("on");
		return false;
	});	
	$(".cn_category_view .cn_brand_sort .cn_sort_list a").on("click",function(){
		if($(this).hasClass("on")){return false;}
		$(this).parents(".cn_sort_list").find("a").removeClass("on");
		$(this).addClass("on");
		$(this).parents(".cn_brand_area").find(".cn_sort_tab_contents").removeClass("on");
		$(this).parents(".cn_brand_area").find(".cn_sort_tab_contents").eq($(".cn_category_view .cn_brand_sort .cn_sort_list a").index(this)).addClass("on");
		
		/*$(".sky_scrapper").find(".cont > div").eq(menu-1).find(".mCSB_scrollTools").height($(".sky_scrapper").find(".cont > div").hide().eq(menu-1).find(".mCustomScrollBox").height() -20).css("top","10px");*/
		
		 return false;
	});
	$(".cn_category_view .cn_sort_cont a").on("click",function(){
		if($(this).hasClass("select")){return false;}
		$(this)	.siblings("a").removeClass("select");
		$(this)	.addClass("select");
		if($(this).hasClass("all")){
			$(".cn_sort_tab_contents .cn_brand_part").show();
			return false;
		}		
		$(this).parents(".cn_brand_area").find(".cn_brand_part").hide();		
		for(var i=0;i < $(this).parents(".cn_brand_area").find("em.part_tit").length; i++){
			if($(this).parents(".cn_brand_area").find(".cn_brand_part").eq(i).find("em.part_tit").text() === $(this).text()){
				$(this).parents(".cn_brand_area").find(".cn_brand_part").eq(i).show();
			}
		}
		return false;
	});
	/* //header  */
	
});	