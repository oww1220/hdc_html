
/* 17.07.24 베스트셀러 추가 */
$(function ( $ ) {
	if ( window.mmain && colsole && colsole.warn ) {
		colsole.warn( '"mmain" 변수가 중복 선언 되었습니다.' );
	}

	/* ========================================== */
	/* ==============	 GNB Menu   ============= */
	/* ========================================== */
	var _gnb = (function () {
		var $warp = $( '.scroll_area' ),
			$menus = $warp.find( 'li' ),
			$activeMenu = $menus.eq( 0 );
		
		$menus.on( 'click', 'a', function (e) {
			e.preventDefault();
			var idx = $( this ).parent().index();
			_mainSlide.change( idx, true );

			var $wrapBox = $( '.cont_wrap' ),
				$moreCont = $wrapBox.find( '.more_view_cont' ),
				$moreBtn = $wrapBox.find( '.more_view' );

			$moreCont.hide();
			$moreBtn.removeClass( 'close' );
			$moreBtn.find( 'span' ).text( '더보기' );
		});

		// ---------- Protected Methods ---------- //
		function active ( index ) {
			$activeMenu = $menus.eq( index );
			$activeMenu.addClass( 'active' ).siblings().removeClass( 'active' );
			setCenterAlign();
		}

		function setCenterAlign () {
			var warpW = $warp.width(),
				menuX = $activeMenu.get(0).offsetLeft,
				menuW = $activeMenu.outerWidth(),
				posX = menuX - ( (warpW / 2) - (menuW / 2) );
			
			$warp.stop().animate( {scrollLeft: posX}, 500 );
		}

		// ---------- Public Methods ---------- //
		return {
			active: active
		};
	})();


	/* ========================================== */
	/* ==============   Main Slide  ============= */
	/* ========================================== */
	var _mainSlide = (function () {
		var activeIndex = 0;

		var $slideArea = $( '.cont_wrap' ),
			$viewport = $slideArea.find( '.ix-list-viewport' ),
			$items = $viewport.find( '.ix-list-item' ),
			$activeItem = $items.eq( activeIndex ),
			$moreBtn = $items.find( '.more_view' );


		$slideArea.on( 'ixSlideLite:init ixSlideLite:change', function (e) {
			activeIndex = e.currentIndex;
			change( activeIndex );
			$activeItem = $items.eq( activeIndex );
			resetHeight();

			var $wrapCont = $( '.cont_wrap' ),
				$moreCont = $wrapCont.find( '.more_view_cont' ),
				$moreButton = $wrapCont.find( '.more_view' );

			$moreCont.hide();
			$moreButton.removeClass( 'close' );
			$moreButton.find( 'span' ).text( '더보기' );
		}).ixSlideLite();

		$( window ).on( 'load resize', function (e) {
			$slideArea.ixSlideLite( 'resize' );
			resetHeight();
		});

		//More contents
		$items.on( 'click', '.more_view', function (e) {
			e.preventDefault();
			if ( $( this ).hasClass( 'close' ) ) {
				$( this ).closest( '.ix-list-item' ).find( '.more_view_cont' ).hide();
				$( this ).removeClass( 'close' );
				$( this ).find( 'span' ).text( '더보기' );
			} else {
				$( this ).closest( '.ix-list-item' ).find( '.more_view_cont' ).show();
				$( this ).addClass( 'close' );
				$( this ).find( 'span' ).text( '닫기' );
			}
			resetHeight();
		});

		resetHeight();

		// ---------- Protected Methods ---------- //
		function change ( index, silent ) {
			if ( !silent ) _gnb.active( index );
			$slideArea.ixSlideLite( 'changeIndex', index );
		}

		function resetHeight () {
			var idx = activeIndex;
			setViewportHeight();

			//image load check 17.08.04 주석해제
			$activeItem.imagesLoaded()
				.always( function ( instance ) {
					if ( idx === activeIndex ) setViewportHeight();
				});
		}

		function setViewportHeight () {
			$viewport.css( 'height', '' ).css( 'height', $activeItem.get(0).clientHeight + 'px' );
		}

		// ---------- Public Methods ---------- //
		return {
			change: change,
			resetHeight: resetHeight
		};
	})();



	/* ========================================== */
	/* ============= Global Methods ============= */
	/* ========================================== */
	window.mmain = {
		//main slide viewport height 값이 변경될 경우 호출
		resetViewportHeight: function () {
			_mainSlide.resetHeight();
		}
	};
});