
/* device division */
if( navigator.userAgent.toLowerCase().indexOf("iphone") > -1 ){ // huawei mate series
	$("body").addClass("iPhone");
}

/* ios keypad bug fixed */
if( $("body").hasClass("mobileApp") && $("body").hasClass("iPhone") ){
	$(document).on('focusin', 'input, textarea', function(){
		$("body").addClass('unfixed');
	})
	.on('focusout', 'input, textarea', function(){
		$("body").removeClass('unfixed');
	});
}

/* var */
var mask = ".bg_dimmed";
var autoSlickOption = {
		dots: true,
		dotsClass : "banner_nav",
		arrows : false,
		autoplay : true,
		autoplaySpeed : 2000,
		cssEase: 'linear',
		slidesToShow: 1,
		pauseOnHover : true,
		pauseOnDotsHover : true
	};
	
var arrowDotOption = {
		dots: true,
		dotsClass : "banner_nav",
		cssEase: 'linear',
		slidesToShow: $(this).data("slideLength"),
		slidesToScroll: $(this).data("slideLength")
	};
	
/* image replace */
function imgReplace(obj, flag) {
	 var imgSrc = obj.attr("src");
	 if (flag == "on") {
		  imgSrc = imgSrc.replace("_off.", "_on.");
	 } else if (flag == "off") {
		  imgSrc = imgSrc.replace("_on.", "_off.");
	 }
	 obj.attr("src", imgSrc);
}
	
/* slick */
var slideBanner = function(className){
	// single slide
	if($(className).length){
		$(className).slick({
			dots: true,
			dotsClass : "banner_nav",
			arrows : false,
			cssEase: 'linear',
			pauseOnHover : false,
			pauseOnDotsHover : true,
			slidesToShow: 1,
			pauseOnHover : true,
			pauseOnDotsHover : true
		});
	}
}

var slideBanner_2 = function(className){
	// single slide
	if($(className).length){
		$(className).slick({
			dots: true,
			autoplay : true,
			dotsClass : "banner_nav",
			arrows : false,
			cssEase: 'linear',
			pauseOnHover : false,
			pauseOnDotsHover : true,
			slidesToShow: 2,
			slidesToScroll: 2,
			pauseOnHover : true,
			pauseOnDotsHover : true
		});
	}                                                                                                                    
}

var tabAutoSlideBanner = function(classNme){
	if($(classNme).length && $(classNme).hasClass("slick-slider") == false){
		$(classNme).each(function(){
			if($(this).is(":visible")){
				$(this).slick({
					dots: true,
					dotsClass : "banner_nav",
					autoplay : true,
					arrows : false,
					cssEase: 'linear',
					pauseOnHover : false,
					pauseOnDotsHover : true,
					slidesToShow: 1,
					pauseOnHover : true,
					pauseOnDotsHover : true
				});
			}
		});
	}
}

var slideAuto = function(className){
	//single auto slide
	if($(className).length && $(className).hasClass("slick-slider") == false){
		$(className).each(function(){
			if($(this).is(":visible")){
				var option = autoSlickOption;
				if(typeof opt != 'undefined'){
					for(var key in opt){
						option[key] = opt[key];
					}
				}
				$(this).slick(option);
			}
		});
	}
}

var slideCenter = function(className){
	//single auto slide
	if($(className).length){
		$(className).each(function(){
			if($(this).is(":visible")){
				$(this).slick({
				  centerMode: true,
				  centerPadding: '60px',
				  autoplay : true,
				  autoplaySpeed : 2000,
				  pauseOnHover : true,
				  responsive: [
					{
					  breakpoint: 768,
					  settings: {
						arrows: false,
						centerMode: true
					  }
					}
				  ]
				});
			}
		});
	}
}


var sliderFor = function(className){
	// rolling navigation, rolling page
	if($(className).length){
		$(className).slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			asNavFor: '.sliderTo'
		});
	}	
}
	
var sliderTo = function(className){
	if($(className).length){
		$(className).slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.sliderFor',
			centerMode: true,
			centerPadding : 0,
			focusOnSelect: true
		});
	}	
}

var slideNavi = function(className){
	// single slide
	if($(className).length){
		$(className).slick({
			arrows : false,
			cssEase: 'linear',
			slidesToShow: 4,
			slidesToScroll: 4
		});
	}
}

var brandSearchSlide = function(className){
	if($(className).length && $(className).hasClass("slick-slider") == false){
		$(className).each(function(){
			if($(this).is(":visible")){
				$(this).slick({
					arrows : true,
					infinite : false,
					slidesToShow: 5,
					slidesToScroll: 5
				});
			}
		});
	}
}

var sliderNavSingle = function(className){
	if($(className).length){
		$(className).slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			centerMode: true,
			centerPadding : 0,
			focusOnSelect: true
		});
	}	
}

// image arrow
var productImgArr = '.js-detail-slide-arrow';
var imgArrOption = {
		arrows : true,
		slidesToShow : 2,
		slidesToScroll : 2
	}
	
var productSwipeArrow = function(classNme){
	if($(classNme).length && $(classNme).hasClass("slick-slider") == false){
		$(classNme).each(function(){
			if($(this).is(":visible")){
				$(this).slick(imgArrOption)
			}
		})
	}
}


var slideArrowDot = function(classNme){
	if($(classNme).length && $(classNme).hasClass("slick-slider") == false){
		$(classNme).each(function(){
			if($(this).is(":visible")){
				var $length = $(this).data("slideLength") == null ? 2 : $(this).data("slideLength");
				var $loop   = $(this).data("slideLoop") == "off" ? false : true; 
				
				$(this).slick({
					dots: true,
					dotsClass : "banner_nav",
					cssEase: 'linear',
					slidesToShow: $length,
					slidesToScroll: $length,
					infinite : $loop
				})
			}
		})
	}
}


/* layer popup */
var windowScrollTopPosition;

function bodyScrollStop(){
	windowScrollTopPosition = $(window).scrollTop();
	$("body").addClass("menuOpen").css({ "overflow" : "hidden" , "position" : "fixed" });
}

function bodyScrollStart(){
	$("body").removeClass("menuOpen").css({ "overflow" : "auto" , "position" : "static" });
	$(window).scrollTop(windowScrollTopPosition);
}

function popClose (_this){

	_this.hide();
	$(mask).fadeOut();
	bodyScrollStart();
}

function popOpen (_this){
	_this.show();
	$(mask).fadeIn();
	var winH =  $(window).height();
	var winW =  $(window).width();
	var thisH 		= _this.height();
	var thisW 		= _this.width();
	var popBox 		= _this.find(".box");
	var popHeader 	= popBox.find(">div:eq(0)");
	var popHeaderH 	= popBox.find(">div:eq(0)").outerHeight();
	var whiteSpace	= popHeaderH + parseInt(popBox.css("marginTop")) + parseInt(popBox.css("marginBottom")) + parseInt(popBox.css("paddingTop")) + parseInt(popBox.css("paddingBottom"));
	positionCtrl();
	
	// 확대 이미지 레이어
	if( _this.attr("id") == "layer-zoom" ){
		_this.height(winH);
		slideBanner('.zoomSlideBanner');
	}
	
	$(window).resize(function(){
		if( _this.is(":visible") ){
			_this.removeAttr("style");
			popBox.find(".pop_contents").find(">*").unwrap();
			winH =  $(window).height();
			winW =  $(window).width();
			thisH =	_this.height();
			thisW =	_this.width();
			positionCtrl();
		}
	});
	
	function positionCtrl(){ 
		if( thisH > winH ){
			_this.css({
				"height" : winH,
				"top"	: 0,
				"left"  : (winW-thisW)/2,
				"overflow" : "hidden"
			});
			
			var popContents =  popHeader.nextAll();
			popHeader.after("<div class='pop_contents'></div>");
			popBox.find(".pop_contents").prepend(popContents);
			
			popBox.find(".pop_contents").css({
				"height" : winH - whiteSpace,
				"overflow" : "auto"
			});
		
		} else {
			/*2019 적립금프로젝트 s*/
			if(_this.hasClass("input_height")){
				if (_this.find(".cont").height() < winH){
					var calcuHeight = parseInt(($(window).height() - _this.find(".cont").height())/2);
					_this.css({
						"top" : calcuHeight,
						"left"  : (winW-thisW)/2,
					});
				}
				else{
					_this.css({
						"top"	: 0,
						"left"  : (winW-thisW)/2,
					});
				}
			}
			else {
				_this.css({
					"top" : parseInt(($(window).height() - _this.height())/2),
					"left"  : (winW-thisW)/2
				});
			}
			
			/*2019 적립금프로젝트 e*/
		}
	}
	
	bodyScrollStop();
}

function popClick (_this){
	var popName = $(_this).attr('id');
	var popTarget = "layer-" + popName;
	var targetLayer = $("#"+popTarget);
	popOpen(targetLayer);
}

function scrollStart(type,scroll) {
	
	var winScroll = $(window).scrollTop();
	if(type == "productDetail"){
		
		var docHeight = $(document).height();
		var winHeight = $(window).height();
		var pageWrap = $("#wrap");
		var topHeader = $("#header");
		//var subHeader = $(".sub_header");
		var headerHeight = topHeader.height();
		var subHeaderHeight = subHeader.outerHeight();
		var container = $("#container");
		var actStart 	= false;
		
		function showHeader(){
			topHeader.css({"top":0, "left" : 0, "position" : "fixed" , "z-index" : 100 });
			subHeader.addClass("ani").css({ "position" : "fixed", "top" : headerHeight });;
			container.css({"padding-top" : headerHeight + subHeaderHeight });
		}
		
		function hideHeader (){
			topHeader.css({"top":-headerHeight, "left" : 0,  "position" : "fixed" , "z-index" : 100 });
			subHeader.css({ "position" : "fixed", "top":0});	
			container.css({"padding-top" : subHeaderHeight});
		}
		
		hideHeader();
		
		if (!actStart){
			actStart = true;
			if (winScroll < 5 && scroll ){
				showHeader();
			} else {
				hideHeader();
				actStart = false;
			}
		}
	}
	
	var btnGotop = $("#top");
	
	if (winScroll ==0){
		btnGotop.fadeOut();
	} else {
		btnGotop.fadeIn();
	}
}

function allCheckboxProp(className,prop){
	$("."+className).each(function(){
		var _this = $(this);
		var _default = {
			checkedTarget 	: "#container",
			relationship	: ""
		}
		var _options 		= $.extend(_default, _this.data())
		var allCheckbox 	= _this.find("input[type='checkbox']");
		var eachCheckbox 	= $(_options.checkedTarget).find("input[type='checkbox']").not(allCheckbox);
		var checked; 
		
		/* if( prop == true ){
			allCheckbox.prop("checked" , true)
			eachCheckbox.prop("checked", true)
		} else {
			allCheckbox.prop("checked", false)
			eachCheckbox.prop("checked", false)
		} */
		
		allCheckbox.on({
			"change" : function(){
				if( $(this).prop("checked") == true){
					checked = true;
				}
				else {
					checked = false;
				}
				eachCheckbox.prop("checked" , checked);
			}
		});
		
		eachCheckbox.on({
			"change" : function(){
				if( eachCheckbox.filter(":checked").size()  == eachCheckbox.size() ){
					allCheckbox.prop("checked" , true);
				} else {
					allCheckbox.prop("checked" , false);
				}
			}
		});
	});
}

function facetOptionCheck(){
	if( $(".search_wrap").find(".condition li.off").size() > 0 ){
		$(".search_wrap").find(".condition .btn_toggle").show();
		if( $(".search_wrap").find(".condition .btn_toggle").hasClass("on")){
			$(".search_wrap").find(".condition .btn_toggle").removeClass("on").find("span").text("상세옵션 열기");
			$(".search_wrap").find(".condition li.off").hide();
		}
	} else {
		$(".search_wrap").find(".condition .btn_toggle").hide();
	}
}

function isNumber(s) {
	s += ''; 
	s = s.replace(/^\s*|\s*$/g, '');
	if (isNaN(s)) return false;
	return true;
}

function productAmountCheck(){	
	if( $(".amount").size() > 0 ){
		$(".amount").each(function(){
			var _this 			= $(this);
			var countInput 		= _this.find("input");
			var volumeSelect 	= _this.parents(".price_info").find("select");
			var beginAmount		= Number(countInput.val());
			var minAmount 		= Number(countInput.data("minAmount")) || 1;
			var maxAmount 		= Number(countInput.data("maxAmount")) || 999999;
			var packageAmount 	= Number(countInput.data("packageAmount")) || 1;
			
			/* if(! _this.find("input").attr("data-min-amount") && _this.find(".minus").size() > 0 ){
				_this.data("minAmount" , _this.val());
			} */
			
			if( _this.find(".minus").size() > 0  ){
				countInput.val( beginAmount );
				countInput.data("prevCount" , countInput.val());
			}

			if( _this.find(".btn_w").size() > 0 ){
				countInput.data("currentCount" , countInput.val());
				volumeSelect.data("selectIndex", volumeSelect.find("option:selected").index() );
				
				var _countConfirmBtn = $(this).find(".btn_w");
				var currentCount 	= Number(countInput.data("currentCount"));
				
				volumeSelect.on({
					"change" : function(){
						if( _countConfirmBtn.length){
							if( Number(countInput.val()) != currentCount || volumeSelect.find("option:selected").index() != volumeSelect.data("selectIndex") ){
								_countConfirmBtn.show();
							} else {
								_countConfirmBtn.hide();
							}	
						}	
					}
				});
			}
		});
	}
}

// number type max length limit
function inputNumberMaxLengthCheck(object){
	if (object.value.length > object.maxLength){
		object.value = object.value.slice(0, object.maxLength);
	}    
}

// color chip
var productColor = '.js-color-slide';

function productDetailColorChip(){
	var colorChip 	= $("#colorChipArea");
	var colorSelect	= colorChip.find("select");
	var colorSlide	= colorChip.find(".color_choice");
	
	colorSlide.find(".js-color-slide a").on({
		"click" : function(e){
			var dataCode = $(this).data("code");
			colorChip.find("option[value='"+dataCode+"']").prop("selected", true);
			colorSlide.find("a").removeClass("active");
			$(this).addClass("active");	
			e.preventDefault();
		}
	});
	
	colorSelect.on({
		"change" : function(){
			var dataCode = $(this).val();
			var target	= colorSlide.find("a[data-code='"+dataCode+"']");
			var idx 		= Number(target.parent().data("slickIndex"));
			var currentIdx 	= Number(colorSlide.find(".slick-current").data("slickIndex"));
			
			$(productColor).slick("slickGoTo", idx );
			colorSlide.find("a").removeClass("active");
			target.addClass("active");
		}
	});
}
	
var productSwipeColor = function(){
	if($(productColor).length){
		$(productColor).each(function(){
			//var idx = $(this).find(".slide").length
			var _this			= $(this);	
			var slideToShowSize = (_this.outerWidth()/31);
			
			if(_this.is(":visible")){
				_this.slick({
					arrows : true,
					infinite:false,
					draggable : true,
					variableWidth : true,
					slidesToShow : slideToShowSize,
					prevArrow : '.js-color-prev',
					nextArrow : '.js-color-next',
					swipeToSlide : true
				});
				
				$(window).resize(function(){
					slideToShowSize = (_this.outerWidth()/31);
					_this.slick("unslick").slick({
						arrows : true,
						infinite:false,
						draggable : true,
						variableWidth : true,
						slidesToShow : slideToShowSize,
						prevArrow : '.js-color-prev',
						nextArrow : '.js-color-next',
						swipeToSlide : true
					});
					productDetailColorChip();
				});
			}
			productDetailColorChip();
		});
		
		$(".js-color-prev, .js-color-next").on("click",function(e){
			e.preventDefault();
		})
	}
}

// 경유지 선택
function cartPaymentInfoViaInit(){
	var val = $(".my_departure_wrap").find("#depPlaceCode").val();
	var viaArea = $(".my_departure_wrap").find(".flight_info");
	
	if( val == "P" || val == "U" || val == "I" ){
		viaArea.find("li:eq(0) input[type='radio']").prop("checked" , true);
		viaArea.find("li:eq(1) input[type='radio']").prop("disabled" , true);
		viaArea.find("select").hide();
	} else if ( val == "" ){
		viaArea.find("li:eq(0) input[type='radio']").prop({"checked" : false, "disabled" : true });
		viaArea.find("li:eq(1) input[type='radio']").prop({"checked" : false, "disabled" : true });
		viaArea.find("select option:eq(0)").prop("selected" , true);
		viaArea.find("select").hide();
	}
	else {
		viaArea.find("li:eq(0) input[type='radio']").prop("disabled" , false);
		viaArea.find("li:eq(1) input[type='radio']").prop("disabled" , false);
	}
	
	var idx = viaArea.find("input[type='radio']:checked").parents("li").index();
	
	if( idx == 0 ){
		viaArea.find("select").hide().find("option:eq(0)").prop("selected" , true);
	} 
	else if( idx == 1 ) {
		viaArea.find("select").show();
	}
}


/* scroll event */
$(window).scroll(function(){
	if( $(".product_detail_wrap").length ){
		scrollStart("productDetail", true);
	} else {
		scrollStart();
	}
});


/* common */
$(function(){
	
	if( $(".product_detail_wrap").length ){
		scrollStart("productDetail");
	} else {
		scrollStart();
	}
	
	$(".all_menu .catagoryLink").find("a").on({
		"click" : function(e){
			$(".total_menu .tab li").eq(0).find("a").trigger("click")
			e.preventDefault();
		}
	});
	
	// 푸터 서비스 이동
	var f_serviceH = $(".fgnb_service").height();
	$(".js-btn-service").data("on" , false);
	$(".js-btn-service").on({
		"click" : function(e){
			var _this = $(this);
			if( _this.data("on") == true ){
				_this.data("on", false);
				$(".fgnb_service").css({ "height" : 0 });
			}
			else {
				_this.data("on", true);
				$(".fgnb_service").css({ "height" : 118 });
			}
			e.preventDefault();	
		}
	});
	
	// footer msg layer 
	var $msg = $(".cbm_wrap");
	var $msgClose = $(".cbm_wrap .close");
	
	$(window).load(function(){
		// main layer
		$(".initial_layer").each(function(){
			popOpen($("#"+ $(this).attr("id")));
		});
		
		setTimeout(function(){
			$msg.slideUp();
		},10000);
	});
	
	$msgClose.on("click",function(e){
		$msg.slideUp();
		e.preventDefault();
	});
	
	
	$("[data-event='accordion']").each(function(){
		$(this).accordion($(this).data());
	});
	
	$("[data-event='tab']").each(function(){
		$(this).tabpannel($(this).data());
	});
	
	// layer popup
	var btnLayer = $('.open-popup');
	btnLayer.on('click', function(e){
		popClick($(this));
		e.preventDefault();
	});
	
	var layerPop = $('.popup_layer');
	var layerClose = layerPop.find('.btn_close , .btn_cancel');
	
	layerClose.on('click', function(e){
		popClose(layerPop);
		e.preventDefault();
	});

	//facet ( detail search )
	var detailSearch 	= $(".search_wrap");
	var btnDetailSearch = detailSearch.find(".search_head .btn a, .search_head .btn_toggle");
	var priceMinData = detailSearch.find("input[type='number']").eq(0).val();
	var priceMaxData = detailSearch.find("input[type='number']").eq(1).val();
	var btnFacetMore = detailSearch.find(".condition .btn_toggle");
	var categorySelect = detailSearch.find("#c_category");
	
	if(! detailSearch.hasClass("category_page") || detailSearch.find(".condition li.off").size() == 0 ){
		btnFacetMore.hide();
	} else {
		btnFacetMore.show();
	}
	
	btnDetailSearch.on("click", function(e){
		if ($(this).hasClass("on")) {
			$(this).removeClass("on");
			$(this).parents(".search_wrap").removeClass("on");
			//$(mask).removeClass("on").css({ "z-index" : "" });
		} else {
			$(this).addClass("on");
			$(this).parents(".search_wrap").addClass("on");
			//$(mask).addClass("on").css({ "z-index" : 10 });
		}
		e.preventDefault();
	});

	detailSearch.find("ul.btn_area > li:eq(0) a").on({
		"click" : function(e){
			detailSearch.find(".condition select").val("");
			detailSearch.find(".condition input[type='checkbox']").prop("checked", false);
			detailSearch.find(".condition input[type='number']").eq(0).val(priceMinData);
			detailSearch.find(".condition input[type='number']").eq(1).val(priceMaxData);
			e.preventDefault();
		}
	});
	
	btnFacetMore.on({
		"click" : function(e){
			var _this = $(this);
			if( _this.hasClass("on") ){
				_this.removeClass("on").find("span").text("상세옵션 열기");
				detailSearch.find(".condition li.off").hide();
			} else {
				_this.addClass("on").find("span").text("상세옵션 닫기");
				detailSearch.find(".condition li.off").show();
			}
			e.preventDefault();
		}
	});
	
	categorySelect.on({
		"change" : function(){
			if(! $(".search_wrap").hasClass("category_page") ){
				facetOptionCheck(); //개발 반영시 제거
			}	
		}
	});
	
	//search order time
	var searchOrderTime	= $(".order_time_search");
	$(".btn_order_time_search > a").on({
		"click" : function(e){
			var _this = $(this);
			
			if( _this.parent().hasClass("on") ){
				_this.parent().removeClass("on");
			} else {
				_this.parent().addClass("on");
			}
			e.preventDefault();
		}
	});
	
	searchOrderTime.find(".btn_area a:eq(0)").on({
		"click" : function(e){
			searchOrderTime.find("select").val("");
			searchOrderTime.find("input[type='radio']").prop("checked", false);
			searchOrderTime.find("input[type='data']").val("");
			e.preventDefault();
		}
	});
	
	
	// main beststop tab
	var $tabParent = $(".js-main-tab .roll_tab");
	var $tabDetail = $(".roll_tab_detail");

	if($tabParent.length){
		$tabDetail.hide();
		$tabDetail.eq(0).show();
	}
	
	$tabParent.find("a").each(function(idx){
		$(this).attr("data-tab-index",idx);
	})
	
	$tabParent.find("a").on("click",function(e){
		var idx = $(this).data("tabIndex");
		
		if($tabParent.find("a").find("img").length){
			$tabParent.find("a").each(function(){
				imgReplace($(this).find("img"),"off");
			})
			
			imgReplace($(this).find("img"),"on");
		}
		
		$tabParent.find("a").removeClass("on")
		$(this).addClass("on")
		
		$tabDetail.hide();
		$tabDetail.eq(idx-1).show();
		e.preventDefault();
	})



	/* product compare */
	var productCompare = {

		setting : {
			compareBtn01 		: $(".compare_start"),
			compareBtn02 		: $(".compare_action"),
			compareCancel 		: $(".compare_stop"),
			compareCont 		: $(".product_list"),
			compareCheck 		: $(".product_list .compare_check"),
			layerAllCheckbox	: $("#layer-compareProducts").find(".all_check input[type='checkbox']"),
			layerCheckbox		: $("#layer-compareProducts").find(".product_off .check input[type='checkbox']"),
			compareProdLength  	: $(".compare_popup .btn_area .txt_red")
		},

		init : function(){
			this.btnEvent();
			this.selectEvent();
			this.layerCheck();
		},

		btnEvent  : function(){
			productCompare.setting.compareBtn01.on({
				"click" : function(e){
					$(this).addClass("on");
					productCompare.setting.compareCheck.show();
					e.preventDefault();
				}
			});

			productCompare.setting.compareBtn02.on({
				"click" : function(e){
					var checkedLength =  productCompare.setting.compareCheck.find("input[type='checkbox']").filter(":checked").size();

					if( checkedLength > 1 ){
						popClick ("#compareProducts");
						
						$("#layer-compareProducts .table_tbody_wrap").slick({
							slidesToShow:2,
							slidesToScroll:2,
							infinite : false
						});
						
						$(".compare_popup .info_top > p:eq(1) span").text($(this).find("span").text());
					}
					e.preventDefault();
				}
			});

			// product compare cancel
			productCompare.setting.compareCancel.on({
				"click" : function(e){
					productCompare.setting.compareCheck.find("input[type='checkbox']").prop("checked" , false);
					productCompare.setting.compareCheck.hide();
					productCompare.setting.compareBtn01.removeClass("on");
					productCompare.setting.compareBtn02.find("strong").text(0);
					e.preventDefault();
				}
			});
		},

		selectEvent : function(){
			var prodCheckbox =  productCompare.setting.compareCheck.find("input[type='checkbox']");
			prodCheckbox.on({
				"change" : function(){
					var checkedLength =  prodCheckbox.filter(":checked").size();
					if( checkedLength > 1 && checkedLength < 11 ){
						productCompare.setting.compareBtn02.addClass("on").find("span").text(checkedLength);
					}
					else if ( checkedLength == 11 ){
						$(this).prop("checked", false);	
						alert("최대 10개의 상품까지 비교 가능합니다.");
					}
					else {
						productCompare.setting.compareBtn02.removeClass("on").find("span").text(0);
					}
				}
			});
		},

		layerCheck : function(){
			var layerAllCheckbox =  productCompare.setting.layerAllCheckbox;
			var layerCheckbox =  productCompare.setting.layerCheckbox;
			layerAllCheckbox.on({
				"change" : function(){
					if( $(this).prop("checked") == true ){
						layerCheckbox.prop("checked" ,  true);
					} else {
						layerCheckbox.prop("checked" ,  false);
					}
					productCompare.setting.compareProdLength.text(layerCheckbox.filter(":checked").size());
				}
			});
			layerCheckbox.on({
				"change" : function(){
					if( layerCheckbox.filter(":checked").size() == layerCheckbox.size() ){
						layerAllCheckbox.prop("checked" ,  true);
					} else {
						layerAllCheckbox.prop("checked" ,  false);
					}
					productCompare.setting.compareProdLength.text(layerCheckbox.filter(":checked").size());
				}
			});
		}
	}
	productCompare.init();

	//slick
	slideBanner('.slideBanner');
	//slideBanner_2('.slide2Auto'); /*개발 수정*/
	slideAuto('.slideAuto');
	slideAuto('.banner_big', {autoplaySpeed : 4000});
	slideAuto('.banner_small', {autoplaySpeed : 5000});
	tabAutoSlideBanner('.tabSlideAuto');
	slideCenter('.slideCenter');
	slideNavi('.slideNavi');
	slideArrowDot('.slideArrowDot');
	sliderFor('.sliderFor');
	sliderTo('.sliderTo');
	//brandSearchSlide(".consonant_wrap");
	sliderNavSingle('.sliderNavSingle');
	productSwipeArrow(productImgArr);

	
	/*bannerslide("defualt_banner");
	bannerslide("category_banner");
	bannerslide("promotion2");
	bannerslide("event_banner_small");
	
	$(".banner_big").singleNavSlide({
		auto : true,
		speed : 400
	});
	$(".banner_small").singleNavSlide({
		auto : true,
		speed : 5000
	});*/


	
	/*$("#sky_scrapper_banner1").singleNavSlide();
	$("#sky_scrapper_banner2").singleNavSlide();
	$("#brandSlide").brandSlide();*/
	
	//all checkbox
	$(".all_check, .check_all").each(function(){
		var _this = $(this);
		var _default = {
			checkedTarget 	: "#container",
			relationship	: ""
		}
		var _options 		= $.extend(_default, _this.data())
		var allCheckbox 	= _this.find("input[type='checkbox']");
		var eachCheckbox 	= $(_options.checkedTarget).find("input[type='checkbox']:not(':disabled')").not(allCheckbox);
		var checked; 
		
		allCheckbox.on({
			"change" : function(){
				if( $(this).prop("checked") == true){
					checked = true;
				}
				else {
					checked = false;
				}
				eachCheckbox.prop("checked" , checked);
			}
		});
		
		eachCheckbox.on({
			"change" : function(){
				if( eachCheckbox.filter(":checked").size()  == eachCheckbox.size() ){
					allCheckbox.prop("checked" , true);
				} else {
					allCheckbox.prop("checked" , false);
				}
			}
		});
	});
	
	// 관심상품 리스트 toogle
	$(".wish_share.box").find("a").on({
		"click" : function(e){
			$(this).toggleClass("on");
			e.preventDefault();
		}
	});
	
	// top button
	$("#top > a").on({
		"click" : function(e){
			$(window).scrollTop(0);
			e.preventDefault();
		}
	});
	
	// all agree
	$(".agreement").each(function(){
		var _this = $(this);
		var allCheckbox 	= _this.find(".all_agree input[type='checkbox']");
		var eachCheckbox 	= _this.find("input[type='checkbox']").not(allCheckbox);
		
		allCheckbox.on({
			"change" : function(){
				if( $(this).prop("checked") == true ){
					eachCheckbox.prop("checked" , true);
				} else {
					eachCheckbox.prop("checked" , false);
				}
			}
		});
		
		eachCheckbox.on({
			"change" : function(){
				if( eachCheckbox.filter(":checked").size() == eachCheckbox.size() ){
					allCheckbox.prop("checked" , true);
				} else {
					allCheckbox.prop("checked" , false);
				}
			}
		});
	});
	
	// datepicker
	$(".ic").each(function(){
		$(this).on({
			"click" : function(e){
				$(this).prev().focus();
				e.preventDefault();
			}
		});
	});
	
	// phone select
	$(".phone_box , .phone_area").each(function(){
		var _this 	= $(this);
		var  select = _this.find(" > select");
		
		select.on({
			"change" : function(){
				var dataId = $(this).find("option:selected").data("id");
				_this.find("*[data-target]").hide();
				_this.find("*[data-target*='"+ dataId +"']").show();
			}
		});
	});
	
	// data max setting 
	$("input[type='date']").each(function(){
		if(! $(this).attr("max") ){
			var todayDate = new Date();
			var maxYear	  = 10;	
			$(this).attr("max" , (todayDate.getFullYear()+maxYear)+"-"+(todayDate.getMonth()+1)+"-"+todayDate.getDate());
		}
	});
	
	// min/max price
	var facetMinPrice 			= $("#c_price_min");
	var facetMaxPrice 			= $("#c_price_max");
	var currentFacetMinPrice 	= facetMinPrice.val();
	var currentFacetMaxPrice 	= facetMaxPrice.val();
	
	facetMinPrice.on({
		"change" : function(){
			var _this = $(this);
			var minVal = Number(_this.val());
			var maxVal = Number(facetMaxPrice.val());
			var maxLimit 	= Number(_this.attr("max"));
			var minLimit 	= Number(_this.attr("min"));
			
			if( minVal > maxVal ){
				_this.val(currentFacetMinPrice);
			}
			else if( minVal > maxLimit ){
				_this.val(currentFacetMinPrice);
			}
			else if( minVal < minLimit ){
				_this.val(currentFacetMinPrice);
			}
			else {
				currentFacetMinPrice = minVal;
			}
		}
	});
	
	facetMaxPrice.on({
		"change" : function(){
			var _this 		= $(this);
			var maxVal 		= Number(_this.val());
			var minVal 		= Number(facetMinPrice.val());
			var maxLimit 	= Number(_this.attr("max"));
			var minLimit 	= Number(_this.attr("min"));
			
			if( maxVal < minVal ){
				_this.val(currentFacetMaxPrice);
			} 
			else if( maxVal > maxLimit ){
				_this.val(currentFacetMaxPrice);
			}
			else if( maxVal < minLimit ){
				_this.val(currentFacetMaxPrice);
			}
			else {
				currentFacetMaxPrice = maxVal;
			}
		}
	});
	
});

/* end : common */





/* layout */
$(function(){
	/* header */
	var $header = $("#header");
	
	/* gnb navigation slide */
	var $gnbwrap = $(".gnb");
	var $gnb = $gnbwrap.find(".inner");
	var $item = $gnb.find(".slide");
	var $menu = $gnb.find(".menu");
	var itemWidth;
	var target;
	var lastTarget = 0;
	var speed = 300;
	var initData = [];
	var $itemOn = $item.filter(".on");
	var $itemOnIdx = $itemOn.index();
	
	var menuTotalWidth;
	
	gnbInit();
	
	$item.each(function(){
		initData.push($(this).offset().left);
	});
	
	if( initData[$itemOnIdx] > initData[1] ){
		$gnb.scrollLeft(initData[$itemOnIdx-1]);
	}
	
	$(window).resize(function(){
		gnbInit()
	});
	
	$gnb.on("touchstart", function() {
		lastTarget = $gnb.scrollLeft();
	});
	
	$gnb.on("touchend", function() {
		/* var near = [];
		var abs = 0;
		var min = menuTotalWidth/$item.length;
		var target = $gnb.scrollLeft();
		var data = [];
		
		$item.each(function(idx){
			data.push($(this).offset().left);
			if (data[idx] < 0 && data[idx]-min < 0){
				near.push(data[idx]);
			}
		});
		
		if(lastTarget > target){
			$gnb.animate({scrollLeft : (near.length*min)-min},speed);
		} else {
			$gnb.animate({scrollLeft : near.length*min},speed);
		} */
	});
	
	function gnbInit(){
		menuTotalWidth = 10;
		$item.each(function(){
			menuTotalWidth += $(this).outerWidth(true); 
		});
		$menu.width(menuTotalWidth);
	}
	
	/* var $gnbwrap = $(".gnb");
	var $gnb = $gnbwrap.find(".inner");
	var $item = $gnb.find(".slide");
	var $menu = $gnb.find(".menu");
	var itemWidthAll = 0;
	var target;
	var data = [];
	var lastTarget = 0;
	var speed = 300;
	
	 if($menu.length){
		$menu.each(function(){
			if($(this).is(":visible")){
				$(this).slick({
					//initialSlide 	: $(this).data("startIndex"),
					arrows 			: false,
					infinite		: false,
					draggable 		: true,
					variableWidth 	: true,
					swipeToSlide 	: true
				});
			}
		});
	} */
	
	// 2016.02 토탈메뉴
	// total menu 

// 2016.02 토탈메뉴
/*
	// total menu 
	var $total 					= $(".total_menu");
	var $totalbtn 				= $(".js-btn-total");
	var $totalCategory 			= $total.find(".total_menu_list.category");
	var $totalBrand 			= $total.find(".total_menu_list.brand");
	var $categoryDepth1 		= $totalCategory.find(".depth01");
	var $brandDepth1 			= $totalBrand.find(".depth01");
	var $categoryDepth2 		= $totalCategory.find(".depth02");
	var $brandDepth2 			= $totalBrand.find(".depth02");
	var $categoryDepth1Anchor 	= $categoryDepth1.find("a");
	var $brandDepth1Anchor 		= $brandDepth1.find("a");
	var $totalCloseBtn			= $total.find(".btn_close");
	
	$totalbtn.on("click",function(e){
		if($searchList.is(":visible")){
		
		}
		$header.removeAttr("style");
		$("#container").removeAttr("style");
		$(".sub_header").removeAttr("style");
		
		if($total.css("left") != 0){
			$total.css("left",0);
			$(mask).fadeIn();
		} else {
			$total.css("left","-100%");
			$(mask).fadeOut();
		}
		
		reset();
		$total.tabpannel($total.data())
		bodyScrollStop();
		e.preventDefault();
	});
	
	$(mask).add($totalCloseBtn).on("click",function(e){
		$(mask).fadeOut();
		$header.removeAttr("style");
		bodyScrollStart();
		$("*[id^='layer-']").hide();
		$("*[id^='main_promotion_banner']").hide();
		
		$total.css("left","-100%",function(){
			reset();
			$total.find("[data-event='tab']").tabpannel({activeIndex:0});
		});
		e.preventDefault();
	});
	
	$categoryDepth1Anchor.on("click",function(e){
		var _this = $(this);
		
		if(! _this.hasClass("on") ){
			var idx = _this.parent().index();
			$categoryDepth1Anchor.removeClass("on")
			_this.addClass("on");
			$categoryDepth2.hide().eq(idx).show()
			
		}
		e.preventDefault();
	});
	
	$brandDepth1Anchor.on("click",function(e){
		var _this = $(this);
		
		if(! _this.hasClass("on") ){
			var idx = _this.parent().index();
			$brandDepth1Anchor.removeClass("on")
			_this.addClass("on");
			$brandDepth2.hide().eq(idx).show()
			
		}
		e.preventDefault();
	});
	
	$categoryDepth2.find(" > ul > li > a").each(function(){
		if(! $(this).next("ul").length ){
			$(this).parent().addClass("noDepth");
		}
	});
	
	$categoryDepth2.find(" > ul > li:not('.all') > a").on({
		"click" : function(e){
			var _this = $(this);
			if(! _this.hasClass("noDepth") ){
				if( _this.hasClass("on") ){
					_this.removeClass("on")
					_this.next().hide();
				} else {
					$categoryDepth2.find(" > ul > li > a").removeClass("on").next().hide();
					_this.addClass("on").next().show();
				}
			}
			e.preventDefault();
		}
	});
	
	function reset(){
		$total.find(".total_menu_list").each(function(){
			var _this = $(this);
			_this.find(".depth01").find("a").removeClass("on");
			_this.find(".depth02").find("a").removeClass("on");
			_this.find(".depth02").hide().eq(0).show();
			_this.find(".depth03").hide();
			_this.find(".depth01").find("> ul > li:eq(0) a").addClass("on");
		});
	}
*/	
	// search list 
	var $field = $(".js-search-field"); 
	//var $del = $(".js-keyword-del");
	var $searchList = $(".search_main_wrap");
	var $result = $searchList.find(".result_word");
	var $resultTab = $searchList.find(".tab_area");
	var $close = $searchList.find(".btn_area .btn_close");
	var $delete = $searchList.find(".result_word > ul > li .btn_close");
	var $autoOff = $searchList.find(".auto_off");
	var $noResult = $searchList.find(".no_result");
	var $btnOption = $searchList.find(".btn_area");
	
	
	$result.eq(0).hide();
	$resultTab.find("li").eq(0).hide();
	searchAction($field)
	
	
	$field.focus(function(){
		$searchList.slideDown();
		//$header.css("z-index","101");
		//$(mask).fadeIn();
		//$('body').css({overflow:'hidden'});
		searchAction(this);
		
	}).focusout(function(){
		//$searchList.hide();
	})
	
	$field.on("keyup",function(){
		searchAction(this);
	})
	
	$close.on("click",function(e){
		$searchList.slideUp();
		//$(mask).fadeOut();
		//$('body').css({overflow:'auto'});
		e.preventDefault();
	})
	
	$resultTab.find("li>a").on("click",function(e){
		var idx = $(this).parent().index();
		$result.hide();
		$result.eq(idx).show();
		$resultTab.find("li").find("a").removeClass("on");
		$resultTab.find("li").eq(idx).find("a").addClass("on");
		e.preventDefault();
	});
	
	/* $btnOption.find("a.auto_complete_on, a.search_word_on").hide(); */
	
	/* $delete.on({
		"click" : function(e){
			e.preventDefault();	
		}
	});
	
	$btnOption.find("a.all_delete").on({
		"click" : function(e){
			$result.eq(1).find(">ul").remove().end().find(".no_result").show();
			$(this).hide();
			e.preventDefault();
		}
	});
	
	$btnOption.find("a.search_word_off").on({
		"click" : function(e){
			$btnOption.find("a.search_word_off").hide().end().find("a.search_word_on").show();
			$result.eq(1).find(">ul").remove().end().find(".no_result").show();
			$btnOption.find("a.all_delete").hide();
			e.preventDefault();
		}
	});
	
	$btnOption.find("a.search_word_on").on({
		"click" : function(e){
			$btnOption.find("a.search_word_on").hide().end().find("a.search_word_off").show();
			e.preventDefault();
		}
	});
	
	$btnOption.find("a.auto_complete_off").on({
		"click" : function(e){
			$btnOption.find("a.auto_complete_off").hide().end().find("a.auto_complete_on").show();
			$result.eq(0).find(">ul").remove().end().find(".auto_off").show();
			e.preventDefault();
		}
	});
	
	$btnOption.find("a.auto_complete_on").on({
		"click" : function(e){
			$btnOption.find("a.auto_complete_on").hide().end().find("a.auto_complete_off").show();
			e.preventDefault();
		}
	});
	
	$(document).on("click", ".search_main_wrap .recent .btn_close", function(e){
		var listWrap =  $(this).parents("ul");
		var list =  $(this).parents("li");
		list.remove();
		
		if( listWrap.find("li").size() < 1 ){
			listWrap.remove();
			$result.eq(1).find(".no_result").show();
			$btnOption.find("a.all_delete").hide();
		}
		e.preventDefault();
	}); */
	
	
	function searchAction(target){
		if($(target).val() == ""){
			$result.eq(0).hide();
			$result.eq(1).show();
			$resultTab.find("li").find("a").removeClass("on");
			$resultTab.find("li").eq(0).hide();
			
			/* if( $result.eq(1).find(">ul").size() > 0 ){
				$result.eq(1).find(".no_result").hide();
				$btnOption.find("a.all_delete").show();
			} else {
				$result.eq(1).find(".no_result").show();
			} */
			
			//$del.hide();
		} else {
			$result.show();
			$result.eq(1).hide();
			$resultTab.find("li").find("a").removeClass("on");
			$resultTab.find("li").eq(0).find("a").addClass("on");
			$resultTab.find("li").show();
			//$del.show();
		}
	}
});

/* product detail */
$(function(){
	var $productInner = $(".product_detail_wrap");
	
	// map select 
	var $mapSelect = $(".js-map-select");
	var $mapDetail = $(".map_detail li");

	$mapSelect.on("change", function(){
		var idx = $(this).find("option:selected").index();
		$mapDetail.hide();
		$mapDetail.eq(idx).show();
	});

	/* amount adjust */
	productAmountCheck();
	
	$(document).on("click", ".amount .minus", function(e){
		var $amountInput 	= $(this).siblings("input");
		var volumeSelect 	= $(this).parents(".price_info").find("select");
		var minAmount 		= Number($amountInput.data("minAmount")) || 1;
		var maxAmount 		= Number($amountInput.data("maxAmount")) || 999999;
		var packageAmount 	= Number($amountInput.data("packageAmount")) || 1;
		var checkedAmount 	= Number($amountInput.val());
		var currentAmount	= Number($amountInput.val());
		
		// min,max 유효성 체크
		if ( minAmount > packageAmount ){
			if( (minAmount % packageAmount) > 0 ){
				minAmount = ( minAmount + (packageAmount - (minAmount % packageAmount)));
			} else {
				minAmount = minAmount;
			}
		} else {
			minAmount = packageAmount;
		}
		
		if( maxAmount > packageAmount ){
			if ( (maxAmount % packageAmount) > 0 ){
				maxAmount = ( maxAmount - (maxAmount % packageAmount ));
			} else {
				maxAmount = maxAmount; 
			}
		} else {
			maxAmount = packageAmount;
		}
		
		if( checkedAmount % packageAmount > 0 ){
			checkedAmount = checkedAmount - (checkedAmount % packageAmount);
		}
		
		if( currentAmount <= maxAmount ){
			for( var i=0; i < packageAmount; i++ ){
				checkedAmount --;
			}
		}
		
		if( checkedAmount < minAmount ){
			alert("최소수량은 " + minAmount + "개 이상입니다." );
			checkedAmount = minAmount;
		} else if( checkedAmount > maxAmount ){
			checkedAmount = maxAmount;
			//alert("최대 수량은 " + maxAmount + "개입니다.");
		} 
		
		$amountInput.val(checkedAmount);
		$amountInput.data("prevCount" , checkedAmount);
		
		var _countConfirmBtn = $(this).parents(".amount").find(".btn_w");
		var currentCount 	= Number($amountInput.data("currentCount"));
		
		if( _countConfirmBtn.length){
			if( Number($amountInput.val()) != currentCount || volumeSelect.find("option:selected").index() != volumeSelect.data("selectIndex") ){
				_countConfirmBtn.show();
			} else {
				_countConfirmBtn.hide();
			}	
		}
		
		e.preventDefault();
	});
	
	$(document).on("click", ".amount .plus", function(e){
		var $amountInput 	= $(this).siblings("input");
		var volumeSelect 	= $(this).parents(".price_info").find("select");
		var minAmount 		= Number($amountInput.data("minAmount")) || 1;
		var maxAmount 		= Number($amountInput.data("maxAmount")) || 999999;
		var packageAmount 	= Number($amountInput.data("packageAmount")) || 1;
		var checkedAmount 	= Number($amountInput.val());
		
		// min,max 유효성 체크
		if ( minAmount > packageAmount ){
			if( (minAmount % packageAmount) > 0 ){
				minAmount = ( minAmount + (packageAmount - (minAmount % packageAmount)));
			} else {
				minAmount = minAmount;
			}
		} else {
			minAmount = packageAmount;
		}
		
		if( maxAmount > packageAmount ){
			if ( (maxAmount % packageAmount) > 0 ){
				maxAmount = ( maxAmount - (maxAmount % packageAmount ));
			} else {
				maxAmount = maxAmount /* - packageAmount;  */
			}
		} else {
			maxAmount = maxAmount;
		}
		
		if( checkedAmount%packageAmount > 0 ){
			checkedAmount = checkedAmount - (checkedAmount%packageAmount);
		}
		
		if( checkedAmount < minAmount ){
			checkedAmount = minAmount;
		} else if( checkedAmount >= maxAmount ){
			checkedAmount = maxAmount;
			alert("최대 수량은 " + maxAmount + "개입니다.");
		} else {
			for( var i=0; i < packageAmount; i++ ){
				checkedAmount ++;
			}
		} 
		
		$amountInput.val(checkedAmount);
		$amountInput.data("prevCount" , checkedAmount);
		
		var _countConfirmBtn = $(this).parents(".amount").find(".btn_w");
		var currentCount 	= Number($amountInput.data("currentCount"));
		
		if( _countConfirmBtn.length){
			if( Number($amountInput.val()) != currentCount || volumeSelect.find("option:selected").index() != volumeSelect.data("selectIndex") ){
				_countConfirmBtn.show();
			} else {
				_countConfirmBtn.hide();
			}	
		}
		
		e.preventDefault();
	});
	
	
	$(document).on("change keyup", ".amount input" , function(e){
		
		if( $(this).parents(".amount").find(".minus").size() > 0 || $(this).hasClass("amountInput") ){
			var $amountInput 	= $(this);
			var volumeSelect 	= $amountInput.parents(".price_info").find("select");
			var minAmount 		= Number($amountInput.data("minAmount")) || 1;
			var maxAmount 		= Number($amountInput.data("maxAmount")) || 999999;
			var packageAmount 	= Number($amountInput.data("packageAmount")) || 1;
			var checkedAmount 	= Number($amountInput.val());
			var _countConfirmBtn = $amountInput.parents(".amount").find(".btn_w");
			var currentCount 	= Number($amountInput.data("currentCount"));
			
			// min,max 유효성 체크
			if ( minAmount > packageAmount ){
				if( (minAmount % packageAmount) > 0 ){
					minAmount = ( minAmount + (packageAmount - (minAmount % packageAmount)));
				} else {
					minAmount = minAmount;
				}
			} else {
				minAmount = packageAmount;
			}
			
			if( maxAmount > packageAmount ){
				if ( (maxAmount % packageAmount) > 0 ){
					maxAmount = ( maxAmount - (maxAmount % packageAmount ));
				} else {
					maxAmount = maxAmount; 
				}
			} else {
				maxAmount = packageAmount;
			}
			
			if( e.type == "change" ){
				if( checkedAmount < minAmount ){
					alert("최소수량은 " + minAmount + "개 이상입니다." );
					$amountInput.val(minAmount);
					$amountInput.data("prevCount", minAmount);
				}
				else if( checkedAmount > maxAmount ){
					alert("최대 수량은 " + maxAmount + "개입니다.");
					$amountInput.val(maxAmount);
					$amountInput.data("prevCount", maxAmount);
				}
				else if( (checkedAmount % packageAmount) > 0 ){
					alert("선택하신 상품의 수량은 묶음단위 수량으로만 변경이 가능합니다.");
					$amountInput.val($amountInput.data("prevCount"));
				} else {
					$amountInput.data("prevCount", checkedAmount);
				}
			}
			
			if( e.type == "keyup" ){
				if( isNumber($(this).val()) == false ){
					$amountInput.val($amountInput.data("prevCount"));
				}	
			}
			
			if( _countConfirmBtn.length){
				if( Number($amountInput.val()) != currentCount || volumeSelect.find("option:selected").index() != volumeSelect.data("selectIndex") ){
					_countConfirmBtn.show();
				} else {
					_countConfirmBtn.hide();
				}	
			}	
		}
	});
	
	/* star check */
	var _sParent = $(".js-star-select");
	var $starBox = $(".js-star-select .star_box");
	var $star = $(".js-star-select a");
	var _sView = $(".star span");
	
	_sParent.find(".star_box").each(function(){
		$(this).find("a").on("click",function(e){
			var idx = $(this).index();
			$(this).closest($starBox).find(_sView).removeAttr("class").addClass("point" + (idx+1));
			$(this).closest($starBox).find("input").removeProp("checked").removeAttr("checked");
			$(this).closest($starBox).find("input").eq(idx).prop("checked",true).attr("checked",true);
			e.preventDefault();
		})
	});
	
	// color chip
	productSwipeColor();
	
});


/* marketing */
$(function(){
	var $orderTime = $(".order_time_search");
	// reset
	$orderTime.find(".btn_area a").eq(0).on({
		"click" : function(e){
			$orderTime.find("input[type='radio']").prop("checked" , false);
			$orderTime.find("input[type='date']").val("");
			$orderTime.find("select").val("");
			e.preventDefault();
		}
	});
	
	$(".bestproduct_wrap .btn_more").on({
		"click" : function(e){
			var _this = $(this);
			if( _this.hasClass("on")){
				_this.removeClass("on").find("span").text("검색어 BEST30 전체보기").end().prev("ul").find("li:gt(3)").hide();
			} else {
				_this.addClass("on").find("span").text("검색어 BEST4만 보기").end().prev("ul").find("li").show();
			} 
			e.preventDefault();
		}
	});
	
	var newArrivals = $(".marketing_event_wrap3");
	newArrivals.find(".tab_menu > li:eq(0) a").on({
		"click" : function(){
			$(".btn_compare").show();	
		}
	});
	
	newArrivals.find(".tab_menu > li:eq(1) a").on({
		"click" : function(){
			$(".btn_compare").hide();	
		}
	});
	
	$(".marketing_wedding_wrap .radio_box").find("input[type='radio']").on({
		"change" : function(){
			if( $(this).filter(":checked").parent().index() == 0 ){
				$(".spouse_input").eq(0).show();
				$(".spouse_input").eq(1).hide();
			} else {
				$(".spouse_input").eq(1).show();
				$(".spouse_input").eq(0).hide();
			}
		}
	});
	
	$(".marketing_wedding_wrap1 .tab_menu > li").on({
		"click" : function(e){
			var FAQ = $(".marketing_wedding_wrap.faq_wrap");
			
			if( $(this).index() == 3 ){
				FAQ.hide();
			} else {
				FAQ.show();
			}
			e.preventDefault();
		}
	});
	
	$(".bestshop_wrap .sliderNavSingle").each(function(){
		var _this = $(this);
		
		if(_this.find("li").size() < 4 ){
			_this.addClass("slick-none");
			_this.find("li").on({
				"click" : function(){
					$(this).siblings().removeClass("on").end().addClass("on");
				}
			});
		}
	});
	
});
/* end : marketing */


/* customer */
$(function(){
	
	// textArea maxLength 
	$(document).on("keyup","textarea.textarea",function(e){
		e = e || window.event;
		
		var othis = $(this);
		var count = othis.next(".check");
		var ls_str = othis.val();
		var li_str_len = ls_str.length; //전체길이
		var i = 0;
		var li_byte = 0;   //한글일경우 2, 그외글자는 1을 더함
		var ls_one_char = "";  //한글자씩 검사				
		var defaultByte = 1300;
		
		if( $(this).data("byte") ){
			defaultByte = Number($(this).data("byte"));
		}
		
		for(i=0; i< li_str_len; i++){
			ls_one_char = ls_str.charAt(i);   //한글자 추출
			if(escape(ls_one_char).length > 4){ 
			  li_byte ++;   //한글이면 2를 더한다
			}else{
			  li_byte++;     //한글아니면 1을 다한다
			}					
		}	
		count.text(li_byte+"/"+defaultByte+"자"); //2016-10-14 수정
		textarea_maxlength( othis, defaultByte);
	});

	function textarea_maxlength(obj, maxLength){
		if( obj.val().length > maxLength ){
			var thisTxt = obj.val();
			thisTxt = thisTxt.substring(0, maxLength)
			obj.val(thisTxt);
		}
	}
	
	$("#order_num").on({
		"keydown" : function(e){
			if( e.keyCode == 13 && $(this).val() ){
				popOpen($("#layer-myorder"));
			}	
		}
	});
	
	function selecthtml(data){
		var select_html = "";
		for(var i in data){
			select_html = select_html+"<option value=''>"+data[i]+"</option>";
		}
		return select_html;
	}
	
	$(document).on("change", ".csform_type1", function(){
		var text = $(this).find("option:selected").text();
		var data = new Array();
		if(text === "회원"){
			data = ["개인 정보","출국 정보","여권 정보","멤버스 정보"];		
			$(".csform_type2").html("<option value='' selected='selected'>문의유형2 선택</option>"+selecthtml(data)).removeAttr("disabled");			
		}else if(text === "상품"){
			data = ["상품 문의","스페셜오더","주문/결제","환불/취소"];		
			$(".csform_type2").html("<option value='' selected='selected'>문의유형2 선택</option>"+selecthtml(data)).removeAttr("disabled");
		}else if(text === "서비스"){
			data = ["사은품/사은권","교환권/쿠폰/별","이벤트/웨딩샵","적립금/OK캐시백","이용 문의","기타"];		
			$(".csform_type2").html("<option value='' selected='selected'>문의유형2 선택</option>"+selecthtml(data)).removeAttr("disabled");
		}else{
			$(".csform_type2").html("<option value='' selected='selected'>문의유형2 선택</option>").attr("disabled","disabled");
			$(".csform_type3").html("<option value='' selected='selected'>문의유형3 선택</option>").attr("disabled","disabled");
		}
	});
	
	$(document).on("change", ".csform_type2", function(){
		var text = $(this).find("option:selected").text();
		var data = new Array();
		switch(text){
			case "개인 정보":
				data = ["ID/PW","주민번호/아이핀","재외국인","회원탈퇴"];
			break;
			case "출국 정보":
				data = ["출국 일시","비행기 편명","출국 장소","도착 장소"];
			break;
			case "여권 정보":
				data = ["여권 이름","여권 번호"];
			break;
			case "멤버스 정보":
				data = ["맴버스 카드 발급/재발급","맴버스 등급","맴벗 혜택","가족 맴버스 신청"];
			break;
			case "상품 문의":
				data = ["상품 재고","상품 재입고","상품 상태","상품 브랜드","상품 교환","상품 AS","상품 구매"];
			break;
			case "스페셜오더":
				data = ["스페셜오더 상품","스페셜오더 브랜드","스페셜오더 신청"];
			break;
			case "주문/결제":
				data = ["주문","주문 시간 확인","무통장/계좌이체 결제","신용카드 결제","휴대폰 결제"];
			break;
			case "환불/취소":
				data = ["출국 취소","상품 미수령","재구매로 인한 취소","무통장/계좌이체 환불","주문 취소 해지","휴대폰 결제 취소/환불"];
			break;
			case "사은품/사은권":
				data = ["사은품 문의","사은권 문의"];
			break;
			case "교환권/쿠폰/별":
				data = ["교화권 다시 출력","쿠폰 사용","별 적립","별 사용"];
			break;
			case "이벤트/웨딩샵":
				data = ["이벤트 문의","이벤트 당첨자 발표","카드사 제휴 이벤트","웨딩샵 문의","웨딩샵 청첩장 등록"];
			break;
			case "적립금/OK캐시백":
				data = ["적립금 사용","적립금 소멸","상품권 전환 적립금","OK캐쉬백 적립","OK캐쉬백 사용"];
			break;
			case "이용 문의":
				data = ["면세점 이용/쇼핑 문의","상품 인도장 문의"];
			break;
		}
		$(".csform_type3").html("<option value='' selected='selected'>문의유형3 선택</option>"+selecthtml(data)).removeAttr("disabled");
		if(text === "기타"){
			$(".csform_type3").html("<option value='' selected='selected'>문의유형3 선택</option>").attr("disabled","disabled");	
		}
	});
	
	$(".cart_complete_wrap .accordion_tit").each(function(){
		var _this = $(this);
		
		_this.find("a").on({
			"click" : function(e){
				if( $(this).hasClass("on") ){
					$(this).removeClass("on");
					_this.parents("table").find(".accordion_cont").hide();
				} else {
					$(this).addClass("on");
					_this.parents("table").find(".accordion_cont").show();
				}
				e.preventDefault();
			}
		});
	});
	
});
/* end : customer */


/* sing up */
$(function(){
	if( $("#layer-passport_term").size() > 0 ){
		//여권 만료일 팝업
		popOpen($("#layer-passport_term"));
	}
});
/* end : sing up */


/* cart */
$(function(){
	
	// barcode zoom
	$("#barcoard-big").on({
		"click" : function(e){
			var imgSrc = $(this).find("img").attr("src");
			var barCodeLayer = $("#layer-barcoard-big");
			barCodeLayer.find(".img img").attr("src" , imgSrc);
			var layerPos = function(){
				var winH 	=  $(window).height();
				var winW 	=  $(window).width();
				if( winH > winW ){
					barCodeLayer.removeClass("horizontal");
				} else {
					barCodeLayer.addClass("horizontal");
				}
			}
			layerPos();
			$(window).resize(function(){
				layerPos();
			});
			e.preventDefault();
		}
	});
	
	//O2O checked
	var o2oContent 			= $(".cart_wrap .pickup");
	var o2oAllCheckBox 		= o2oContent.find("#all_check");
	var o2oEachCheckBox 	= o2oContent.find(".box .check_product input[type='checkbox']").not(":disabled");
	var o2oEachCheckBoxSize = o2oEachCheckBox.size(); 
	
	o2oAllCheckBox.on({
		"change" : function(){
			if( $(this).prop("checked") == true ){
				o2oEachCheckBox.prop("checked" , true);
				
				o2oContent.find(".box .indo_radio").each(function(){
					$(this).find(".radio_list li:eq(0) input[type='radio']").prop("checked", true);
				});
				
			} else {
				o2oEachCheckBox.prop("checked" , false);
				o2oContent.find(".box .indo_radio").each(function(){
					$(this).find(".radio_list li:eq(1) input[type='radio']").prop("checked", true);
				});
			}
		}
	});
	
	o2oEachCheckBox.on({
		"change" : function(){
			var _this = $(this);
			
			if( o2oEachCheckBox.filter(":checked").size() == o2oEachCheckBoxSize ){
				o2oAllCheckBox.prop("checked" , true);
			} else {
				o2oAllCheckBox.prop("checked" , false);
			}
			
			if( _this.prop("checked") == true ){
				_this.parents(".box").find(".indo_radio").each(function(){
					$(this).find(".radio_list li:eq(0) input[type='radio']").prop("checked", true);
				});
			} else {
				_this.parents(".box").find(".indo_radio").each(function(){
					$(this).find(".radio_list li:eq(1) input[type='radio']").prop("checked", true);
				});
			}
		}
	});
	
	o2oContent.find(".box").each(function(){
		var _this = $(this);
		_this.find("input[type='radio']").on({
			"change" : function(){
				var listSize 	= _this.find(".indo_radio").size();
				var checked1 	= 0;
				var checked2 	= 0;
				var allChecked 	= 0;
				
				_this.find(".indo_radio").each(function(){
					if( $(this).find(".radio_list li").eq(1).find("input[type='radio']").prop("checked") == true ){
						checked1++;
					}
					if( $(this).find(".radio_list li").eq(0).find("input[type='radio']").prop("checked") == true ){
						checked2++;
					}
				});
				if( checked1 > 0 ){
					_this.find(".check_product input[type='checkbox']").prop("checked" , false);
					o2oAllCheckBox.prop("checked" , false);
				}
				
				if( checked2 == listSize ){
					_this.find(".check_product input[type='checkbox']").prop("checked" , true);
					
					o2oEachCheckBox.each(function(){
						if( $(this).prop("checked") == false ){
							allChecked ++;
						}
					});
					if( allChecked == 0 ){
						o2oAllCheckBox.prop("checked" , true);
					}
				}
			}
		});
	});

	// 경유지 선택
	var myDepartureWrap = $(".my_departure_wrap");
	var viaArea = myDepartureWrap.find(".flight_info");
	
	myDepartureWrap.find("#depPlaceCode").on({
		"change" : function(){
			var val = $(this).val();
			
			if( val == "P" || val == "U" || val == "I" ){
				viaArea.find("li:eq(0) input[type='radio']").prop("checked" , true);
				viaArea.find("li:eq(1) input[type='radio']").prop("disabled" , true);
				viaArea.find("select").hide();
			} 
			else if ( val == "" ){
				viaArea.find("li:eq(0) input[type='radio']").prop({"checked" : false, "disabled" : true });
				viaArea.find("li:eq(1) input[type='radio']").prop({"checked" : false, "disabled" : true });
				viaArea.find("select option:eq(0)").prop("selected" , true);
				viaArea.find("select").hide();
			}
			else {
				viaArea.find("li:eq(0) input[type='radio']").prop({"checked" : true, "disabled" : false });
				viaArea.find("li:eq(1) input[type='radio']").prop("disabled" , false);
				viaArea.find("select option:eq(0)").prop("selected" , true);
			}
		}
	});
	
	viaArea.find("input[type='radio']").on({
		"change" : function(){
			if( viaArea.find("input[type='radio']:checked").parents("li").index() == 0 ){
				viaArea.find("select").hide().find("option:eq(0)").prop("selected" , true);
			} else {
				viaArea.find("select").show();
				alert("환승 시 액체류나 젤류 등을 동반할 시 투명비닐을 준비하셔야 합니다.\n자세한 정보는 기내반입제한품목을 확인하시기 바랍니다.");
			}
		}
	});
	
	$(".cart_payment_wrap .my_departure_wrap .flight_info").find("select").on({
		"change" : function(){
			var val = $(this).val();
			var msg;
			
			if ( val == 1 || val == 4 || val == 101 || val == 16 || val == 18 || val == 13 || val == 5 || val == 19 || val == 20 ){
				msg = "선택하신 국가를 경유하시는 경우에는 액체류, 젤류 제품을 구매하실 수 없습니다.\n다만, 개인물품 휴대 기내반입조건에 충족되는 경우 구입 가능 하오니 기내반입제한 안내를 확인해 보시기 바랍니다.\n계속 주문하시겠습니까? (취소를 선택하시면, 장바구니로 되돌아갑니다.)";
				if(! confirm(msg)) {
					redirectCart();
				}
			} 
			else if ( val == 15 ){
				msg = "미국을 경유하실 경우, 불투명용기 및 금속용기에 담긴 액체류, 젤류 제품은 반입이 제한됩니다.\n(기타 투명용기에 담긴 제품은 구매, 반입이 가능합니다.)\n\n다만, 경유 후 도착지가 미국령, 호주령, 캐나다, 버진아일랜드인 경우에는 구매하실 수 없습니다.\n계속 주문하시겠습니까? (취소를 선택하시면, 장바구니로 되돌아갑니다.)";
				if(! confirm(msg)) {
					redirectCart();
				}
			} 
			else if ( val == 21 ){
				msg = "그 외의 국가를 경유하실 경우에는 액체류, 젤류 제품의 구매가 가능합니다.\n(환승시 해당국가의 보안규정이 달라 액체류에 대한 압수절차를 따라야 할 경우가 있으니, 이용하시는 항공사에 사전문의 해주시기 바랍니다.)\n\n다만, 경유 후 도착지가 미국령, 호주령, 캐나다, 버진아일랜드인 경우에는 구매하실 수 없습니다.\n계속 주문하시겠습니까? (취소를 선택하시면, 장바구니로 되돌아갑니다.)";
				if(! confirm(msg)) {
					redirectCart();
				}
			} 
			else {
				
			}
		}
	});
	
	function redirectCart(){
		document.location = "/estore/kr/ko/cart";
	}
	
});
/* end : cart */


/* my page */
$(function(){
	
	// 친구 관심 상품리스트 아이디 계정 입력폼 선택 
	$(".my_add_wrap .choice_box  input[type='radio']").on({
		"change" : function(){
			var _this = $(this);
			$(".my_add_wrap .choice_wrap .choice_cont").hide();
			if( _this.parents("li").index() == 0 ){
				$(".my_add_wrap .choice_wrap .choice01").show();
			} else {
				$(".my_add_wrap .choice_wrap .choice02").show();
			}
		}
	});
	
	// push alert
	$(".push_btn").each(function(){
		var touchstartX,touchendX,touches,direction;
		$(this).find("a").on({
			"touchstart" : function(event){
				if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
					touches = event.originalEvent.touches[0];
				}
				touchstartX = touches.clientX;
				event.preventDefault();
			},
			"touchmove" : function(event){
				if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
					touches = event.originalEvent.touches[0];
				}
				touchendX = touches.clientX;
			},
			"touchend" : function(event){
				if( touchstartX > touchendX ){
					direction = "left";
					$(this).removeClass("on");
				} else {
					direction = "right";
					$(this).addClass("on");
				}
				event.preventDefault();
			}
		});
	});
	
	$(".my_specialorder_wrap .ic_toggle").on({
		"click" : function(e){
			var _this = $(this);
			if( _this.find("span").hasClass("on") ){
				_this.find("span").removeClass("on");
				$(".answer").hide();
			} else {
				_this.find("span").addClass("on");
				$(".answer").show();
			}
			e.preventDefault();
		}
	});
	
	
	// 수신, 비수신
	$("[data-radio-receive]").each(function(){
		var _this = $(this);
		var target = _this.data("radioReceive");
		_this.on({
			"change" : function(){
				$("[data-receive-target='"+ target +"']").find("input[type='checkbox']").prop({ "disabled" : false });
			}
		});
	});
	
	$("[data-radio-noreceive]").each(function(){
		var _this = $(this);
		var target = _this.data("radioNoreceive");
		_this.on({
			"change" : function(){
				$("[data-receive-target='"+ target +"']").find("input[type='checkbox']").prop({ "disabled" : true, "checked" : false });
			}
		});
	});
	
});
/* end : my page */


/* brand */
$(function(){
	$(".brand_all").each(function(){
		var _this = $(this)
		
		_this.find(".consonant_wrap a").on({
			"click" : function(e){
				var data = $(this).index();
				_this.find("div[data-sort-target]").hide();
				_this.find("div[data-sort-target='"+data+"']").show();
				
				_this.find(".consonant_wrap a").removeClass("on");
				$(this).addClass("on");
				
				e.preventDefault();
			}
		});
	});
});	
/* end : brand */


/* accordion */
$.fn.accordion = function(options){
	var self = this;
	var _setting = options;
	var _default = {
		accordionElement : ".btn-accordion",
		accordionDetail : ".accordion-detail",
		activeElement : ":on",
		activeIndex : 0,
		slideType : 0,
		relationship : "next" 
	}
	
	self.option = $.extend(_default, _setting);
	
	var activeElem = self.option.activeElement.split(":")[0];
	var activeClass = self.option.activeElement.split(":")[1];
	var elem = self.option.accordionElement;
	var detail = self.option.accordionDetail;
	var slideType = self.option.slideType;
	var idx = self.option.activeIndex;
	var relationship = self.option.relationship;

	function init(){
		
		if( idx != null ){
			build();
		}
		
		self.find(elem).find("a").on("click",function(e){
			action(this);
			e.preventDefault();
		})
	}
	
	function build(){
		//reset
		
		self.find(detail).hide();
		if(relationship == "next"){
			self.find(elem).eq(idx).next(detail).show();
		} else if(relationship == "children") {
			self.find(elem).eq(idx).find(detail).show();
		}
		
		if(activeElem == "a"){
			self.find(elem).eq(idx).find("a").addClass(activeClass);
		} else {
			self.find(elem).eq(idx).addClass(activeClass);
		}
		
		if( idx == "-1" ){
			self.find(elem).find("a").removeClass("on");
			self.find(detail).hide();
		}
	}
	
	function action(target){
		var $parent = $(target).parent(elem);
		var $target;
		
		if(relationship == "next"){
			$target = $parent.next(detail);
		} 
		else if(relationship == "children"){
			$target = $parent.find(detail);
		}
		
		if($target.is(":visible")){
			$target.slideUp();
			if(activeElem == "a"){
				$(target).removeClass(activeClass);
			} else {
				$parent.removeClass(activeClass);
			}
		} else {
			if(slideType == 0){
				self.find(detail).slideUp();
			}
			
			$target.slideDown();
			if(activeElem == "a"){
				if(slideType == 0){
					self.find(elem).find("a").removeClass(activeClass);
				}
				$(target).addClass(activeClass);
			} else {
				if(slideType == 0){
					self.find(elem).removeClass(activeClass);
				}
				$parent.addClass(activeClass);
			}
		}
		slickReset($target);
	}
	
	function slickReset(target){
		var slickAuto = $(".slideAuto");
		var slideClass = $(".slideArrowDot");
		
		if($(target).find($(productImgArr)).length){
			productSwipeArrow($(target).find($(productImgArr)));
		}
		
		if($(target).find(slideClass).length){
			slideArrowDot($(target).find(slideClass));
		}
	}
	
	init();
}

/* tab menu */
$.fn.tabpannel = function(options){
	var self = this;
	var _setting = options;
	var _default = {
		tabMenu : ".tab_menu",
		tabDetail : ".tab_detail",
		activeElement : ":on",
		activeIndex : 0
	}
	
	self.option = $.extend(_default, _setting);
	
	var activeElm = self.option.activeElement.split(":")[0];
	var activeClass = self.option.activeElement.split(":")[1];
	var elem = self.option.tabMenu;
	var detail = self.option.tabDetail;
	var idx = self.option.activeIndex;
	
	function init(){
		build();
	
		self.find(elem).find("a").on("click",function(e){
			action(this);
			e.preventDefault();
		})
	}
	
	function build(){
		//reset
		self.find(detail).hide();
		self.find(detail).eq(idx).show();
		
		if(activeElm == "a"){
			self.find(elem).children().find("a").removeClass(activeClass);
			if(self.find(elem).children().length > 1){
				self.find(elem).children().eq(idx).find("a").addClass(activeClass);
			} else {
				self.find(elem).children().children().eq(idx).find("a").addClass(activeClass);
			}
		} else {
			self.find(elem).children().removeClass(activeClass);
			self.find(elem).children().eq(idx).addClass(activeClass);
		}
	}
	
	function action(target){
		var $parent = $(target).parent();
		var idx = $parent.index();
		self.find(detail).hide();
		self.find(detail).eq(idx).show();
		
		if(activeElm == "a"){
			$parent.parent().find("a").removeClass(activeClass);
			$(target).addClass(activeClass)
		} else {
			$parent.siblings().removeClass(activeClass);
			$parent.addClass(activeClass)
		}
		
		slickReset(idx);
	}
	
	function slickReset(idx){
		var slideClass = $(".slideArrowDot");
		
		if(self.find(detail).eq(idx).find(slideClass).length){
			slideArrowDot(self.find(detail).eq(idx).find(slideClass));
		}
		
		if(self.find(detail).eq(idx).find(".tabSlideAuto").length){
			tabAutoSlideBanner(self.find(detail).eq(idx).find(".tabSlideAuto"));
		}
		
		if(self.find(detail).eq(idx).find(productImgArr).length){
			productSwipeArrow(self.find(detail).eq(idx).find(productImgArr));
		}	
	}
	
	init();
}

// 청첩장 등록
$(document).ready(function(){	
	'use strict';
	
	$(".datepicker").each(function(){
		var thisData = $(this).data();
		var _default = {
			minDateValue : "-0d",
			maxDateValue : null,
		}
	});	

	$(".border .list").find("input[type='radio']").on({
		"change" : function(){
			if( $(this).filter(":checked").index() == 0 ){
				$(".spouse_id").show();
				$(".spouse_sns").hide();
			} else {
				$(".spouse_id").hide();
				$(".spouse_sns").show();
			}
		}
	});


	if ($('.product_detail_wrap .btn_area.floating').length != 0)
	{
		$('#footer').css('padding-bottom',$('.product_detail_wrap .btn_area.floating').height()+'px');
	}
	if ($('.btn_compare.clear_both').length != 0)
	{
		$('#footer').css('padding-bottom',$('.btn_compare.clear_both').height()+'px');
	}
});

// 열고닫기
function layer_toggle(obj) {
		if (obj.style.display == 'none') obj.style.display = 'block';
		else if (obj.style.display == 'block') obj.style.display = 'none';
}