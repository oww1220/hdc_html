(function(){
	/* device division */
	var diviceUserAgentArry = [
		{ name : "huaweimt", className : "Huawei-MT" }, // huawei mate series
		{ name : "iphone", className : "iPhone" }		// iphone series
	];

	$.each(diviceUserAgentArry, function(idx, val){
		if( navigator.userAgent.toLowerCase().indexOf(val.name) > -1 ){ 
			$("body").addClass(val.className);
		}
	});
	
	/* ios keypad bug fixed */
	if( $("body").hasClass("mobileApp") && $("body").hasClass("iPhone") ){
		/*$(document).on('focusin', 'input, textarea', function(){
			$("body").addClass('unfixed');
		})
		.on('focusout', 'input, textarea', function(){
			$("body").removeClass('unfixed');
		});*/                                                                              
	}
});

/* 2016-09-29 추가 */
$(function(){	
	
	// 수신, 비수신
	$("[data-radio-receive]").each(function(){
		var _this = $(this);
		var target = _this.data("radioReceive");
		_this.on({
			"change" : function(){
				$("[data-receive-target='"+ target +"']").find("input[type='checkbox']").prop({ "disabled" : false });
			}
		});
	});
	
	$("[data-radio-noreceive]").each(function(){
		var _this = $(this);
		var target = _this.data("radioNoreceive");
		_this.on({
			"change" : function(){
				$("[data-receive-target='"+ target +"']").find("input[type='checkbox']").prop({ "disabled" : true, "checked" : false });
			}
		});
	});

});
/* //2016-09-29 추가 */

/* loading value */
var lastScroll 		= $(window).scrollTop();
var mask 			= ".bg_dimmed"; // 2016-11-11 수정
var nav 			= false;
var headerMainCheck = true;
var winScrollTop, allCheckLinkage;
	
/* layer popup */
var windowScrollTopPosition, windowScrollFuncPrevent;

function bodyScrollStop(){
	windowScrollFuncPrevent = true;
	windowScrollTopPosition = $(window).scrollTop();
	$("body").css({ "overflow" : "hidden" , "position" : "fixed" });
	$("#wrap").css({"position" : "fixed" });
}

function bodyScrollStart(){
	$("body").css({ "overflow" : "auto" , "position" : "static" });
	$("#wrap").css({"position" : "static" });
	$(window).scrollTop(windowScrollTopPosition);
	windowScrollFuncPrevent = false;
}

function popClose (_this){
	_this.hide();
	_this.find(".pop_content").removeAttr("style");
	$(mask).removeClass("on").css({ "z-index" : 100 }); //2016-11-11 딤드노출
	bodyScrollStart();
}

function popOpen (_this){
	_this.show();
	popPos(_this);
	$(mask).addClass("on").css({ "z-index" : _this.css("zIndex") - 1 }); //2016-11-11 딤드노출
}

function popPos(target){
	var popContent  = target.find(".pop_content , .pop_compare");
	target.removeAttr("style");
	popContent.removeAttr("style");
	var winH 		= $(window).height();
	var winW 		= $(window).width();
	var thisH 		= target.height();
	var thisW 		= target.width();
	var popHeaderH 	= target.find(".pop_header").outerHeight();
	var popFooterH  = target.find(".pop_footer").size() > 0 ?  parseInt(target.find(".pop_footer").outerHeight()) : 0; 
	var exception	= target.hasClass("sitemap") ? true : false;	
	
	positionCtrl();
	
	$(window).resize(function(){
		if( target.is(":visible") ){
			target.removeAttr("style");
			popContent.removeAttr("style");
			
			winH 		= $(window).height();
			winW 		= $(window).width();
			thisH 		= target.height();
			thisW 		= target.width();
			
			positionCtrl();
		}
	});
	
	function positionCtrl(){ 
		if( thisH > winH || exception ){
			target.css({
				"top"	: 0,
				"left"  : (winW-thisW)/2,
				"height" : winH,
				"overflow" : "hidden"
			});
			
			popContent.css({
				"height" : winH - popHeaderH - popFooterH - parseInt(popContent.css("paddingTop")) - parseInt(popContent.css("paddingBottom")),
				"overflow" : "auto"
			});
			bodyScrollStop();
		} else {
			target.css({
				"top" : parseInt(($(window).height() - target.height())/2),
				"left"  : (winW-thisW)/2
			});
			bodyScrollStop();
		}
	}
}

function popClick (_this){
	var popName = $(_this).attr('id');
	var popTarget = "layer-" + popName;
	var targetLayer = $("#"+popTarget);
	
	if( $(_this).hasClass("self-close") ){
		$(_this).parents(".popup_layer_wrap").hide();
		$(mask).removeClass("on");
	}
	/* 2016-12-21 수정 */
	if( popName == 'open-membership' ){
		popOpen2(targetLayer);
	}else{
		popOpen(targetLayer);
	}
	/* //2016-12-21 수정 */
}

/* 2016-12-21 수정 */
function popClose2 (_this){
	_this.removeAttr("style").hide()
		.find(".pop_content").removeAttr("style");
	_this.find(".pop_contents").find(">*").unwrap();
	_this.hide();
	$(mask).fadeOut();
	if(! $(".popup_layer:visible").length ){
		/* 20180306공유하기 레이어팝업 s */
		if(!$("#layer-share-info").length){
			$(mask).removeClass("on");
		}
		else{
			$(mask).removeClass("share");
		}
		/* 20180306공유하기 레이어팝업 e */
	}
	bodyScrollStart();
}
function popOpen2 (_this){
	_this.show();
	$(mask).fadeIn();
	/* 20180306공유하기 레이어팝업 s */
	if(!$("#layer-share-info").length){
		$(mask).addClass("on");
	}
	else{
		$(mask).addClass("share");
	}			
	/* 20180306공유하기 레이어팝업 e */
	
	var winH =  $(window).height();
	var winW =  $(window).width();
	//var thisH 		= _this.height();
	var thisH 		= _this.outerHeight();
	var thisW 		= _this.width();
	var popBox 		= _this.find(".box");
	var popHeader 	= popBox.find(">div:eq(0)");
	var popHeaderH 	= popBox.find(">div:eq(0)").outerHeight();
	var whiteSpace	= popHeaderH + parseInt(popBox.css("marginTop")) + parseInt(popBox.css("marginBottom")) + parseInt(popBox.css("paddingTop")) + parseInt(popBox.css("paddingBottom"));
	
	console.log("h : " + thisH + ", w : " + thisW );
	
	positionCtrl();
	
	// 확대 이미지 레이어
	if( _this.attr("id") == "layer-zoom" ){
		_this.height(winH);
		slideBanner('.zoomSlideBanner');
	}
	
	$(window).resize(function(){
		if( _this.is(":visible") ){
			/* 20180306공유하기 레이어팝업 s */
			if(!$("#layer-share-info").length){
				_this.removeAttr("style");
			}			
			/* 20180306공유하기 레이어팝업 e */
			popBox.find(".pop_contents").find(">*").unwrap();
			winH =  $(window).height();
			winW =  $(window).width();
			thisH =	_this.height();
			thisW =	_this.width();
			positionCtrl();
		}
	});
	
	function positionCtrl(){ 
		if( thisH > winH ||  _this.attr("id") == "layer-passportinfo"){
			_this.css({
				"height" : winH,
				"top"	: 0,
				"left"  : (winW-thisW)/2,
				"overflow" : "hidden"
			});
			
			var popContents =  popHeader.next();
			popHeader.after("<div class='pop_contents'></div>");
			popBox.find(".pop_contents").prepend(popContents);
			
			popBox.find(".pop_contents").css({
				"height" : winH - whiteSpace,
				"overflow" : "auto"
			});
		} else {
			_this.css({
				"top" : parseInt(($(window).height() - _this.height())/2),
				"left"  : (winW-thisW)/2
			});
		}
	}
	
	bodyScrollStop();
}

function popClick2 (_this){
	var popName = $(_this).attr('id');
	var popTarget = "layer-" + popName;
	var targetLayer = $("#"+popTarget);
	popOpen2(targetLayer);
}
/* //2016-12-21 수정 */


function popSelfClose (id){
	$("#"+id).hide();
	$(mask).removeClass("on");
}

function allCheckboxProp(className,prop){
	$("."+className).each(function(){
		var _this = $(this);
		var _default = {
			checkedTarget 	: "#container",
			relationship	: ""
		}
		var _options 		= $.extend(_default, _this.data())
		var allCheckbox 	= _this.find("input[type='checkbox']");
		var eachCheckbox 	= $(_options.checkedTarget).find("input[type='checkbox']").not(allCheckbox);
		var checked; 
		
		/* if( prop == true ){
			allCheckbox.prop("checked" , true)
			eachCheckbox.prop("checked", true)
		} else {
			allCheckbox.prop("checked", false)
			eachCheckbox.prop("checked", false)
		} */
		
		allCheckbox.on({
			"change" : function(){
				if( $(this).prop("checked") == true){
					checked = true;
				}
				else {
					checked = false;
				}
				eachCheckbox.prop("checked" , checked);
			}
		});
		
		eachCheckbox.on({
			"change" : function(){
				if( eachCheckbox.filter(":checked").size()  == eachCheckbox.size() ){
					allCheckbox.prop("checked" , true);
				} else {
					allCheckbox.prop("checked" , false);
				}
			}
		});
	});
}

function facetOptionCheck(){
	if( $(".search_wrap").find(".condition li.off").size() > 0 ){
		$(".search_wrap").find(".condition .btn_toggle").show();
		if( $(".search_wrap").find(".condition .btn_toggle").hasClass("on")){
			$(".search_wrap").find(".condition .btn_toggle").removeClass("on").find("span").text("更多条件");
			$(".search_wrap").find(".condition li.off").hide();
		}
	} else {
		$(".search_wrap").find(".condition .btn_toggle").hide();
	}
}

var beforeReviewHeight = $(".js-product-detail .review_list").outerHeight(); 

function commentAboutProductHeightCtrl(){
	var slickList = $(".js-product-detail").find("> .slick-list");
	var beforegHeight =  slickList.height();
	var afterReviewHeight = $(".js-product-detail .review_list").outerHeight();
	slickList.height(beforegHeight+(afterReviewHeight-beforeReviewHeight));
	beforeReviewHeight = afterReviewHeight;
}

/* slick */

var autoSlickOption = {
		dots: true,
		dotsClass : "banner_nav",
		arrows : false,
		autoplay : true,
		autoplaySpeed : 2000,
		cssEase: 'linear',
		slidesToShow: 1,
		pauseOnHover : true,
		pauseOnDotsHover : true
	};//2016-11-01 수정

var manualSlickOption = {
		dots: true,
		dotsClass : "banner_nav",
		arrows : false,
		autoplay : false,
		autoplaySpeed : 2000,
		cssEase: 'linear',
		slidesToShow: 1,
		pauseOnHover : true,
		pauseOnDotsHover : true
	};//2016-11-01 수정

var slideBanner = function(className){
	// single slide
	$(className).slick({
		autoplay : true,
		dots: true,
		dotsClass : "banner_nav",
		arrows : false,
		cssEase: 'linear',
		pauseOnHover : false,
		pauseOnDotsHover : true,
		slidesToShow: 1,
		pauseOnHover : true,
		pauseOnDotsHover : true
	});
}

var slideManual = function(className){
	//single manual slide
	if($(className).length && $(className).hasClass("slick-slider") == false){
		$(className).each(function(){
			if($(this).is(":visible")){
				$(this).slick(manualSlickOption);
			}
		});
	}
}
/* 2016-11-01 수정 */
var slideAuto = function(className, opt){
	//single auto slide
	if($(className).length && $(className).hasClass("slick-slider") == false){
		$(className).each(function(){
			if($(this).is(":visible")){
				var option = autoSlickOption;
				if(typeof opt != 'undefined'){
					for(var key in opt){
						option[key] = opt[key];
					}
				}
				$(this).slick(option);
			}
		});
	}
}
/* //2016-11-01 수정 */
	
var sliderFor = function(className){
	// rolling navigation, rolling page
	$(className).slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '.sliderNav'
	});
}
	
var sliderNav = function(className){
	$(className).slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.sliderFor',
		centerMode: true,
		centerPadding : 0,
		focusOnSelect: true
	});
}

var sliderNavSingle = function(className){
	$(className).slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		centerMode: false,
		focusOnSelect: true
	});
}
/* 2016-10-20 추가 */
var cateSliderNavSingle = function(classNme){
	if($(classNme).length && $(classNme).hasClass("slick-slider") == false){
		$(classNme).each(function(){
			if($(this).is(":visible")){
				$(this).slick({
					arrows : true,
					slidesToShow: 3,
					slidesToScroll: 1,
					centerMode: true,
					centerPadding : 0,
					focusOnSelect: true
				});
			}
		});
	}
}
/* //2016-10-20 추가 */
	
/* layout action */
function layout(){
	var winH 	= $(window).height();
	var hHeight = $("#header").outerHeight() - $(".gnb").outerHeight();
	$("#container").css({"-webkit-overflow-scrolling":"touch"});
	//$(".total_menu").css({"padding-top":hHeight})
	//$(".total_menu").find(".inner").height(winH-hHeight);
	
	$(window).resize(function(){
		winH 	= $(window).height();
		//$(".total_menu").find(".inner").height(winH-hHeight);
	});
}



function scrollStart (type) {
	var winScroll 		= $(window).scrollTop();
	var docHeight 		= $(document).height();
	var winHeight 		= $(window).height();
	var pageWrap 		= $("#wrap");
	var topHeader 		= $("#header");
	var subHeader 		= $(".sub_header.fixed");
	var headerHeight 	= topHeader.height();
	var gnbMenu			= topHeader.find(".gnb .menu");
	var container 		= $("#container");
	var naviMenu		= $(".nav_menu");
	var btnGotop 		= $(".go_top"); /*개발 수정*/
	var btnGoPrev		= $(".go_prev"); /*개발 수정*/
	var btnPayment 		= $(".btn_payment");
	var btnCompare 		= $(".btn_compare_wrap");
	var layerMsg		= $(".layer_msg");
	
	var actStart 		= false;
	var btnGotopBottom;
	
	var msgH 			= layerMsg.filter(":visible").outerHeight();
	var btnCompareH		= btnCompare.outerHeight();
	var btnPaymentH		= btnPayment.outerHeight();
	var naviMenuH		= naviMenu.outerHeight();
	var whiteSpaceH		= 10;
	
	var appCheck    	= $(".mobileApp").length > 0 ? true : false;
	var	paymentCheck    = btnPayment.length > 0 ? true : false;
	var	compareCheck    = btnCompare.length > 0 ? true : false;
	var	layerMsgCheck   = layerMsg.filter(":visible").length > 0 ? true : false;
	
	//scrollUp_btnPosition();
	
	function showHeader (){
		topHeader.css({"top":0});
		//subHeader.css({"top":headerHeight});
		//container.css({"padding-top" : headerHeight});
	}
	
	function gnbIncludeHeader(){
		topHeader.css({"top": - (headerHeight - 35)});
		//container.css({"padding-top" : 35});
	}
	
	function hideHeader (){
		if( $("body").addClass("Huawei-MT") ){
			topHeader.css({"top":-headerHeight + 1 });
		} else {
			topHeader.css({"top":-headerHeight });
		}
		//subHeader.css({"top":0});	
		container.css({"padding-top" : 0});
	}
	
	function scrollUp_btnPosition(){
		// app일 경우 /*개발 수정*/
		/*if( isAppConnect ){
			if( layerMsgCheck ){ //레이어알림창 있을 경우
				if( paymentCheck ){
					btnGotopBottom = msgH +  btnPaymentH + whiteSpaceH;
					btnPayment.css( "top" , 0 );
				} 
				else if ( compareCheck ) {
					btnGotopBottom = msgH +  btnCompareH + whiteSpaceH;
					btnCompare.css( "top" , 0 );
				} else {
					btnGotopBottom = msgH +  whiteSpaceH;
				}
			} else { //레이어알림창 없을 경우
				if( paymentCheck ){
					btnGotopBottom =  btnPaymentH + whiteSpaceH;
					btnPayment.css( "top" , 0 );
				} 
				else if ( compareCheck ) {
					btnGotopBottom =  btnCompareH + whiteSpaceH;
					btnCompare.css( "top" , 0 );
				} else {
					btnGotopBottom =  whiteSpaceH;
				}
			}
		} 
		// web일 경우
		else {
			if( layerMsgCheck ){ //레이어알림창 있을 경우
				if( paymentCheck ){
					btnGotopBottom = msgH + naviMenuH + btnPaymentH + whiteSpaceH;
				} 
				else if ( compareCheck ) {
					btnGotopBottom = msgH + naviMenuH + btnCompareH + whiteSpaceH;
				} else {
					btnGotopBottom = msgH + naviMenuH + whiteSpaceH;
				}
			} else { //레이어알림창 없을 경우
				if( paymentCheck ){
					btnGotopBottom = naviMenuH + btnPaymentH + whiteSpaceH;
				} 
				else if ( compareCheck ) {
					btnGotopBottom = naviMenuH + btnCompareH + whiteSpaceH;
				} else {
					btnGotopBottom = naviMenuH + whiteSpaceH;
				}
			}
		}*/
		btnGotop.css("bottom", btnGotopBottom );
		btnGoPrev.css("bottom", btnGotopBottom ); /*개발 수정*/
	} 
	
	function scrollDown_btnPosition(){
		// app일 경우 /*개발 수정*/
		/*if( isAppConnect ){
			if( layerMsgCheck ){ //레이어알림창 있을 경우
				if( paymentCheck ){
					btnGotopBottom = msgH +  whiteSpaceH;
					btnPayment.css( "top" , btnPaymentH );
				} 
				else if ( compareCheck ) {
					btnGotopBottom = msgH +  whiteSpaceH;
					btnCompare.css( "top" , btnCompareH );
				} else {
					btnGotopBottom = msgH + whiteSpaceH;
				}
			} else { //레이어알림창 없을 경우
				if( paymentCheck ){
					btnGotopBottom =  whiteSpaceH;
					btnPayment.css( "top" , btnPaymentH );
				} 
				else if ( compareCheck ) {
					btnGotopBottom =  whiteSpaceH;
					btnCompare.css( "top" , btnCompareH );
				} else {
					btnGotopBottom = whiteSpaceH;
				}
			}
		}
		// web일 경우
		else {
			if( layerMsgCheck ){ //레이어알림창 있을 경우
				if( paymentCheck ){
					btnGotopBottom = msgH + naviMenuH + btnPaymentH + whiteSpaceH;
				} 
				else if ( compareCheck ) {
					btnGotopBottom = msgH + naviMenuH + btnCompareH + whiteSpaceH;
				} else {
					btnGotopBottom = msgH + naviMenuH + whiteSpaceH;
				}
			} else { //레이어알림창 없을 경우
				if( paymentCheck ){
					btnGotopBottom = naviMenuH + btnPaymentH + whiteSpaceH;
				} 
				else if ( compareCheck ) {
					btnGotopBottom = naviMenuH + btnCompareH + whiteSpaceH;
				} else {
					btnGotopBottom = naviMenuH + whiteSpaceH;
				}
			}
		}*/
		btnGotop.css("bottom", btnGotopBottom );
		btnGoPrev.css("bottom", btnGotopBottom ); /*개발 수정*/
	} 
	
	function scrollActUp (){
		pageWrap.removeClass("scrollDown");
		pageWrap.addClass("scrollUp");
		scrollUp_btnPosition();
		lastScroll = winScroll;
	}
	
	function scrollActDown (){
		pageWrap.removeClass("scrollUp");
		pageWrap.addClass("scrollDown");
		scrollDown_btnPosition();
		lastScroll = winScroll;
	}
	
	if(!type){
		if (!actStart){
			actStart = true;
			if (winScroll < 1 || type == "up" ){
				pageWrap.addClass("scrollTop").removeClass("scrollBottom");
				if (topHeader.hasClass("main")){
					showHeader();
				} else if( topHeader.hasClass("gnb_include") ){
					gnbIncludeHeader();
				}
				scrollActUp();
				//btnGotop.css({"opacity":0}); /*개발 수정*/
				btnGotop.fadeOut(); /*개발 수정*/
				btnGoPrev.fadeOut(); /*개발 수정*/
				/*if(UserAgent.data.iOs){  /*개발 수정*//*
					btnGoPrev.fadeOut();
				}*/
				if(! topHeader.hasClass("main") &&  headerMainCheck == true ){
					headerMainCheck = false;
				} else {
					$(".inner.fixed").css({ "top" : headerHeight });
					showHeader();
				}
			} else {
				pageWrap.removeClass("scrollTop");
				if (winScroll < lastScroll) {
					scrollActUp();
					pageWrap.removeClass("scrollBottom");
				} else if (winScroll == docHeight - winHeight) {
					scrollActUp();
					pageWrap.addClass("scrollBottom");
				} else {
					scrollActDown();
					pageWrap.removeClass("scrollBottom");
				}
				hideHeader();
				//btnGotop.css({"opacity":1}); /*개발 수정*/
				btnGotop.fadeIn(); /*개발 수정*/
				btnGoPrev.fadeIn(); /*개발 수정*/
				/*if(UserAgent.data.iOs){ /*개발 수정*//*
					btnGoPrev.fadeIn();	
				}*/
				$(".inner.fixed").css({ "top" : 0 });
				actStart = false;
			}
		}
	} else if(type == "productDetail"){
		showHeader();
	}
}

function toggleClass (target, className) {
	if (target.hasClass(className)) {
		target.removeClass(className);
	} else {
		target.addClass(className);
	}
}



function winScrollDisable(winScroll, nav){
	if (winScroll){
		$("#container").removeClass("on");
		$(window).scrollTop(winScrollTop);
		if (!nav){
			$("#container").removeClass("layerZindex");
		}
	} else {
		winScrollTop = $(window).scrollTop();
		$("#container").addClass("on").scrollTop(winScrollTop);
		winScrollTop = $("#container").scrollTop();
		if (!nav){
			$("#container").addClass("layerZindex");
		}
	}
}

function isNumber(s) {
	s += ''; 
	s = s.replace(/^\s*|\s*$/g, '');
	if (isNaN(s)) return false;
	return true;
}

function productAmountCheck(){
	if( $(".amount").size() > 0 ){
		$(".amount").each(function(){
			var _this = $(this);
			var countInput 		= _this.find("input");
			var beginAmount		= Number(countInput.val());
			var minAmount 		= Number(countInput.data("minAmount")) || 1;
			var maxAmount 		= Number(countInput.data("maxAmount")) || 999999;
			var packageAmount 	= Number(countInput.data("packageAmount")) || 1;
			
			if( _this.find(".minus").size() > 0 ){
				countInput.val( beginAmount );
				countInput.data("prevCount" , countInput.val());
			}
			
			if( _this.find(".btn_modify").size() > 0 ){
				countInput.data("currentCount" , countInput.val());
			}
			
			if(! _this.find("input").attr("data-min-amount") && _this.find(".minus").size() > 0 ){
				_this.data("minAmount" , _this.val());
			}
			
		});
	}
}

// number type max length limit
function inputNumberMaxLengthCheck(object){
	if (object.value.length > object.maxLength){
		object.value = object.value.slice(0, object.maxLength);
	}    
}

// via info
function cartPaymentInfoViaInit(){
	var viaArea = $(".payment_info").find(".space");
	var val = $(".payment_info").find("#airport").val();
	
	if( val == "P" || val == "U" || val == "I" ){
		viaArea.find(".radio_list li:eq(0) input[type='radio']").prop({"checked" : true, "disabled" : true });
		viaArea.find(".radio_list li:eq(1) input[type='radio']").prop("disabled" , true);
		viaArea.find("select").hide();
	} 
	else if ( val == "" ){
		viaArea.find(".radio_list li:eq(0) input[type='radio']").prop({"checked" : false, "disabled" : true });
		viaArea.find(".radio_list li:eq(1) input[type='radio']").prop({"checked" : false, "disabled" : true });
		viaArea.find("select option:eq(0)").prop("selected" , true);
		viaArea.find("select").hide();
	}
	else {
		viaArea.find(".radio_list li:eq(0) input[type='radio']").prop("disabled" , false);
		viaArea.find(".radio_list li:eq(1) input[type='radio']").prop("disabled" , false);
	}
	
	var idx = viaArea.find(".radio_list input[type='radio']:checked").parents("li").index();
	
	if( idx == 0 ){
		viaArea.find("select").hide().find("option:eq(0)").prop("selected" , true);
	} 
	else if( idx == 1 ) {
		viaArea.find("select").show();
	}
}

$(window).scroll(function() {
	scrollStart(windowScrollFuncPrevent);
});

$(document).on("touchmove",function(){
	if($(document).scrollTop() == 0){
		scrollStart("productDetail");
	}
});




/* common */
$(function(){
	
	// datepicker
	if( $(".datepicker").size() > 0 ){
		$(".datepicker").each(function(){
			var thisData = $(this).data();
			var _default = {
				minDateValue : "-0d",
				maxDateValue : null,
			}
			var _option = $.extend(_default, thisData);
			
			if( $(this).hasClass("no-limit") ){
				_option.minDateValue = null;
				_option.maxDateValue = null;
			}
			
			$(this).datepicker({
				//changeMonth:true,
				//changeYear:true,
				showOn:'button',
				buttonImage:'../img/common/ico_calendar.png',
				buttonImageOnly:true,
				langtit:"",
				showMonthAfterYear:true,
				minDate: _option.minDateValue,
				maxDate: _option.maxDateValue,
				dayNamesMin:['Su','Mo','Tu','We','Th','Fr','Sa'],
				monthNames: [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" ],
				monthNamesShort: [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" ],
				dateFormat:'yy-mm-dd',
				onClose: function(){ 
					$(".dimmed").css({ "z-index" : "" });
				}
			});	
			
			$(this).next(".ui-datepicker-trigger").on({
				"click" : function(e){
					$(".dimmed").addClass("on").css({ "z-index" : 112 });
					e.preventDefault();
				}
			});
		});
	}
	
	/* layout action */
	layout();
	scrollStart();
	
	// slick 
	slideBanner('.slideBanner');
	slideManual('.slideManual');
	slideAuto('.slideAuto');
	slideAuto('.bn_oneday', {autoplaySpeed : 5000});/* 2016-11-01 추가 */
	sliderFor('.sliderFor');
	sliderNav('.sliderNav');
	sliderNavSingle('.sliderNavSingle');
	cateSliderNavSingle('.cateSliderNav');/* 2016-10-20 추가 */

	/*20181004상품배너 변경 start*/
	slideAuto('.product_bn', {autoplaySpeed : 3000});
	/*20181004상품배너 변경 end*/
	
	// header menu
	var headerArea = $("#header");
	var btnNav = headerArea.find(".btn_total");
	var navON = false;
	var $searchList = $(".top_search_result");

	//$(".total_menu").hide();
	
	/*
	btnNav.on("click", function(){
		if (!navON){
			navON = true;
			if (headerArea.hasClass("open")){alert("open");
				winScrollDisable(true);
				headerArea.removeClass("open");
				$(".total_menu").css({"-webkit-transform":"translateY(-100%)","left":"-100px"});
				setTimeout(function(){
					$(".total_menu").hide();
					navON = false;
				}, 300);
				$(mask).removeClass("navOn on");
				bodyScrollStart();
			} else {alert("close");
				headerArea.addClass("open");
				$(".total_menu").show(5, function(){
					$(".total_menu").css({"-webkit-transform":"translateY(0)","left":"0px"});
				});
				setTimeout(function(){
					navON = false;
				}, 300);
				$(mask).addClass("navOn on");
				$searchList.hide();
				winScrollDisable(false, true);
				bodyScrollStop();
			}
		}
		return false;
	});
*/
	//footer menu
	footerMenu.init();
	
	// footer msg layer 
	var $msg = $(".layer_msg");
	var $msgClose = $(".layer_msg .close");
	
	/* window load time */
	$(window).load(function(){
		// main layer
		$(".initial_layer").each(function(){
			popOpen($("#"+ $(this).attr("id")));
		});
	
		setTimeout(function(){
			$msg.removeClass("visible");
			$msg.slideUp(function(){
				scrollStart();
			});
		},10000);
	});
	
	$msgClose.on("click",function(e){
		$msg.removeClass("visible");
		$msg.slideUp(function(){
			scrollStart();
		});
		e.preventDefault();
	});
	
	/* gnb navigation slide */
	/* gnb navigation slide */
	var $gnbwrap = $(".gnb");
	var $gnb = $gnbwrap.find(".inner");
	var $item = $gnb.find(".slide");
	var $menu = $gnb.find(".menu");
	var itemWidth;
	var target;
	var lastTarget = 0;
	var speed = 300;
	var initData = [];
	var $itemOn = $item.filter(".on");
	var $itemOnIdx = $itemOn.index();
	
	var menuTotalWidth;
	
	gnbInit();
	
	$item.each(function(){
		initData.push($(this).offset().left);
	});
	
	if( initData[$itemOnIdx] > initData[1] ){
		$gnb.scrollLeft(initData[$itemOnIdx-1]);
	}
	
	$(window).resize(function(){
		gnbInit()
	});
	
	$gnb.on("touchstart", function() {
		lastTarget = $gnb.scrollLeft();
	});
	
	$gnb.on("touchend", function() {
		/* var near = [];
		var abs = 0;
		var min = menuTotalWidth/$item.length;
		var target = $gnb.scrollLeft();
		var data = [];
		
		$item.each(function(idx){
			data.push($(this).offset().left);
			if (data[idx] < 0 && data[idx]-min < 0){
				near.push(data[idx]);
			}
		});
		
		if(lastTarget > target){
			$gnb.animate({scrollLeft : (near.length*min)-min},speed);
		} else {
			$gnb.animate({scrollLeft : near.length*min},speed);
		} */
	});
	
	function gnbInit(){
		menuTotalWidth = 10;
		$item.each(function(){
			menuTotalWidth += $(this).outerWidth(true); 
		});
		$menu.width(menuTotalWidth);
	}

	// layer popup
	var btnLayer = $('.open-popup');
	btnLayer.on('click', function(e){
		popClick($(this));
		e.preventDefault();
	});
	
	var layerPop = $('.popup_layer_wrap');
	var layerClose = layerPop.find('.close, .btn_close'); //2016-11-11 수정
	
	layerClose.on('click', function(e){
		popClose($(this).parents(".popup_layer_wrap"));
		e.preventDefault();
	});
	
	// layer popup
	var btnLayer2 = $('.open-popup2');
	btnLayer2.on('click', function(e){
		popClick2($(this));
		e.preventDefault();
	});
	
	var layerPop2 = $('.popup_layer2');
	var layerClose2 = layerPop2.find('.btn_close , .btn_cancel');

	/*2019적립금수정s*/		
	$(".cart_new_wrap").on("click", ".btn_close", function(e){
		popClose(layerPop2);
		e.preventDefault();
	});

	$(document).on("click", ".bg_dimmed", function(e){
		popClose(layerPop2);
		e.preventDefault();
	});
	/*2019적립금수정e*/
	
	layerClose2.on('click', function(e){
		popClose2(layerPop2);
		e.preventDefault();
	});

	/*2019적립금수정s*/		
	$(".cart_new_wrap").on("click", ".btn_close", function(e){
		popClose(layerPop2);
		e.preventDefault();
	});

	$(document).on("click", ".bg_dimmed", function(e){
		popClose(layerPop2);
		e.preventDefault();
	});
	/*2019적립금수정e*/

	/*20181016레이어팝업 s*/
	if($("#main_promotion_banner").length){
		(function(){
			var layerPop = $('.popup_layer_new');
			var layerClose = layerPop.find('.btn_close');
			layerClose.on('click', function(e){
				$(mask).fadeOut();
				popClose(layerPop);
				e.preventDefault();
			});
			$(mask).on('click', function(e){
				$(this).fadeOut();
				popClose(layerPop);
				e.preventDefault();
			});
		})();
	}
	/*20181016레이어팝업 e*/

	/* 20180306공유하기 레이어팝업 s */
	if($("#layer-share-info").length){
		(function(){
			var doubleSubmitFlag = false;
			var doubleSubmitTime;
			$(".open-share").on('click', function(e){
				if(doubleSubmitCheck()) return;
				clearTimeout(doubleSubmitTime);
				popClick2($(this));
				e.preventDefault();
				doubleSubmitTime=setTimeout(function(){
					doubleSubmitFlag = false;
				},500);		
			});
			$(mask).on('click', function(e){
				popClose2(layerPop2);
				e.preventDefault();
			});
			function doubleSubmitCheck(){
				if(doubleSubmitFlag){
					return doubleSubmitFlag;
				}else{
					doubleSubmitFlag = true;
					return false;
				}
			}
		})();	
	}
	/* 20180306공유하기 레이어팝업 e */

	// accordion
	var accoList = $(".accordion_list");
	var btnAccoOpen = accoList.find(".tit a");
	
	if( accoList.parents(".cart_paymentinfo").size() > 0 ){
		var acc_paymentinfo = true;
	}

	btnAccoOpen.on("click",function(e){
		var $target = $(this).parent(".tit").parent("li");
		if ($target.hasClass("on")) {
			$target.removeClass("on");
			$target.find('.slideBanner').slick("unslick");
		} else {
			if(! acc_paymentinfo ){
				accoList.find("li").removeClass("on");
			}
			accoList.find("li").find('.slideBanner').slick("unslick");
			$(this).parent(".tit").parent("li").addClass("on");
			slideBanner($target.find('.slideBanner'));
		}
		e.preventDefault();
	});

	/* each */
	//facet ( detail search )
	var detailSearch 	= $(".search_wrap");
	var btnDetailSearch = detailSearch.find(".search_head .btn a, .search_head .btn_toggle");
	var priceMinData = detailSearch.find("input[type='number']").eq(0).val();
	var priceMaxData = detailSearch.find("input[type='number']").eq(1).val();
	var btnFacetMore = detailSearch.find(".condition .btn_toggle");
	var categorySelect = detailSearch.find("#c_brand");
	
	if(! detailSearch.hasClass("category_page") ){
		btnFacetMore.hide();
	}
	
	
	btnDetailSearch.on("click", function(e){
		if ($(this).hasClass("on")) {
			$(this).removeClass("on");
			$(this).parents(".search_wrap").removeClass("on");
			//$(mask).removeClass("on").css({ "z-index" : "" });
		} else {
			$(this).addClass("on");
			$(this).parents(".search_wrap").addClass("on");
			//$(mask).addClass("on").css({ "z-index" : 10 });
		}
		e.preventDefault();
	});

	detailSearch.find("ul.btn_area > li:eq(0) a").on({
		"click" : function(e){
			detailSearch.find(".condition select").val("");
			detailSearch.find(".condition input[type='checkbox']").prop("checked", false);
			detailSearch.find(".condition input[type='number']").eq(0).val(priceMinData);
			detailSearch.find(".condition input[type='number']").eq(1).val(priceMaxData);
			e.preventDefault();
		}
	});
	
	btnFacetMore.on({
		"click" : function(e){
			var _this = $(this);
			if( _this.hasClass("on") ){
				_this.removeClass("on").find("span").text("更多条件");
				detailSearch.find(".condition li.off").hide();
			} else {
				_this.addClass("on").find("span").text("关闭选择项目");
				detailSearch.find(".condition li.off").show();
			}
			e.preventDefault();
		}
	});
	
	categorySelect.on({
		"change" : function(){
			if(! $(".search_wrap").hasClass("category_page") ){
				//facetOptionCheck(); //개발 반영시 제거
			}
		}
	});

	// best seller search word
	var bestSearchWrap = $(".brand_best_wrap");
	var bestSearchWords = bestSearchWrap.find(".brand_best");
	var bestSearchMore = bestSearchWrap.find(".btn_more");
	bestSearchMore.on("click", function(){
		toggleClass(bestSearchWords, "on");
		toggleClass(bestSearchMore, "on");
		
		return false;
	});

	$("[data-event='accordion']").each(function(){
		$(this).accordion($(this).data());
	});
	
	$("[data-event='tab']").each(function(){
		$(this).tabpannel($(this).data());
	});
	
	/* $("[data-layer]").on("click",function(){
		layerpopup($(this));
	})*/
	
	//all checkbox
	$(".all_check").each(function(){
		var _this = $(this);
		var _default = {
			checkedTarget 	: "#container",
			relationship	: ""
		}
		var _options 		= $.extend(_default, _this.data())
		var allCheckbox 	= $(".all_check").find("input[type='checkbox']");
		var eachCheckbox 	= $(_options.checkedTarget).find("input[type='checkbox']").not(allCheckbox);
		var checked; 
		
		allCheckbox.on({
			"change" : function(){
				if( $(this).prop("checked") == true){
					checked = true;
					allCheckbox.prop("checked", true);
					$(".disable").eq(0).hide();
				}
				else {
					checked = false;
					allCheckbox.prop("checked", false);
					//addInfoReset();
					$(".disable").eq(0).show();
				}
				eachCheckbox.prop("checked" , checked);
				allCheckLinkage($(this));
			}
		});
		
		eachCheckbox.on({
			"change" : function(){
				if( eachCheckbox.filter(":checked").size()  == eachCheckbox.size() ){
					allCheckbox.prop("checked" , true);
					$(".disable").eq(0).hide();
				} else {
					allCheckbox.prop("checked" , false);
					addInfoReset();
					$(".disable").eq(0).show();
				}
			}
		});
	});
	/* 2016.03.21 추가 (동의시 작성가능)*/
	
	var interestedBrandId 	= [];
	var interestedBrandOptionSize;
	
	var _accout_reset	= $(".accout_reset");	
	_accout_reset.find("input[type='checkbox']").on({
		"change" : function(){
			if( $(this).prop("checked") == true ){
				$(".disable").eq(0).hide();
			} else {
				//addInfoReset();
				$(".disable").eq(0).show();
			}
		}
	});

	function addInfoReset(){
		var target = $(".add_information");
		target.find("input[type='checkbox']").prop({ "checked" : false, "diabled" : false, "readonly" : false }).next().css("background-position", "0 0");
		target.find("input[type='radio']").prop({ "checked" : false, "diabled" : false, "readonly" : false }).next().css("background-position", "0 -100px");
		target.find("input[type='text']").val("");
		target.find("select").find("option:first-child").prop("selected" , true);
		
		target.find("[data-append-target='interested-brand']").empty();
		
		interestedBrandOptionSize = 0;
		$("*[data-select]").each(function(i){
			interestedBrandId[i] = [];
		});
		
	}

	// top button
	$(".go_top").on({
		"click" : function(e){
			$(window).scrollTop(0)
			e.preventDefault();
		}
	});
	
	// phone select
	$(".phone_box").each(function(){
		var _this 	= $(this);
		var  select = _this.find(" > select");
		
		select.on({
			"change" : function(){
				var dataId = $(this).find("option:selected").data("id");
				_this.find("*[data-target]").hide();
				_this.find("*[data-target='"+ dataId +"']").show();
			}
		});
	});


// country phone select
	$(".tel_box").each(function(){
		var _this 	= $(this);
		var  select = _this.find("select");
		
		select.on({
			"change" : function(){
				var dataId = $(this).find("option:selected").data("id");
				_this.find("*[data-target]").hide();
				_this.find("*[data-target='"+ dataId +"']").show();
			}
		});
	});
	/* end : member */
	
	
	// 수신, 비수신
	$("[data-radio-receive]").each(function(){
		var _this = $(this);
		var target = _this.data("radioReceive");
		_this.on({
			"change" : function(){
				$("[data-receive-target='"+ target +"']").find("input[type='checkbox']").prop({ "disabled" : false });
			}
		});
	});
	
	$("[data-radio-noreceive]").each(function(){
		var _this = $(this);
		var target = _this.data("radioNoreceive");
		_this.on({
			"change" : function(){
				$("[data-receive-target='"+ target +"']").find("input[type='checkbox']").prop({ "disabled" : true, "checked" : false });
			}
		});
	});



	
	// data max setting 
	$("input[type='date']").each(function(){
		if(! $(this).attr("max") ){
			var todayDate = new Date();
			var maxYear	  = 10;	
			$(this).attr("max" , (todayDate.getFullYear()+maxYear)+"-"+(todayDate.getMonth()+1)+"-"+todayDate.getDate());
		}
	});
	
	// min/max price
	var facetMinPrice 			= $("#c_price_min");
	var facetMaxPrice 			= $("#c_price_max");
	var currentFacetMinPrice 	= facetMinPrice.val();
	var currentFacetMaxPrice 	= facetMaxPrice.val();
	
	facetMinPrice.on({
		"change" : function(){
			var _this = $(this);
			var minVal = Number(_this.val());
			var maxVal = Number(facetMaxPrice.val());
			var maxLimit 	= Number(_this.attr("max"));
			var minLimit 	= Number(_this.attr("min"));
			
			if( minVal > maxVal ){
				_this.val(currentFacetMinPrice);
			}
			else if( minVal > maxLimit ){
				_this.val(currentFacetMinPrice);
			}
			else if( minVal < minLimit ){
				_this.val(currentFacetMinPrice);
			}
			else {
				currentFacetMinPrice = minVal;
			}
		}
	});
	
	facetMaxPrice.on({
		"change" : function(){
			var _this 		= $(this);
			var maxVal 		= Number(_this.val());
			var minVal 		= Number(facetMinPrice.val());
			var maxLimit 	= Number(_this.attr("max"));
			var minLimit 	= Number(_this.attr("min"));
			
			if( maxVal < minVal ){
				_this.val(currentFacetMaxPrice);
			} 
			else if( maxVal > maxLimit ){
				_this.val(currentFacetMaxPrice);
			}
			else if( maxVal < minLimit ){
				_this.val(currentFacetMaxPrice);
			}
			else {
				currentFacetMaxPrice = maxVal;
			}
		}
	});
	
	// textArea maxLength 
	$(document).on("keyup",".textcount",function(e){
		e = e || window.event;
		if( $(this).data("maxLength") != "unlimited" ){
			var othis = $(this);
			var count = othis.parents("tr").find(".txt_byte span");
			var ls_str = othis.val();
			var li_str_len = ls_str.length; //전체길이
			var i = 0;
			var li_byte = 0; 
			var ls_one_char = "";
			
			var defaultByte = 1300;
			
			if( $(this).data("maxLength") ){
				defaultByte = Number($(this).data("maxLength"));
			}
			
			for(i=0; i< li_str_len; i++){
				ls_one_char = ls_str.charAt(i);   //한글자 추출
				if(escape(ls_one_char).length > 4){ 
				  li_byte ++;   //한글 + 1
				}else{
				  li_byte ++;   //영어, 숫자, 한자 + 1
				}					
			}	
			
			count.text(li_byte);
			textarea_maxlength( othis, count, defaultByte );
		}	
	});

	function textarea_maxlength(obj, limitGuideTxt, maxLength){
		if( obj.val().length > maxLength ){
			alert("已经超过"+maxLength+"字，无法再输入。");
			var thisTxt = obj.val();
			thisTxt = thisTxt.substring(0, maxLength)
			obj.val(thisTxt);
			limitGuideTxt.text(maxLength);
		}
	}
	
});

/* end : common */


/* product detail */
$(function(){
	/* full swipe */
	var full = '.js-product-detail';
	var fullNav = '.js-product-nav';
	var current = 0;
	
	
	if($(full).length){
		// reset 
		$(full).find(".btn_prev, .btn_next, .js-color-prev, .js-color-prev").on("click",function(e){
			e.preventDefault();
		})
		
		$(full).on({
			"click" : function(){
				setTimeout(function(){
					slickH = $(full).find("> .slick-list > .slick-track > .slick-slide.slick-active").outerHeight();
					$(full).find("> .slick-list ").outerHeight(slickH);
				}, 500);
			}
		});
		
		$(full).slick({
			arrows: false,
			infinite : true,
			speed : 500,
			touchMove: false,
			adaptiveHeight: true,
			onInit : function(){
				
			}
			
		}).on("swipe", function(event, slick){
			// full swipe
			if($(slick.$slider[0]).hasClass('js-product-detail')) {
				current = $(full).slick('slickCurrentSlide');
				swipeAction(current);
				$(fullNav).find(">div").removeClass("on");
				$(fullNav).find(">div").eq(current).addClass("on");
			} 
		})
		
		// full swipe navigation
		$(fullNav).find(">div>a, >div").on("click",function(e){
			current = $(this).closest("div").index();
			
			swipeAction(current);
			$(full).slick('slickGoTo',current);
			
			e.preventDefault();
		})
	}
		
	// nav active && scroll reset
	function swipeAction(index){
		$(window).scrollTop(0);
		$(fullNav).find(">div").removeClass("on");
		$(fullNav).find(">div").eq(index).addClass("on");
	}
	
	/* product check */
	var $productCheck = $(".js-product-check");
	var checkElem = '.js-check';
	var checkVal = '.js-checkval';
	
	$productCheck.find(checkElem).on("click change", function(e){
		var $activeTarget = $(this).find(">div");
		var $checkInput =  $(this).find(checkVal);
		if($activeTarget.hasClass("on")){
			$activeTarget.removeClass("on");
			$checkInput.prop("checked",false).attr("checked",false);
		} else {
			$activeTarget.addClass("on");
			$checkInput.prop("checked",true).attr("checked",true);
		}
		
		e.preventDefault();
	});
	
	/* product recommend */
	var $moreParent = $(".js-more-parent");
	var $moreView = $(".js-more-view");
	var $moreBtn = $(".js-more-btn");
	
	$moreBtn.on("click",function(e){
		if($moreParent.hasClass("on")){
			$moreParent.removeClass("on");
			//$moreView.removeAttr("style");
		} else {
			$moreParent.addClass("on");
			//$moreView.height("auto");
		}
		
		e.preventDefault();
	})


	productAmountCheck();
	
	$(document).on("click", ".amount .minus", function(e){
		var $amountInput 	= $(this).siblings("input");
		var minAmount 		= Number($amountInput.data("minAmount")) || 1;
		var maxAmount 		= Number($amountInput.data("maxAmount")) || 999999;
		var packageAmount 	= Number($amountInput.data("packageAmount")) || 1;
		var checkedAmount 	= Number($amountInput.val());
		var currentAmount	= Number($amountInput.val());
		
		// min,max 유효성 체크
		if ( minAmount > packageAmount ){
			if( (minAmount % packageAmount) > 0 ){
				minAmount = ( minAmount + (packageAmount - (minAmount % packageAmount)));
			} else {
				minAmount = minAmount;
			}
		} else {
			minAmount = packageAmount;
		}
		
		if( maxAmount > packageAmount ){
			if ( (maxAmount % packageAmount) > 0 ){
				maxAmount = ( maxAmount - (maxAmount % packageAmount ));
			} else {
				maxAmount = maxAmount; 
			}
		} else {
			maxAmount = packageAmount;
		}
		
		if( checkedAmount % packageAmount > 0 ){
			checkedAmount = checkedAmount - (checkedAmount % packageAmount);
		}
		
		if( currentAmount <= maxAmount ){
			for( var i=0; i < packageAmount; i++ ){
				checkedAmount --;
			}
		}
		
		if( checkedAmount < minAmount ){
			alert("该商品最少"+minAmount+"个为止可订购。");
			checkedAmount = minAmount;
		} else if( checkedAmount > maxAmount ){
			checkedAmount = maxAmount;
			//alert("최대 수량은 " + maxAmount + "개입니다.");
		} 
		
		$amountInput.val(checkedAmount);
		$amountInput.data("prevCount" , checkedAmount);
		
		var _countConfirmBtn = $(this).parents(".amount").find(".btn_modify");
		var currentCount 	= Number($amountInput.data("currentCount"));
		
		if( _countConfirmBtn.length){
			if( Number($amountInput.val()) != currentCount  ){
				_countConfirmBtn.filter(".on").show();
				_countConfirmBtn.filter(".off").hide();
			} else {
				_countConfirmBtn.filter(".off").show();
				_countConfirmBtn.filter(".on").hide();
			}	
		}
		
		e.preventDefault();
	});
	
	$(document).on("click", ".amount .plus", function(e){
		var $amountInput 	= $(this).siblings("input");
		var minAmount 		= Number($amountInput.data("minAmount")) || 1;
		var maxAmount 		= Number($amountInput.data("maxAmount")) || 999999;
		var packageAmount 	= Number($amountInput.data("packageAmount")) || 1;
		var checkedAmount 	= Number($amountInput.val());
		
		// min,max 유효성 체크
		if ( minAmount > packageAmount ){
			if( (minAmount % packageAmount) > 0 ){
				minAmount = ( minAmount + (packageAmount - (minAmount % packageAmount)));
			} else {
				minAmount = minAmount;
			}
		} else {
			minAmount = packageAmount;
		}
		
		if( maxAmount > packageAmount ){
			if ( (maxAmount % packageAmount) > 0 ){
				maxAmount = ( maxAmount - (maxAmount % packageAmount ));
			} else {
				maxAmount = maxAmount /* - packageAmount;  */
			}
		} else {
			maxAmount = maxAmount;
		}
		
		if( checkedAmount % packageAmount > 0 ){
			checkedAmount = checkedAmount - (checkedAmount % packageAmount);
		}
		
		if( checkedAmount < minAmount ){
			checkedAmount = minAmount;
		} else if( checkedAmount >= maxAmount ){
			checkedAmount = maxAmount;
			alert("该商品最多"+maxAmount+"个为止可订购。");
		} else {
			for( var i=0; i < packageAmount; i++ ){
				checkedAmount ++;
			}
		} 
		
		$amountInput.val(checkedAmount);
		$amountInput.data("prevCount" , checkedAmount);
		
		var _countConfirmBtn = $(this).parents(".amount").find(".btn_modify");
		var currentCount 	= Number($amountInput.data("currentCount"));
		
		if( _countConfirmBtn.length){
			if( Number($amountInput.val()) != currentCount  ){
				_countConfirmBtn.filter(".on").show();
				_countConfirmBtn.filter(".off").hide();
			} else {
				_countConfirmBtn.filter(".off").show();
				_countConfirmBtn.filter(".on").hide();
			}	
		}
		e.preventDefault();
	});
	
	$(document).on("change keyup", ".amount input", function(e){
		
		if( $(this).parents(".amount").find(".minus").size() > 0 ){
			var $amountInput 	= $(this);
			var minAmount 		= Number($amountInput.data("minAmount")) || 1;
			var maxAmount 		= Number($amountInput.data("maxAmount")) || 999999;
			var packageAmount 	= Number($amountInput.data("packageAmount")) || 1;
			var checkedAmount 	= Number($amountInput.val());
			var _countConfirmBtn = $amountInput.parents(".amount").find(".btn_modify");
			var currentCount 	= Number($amountInput.data("currentCount"));

			// min,max 유효성 체크
			if ( minAmount > packageAmount ){
				if( (minAmount % packageAmount) > 0 ){
					minAmount = ( minAmount + (packageAmount - (minAmount % packageAmount)));
				} else {
					minAmount = minAmount;
				}
			} else {
				minAmount = packageAmount;
			}
			
			if( maxAmount > packageAmount ){
				if ( (maxAmount % packageAmount) > 0 ){
					maxAmount = ( maxAmount - (maxAmount % packageAmount ));
				} else {
					maxAmount = maxAmount; 
				}
			} else {
				maxAmount = packageAmount;
			}
			
			if( e.type == "change" ){
				
				if( checkedAmount < minAmount ){
					alert("该商品最少"+minAmount+"个为止可订购。");
					$amountInput.val(minAmount);
					$amountInput.data("prevCount", minAmount);
				}
				else  if( checkedAmount > maxAmount ){
					alert("该商品最多"+maxAmount+"个为止可订购。");
					$amountInput.val(maxAmount);
					$amountInput.data("prevCount", maxAmount);
				}
				else if( (checkedAmount % packageAmount) > 0 ){
					alert("您选择的此项商品只能以绑定数量为单位做数量变更。");
					$amountInput.val($amountInput.data("prevCount"));
				} else {
					$amountInput.data("prevCount", checkedAmount);
				}
			}
			
			if( e.type == "keyup" ){
				if( isNumber($(this).val()) == false ){
					$amountInput.val($amountInput.data("prevCount"));
				}	
			}
			
			if( _countConfirmBtn.length){
				if( Number($amountInput.val()) != currentCount  ){
					_countConfirmBtn.filter(".on").show();
					_countConfirmBtn.filter(".off").hide();
				} else {
					_countConfirmBtn.filter(".off").show();
					_countConfirmBtn.filter(".on").hide();
				}	
			}
		}	
	});
	
	/* privacy scroll */
	var $area = $(".privacy_policy > div");
	var $link = $area.find(".link_list li");
	
	$link.find("a").on("click",function(e){
		var idx = $(this).parent("li").index();
		var $offset = $area.eq(idx + 1).offset().top - $(".sub_header").height()
		
		$("html body").scrollTop($offset);
		e.preventDefault();
	});
	
	/* star check */
	var _sParent = $(".js-star-select");
	var $star = $(".js-star-select a");
	var _sView = $(".star span");
	
	$star.on("click",function(e){
		var idx = $(this).index();
		
		$(this).closest(_sParent).find(_sView).removeAttr("class").addClass("point" + (idx+1));
		$(this).closest(_sParent).find("input").removeProp("checked").removeAttr("checked");
		$(this).closest(_sParent).find("input").eq(idx).prop("checked",true).attr("checked",true);
		e.preventDefault();
	});
	
	productSwipeDot();
	productSwipeArrow(productImgArr);
	productSwipeNav();
	productSwipeColor();
});


/* product swipe */
// image dots
var productImg = '.js-detail-slide';
var productSwipeDot = function(){
	if($(productImg).length){
		$(productImg).each(function(){
			if($(this).is(":visible")){
				$(this).slick({
					arrows : false,
					dots: true,
					dotsClass : "banner_nav",
					autoplay: true,
					autoplaySpeed: 2000
				});
			}
		});
	}
}

// image arrow
var productImgArr = '.js-detail-slide-arrow';
var imgArrOption = {
		arrows : true,
		slidesToShow : 2,
		slidesToScroll : 2
	}
	
var productSwipeArrow = function(className){
	if($(className).length && $(className).hasClass("slick-slider") == false){
		$(className).each(function(){
			if($(this).is(":visible")){
				$(this).slick(imgArrOption)
			}
		})
	}
}

/* list or page nav */
var productList = '.js-product-list';
var productListNav = '.js-product-list-nav';
var productSwipeNav = function(){
	var current = 0;
	if($(productList).length){
		$(productList).slick({
			arrows: false,
			infinite : true,
			speed : 500,
			adaptiveHeight: true,
			focusOnSelect: true,
			touchThreshold : 3
		}).on("swipe", function(event, slick){
			// full swipe
			if($(slick.$slider[0]).hasClass("js-product-list")) {
				current = $(productList).slick('slickCurrentSlide');
				action(current);
				$(productListNav).find(">div").removeClass("on");
				$(productListNav).find(">div").eq(current).addClass("on");
			} 

		})
		
		// navigation
		$(productListNav).find(">div>a, >div").on("click",function(e){
			current = $(this).closest("div").index();
			
			action(current);
			$(productList).slick('slickGoTo',current);
			
			e.preventDefault();
		})
		
		function action(index){
			$(productListNav).find(">div").removeClass("on");
			$(productListNav).find(">div").eq(index).addClass("on");
		}
	}
}

// cart list option
function cartListOptionCheck(){
	$(".cart_list_wrap .optionArea").each(function(){
		var _this = $(this);
		var orderSelect	 		= _this.find("select");
		var countConfirmBtn 	= _this.find(".btn_modify");
		
		orderSelect.data("selectIndex", orderSelect.find("option:selected").index());
		
		orderSelect.on({
			"change" : function(){
				if( countConfirmBtn.length){
					if( orderSelect.find("option:selected").index() != orderSelect.data("selectIndex") ) {
						countConfirmBtn.filter(".on").css({ "display" : "block" });
						countConfirmBtn.filter(".off").css({ "display" : "none" });
					} else {
						countConfirmBtn.filter(".on").css({ "display" : "none" });
						countConfirmBtn.filter(".off").css({ "display" : "block" });
					}	
				}	
			}
		});
		
	});
}

// color list
var productColor = '.js-color-slide';

// color chip
function productDetailColorChip(obj){
	var colorChip 		= obj.parents(".colorChipArea , #colorChipArea");
	var colorSelect		= colorChip.find("select");
	var colorSlide		= colorChip.find(".color_choice");
	var colorSelectIdx 	= colorSelect.find("option:selected").index();
	var countConfirmBtn = colorChip.find(".btn_modify");
	colorSelect.data("selectIndex", colorSelect.find("option:selected").index());
	
	init();
	
	function init(){
		var dataCode 	= colorSelect.val();
		var target		= colorSlide.find("a[data-code='"+dataCode+"']");
		var idx 		= Number(target.parent().data("slickIndex"));
		var currentIdx 	= Number(colorSlide.find(".slick-current").data("slickIndex"));
		
		obj.slick("slickGoTo", idx );
		colorSlide.find("a").removeClass("active");
		target.addClass("active");
	}
	
	colorSlide.find(".js-color-slide a").on({
		"click" : function(e){
			var dataCode = $(this).data("code");
			colorChip.find("option[value='"+dataCode+"']").prop("selected", true);
			colorSlide.find("a").removeClass("active");
			$(this).addClass("active");	
			
			if( countConfirmBtn.length){
				if( colorSelect.find("option:selected").index() != colorSelect.data("selectIndex") ) {
					countConfirmBtn.filter(".on").css({ "display" : "block" });
					countConfirmBtn.filter(".off").css({ "display" : "none" });
				} else {
					countConfirmBtn.filter(".on").css({ "display" : "none" });
					countConfirmBtn.filter(".off").css({ "display" : "block" });
				}	
			}	
			e.preventDefault();
		}
	});
	
	colorSelect.on({
		"change" : function(){
			init();
		}
	});
}
	
var productSwipeColor = function(){
	if($(productColor).length){
		$(productColor).each(function(){
			//var idx = $(this).find(".slide").length
			var _this			= $(this);	
			var slideToShowSize = (_this.outerWidth()/31);
			
			if(_this.is(":visible")){
				_this.slick({
					arrows : true,
					infinite:false,
					draggable : true,
					variableWidth : true,
					slidesToShow : slideToShowSize,
					prevArrow : '.js-color-prev',
					nextArrow : '.js-color-next',
					swipeToSlide : true
				});
				
				$(window).resize(function(){
					slideToShowSize = (_this.outerWidth()/31);
					_this.slick("unslick").slick({
						arrows : true,
						infinite:false,
						draggable : true,
						variableWidth : true,
						slidesToShow : slideToShowSize,
						prevArrow : '.js-color-prev',
						nextArrow : '.js-color-next',
						swipeToSlide : true
					});
					productDetailColorChip(_this);
				});
			}
			productDetailColorChip(_this);
		});
		
		$(".js-color-prev, .js-color-next").on("click",function(e){
			e.preventDefault();
		})
	}
}

/* layer popup
function layerpopup(target){
	var $target = $(target.data("layer"));
	var $mask = $(".js-mask");
	
	$target.show();
	$mask.show();
} */

$(function(){
	$(".btn_wrap .btn_basic").on("click",function(){
		$(this).addClass("dark");
		$(this).siblings().removeClass("dark");
	})
})


/* product compare */
$(function(){
	var productCompare = {
		setting : {
			compareBtn01 		: $(".btn_compare01"),
			compareBtn02 		: $(".btn_compare02"),
			compareCancel 		: $(".btn_compareCancel"),
			compareCont 		: $(".product_list"),
			compareCheck 		: $(".product_list .check"),
			layerAllCheckbox	: $("#layer-compareProducts").find(".check_all input[type='checkbox']"),
			layerCheckbox		: $("#layer-compareProducts").find(".product_off .check input[type='checkbox']"),
			compareProdLength	: $(".pop_compare .notice .btn_area_wrap > p strong")
		},

		init : function(){
			this.btnEvent();
			this.selectEvent();
			this.layerCheck();
		},

		btnEvent  : function(){
			productCompare.setting.compareBtn01.on({
				"click" : function(e){
					$(this).addClass("on");
					productCompare.setting.compareCheck.show();
					e.preventDefault();
				}
			});

			productCompare.setting.compareBtn02.on({
				"click" : function(e){
					var checkedLength =  productCompare.setting.compareCheck.find("input[type='checkbox']").filter(":checked").size();

					if( checkedLength > 1 ){
						popClick ("#compareProducts");

						$("#layer-compareProducts .products_area > .inner").slick({
							slidesToShow:2,
							slidesToScroll:2,
							infinite : false
						});
						
						$(".pop_compare h2 span").text($(this).find("strong").text());
					}
					e.preventDefault();
				}
			});

			// product compare cancel
			productCompare.setting.compareCancel.on({
				"click" : function(e){
					productCompare.setting.compareCheck.find("input[type='checkbox']").prop("checked" , false);
					productCompare.setting.compareCheck.hide();
					productCompare.setting.compareBtn01.removeClass("on");
					productCompare.setting.compareBtn02.find("strong").text(0);
					e.preventDefault();
				}
			});
		},

		selectEvent : function(){
			var prodCheckbox =  productCompare.setting.compareCheck.find("input[type='checkbox']");
			prodCheckbox.on({
				"change" : function(){
					var checkedLength =  prodCheckbox.filter(":checked").size();
					if( checkedLength > 1 && checkedLength < 11 ){
						productCompare.setting.compareBtn02.removeClass("btn_brown").addClass("btn_red").find("strong").text(checkedLength);
					}
					else if ( checkedLength == 11 ){
						$(this).prop("checked", false);	
						alert("最多可选择10项商品进行商品比较");
					}
					else {
						productCompare.setting.compareBtn02.removeClass("btn_red").addClass("btn_brown").find("strong").text(0);
					}
				}
			});
		},

		layerCheck : function(){
			var layerAllCheckbox =  productCompare.setting.layerAllCheckbox;
			var layerCheckbox =  productCompare.setting.layerCheckbox;
			layerAllCheckbox.on({
				"change" : function(){
				   if( $(this).prop("checked") == true ){
					   layerCheckbox.prop("checked" ,  true);
				   } else {
					   layerCheckbox.prop("checked" ,  false);
				   }
				   productCompare.setting.compareProdLength.text(layerCheckbox.filter(":checked").size());
				}
			});
			layerCheckbox.on({
				"change" : function(){
					if( layerCheckbox.filter(":checked").size() == layerCheckbox.size() ){
						layerAllCheckbox.prop("checked" ,  true);
					} else {
						layerAllCheckbox.prop("checked" ,  false);
					}
					productCompare.setting.compareProdLength.text(layerCheckbox.filter(":checked").size());
				}
			});
		}
	}

	productCompare.init();


	// barcode zoom
	$(".barcode_zoom").on({
		"click" : function(e){
			var imgSrc = $(this).parents(".barcode_img").find("img").attr("src");
			var barCodeLayer = $("#layer-barcode");
			barCodeLayer.show().find(".barcode_img img").attr("src" , imgSrc);
			$(".dimmed").addClass("over").css({ "z-index" : "" });
			var layerPos = function(){
				var winH 	=  $(window).height();
				var winW 	=  $(window).width();
				if( winH > winW ){
					barCodeLayer.removeClass("horizontal");
				} else {
					barCodeLayer.addClass("horizontal");
				}
			}
			layerPos();
			$(window).resize(function(){
				layerPos();
			});
			e.preventDefault();
		}
	});

	$("#layer-barcode .close").on({
		"click" : function(e){
			$(".dimmed").removeClass("over");
			e.preventDefault();
		}
	});
});


/* marketing */
$(function(){
	var newArrivals = $(".newarrivals_wrap");
	
	newArrivals.find(".tabmenu .tab1 a").on({
		"click" : function(){
			$(".btn_compare_wrap").show();	
		}
	});
	
	newArrivals.find(".tabmenu .tab2 a").on({
		"click" : function(){
			$(".btn_compare_wrap").hide();
		}
	});
	
	$(".marketing_wrap .sliderNavSingle").each(function(){
		var _this = $(this);
		
		if(_this.find(".list").size() < 4 ){
			_this.addClass("slick-none");
			_this.find(".list").on({
				"click" : function(){
					$(this).siblings().removeClass("on").end().addClass("on");
				}
			});
		}
	});
});
/* end : marketing */


/* cart */
$(function(){
	
	//출발지점 selectBox 레이어 호출
	/* $("#airportSelect").each(function(){
		var _this = $(this);
		var data = $(this).data();
		
		$(".btn_airport_select").on({
			"click" : function(e){
				if ( _this.val() == data.port ){
					popOpen($("#layer-searchship"));
				} 
				else {
					popOpen($("#layer-searchflight"));
				}
				e.preventDefault();
			}
		});
	}); */

	
	// default open layer
	if( $("#layer-password_confirm").size() > 0 ){
		popOpen($("#layer-password_confirm"))
	}

	// cart_paymentinfo
	$(document).on("click" , ".cart_paymentinfo .ic_delete" , function(e){
		$(this).parent("li").remove();
		e.preventDefault();
	});
	
	//O2O checked
	var o2oContent 			= $("#layer-o2oproduct");
	var o2oAllCheckBox 		=  o2oContent.find("#all_check");
	var o2oEachCheckBox 	=  $(".o2o_product .tit input[type='checkbox']").not(":disabled");
	var o2oEachCheckBoxSize = o2oEachCheckBox.size(); 
	
	o2oAllCheckBox.on({
		"change" : function(){
			if( $(this).prop("checked") == true ){
				o2oEachCheckBox.prop("checked" , true);
				
				o2oContent.find(".o2o_product .payment_info").each(function(){
					$(this).find(".place_select span:eq(0) input[type='radio']").prop("checked", true);
				});
				
			} else {
				o2oEachCheckBox.prop("checked" , false);
				o2oContent.find(".o2o_product .payment_info").each(function(){
					$(this).find(".place_select span:eq(1) input[type='radio']").prop("checked", true);
				});
			}
		}
	});
	
	o2oEachCheckBox.on({
		"change" : function(){
			var _this = $(this);
			
			if( o2oEachCheckBox.filter(":checked").size() == o2oEachCheckBoxSize ){
				o2oAllCheckBox.prop("checked" , true);
			} else {
				o2oAllCheckBox.prop("checked" , false);
			}
			
			if( _this.prop("checked") == true ){
				_this.parents(".o2o_product").find(".payment_info").each(function(){
					$(this).find(".place_select span:eq(0) input[type='radio']").prop("checked", true);
				});
			} else {
				_this.parents(".o2o_product").find(".payment_info").each(function(){
					$(this).find(".place_select span:eq(1) input[type='radio']").prop("checked", true);
				});
			}
		}
	});
	
	o2oContent.find(".o2o_product").each(function(){
		var _this = $(this);
		_this.find("input[type='radio']").on({
			"change" : function(){
				var listSize 	= _this.find(".payment_info").size();
				var checked1 	= 0;
				var checked2 	= 0;
				var allChecked 	= 0;
				
				_this.find(".payment_info").each(function(){
					if( $(this).find(".place_select span").eq(1).find("input[type='radio']").prop("checked") == true ){
						checked1++;
					}
					if( $(this).find(".place_select span").eq(0).find("input[type='radio']").prop("checked") == true ){
						checked2++;
					}
				});
				if( checked1 > 0 ){
					_this.find(".tit input[type='checkbox']").prop("checked" , false);
					o2oAllCheckBox.prop("checked" , false);
				}
				
				if( checked2 == listSize ){
					_this.find(".tit input[type='checkbox']").prop("checked" , true);
					
					o2oEachCheckBox.each(function(){
						if( $(this).prop("checked") == false ){
							allChecked ++;
						}
					});
					if( allChecked == 0 ){
						o2oAllCheckBox.prop("checked" , true);
					}
				}
			}
		});
	});
	
	// 경유지 선택
	var cartPaymentInfo = $(".payment_info");
	var viaArea = cartPaymentInfo.find(".space");
	
	cartPaymentInfo.find("#airport").on({
		"change" : function(){
			var val = $(this).val();
			
			if( val == "P" || val == "U" || val == "I" ){
				viaArea.find(".radio_list li:eq(0) input[type='radio']").prop({"checked" : true, "disabled" : true });
				viaArea.find(".radio_list li:eq(1) input[type='radio']").prop("disabled" , true);
				viaArea.find("select").hide();
			} 
			else if ( val == "" ){
				viaArea.find(".radio_list li:eq(0) input[type='radio']").prop({"checked" : false, "disabled" : true });
				viaArea.find(".radio_list li:eq(1) input[type='radio']").prop({"checked" : false, "disabled" : true });
				viaArea.find("select option:eq(0)").prop("selected" , true);
				viaArea.find("select").hide();
			}
			else {
				viaArea.find(".radio_list li:eq(0) input[type='radio']").prop({"checked" : true, "disabled" : false });
				viaArea.find(".radio_list li:eq(1) input[type='radio']").prop("disabled" , false);
				viaArea.find("select option:eq(0)").prop("selected" , true);
			}
		}
	});
	
	viaArea.find(".radio_list input[type='radio']").on({
		"change" : function(){
			if( viaArea.find(".radio_list input[type='radio']:checked").parents("li").index() == 0 ){
				viaArea.find("select").hide().find("option:eq(0)").prop("selected" , true);
			} else {
				viaArea.find("select").show();
				alert("携带液体类或凝胶类等个人物品请准备透明塑料袋，若想确认更详细的信息，请查阅机内限制携带物品清单。")
			}
		}
	});
	
	viaArea.find("select").on({
		"change" : function(){
			var val = $(this).val();
			
			if ( val == 2 || val == 8 ){
				alert("最终目的地为美国、澳大利亚的情况下，不可购买液体类、凝胶类商品。\n 请再次确认所订购的商品。");
			} 
			else if ( val == 10 ){
				alert("以托运方式处理可购买液体类、凝胶类产品");
			} 
			else {
				alert("不可购买液体类、凝胶类商品。\n 请再次确认所订购的商品");
			}
		}
	});
	
	// cart list option
	cartListOptionCheck();
	
});
/* end : cart */


/* my page */
$(function(){
	$(".table_tbody").find("#nationality").on({
		"change" : function(){
			if( $(this).find("option:selected").text() == "中国" ){
				$(".table_tbody").find("#address").show();
			} else {
				$(".table_tbody").find("#address").hide();
			}	
		}
	});
	
	// push alert
	$(".push_btn").each(function(){
		var touchstartX,touchendX,touches,direction;
		$(this).find("a").on({
			"touchstart" : function(event){
				if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
					touches = event.originalEvent.touches[0];
				}
				touchstartX = touches.clientX;
				event.preventDefault();
			},
			"touchmove" : function(event){
				if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
					touches = event.originalEvent.touches[0];
				}
				touchendX = touches.clientX;
			},
			"touchend" : function(event){
				if( touchstartX > touchendX ){
					direction = "left";
					$(this).removeClass("on");
				} else {
					direction = "right";
					$(this).addClass("on");
				}
				event.preventDefault();
			}
		});
	});
	
});
/* end : my page */
 

/* member */
$(function(){
	// 정회원 전환 팝업
	var _switchMember 			= $("*[data-switch-member]");
	var _presonalArea			= _switchMember.filter(".addition_change");
	var _choiceArea	 			= _switchMember.find(".disable_box");
	var _choiceAreaMaskLayer	= _choiceArea.find(".disable");		
	var interestedBrandId 		= [];
		
	allCheckLinkage = function(target){
		if( target.prop("checked") == true ){
			if( target.parents("[data-switch-member]").size() > 0 ){
				_choiceAreaMaskLayer.removeClass("disable");
			}
		} else {
			if( target.parents("[data-switch-member]").size() > 0 ){
				_choiceAreaMaskLayer.addClass("disable");
				switchMemberChoiceAreaReset();
			}
		}
	}
	
	function switchMemberChoiceAreaReset(){
		_choiceArea.find("select").find("option:eq(0)").prop("selected" , true);
		_choiceArea.find("input[type='text']").val("");
		_choiceArea.find("input[type='number']").val("");
		_choiceArea.find("input[type='tel']").val("");
		_choiceArea.find("input[type='date']").val("");
		_choiceArea.find("input[type='radio']").prop("checked" , false);
		_choiceArea.find("input[type='checkbox']").prop("checked" , false);
		_choiceArea.find(".brand_choice").empty();
		$(".interested_choice").each(function(i){
			interestedBrandId[i] = [];
		});
	}
	
	if( _presonalArea.find("#agree_optional").prop("checked") == true ){
		_choiceAreaMaskLayer.removeClass("disable");
	} else {
		_choiceAreaMaskLayer.addClass("disable");
	}
	
	_switchMember.find("#agree_optional").on({
		"change" : function(){
			var _this = $(this);
			if( _this.prop("checked") == true ){
				_choiceAreaMaskLayer.removeClass("disable");
			} else {
				if(_presonalArea.length){
					if( confirm("您若取消勾选个人（选择）信息收集同意书，您所填写的附加信息内容将被删除。\n确定要取消勾选个人（选择）信息收集同意书吗?") ){
						_choiceAreaMaskLayer.addClass("disable");
						switchMemberChoiceAreaReset();
						alert("您已取消同意个人（选择）信息收集同意书。");
					} else {
						_this.prop("checked", true);
					}
				} else {
					_choiceAreaMaskLayer.addClass("disable");
					switchMemberChoiceAreaReset();
				}
			}
		}
	});
	
	//관심 브랜드 선택
	$(".interested_choice").each(function(i){
		var _this 				= $(this);
		var select				= _this.find("*[data-select]");
		var selectEventTarget 	= select.attr("data-select");
		var selectAppendTarget  = _this.find("*[data-append-target='"+selectEventTarget+"']");
		var size 				= selectAppendTarget.children().size();
		var sizeVar;
		
		if( selectEventTarget == "interested-brand" ){
			sizeVar = 3;
		} 
		else if( selectEventTarget == "interested-category" ){
			sizeVar = 6;
		}
		
		interestedBrandId[i]	= [];
		
		select.on({
			"change" : function() {
				size 			= selectAppendTarget.children().size();
				var brandTxt 	= select.find("option:selected").text();
				var brandVal 	= select.find("option:selected").val();
				
				if ( select.find("option:selected").index() != 0  && $.inArray( brandVal, interestedBrandId[i] ) == -1 && size < sizeVar  ) {
					
					interestedBrandId[i].push(brandVal);
					
					selectAppendTarget.append("<a href='#'><span>" + brandTxt + "</span><input type='text' style='display:none' value='" + brandVal + "'/></a>");
					size++;

					selectAppendTarget.find("a").off("click").on({
						"click": function (e) {
							remove($(this));
							e.preventDefault();
						}
					});
				} else if ( size >= sizeVar ){
					if( selectEventTarget == "interested-brand" ){
						alert("可选择最多"+sizeVar+"个喜爱的品牌");
					} else if ( selectEventTarget == "interested-category" ){
						alert("可选择最多"+sizeVar+"个商品类别");
					}
				} else if ( $.inArray( brandVal, interestedBrandId[i] ) != -1 ){
					if( selectEventTarget == "interested-brand" ){
						alert("这是已经选过的品牌");
					} 
				}
			}
		});

		selectAppendTarget.find("a").on({
			"click" : function(e){
				remove($(this));
				e.preventDefault();
			}
		});

		function remove(target){
			var selectVal = target.find("input").val();
			interestedBrandId[i][$.inArray(selectVal,interestedBrandId[i])] = null;
			target.remove();
			size --;
		}
	});
	
	// input delete button
	$(".input_delete").each(function(){
		var _this = $(this);
		_this.find("a").on({
			"click" : function(e){
				_this.find("input[type='text']").val("");
				e.preventDefault();
			}
		});
	});
	
});
/* end : member */


/* tab menu */
$.fn.tabpannel = function(options){
	//var self = this;

	this.each(function(){

		var self = $(this);

		var _setting = options;
		var _default = {
			tabMenu : ".tab_menu",
			tabDetail : ".tab_detail",
			activeElement : ":on",
			activeIndex : 0
		}

		self.option = $.extend(_default, _setting);

		var activeElm = self.option.activeElement.split(":")[0];
		var activeClass = self.option.activeElement.split(":")[1];
		var elem = self.option.tabMenu;
		var detail = self.option.tabDetail;
		var idx = self.option.activeIndex;

		function init(){
			build();

			self.find(elem).find("a").on("click",function(e){
				action(this);
				e.preventDefault();
			})
		}

		function build(){
			//reset
			self.find(detail).hide();
			self.find(detail).eq(idx).show();

			if(activeElm == "a"){
				self.find(elem).children().find("a").removeClass(activeClass);
				if(self.find(elem).children().length > 1){
					self.find(elem).children().eq(idx).find("a").addClass(activeClass);
				} else {
					self.find(elem).children().children().eq(idx).find("a").addClass(activeClass);
				}
			} else {
				self.find(elem).children().removeClass(activeClass);
				self.find(elem).children().eq(idx).addClass(activeClass);
			}
		}

		function action(target){
			var $parent = $(target).parent();
			var idx = $parent.index();
			self.find(detail).hide();
			self.find(detail).eq(idx).show();


			if(activeElm == "a"){
				$parent.parent().find("a").removeClass(activeClass);
				$(target).addClass(activeClass)
			} else {
				$parent.siblings().removeClass(activeClass);
				$parent.addClass(activeClass)
			}

			slickReset(idx);
		}

		function slickReset(idx){
			var slickAuto = $(".slideAuto")

			if(self.find(detail).eq(idx).find($(productImgArr)).length){
				productSwipeArrow(self.find(detail).eq(idx).find($(productImgArr)))
			}

			if(self.find(detail).eq(idx).find(slickAuto).length){
				self.find(detail).eq(idx).find(slickAuto).slick("unslick");
				slideAuto(self.find(detail).eq(idx).find(slickAuto));
			}
			/* 2016-10-20 추가 */
			if(self.find(detail).eq(idx).find(".cateSliderNav").length){
				self.find(detail).eq(idx).find(".cateSliderNav").slick("unslick");
				cateSliderNavSingle(self.find(detail).eq(idx).find(".cateSliderNav"));
			}
			/* //2016-10-20 추가 */
		}
		init();
	});
}


//footer catagory/brand layer
var footerMenu = {

	setting : {
		navi			: $(".nav_menu"),
		layerCont		: $(".layer_menu"),
		categoryBtn 	: $(".btn_category_menu"),
		categoryCont	: $(".layer_menu.category"),
		brandBtn		: $(".btn_brand_menu"),
		brandCont		: $(".layer_menu.brand")
	},

	init : function(){
		this.layerToggle();
		this.layerClose();
		this.menubarToggle();
		this.catagoryAsideMenu();
		this.categoryAccordion();
		this.brandTab();
		
		// app
		$(".footer_nav.app").find(".nav_menu > ul > li").eq(4).find("a").on({
			"click" : function(e){
				var appWrap =  $(this).parents(".app")
				if( appWrap.hasClass("off") ){
					appWrap.removeClass("off");
				} 
				scrollStart("up");
				e.preventDefault();
			}
		});
		
		$(".footer_nav.app .expanded_menu .head_btn > a").on({
			"click" : function(e){
				$(".footer_nav.app").addClass("off").find(".nav_menu > ul > li:eq(4)").removeClass("on");
				scrollStart("up");
				e.preventDefault();
			}
		});
	},
	
	layerToggle : function(){
		footerMenu.setting.categoryBtn.on({
			"click" : function(e){
				//footerMenu.categoryOpen();
				$(".btn_total").trigger('click'); //개발 수정
				e.preventDefault();
			}
		});

		footerMenu.setting.brandBtn.on({
			"click" : function(e){
				footerMenu.brandOpen();
				e.preventDefault();
			}
		});
	},

	layerClose : function(){
		footerMenu.setting.layerCont.find(".close").on({
			"click" : function(e){
				$(this).parents(".layer_menu").removeClass("on");
				winScrollDisable(true);
				bodyScrollStart();
				e.preventDefault();
			}
		});
	},

	categoryOpen : function(){
		$(".layer_menu").removeClass("on");
		footerMenu.setting.categoryCont.addClass("on");
		bodyScrollStop();
		winScrollDisable(false);
		footerMenu.layerResize();
	},
	
	brandOpen : function(){
		$(".layer_menu").removeClass("on");
		$(".total_menu").hide();
		$(".dimmed").removeClass("navOn on");
		$("#header").removeClass("open");
		footerMenu.setting.brandCont.addClass("on");
		bodyScrollStop();
		winScrollDisable(false);
		footerMenu.layerResize();
	},

	menubarToggle : function(){
		footerMenu.setting.navi.find("li > a").on({
			"click" : function(e){
				if( $(this).attr("href") == "#" || $(this).attr("href") == "" || $(this).attr("href") == undefined ){
					e.preventDefault();
				}
				footerMenu.setting.navi.find("li").removeClass("on");
				$(this).closest("li").addClass("on");
				$(".footer_nav.app").addClass("off");
			}
		});
	},

	catagoryAsideMenu : function(){
		footerMenu.setting.categoryCont.find(".depth01 > a").each(function(){
			var _this = $(this);

			_this.on({
				"click" : function(e){
					footerMenu.setting.categoryCont.find(".list").addClass("open");
					footerMenu.setting.categoryCont.find(".category_list > li.on").removeClass("on");
					_this.parent().parent().addClass("on")
					e.preventDefault();
				}
			});
		});
	},

	layerResize : function(){
		footerMenu.setting.layerCont.each(function(){
			var _this = $(this);
			var winH 	= $(window).height();
			var headerH = _this.find(".tit").height();
			_this.find(".list").height(winH-headerH);

			$(window).resize(function(){
				winH 	= $(window).height();
				headerH = _this.find(".tit").height();
				_this.find(".list").height(winH-headerH);
			});
		});
	},

	categoryAccordion : function(){
		var accMenu = footerMenu.setting.categoryCont.find(".category_list > li .depth02");

		accMenu.each(function() {
			var _this = $(this);

			_this.find("ul li").removeClass("on");

			_this.find("ul li > a").on({
				"click": function (e) {
					if ($(this).next().size() > 0) {
						if( $(this).parent().hasClass("active") ){
							$(this).parent().removeClass("on active").find("ul").slideUp();
							$(this).parent().find("li").removeClass("on active");
						} else {
							$(this).parent().find("li").removeClass("on active");
							$(this).parent().siblings().removeClass("on active").find("ul").slideUp();
							$(this).parent().addClass("on active").find(">ul").slideDown();
						}
						e.preventDefault();
					}
				}
			});
		});
	},

	brandTab : function(){
		var  tabMenu = footerMenu.setting.brandCont.find(".tab_area");
		tabMenu.find(".tit_tab > li > a").on({
			"click" : function(e){
				var idx = $(this).parent().index();
				if(! $(this).parent().hasClass("on") ){
					tabMenu.find(".tit_tab > li").removeClass("on");
					$(this).parent().addClass("on");
					tabMenu.find("> div").hide().eq(idx).show();
				}
				e.preventDefault();
			}
		});

		footerMenu.setting.brandCont.find(".choice_brand").each(function(){
			var _this = $(this);

			_this.find(".sort_tit > a").on({
				"click" : function(e){

					if( _this.hasClass("on") ){
						_this.removeClass("on");
						_this.find(".sort_btn").slideUp("fast");
					} else {
						_this.addClass("on");
						_this.find(".sort_btn").slideDown("fast");
					}

					e.preventDefault();
				}
			});

			_this.find(".sort_btn a").on({
				"click": function (e) {
					if( $(this).parents("li").hasClass("on") || $(this).parents("li").hasClass("disabled") ){
					
					} else {
						var dataEvent = $(this).parents("li").attr("data-sort-event");
						var sortTxt =  $(this).text();
						_this.removeClass("on");
						_this.find(".sort_tit a span").text(sortTxt);
						_this.find(".sort_btn").slideUp("fast");
						_this.find(".sort_choice > li").removeClass("on");
						$(this).parents("li").addClass("on");
						_this.next(".sort_result").find(">div").hide();
						_this.next(".sort_result").find(">div[data-sort-target='"+dataEvent+"']").show();
					}
					e.preventDefault();
				}
			});
		});
	}
}
/* layout */
$(function(){
	var target;
/* 17.06.21 gnb brand anchor 추가 - 기존 gnb brand anchor 전부 삭제 s */
$(function(){
	var _$scrollArea = $( '.total_menu .brand_lang' );
		_$topBtn = $( '.total_menu #brd_top' );// 17.07.12 추가

	var _isBrandTabActive = false; //17.07.13 추가

	//tab 활성화시점
	$( '.total_menu .tab, .total_menu .sub_tab li > a' ).on( 'click', function (e) {
		_$scrollArea.stop().scrollTop( 0 );
		activeBtn( 0 );
	});

	//brand tab 활성화시점 17.07.13 추가
	$( '.total_menu .tab > li > a' ).on( 'click', function (e) {
		//brand tab
		if ( $(e.currentTarget).parent().index() === 1 ) {
			_isBrandTabActive = true;
			_$topBtn.hide();
		} else {
			_isBrandTabActive = false;
			_$topBtn.hide();
		}
	});

	//알파벳 버튼 이벤트 (17.06.28 수정)
	$( '.total_menu .lang_brd .index_brd li > a[href^="#"]' ).on( 'click', function (e) {
		e.preventDefault();

		if ( !$(e.currentTarget).hasClass('disabled') ) {
			var id = $( e.currentTarget ).attr( 'href' ),
				idx = $( e.currentTarget ).parent().index();

			activeBtn( idx );
			scrollAnchor( id );
		}
	});

	//브랜드 없으면 알파벳 버튼 비활성화 (17.06.29 수정)
	$( '.total_menu .lang_brd' ).each( function () {
		var $btns = $( this ).find( '.index_brd li' );

		$( this ).find( '.brand_list ul' ).each( function () {
			var length = $( this ).find( '> li' ).length;

			if ( !length ) {
				var id = $( this ).prev( 'p' ).attr( 'id' );
				$btns.find( '> a[href="#' + id + '"]' ).addClass( 'disabled' );
			}
		});
	});

	/* 브랜드 tab top버튼 적용 17.07.13 수정 */
	_$scrollArea.on( 'scroll', function () {
		if ( _$scrollArea.scrollTop() === 0 ) {
			_$topBtn.hide();
		} else {
			if ( _isBrandTabActive ) _$topBtn.show();
		}
	});

	_$topBtn.on( 'click', function (e) {
		e.preventDefault();
		_$scrollArea.stop().scrollTop( 0 );
	});

	$('.totalmenu_close').on('click',function(){
		_$topBtn.hide();
	});
	/* 브랜드 tab top버튼 적용 17.07.13 수정 e */

	/* ========== Methods ========== */

	function activeBtn ( index ) {
		_$scrollArea.find( '.index_brd' ).each( function ( idx, el ) {
			$( el ).find( '> li' ).removeClass( 'on' ).eq( index ).addClass( 'on' );
		});
	}

	function getPosY ( id ) {
		var position = _$scrollArea.find( id ).position();
		return ( position && position.top )? position.top : 0;
	}

	function scrollAnchor ( id ) {
		var posY = getPosY( id );

		_$scrollArea.stop().animate({
			scrollTop: posY
		}, 900, null, function (e) {
			//hash 변경하면 화면이 위로 올라감.
			//window.location.hash = id;
		});
	}
	/* 17.06.21 gnb brand anchor 추가 - 기존 gnb brand anchor 전부 삭제 e */
});
	
			
	var $header = $("#header");
	
	var $total = $(".total_menu");
	var $totalbtn = $(".btn_total, .section_menu li.btn_menu a");
	var $depthList = $total.find(".depth01");
	var $anchor = $depthList.find("a");
	var $totalClose = $(".totalmenu_close");
	var $totalDim = $( '.bg_dimmed' );
	var $depthLi2 = $total.find(".depth02");/* 17.06.21 추가 */
	var $anchor2 = $depthLi2.find("a");/* 17.06.21 추가 */
	
	$totalbtn.on("click",function(e){
		var w_h = $(window).height();
		$(".total_menu_list.brnd_list").height(w_h - 47); //17.06.21 수정
		
		if($total.css("left") != 0){
			$(".total_menu").css({"left":"0"});
			///$(mask).addClass("navOn on");
			
			$total.find(".user_info_area").css({"left":"0"}); /*개발수정*/
			/*$total.find(".tab").css({"left":"0"}); 17.07.26 퍼블-숨김 개발수정*/
			
			$totalClose.show();
			$totalDim.show();
		} else {
			$(".total_menu").css({"left":"-100%"});
			$(mask).removeClass("navOn on");
			
			$total.find(".user_info_area").css({"left":"-100%"}); /*개발수정*/
			/*$total.find(".tab").css({"left":"-100%"}); 17.07.26 퍼블-숨김 개발수정*/
			
			$totalClose.hide();
		}
		
		reset();
		$total.tabpannel($total.data())
		$('body').css({overflow:'hidden'});
		$('#wrap').css({position:'fixed'});
		$( '.sticky_search' ).css( 'display', 'none' );
		
		e.preventDefault();
	})
	
	function total_close( elm ){
		$(elm).on("click",function(){
			$totalClose.removeClass("open");// 17.06.21 수정 
			$totalDim.hide();
		$(mask).removeClass("navOn on");
		//$header.removeAttr("style");
		$('body').css({overflow:'auto'});
		$('#wrap').css({position:'static'});
		$( '.sticky_search' ).css( 'display', '' );
		
		$totalClose.hide();
		$total.find(".user_info_area").css({"left":"-100%"}); /*개발수정*/
		/*$total.find(".tab").css({"left":"-100%"}); 17.07.26 퍼블-숨김 개발수정*/
		$total.css("left","-100%",function(){
			reset();
			$total.find("[data-event='tab']").tabpannel({activeIndex:0});
		});
	})
	}
	total_close( mask );
	total_close( $totalClose );
	
	$anchor.on("click",function(e){
		var _this = $(this);
		
		/* 17.06.21 숨김 
 		if(_this.next("ul").length){
			if( _this.hasClass("on") ){
				_this.removeClass("on");
				_this.next("ul").hide();
			} else {
				_this.addClass("on");
				_this.next("ul").show();
			}
			
			_this.closest("li").siblings().find(">ul").hide();
			_this.closest("li").siblings().find(">a").removeClass("on");
			
			if(_this.closest("li").parent("ul").hasClass("depth01")){
				_this.closest("li").find("ul:not(:eq(0))").hide();
				_this.closest("li").find("ul a").removeClass("on")
			} 
			
			e.preventDefault();
		}*/
		/* 17.06.21 카테고리 drawer depth1 추가 s */
		_this.closest("li").siblings().find("a").removeClass("on"); 
		_this.addClass("on");

		var dep01_idx = _this.closest("li").index();

		$(".depth02").hide();
		_this.closest("div").siblings("div:eq("+dep01_idx+")").show();
		/* 17.06.21 추가 e */
	})

	/* 17.06.21  카테고리 drawer depth2 추가 s */
	$anchor2.on("click",function(e){
		var _this = $(this);
		
		if(_this.next("ul").length){
			if( _this.hasClass("on") ){
				_this.removeClass("on");
				_this.next("ul").hide();
			} else {
				_this.addClass("on");
				_this.next("ul").show();
			}
			
			_this.closest("li").siblings().find(">ul").hide();
			_this.closest("li").siblings().find(">a").removeClass("on");
			
			e.preventDefault();
		}
	})
	/* 17.06.21 추가 s */

	
	function reset(){
		$depthList.each(function(){
			var _this = $(this);
			
			_this.find("a").removeClass("on");
			//_this.find("ul").hide(); 17.06.21 숨김
			_this.find("ul>li").eq(0).find(">a").addClass("on"); //17.06.21 수정
			_this.find(">li").eq(0).find(">ul").show();
			_this.closest("div").siblings("div").eq(0).show();//17.06.21 추가
		})
	}
});


/* accordion */
$.fn.accordion = function(options){
	var self = this;
	var _setting = options;
	var _default = {
		accordionElement : ".btn-accordion",
		accordionDetail : ".accordion-detail",
		activeElement : ":on",
		activeIndex : 0,
		slideType : 0,
		relationship : "next" 
	}
	
	self.option = $.extend(_default, _setting);
	
	var activeElem = self.option.activeElement.split(":")[0];
	var activeClass = self.option.activeElement.split(":")[1];
	var elem = self.option.accordionElement;
	var detail = self.option.accordionDetail;
	var slideType = self.option.slideType;
	var idx = self.option.activeIndex;
	var relationship = self.option.relationship;

	function init(){
		
		if( idx != null ){
			build();
		}
		
		self.find(elem).find("a").on("click",function(e){
			action(this);
			e.preventDefault();
		})
	}
	
	function build(){
		//reset
		
		self.find(detail).hide();
		if(relationship == "next"){
			self.find(elem).eq(idx).next(detail).show();
		} else if(relationship == "children") {
			self.find(elem).eq(idx).find(detail).show();
		}
		
		if(activeElem == "a"){
			self.find(elem).eq(idx).find("a").addClass(activeClass);
		} else {
			self.find(elem).eq(idx).addClass(activeClass);
		}
		
		if( idx == "-1" ){
			self.find(elem).find("a").removeClass("on");
			self.find(detail).hide();
		}
	}
	
	function action(target){
		var $parent = $(target).parent(elem);
		var $target;
		
		if(relationship == "next"){
			$target = $parent.next(detail);
		} 
		else if(relationship == "children"){
			$target = $parent.find(detail);
		}
		
		if($target.is(":visible")){
			$target.slideUp();
			if(activeElem == "a"){
				$(target).removeClass(activeClass);
			} else {
				$parent.removeClass(activeClass);
			}
		} else {
			if(slideType == 0){
				self.find(detail).slideUp();
			}
			
			$target.slideDown();
			if(activeElem == "a"){
				if(slideType == 0){
					self.find(elem).find("a").removeClass(activeClass);
				}
				$(target).addClass(activeClass);
			} else {
				if(slideType == 0){
					self.find(elem).removeClass(activeClass);
				}
				$parent.addClass(activeClass);
			}
		}
		slickReset($target);
	}
	
	function slickReset(target){
		var slickAuto = $(".slideAuto");
		var slideClass = $(".slideArrowDot");
		
		if($(target).find($(productImgArr)).length){
			productSwipeArrow($(target).find($(productImgArr)));
		}
		
		if($(target).find(slideClass).length){
			slideArrowDot($(target).find(slideClass));
		}
	}
	
	init();

}

// 상품 버튼노출시 푸터 영역
$(document).ready(function(){
	if ($('.footer_nav .btn_payment').length != 0)
	{
		if(!isAppConnect){
			$('.footer_wrap').css('padding-bottom',$('.footer_nav .btn_payment').height()+'px');
			$('.footer_wrap').css('margin-bottom','63px');
		} else {
			$('.footer_wrap').css('margin-bottom','51px');
		}
		
	}
});

// 열고닫기
function layer_toggle(obj) {
		if (obj.style.display == 'none') obj.style.display = 'block';
		else if (obj.style.display == 'block') obj.style.display = 'none';
}

/**
* 상품리스트 유형화 type 2종
* @2017-02-23
* @param str
* @returns
*/
/* 17.08.08 수정 s */
$( function(){

	var state = 0;
	
	$( '.btn_view span' ).click( function(){
		$( '.btn_view span' ).removeClass( 'on' );
		if ( state == 0 ){
			$( '.btn_view .list' ).addClass( 'on' );
			$( '.product_list > ul' ).removeClass( 'list_type2' );
			$( '.product_list > ul' ).addClass( 'list_type1' );
			state = 1;
		} else{
			$( '.btn_view .thum' ).addClass( 'on' );
			$( '.product_list > ul' ).removeClass( 'list_type1' );
			$( '.product_list > ul' ).addClass( 'list_type2' );
			state = 0;
		}		
		return false;
	});
});	
/* 17.08.08 수정 e */

/* add */

/* Main Slide & Visual Slide */
$( function () {
	$( '.section_visual' ).on( 'ixSlideMax:init ixSlideMax:change', function (e) {
		var viewLength = $( this ).ixOptions( 'view-length' ),
			currentPage = Math.ceil( e.currentIndex / viewLength ),
			totalPage = Math.ceil( e.totalLength / viewLength );

		$( this ).find( '.paging > .current' ).text( currentPage + 1 );
		$( this ).find( '.paging > .total' ).text( totalPage );
	}).ixSlideMax();

	/*20190614 메인배너 신규추가s-제이쿼리 함수 실인수 문자열 변경(, .section_brand_bn .brand_bnlist 추가)*/
	var $slide = $( '.section_visual, .key_word, .section_sale .sale_list, .oneday_off, .oneday_prd_list, .section_curation .curation_list, .section_recommand .list_recommand, .section_brand .list_brand, .section_spot_sale .list_spot, .section_brand_bn .brand_bnlist' ).ixSlideMax();
	/*20190614 메인배너 신규추가e*/

	$( window ).on( 'resize', function(e) {
		$slide.ixSlideMax( 'resize' );
	});
});

/* Search 2017-11-15 숨김
$( function () {
	$( window ).scroll( function (e) {
		var windowT = $(window).scrollTop(),
			$search = $( '.sticky_search' ),
			$searchT = $search.offset().top;

		$search.addClass( 'open' );
		if ( windowT <= 0 ) {
			$search.removeClass( 'open' );
		}
	})
});*/

/* Right Drawer Menu */
$( function () {
	var $wrap = $( '.more_view_wrap' ),
		$body = $( 'body' );

	$( '#header .more_view_btn' ).on( 'click', function (e) {
		var _this = $( this );

		$wrap.addClass( 'open' );
		$( '.bg_dimmed' ).show();
		if ( $wrap.hasClass( 'open' ) ) {
			//_this.addClass( 'open' );
			$body.css( 'position', 'fixed' );
			$( '.sticky_search' ).css( 'display', 'none' );
		} else {
			//_this.removeClass( 'open' );
		}
		e.preventDefault();
	});

	$( '.more_view_wrap .btn_close, .bg_dimmed' ).on( 'click', function (e) {
		var _this = $( this ),
			$wrap = $( '.more_view_wrap' );

		$( '.bg_dimmed' ).hide();
		$wrap.removeClass( 'open' );
		$body.css( 'position', '' );
		$( '.sticky_search' ).css( 'display', '' );
		e.preventDefault();
	});
});

/* Brand */
$( function () {
	var $sectionBrand = $( '.section_brand' ),
		$brandList = $sectionBrand.find( '.brand_inlist' ),
		$brandListLi = $brandList.find( 'li' ),
		$sectionMoreBtn = $sectionBrand.find( '.more_list_view' );

	if( $brandListLi.length >= 11 ){/* 17.07.13 수정 */
		$sectionMoreBtn.css( 'display', 'block' );
	}

	$sectionMoreBtn.on( 'click', function(e) {
		var _this = $( this ),
			$moreBtn = _this.find( 'span' );

		if ( _this.hasClass( 'close' ) ) {
			_this.removeClass( 'close' );
			$moreBtn.text('查看更多');
			$brandList.find( 'li:eq(9) ~ li' ).hide();/* 17.07.13 수정 */
		} else {
			_this.addClass( 'close' );
			$moreBtn.text('关闭');
			$brandList.find( 'li:eq(9) ~ li' ).show();/* 17.07.13 수정 */
		}
		e.preventDefault();
	});
});
/* 17.07.18 슬라이드 추가 */
(function () {
		$( '.slide' ).each( function ( idx, el ) {
			var _$slide = $( el ),
				 _$paging = _$slide.find( '.pgnumber' );
		
			_$slide.on( 'ixSlideMax:init ixSlideMax:change', function(e) {
				_$paging.text( e.currentIndex+1 + '/' + e.totalLength );
			}).ixSlideMax();

			$( window ).on( 'resize', function () {
				_$slide.ixSlideMax( 'resize' );
			});
		});
	})();

/* 17.08.25 기획전 tab UI 추가 s */
$(function(){
	$('.marketing_wrap .sort_open').on('click', function (e) {
		var _this = $(this);
		var dep1Ul = $(this).siblings('.scroll_areas');
		var atit = $('.marketing_wrap .sort_open');
		var sortUl = $('.marketing_wrap .sort2 .scroll_areas');
		
		_this.addClass('on');

		if ( dep1Ul.hasClass( 'on' ) ) {
			dep1Ul.removeClass('on');
			_this.removeClass( 'on' );
			
		} else {
			atit.removeClass('on');
			sortUl.removeClass('on');
			dep1Ul.addClass('on');
			_this.addClass( 'on' );
		}
		e.preventDefault();
	});
	
	$('.marketing_wrap .sort_list li a').click(function (e) {  
	    var part = $(this).parent().parent().parent('.scroll_areas');
        var sorta = $(this).parent().parent().parent().siblings('.sort_open');
        part.removeClass('on');  
        sorta.removeClass('on');

        var selectValue = $(e.currentTarget).attr('data-value');
        var oldValue = sorta.attr('data-value');
        sorta.text( $(e.currentTarget).text() );
        sorta.attr( 'data-value', selectValue );

		if ( oldValue != selectValue ) {
	        sorta.triggerHandler({type: 'select-change', value: selectValue});
        }
	});
});

//개발에서 변경된 값 받을때
$( '.marketing_wrap .sort_open' ).on( 'select-change', function (e) {
	//바뀐값
	console.log( e.value );
}); 
/* 17.08.25 기획전 tab UI 추가 e */

 /**
* 회원가입 부가정보 입력
* @2017-11-06
*/
$( function ($) {
	$( '.account_success .btn_red' ).click(function(){
		$( '.disable_box' ).show();
	});
});

 /* 2017-11-15 추가 s */
//sticky
$( function () {
	var _oldScrollTop = $( window ).scrollTop(),
		$search = $( '.top_search_area' ),
		$searchCont = $search.find( '.top_search_wrap' ),
		$bodyH = $( 'body' ).outerHeight(),
		$winH = $(window).outerHeight();

	$( window ).scroll( function () {
		var winTop = $( window ).scrollTop();

		//search
		if ( $search.is( ':visible' ) ) {
			var $searchTop = $search.offset().top,
				$searchH = $search.outerHeight();

			if ( $searchTop < winTop ) {
				$searchCont.addClass( 'fixed' );
				$search.css( 'height', $searchH );
			} else {
				$searchCont.removeClass( 'fixed' );
				$search.css( 'height', '' );
			}
		}
	});
});

//인기검색어, 최근검색어 tab
$( function () {
	var $searchArea = $( '.search_area' ),
		$searchTab = $searchArea.find( '.tab li' ),
		$searchCont = $searchArea.find( '.tab_cont' );

	$searchTab.on( 'click', 'a', function (e) {
		e.preventDefault();

		var _this = $( this );

		_this.parents( 'li' ).addClass( 'on' ).siblings( 'li' ).removeClass( 'on' );
		$searchCont.hide();
		$searchCont.eq( _this.parents( 'li' ).index() ).show();
	});
});

//search value clear
$( function () {
	$( '.top_search_wrap .search_del' ).on( 'click', function (e) {
		e.preventDefault();

		$( this ).parents( '.top_search_wrap' ).find( 'input[type="text"]' ).val( '' );
	});
});
/* 2017-11-15 추가 e */

/*20180308검색레이어수정 s*/
$(function(){
	var $head=$("#header");
	var $cont=$("#container");
	var $tg=$(".search_section");
	var $tit=$(".sub_category_area");
	var $tot=$(".total_menu");
	var $foot=$(".footer_nav").siblings();
	$head.find(".top_search .search").on("click","input[type='text']",function(){
		$head.hide();
		$cont.hide();
		$foot.hide();
		$tit.hide();
		$tg.show();
		$(document).scrollTop(0);
		$tg.find("#search-text-new").focus();

	});
	$tg.on("click",".history_back, .close",function(){
		$head.show();
		$cont.show();
		$foot.show();
		$tit.show();
		$tot.removeAttr("style");
		$tg.hide();
		setTimeout(function(){
			$(document).scrollTop(1);
		},0);	
	});
});
/*20180308검색레이어수정 e*/


/*20190731상품리스트변경*/
$(function(){
	var typeNum  = 0;
		parent = $(".category_wrap.renew2019"),
		target = parent.find(".select_area .view span"),
		list = parent.find(".product_list > ul");

	target.on("click", function(e){
		typeNum++; 
		if (typeNum === 3) typeNum = 0;
		$(this).attr("class" ,"list_type" + typeNum);
		list.attr("class" ,"list_type" + typeNum);
		e.preventDefault;
	});

});